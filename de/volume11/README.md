#### Geschichte der Stadt Rom im Mittelalter

## Elftes Buch

#### Geschichte der Stadt Rom im XIV. Jahrhundert vom Jahre 1305 bis 1354

### Inhalt:

**Erstes Kapitel**

[1.](#rom1111) Das XIV. Jahrhundert. Verfall des Papsttums. Guelfen und Ghibellinen. Die neu-klassische Kultur. Florenz und Rom. Rom seit 1305. Der Volksrat der Dreizehn. Clemens V. erhält die städtische Gewalt. Avignon. Brand der Lateranischen Basilika. Vereinsamung Roms. Der Papst überträgt dem Volk die Wahl seiner Regenten. Ermordung Albrechts. Heinrich von Luxemburg König der Römer. Italien fordert den Romzug. Robert von Neapel. Dante und das Reich. Sein Traktat _De Monarchia_. Ghibellinisches Kaiserideal.

[2.](#rom1112) Heinrich VII. kündigt seinen Romzug an. Versammlung in Lausanne. Clemens V., Robert und Heinrich. Der Papst kündigt den Romzug des Königs an. Aufbruch. Erstes Auftreten Heinrichs in der Lombardei. Die Gesandtschaft der Römer. Ludwig von Savoyen Senator. Krönung in Mailand. Sturz der Torri. Abfall lombardischer Städte. Brescia. Heinrich in Genua. Zustände in Rom. Orsini und Colonna. Johann von Achaja. Die Liga der Guelfen. Üble Lage Ludwigs von Savoyen in Rom.

[3.](#rom1113) Heinrich in Pisa. Er schickt Boten an den Prinzen Johann und den König Robert. Marsch nach Rom. Seine ghibellinischen Bundesgenossen. Einzug in Rom. Zustand der Stadt. Die Schanzen der Guelfen und der Ghibellinen. Heinrich bemächtigt sich vieler Aristokraten. Übergabe ihrer Burgen. Fall des Kapitols. Straßenkrieg. Heinrich will im Lateran gekrönt sein. Volksbeschlüsse. Die Kardinallegaten krönen den Kaiser im Lateran.

  
**Zweites Kapitel**

[1.](#rom1121) Heinrich und Friedrich von Sizilien. Die Römer halten ihren Kaiser in der Stadt fest. Erstürmung des Grabmals der Caecilia Metella. Johann Savigny Kapitän des römischen Volkes. Der Kaiser in Tivoli. Ankunft der Briefe des Papsts. Dessen Forderungen an den Kaiser. Heinrich verwahrt die imperatorischen Rechte. Waffenstillstand in Rom. Abzug des Kaisers.

[2.](#rom1122) Die Colonna besetzen den Vatikan. Die kaiserliche Besatzung abgerufen. Colonna und Orsini versöhnen sich. Flucht des Johann Savigny. Das Volk stürzt das Adelsregiment und macht Jakob Arlotti zum Kapitän. Dessen kraftvolles Regiment. Heinrich VII. wird vom Volk eingeladen, in Rom zu residieren. Clemens V. anerkennt die Demokratie in Rom. Velletri unterwirft sich dem Kapitol. Die Gaëtani in der Campagna. Sturz des Arlotti. Der Kaiser im Kampf mit Florenz. Seine Rüstung in Pisa wider Neapel. Drohende Bulle des Papsts. Heinrichs Auszug, sein Tod und dessen Folgen.

[3.](#rom1123) Die ghibellinischen Lager nach Heinrichs Tode. Macht des Königs Robert. Clemens V. erklärt sich zum Gebieter des vakanten Reichs. Sein Tod. Seine Unterwürfigkeit unter Frankreich. Aufopferung der Tempelherren. Der Prozeß Bonifatius' VIII. beendigt. Die Kardinäle, ihr nationaler Widerspruch, ihr zersprengtes Konklave in Carpentras. Johann XXII. Papst. Ludwig der Bayer und Friedrich der Schöne. König Robert regiert Rom. Folgen der Abwesenheit des Papsts für die Stadt.

  
**Drittes Kapitel**

[1.](#rom1131) Deutscher Thronstreit. Der Papst erklärt sich zum Verwalter des Reichs. Haltung der Ghibellinen in Italien. Die Schlacht bei Mühldorf und ihre Folgen. Ludwig entsetzt Mailand. Der Papst erhebt Prozeß wider ihn. Gegenerklärungen Ludwigs. Er wird in den Bann getan. Verbündete Ludwigs. Das Schisma der Minoriten. Die Doktrin von der Armut und ihr Verhältnis zur weltherrlichen Kirche.

[2.](#rom1132) Anfänge der Reformation. Die kanonischen Beweise für die päpstliche Universalgewalt. Die Lehre des Thomas von Aquino vom Verhältnis des Staats zur Kirche. Reaktion gegen die Kanonisten seit Philipp dem Schönen. Dantes Buch _De Monarchia_. Die Schule der Monarchisten greift das Papsttum an. Der Defensor Pacis des Marsilius von Padua. Die Acht Quästionen Wilhelms von Ockham und ähnliche Traktate der ersten Reformatoren.

[3.](#rom1133) Ludwig versöhnt sich mit Friedrich von Österreich. Die Guelfen-Liga. Castruccio Castracani. Die Ghibellinen rufen Ludwig. Parlament in Trient. Ludwig nimmt die eiserne Krone. Er rückt bis vor Pisa. Umwälzung in Rom. Sciarra Colonna Kapitän des Volks. Vereitelte Versuche des Kardinallegaten, der Neapolitaner und Exilierten, in Rom einzudringen. Sieg Sciarras im Borgo des Vatikan. Pisa fällt. Ludwig und Castruccio ziehen nach Rom. Einzug des Königs.

[4.](#rom1134) Das Volk übergibt Ludwig die Signorie und bestimmt seine Kaiserkrönung. Er nimmt die Krone durch das Volk im St. Peter. Krönungsedikte. Castruccio Senator. Plötzlicher Abzug Castruccios nach Lucca. Mißstimmung in Rom. Marsilius und Johann von Jandunum bearbeiten das Volk. Edikte des Kaisers vom 14. April. Absetzung des Papsts. Kühner Protest des Jakob Colonna. Dekret über die Residenz der Päpste in Rom. Der Mönch von Corvaro wird als Papst Nikolaus V. aufgestellt.

  
**Viertes Kapitel**

[1.](#rom1141) Robert bekriegt den Kaiser. Der Gegenpapst findet wenig Anerkennung. Ludwig in der Campagna. Seine Rückkehr von Tivoli. Mißstimmung in Rom. Abzug des Kaisers. Restauration des päpstlichen Regiments in Rom. Weitere Unternehmungen Ludwigs. Tod Castruccios. Der Kaiser in Pisa; in der Lombardei. Seine Rückkehr nach Deutschland. Sieg des Papsts und der Guelfen. Der Gegenpapst unterwirft sich.

[2.](#rom1142) Rom unterwirft sich dem Papst. Feierlicher Widerruf der Römer. Die Häupter der römischen Ghibellinen widerrufen. Der Kaiser bietet vergebens die Hand zur Versöhnung. Rätselhaftes Auftreten des Königs Johann von Böhmen in Italien.

[3.](#rom1143) Versunkenheit Roms. Krieg der Colonna und Orsini. Empörung der Romagna. Bologna befreit sich. Flucht des Kardinals Bertrand. Die Flagellanten. Fra Venturino in Rom. Johann XXII. stirbt. Wesen dieses Papsts. Benedikt XII. Die Römer laden ihn nach Rom ein. Krieg der Adelsfaktionen. Petrarca in Capranica und Rom. Die Römer geben dem Papst die Signorie. Friede zwischen den Colonna und Orsini. Das römische Volk richtet die Republik nach dem Muster von Florenz ein. Der Papst stellt seine Gewalt wieder her.

  
**Fünftes Kapitel**

[1.](#rom1151) Francesco Petrarca. Seine Verbindung mit dem Haus der Colonna. Seine Sehnsucht nach Rom und erste Ankunft in der Stadt. Eindruck Roms auf ihn. Seine Dichterkrönung auf dem Kapitol. Das Diplom des Senats.

[2.](#rom1152) Benedikt XII. baut den Palast zu Avignon. Unglückliche Verhältnisse Italiens. Der Papst und das Reich. Vergebliche Versöhnungsversuche Ludwigs des Bayern. Unabhängigkeitserklärung des Reichs. Benedikt XII. stirbt. Clemens VI. wird Papst. Die Römer übertragen ihm die Signorie und laden ihn zur Rückkehr ein. Robert von Neapel stirbt. Umwälzung in Rom. Erstes Auftreten Cola di Rienzos.

[3.](#rom1153) Ursprung und Lebensgang Colas. Cola Notar der städtischen Kammer und Haupt einer Verschwörung. Er reizt das Volk durch allegorische Bilder auf. Seine geistvolle Erklärung der Lex Regia. Bedeutende Vorgänge in Neapel und Florenz wirken auf Rom. Allgemeines Aufstreben der Zünfte in den Städten zur Gewalt, mit Ausschluß des Adels. Die Zustände des Volks in Rom. Die Revolution vom 20. Mai 1347. Cola di Rienzo Diktator und Tribun.

  
**Sechstes Kapitel**

[1.](#rom1161) Rom huldigt dem Tribun. Er beruft die Italiener zu einem Nationalparlament. Seine Einrichtungen in Rom, seine strenge Justiz, Finanzverwaltung und sonstige Ordnung des Gemeinwesens. Die Antworten auf seine Sendschreiben. Zauberische Macht der Idee von Rom. Petrarca und Cola di Rienzo.

[2.](#rom1162) Unterwerfung des Stadtpräfekten. Dekret vom Heimfall aller Majestätsrechte an die Stadt Rom. Das nationale Programm Colas. Die Feste vom 1. und 2. August. Colas Erhebung zum Ritter. Edikt vom 1. August. Cola gibt allen Italienern das römische Bürgerrecht. Vorladung der Reichsfürsten. Theorien über die unveräußerliche Majestät Roms. Verbindungsfest Italiens am 2. August. Kaiser Ludwig und der Papst. Wahl Karls IV. Seine Erniedrigung unter den Papst.

[3.](#rom1163) Der Ungarnkönig und Johanna von Neapel appellieren an das Urteil Colas. Der Tribun läßt sich am 15. August krönen. Krönungserlasse. Die Gaëtani unterwerfen sich. Cola kerkert die Häupter der Colonna und Orsini ein, verurteilt und begnadigt sie. Der Papst ergreift Maßregeln wider ihn. Colas Plan vom nationalitalienischen Kaisertum. Der Papst beginnt den Prozeß. Bertrand de Deus Kardinallegat. Der Tribun schickt seine Rechtfertigung an den Papst.

[4.](#rom1164) Die Aristokraten beginnen den Krieg. Cola belagert Marino. Seine Zusammenkunft mit dem Kardinallegaten in Rom. Der Adel beschließt von Palestrina aus den Zug gegen Rom. Blutige Niederlage der Barone am 20. November. Tragischer Fall des Hauses Colonna. Triumphe des Tribunen. Verändertes Wesen Colas. Seine Schwäche und Mutlosigkeit. Er unterwirft sich dem Kardinal. Aufstand in Rom und Abzug Colas vom Kapitol.

  
**Siebentes Kapitel**

[1.](#rom1171) Restauration des päpstlichen Regiments und des Adels. Cola in der Engelsburg, geächtet und auf der Flucht. Die Kompanie des Herzogs Werner. Anagni wird zerstört. Anarchie in Rom. Der schwarze Tod. Das Jubeljahr 1350. Der Kardinal Annibaldo. Pilgerzüge. Wüster Zustand der Stadt. Ludwig von Ungarn; Petrarca in Rom.

[2.](#rom1172) Unruhen in Rom. Beratung in Avignon über die beste Verfassung der Stadt. Die Ansicht Petrarcas. Aufstand der Römer. Johann Cerroni Diktator. Krieg wider den Präfekten. Orvieto fällt in dessen Gewalt. Cerroni flieht aus Rom. Clemens VI. stirbt. Die Erwerbung Avignons. Der Kirchenstaat in Rebellion. Innocenz VI. Papst. Egidius Albornoz Legat in Italien.

[3.](#rom1173) Volksaufstand in Rom. Bertold Orsini wird umgebracht. Francesco Baroncelli zweiter Volkstribun. Schicksal des Cola seit seiner Flucht. Sein Aufenthalt in den Abruzzen. Seine mystischen Träume und Pläne. Cola in Prag. Seine Mitteilungen an Karl IV. Petrarca und Karl IV. Cola in Raudnitz; in Avignon. Sein Prozeß. Innocenz VI. amnestiert ihn. Cola begleitet den Kardinal Albornoz nach Italien.

[4.](#rom1174) Albornoz kommt nach Italien. Er geht nach Montefiascone. Sturz des Baroncelli. Guido Jordani Senator. Unterwerfung des Stadtpräfekten. Erfolge und Ansehen des Albornoz. Cola in Perugia. Fra Monreale und dessen Brüder. Cola Senator. Sein Einzug in Rom. Seine zweite Regierung. Sein Verhältnis zum Adel. Krieg gegen Palestrina. Fra Monreale in Rom. Seine Hinrichtung. Cola als Tyrann. Gianni di Guccio. Fall Cola di Rienzos auf dem Kapitol.
