#### Geschichte der Stadt Rom im Mittelalter

## Viertes Buch

#### Vom Pontifikat Gregors II. im Jahre 715 bis auf die Kaiserkrönung Karls im Jahre 800

### Inhalt:

**Erstes Kapitel**

[1.](#rom0411) Pontifikat Gregors II. im Jahre 715. Seine Tätigkeit. Bonifatius bekehrt Deutschland. Leo der Isaurier. Der Kultus der Heiligenbilder. Die bronzene Figur des St. Petrus im Vatikan.

[2.](#rom0412) Edikt Leos gegen den Bilderdienst. Widerstand Roms und Italiens. Plan auf Gregors Leben. Die Römer und die Langobarden ergreifen die Waffen. Rebellion gegen Byzanz. Die Briefe Gregors an den Kaiser.

[3.](#rom0413) Die Haltung Liutprands. Er erobert Ravenna. Er schenkt Sutri dem Papst. Koalition zwischen dem Papst, den Venetianern und den Griechen gegen Liutprand. Der König rückt vor Rom und zieht ab. Ein Usurpator in Tuszien. Gregor II. stirbt 731. Gregor III. Papst 731. Römische Synode gegen die Bilderstürmer. Die Kunst im Abendlande. Bauten Gregors III. Herstellung der Stadtmauern.

[4.](#rom0414) Leo der Isaurier schickt eine Armada gegen Italien. Er zieht römische Kirchengüter ein. Der Papst gewinnt Gallese. Er schließt ein Bündnis mit Spoleto und Benevent. Liutprand rückt in den Dukat. Gregor III. wendet sich an Karl Martell. Tod Gregors III., Karl Martells und Leos des Isauriers im Jahre 741.

  
**Zweites Kapitel**

[1.](#rom0421) Zacharias Papst 741. Er unterhandelt mit Liutprand. Er reist zu ihm. Neue langobardische Schenkung an die Kirche. Reise des Papsts zu Liutprand. Der König stirbt. Ratchis folgt auf dem Thron von Pavia.

[2.](#rom0422) Der Kaiser fortdauernd anerkannt. Friedliches Verhältnis zu Byzanz. Karlmann wird Mönch. Ratchis wird Mönch. Aistulf König der Langobarden 749. Anerkennung der Usurpation Pippins durch den Papst. Zacharias stirbt 752. Seine Bauten am Lateranischen Palast. Die _domus cultae_.

[3.](#rom0423) Stephan II. Papst. Aistulf erobert Ravenna im Jahre 751. Stephan sucht Hilfe beim Kaiser, dann bei Pippin. Er reist ins Frankenland. Salbung Pippins und seiner Söhne zu Königen im Jahre 754. Schutzvertrag zu Quierzy mit Pippin. Dessen Erhebung zum Patricius der Römer.

[4.](#rom0424) Vergebliche Unterhandlungen mit Aistulf. Rückkehr Stephans. Pippin zieht nach Italien. Aistulf nimmt den Frieden an. Die erste Schenkungsurkunde Pippins im Jahre 754. Der Langobardenkönig rückt in den Dukat. Belagerung Roms 756. Verwüstung der Campagna. Plünderung der Katakomben Roms. Schreiben Stephans an die Franken. Petrus schreibt an die Frankenkönige.

[5.](#rom0425) Pippin zieht nach Italien. Aistulf hebt die Belagerung Roms auf. Eintreffen von byzantinischen Gesandten und deren Enttäuschung. Aistulf unterwirft sich. Die Pippinische Schenkungsurkunde. Stiftung des Kirchenstaats. Aistulf stirbt 756. Anerkennung des Desiderius als Langobardenkönig. Stephan stirbt 757.

  
**Drittes Kapitel**

[1.](#rom0431) Paulus I. Papst 757. Schreiben der Römer an Pippin. Freundliche Beziehungen des Papsts zu diesem Könige. Desiderius bestraft die rebellischen Herzöge von Spoleto und Benevent. Er kommt nach Rom. Politisches Verfahren Pauls. Verhältnis des Papsts und Roms zu Byzanz. Frieden mit Desiderius.

[2.](#rom0432) Bauten Stephans II. und Pauls I. Der Vatikan und St. Peter. Der erste Glockenturm in Rom. Die Kapelle der S. Petronilla. Versetzung der Heiligen aus den Katakomben nach der Stadt. Gründung des Klosters S. Silvestro in Capite.

[3.](#rom0433) Paul I. stirbt 767. Usurpation des Dux Toto. Der Pseudopapst Constantin. Gegenrevolution in Rom. Christophorus und Sergius überrumpeln Rom mit langobardischer Hilfe. Die Langobarden setzen Philippus im Lateran ein. Stephan III. Papst. Terrorismus in Rom. Strafgericht über die Usurpatoren. Tod Pippins 768. Lateranisches Konzil 769.

  
**Viertes Kapitel**

[1.](#rom0441) Macht des Christophorus und Sergius in Rom. Stephan III. verbindet sich mit Desiderius. Der Langobardenkönig rückt vor die Stadt. Sturz jener Männer und Schuld des Papsts an ihrem tragischen Ende. Projekt einer Doppelheirat zwischen den Dynastien von Pavia und vom Frankenland. Intrigen des Papsts dagegen. Widerstand Ravennas gegen Rom. Wendung der Politik des fränkischen Hofs zugunsten des Papsts. Stephan III. stirbt 772.

[2.](#rom0442) Hadrianus I. Papst. Sturz der langobardischen Partei in Rom. Feindliches Vorschreiten des Desiderius. Sturz des Paul Afiarta. Der Stadtpräfekt. Desiderius verwüstet den römischen Dukat. Hadrian rüstet die Verteidigung. Rückzug der Langobarden.

[3.](#rom0443) Heereszug Karls nach Italien. Belagerung Pavias. Karl feiert das Osterfest in Rom. Bestätigung der Pippinischen Schenkung. Der Fall Pavias und des Langobardenreichs im Jahre 774.

[4.](#rom0444) Die Schenkung Constantins. Geographischer Inhalt der Karolingischen Schenkung. Spoleto; Tuszien; die Sabina; Ravenna. Ansprüche Karls auf das Bestätigungsrecht der Erzbischöfe von Ravenna. Der Patriziat des St. Petrus. Beweis, daß der Papst den oberherrlichen Befehlen Karls Folge leistete. Sklavenhandel der Venetianer und Griechen.

[5.](#rom0445) Benevent. Arichis macht sich unabhängig. Päpstlicher Krieg um Terracina. Karls zweite Anwesenheit in Rom. Sein dritter Aufenthalt daselbst. Zug gegen Benevent und Friedensschluß. Neue Schenkung Karls. Arichis unterhandelt mit Byzanz. Die dortigen Verhältnisse. Beilegung des Bilderstreits. Grimoald Herzog von Benevent.

  
**Fünftes Kapitel**

[1.](#rom0451) Zustände Roms. Tiberüberschwemmung im Jahre 791. Hadrian stellt die Stadtmauern her. Er restauriert die Aqua Traiana, die Claudia, Jobia und Aqua Virgo. Seine Sorge um die Kolonisation der Campagna. Verhältnisse der Kolonen. Die Domuskulte Hadrians. Capracorum.

[2.](#rom0452) Kirchenbauten Hadrians. Der vatikanische Porticus. St. Peter. Der Lateran. St. Paul. Die Kunsttätigkeit in Rom. _S. Giovanni ante Portam Latinam_. _S. Maria in Cosmedin_. Die _Schola Graeca_. _Monte Testaccio_.

[3.](#rom0453) Zustand der Wissenschaften zur Zeit Hadrians. Unwissenheit der Römer. Kultur der Langobarden. Adalberga. Paul Diaconus. Schulen in Rom. Die geistliche Musik. Verschwinden der Poesie. Die epigrammatische Dichtung. Ruin der lateinischen Sprache. Erste Anfänge der neurömischen Sprache.

  
**Sechstes Kapitel**

[1.](#rom0461) Innere Zustände Roms und der Römer. Die drei Volksklassen. Militische Organisation. Der Exercitus Romanus. Das System der Scholen. Allgemeinheit des Zunftwesens. Die Scholen der Fremden: Juden, Griechen, Sachsen, Franken, Langobarden und Friesen.

[2.](#rom0462) Zivilverwaltung der Stadt Rom. Nichtexistenz des Senats. Die Konsuln. Die Beamten der Stadt. Der Adel. Justizwesen. Stadtpräfekt. Der päpstliche Hof. Die sieben Palastminister und andere Hausoffizianten.

[3.](#rom0463) Verhältnisse in anderen Städten. Duces. Tribuni. Comites. Der Ducatus Romanus und seine Grenzen. Römisch Tuszien. Kampanien. Sabina. Umbria.

  
**Siebentes Kapitel**

[1.](#rom0471) Tod Hadrians 795. Leo III. Papst. Seine Gesandtschaft an Karl und dessen Vertrag mit der Kirche. Bedeutung der Symbole der Schlüssel vom Grabe Petri und des Banners von Rom. Karls oberste Richtergewalt in Rom als Patricius. Darstellung der Harmonie zwischen der geistlichen und weltlichen Gewalt. Die Mosaiken in S. Susanna. Das Mosaikbild im Triclinium Leos III.

[2.](#rom0472) Verschwörung der Nepoten Hadrians und anderer Aristokraten gegen Leo III. Attentat auf sein Leben. Seine Flucht nach Spoleto. Seine Reise nach Deutschland und Zusammenkunft mit Karl. Rom in der Gewalt des Adels. Alcuins Rat in betreff des Verfahrens Karls mit Rom. Rückkehr Leos nach Rom 799. Prozeß Karls gegen die Angeklagten durch seine Machtboten.

[3.](#rom0473) Romfahrt Karls des Großen. Parlament in der St. Peterskirche. Gericht Karls über die Römer und den Papst. Der Reinigungseid Leos. Kaiserwahl Karls durch die Römer. Die Erneuerung des westlichen Reichs. Krönung Karls des Großen zum Kaiser durch den Papst im Jahre 800. Ansichten über die Rechtsquelle und den Begriff des neuen Imperium.
