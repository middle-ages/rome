#### Geschichte der Stadt Rom im Mittelalter

## Sechstes Buch

#### Geschichte der Stadt Rom im X. Jahrhundert

### Inhalt:

**Erstes Kapitel**

[1.](#rom0611) Benedictus IV. krönt Ludwig von der Provence zum Kaiser 901. Die angesehensten Optimaten Roms jener Zeit. Leo V. und Christophorus. Sergius III. wird Papst. Bullen von ihm. Er baut die Lateranische Basilika wieder auf. Anastasius III. und Lando.

[2.](#rom0612) Johann X. Seine Vergangenheit. Er verdankt die Tiara der Römerin Theodora. Deren Gemahl Theophylactus, Konsul und Senator der Römer. Der Emporkömmling Alberich. Sein Verhältnis zu Marozia. Theodora und Marozia.

[3.](#rom0613) Schreckliche Verwüstungen durch die Sarazenen. Farfa wird zerstört. Subiaco. Sarazenische Raubburgen in der Campagna. Johann X. bietet Berengar die Kaiserkrone. Einzug Berengars in Rom und seine Krönung anfangs Dezember 915.

[4.](#rom0614) Feldzug gegen die Sarazenen. Kämpfe in der Sabina und Campagna. Vertrag Johanns X. mit den unteritalischen Fürsten. Vernichtung der Sarazenen am Garigliano im August 916. Rückkehr des Papsts und Alberichs nach Rom. Stellung Alberichs. Sturz Berengars. Dessen Folgen in Rom. Ungewisses Ende Alberichs.

[5.](#rom0615) Vertreibung Rudolfs von Burgund. Ränke der Weiber, um Hugo zu erheben. Johann X. schließt mit ihm einen Vertrag. Marozia vermählt sich mit Guido von Tuszien. Bedrängnis Johanns X. Sein Bruder Petrus wird vertrieben. Revolution in Rom. Ermordung des Petrus. Sturz und Tod Johanns X.

  
**Zweites Kapitel**

[1.](#rom0621) Leo VI. und Stephanus VII. Der Sohn Marozias besteigt als Johannes XI. den Päpstlichen Stuhl. Der König Hugo. Marozia bietet ihm ihre Hand und Rom an. Ihre Vermählung. Die Engelsburg. Revolution in Rom. Der junge Alberich bemächtigt sich der Gewalt.

[2.](#rom0622) Charakter der Umwälzung in Rom. Alberich _Princeps_ und _Senator omnium Romanorum_. Begriff dieser Titel. Der Senat. Die Senatrices. Grundlagen der Gewalt Alberichs. Die Aristokratie. Zustand der römischen Bürgerschaft. Die Stadtmiliz. Das Justizwesen unter Alberich.

[3.](#rom0623) Mäßigung Alberichs. Hugo belagert wiederholt Rom. Er vermählt Alberich seine Tochter Alda. Dessen Beziehungen zu Byzanz. Leo VII. Papst 936. Rückblick auf die Bedeutung des benediktinischen Mönchtums. Sein Verfall. Die cluniazensische Reform. Tätigkeit Alberichs in diesem Sinn. Odo von Cluny in Rom. Fortsetzung der Geschichte von Farfa. Die Provinz Sabina.

[4.](#rom0624) Stephanus VIII. Papst 939. Alberich unterdrückt einen Aufstand. Marinus II. Papst 942. Neue Belagerung Roms durch Hugo. Sein Sturz durch Berengar von Ivrea. Lothar König von Italien. Friede zwischen Hugo und Alberich. Agapitus II. Papst 946. Tod Lothars. Berengarius König von Italien 950. Die Italiener rufen Otto den Großen. Alberich weist Otto von Rom ab. Berengar wird Ottos Vasall. Tod Alberichs im Jahre 954.

  
**Drittes Kapitel**

[1.](#rom0631) Octavianus folgt Alberich in der Gewalt. Er wird Papst im Jahre 955 als Johann XII. Seine Ausschweifungen. Er verläßt die Politik seines Vaters. Die Lombarden und Johann XII. rufen Otto I. Sein Vertrag mit dem Papst und sein Schwur. Seine Kaiserkrönung in Rom am 2. Februar 962. Charakter des neuen Römischen Imperium deutscher Nation.

[2.](#rom0632) Das Privilegium Ottos. Johann und die Römer huldigen ihm. Johann konspiriert gegen den Kaiser. Er nimmt Adalbert in Rom auf. Otto zieht wieder in Rom ein, woraus der Papst entflieht. Der Kaiser nimmt den Römern die freie Papstwahl. Die Novembersynode. Absetzung Johanns XII. Leo VIII. Mißglückter Aufstand der Römer. Otto verläßt Rom.

[3.](#rom0633) Rückkehr Johanns XII. Leo VIII. entflieht. Er wird auf einem Konzil abgesetzt. Rache Johanns an seinen Feinden. Er stirbt im Mai 964. Die Römer wählen Benedikt V. Otto führt Leo VIII. nach Rom zurück. Benedikt V. wird abgesetzt und exiliert. Unterwerfung des Papsttums unter den deutschen Kaiser. Das Privilegium Leos VIII.

[4.](#rom0634) Otto kehrt heim. Leo VIII. stirbt im Frühling 965. Johannes XIII. Papst. Seine Familie. Seine Vertreibung. Otto rückt gegen Rom. Der Papst wird wieder aufgenommen. Barbarische Bestrafung der Aufständischen. Der Caballus Constantini. Klagestimme über den Fall Roms unter die Sachsen.

  
**Viertes Kapitel**

[1.](#rom0641) Kaiserkrönung Ottos II. Die Gesandtschaft Liutprands in Byzanz. Praeneste oder Palestrina. Verleihung dieser Stadt an die Senatrix Stephania im Jahre 970.

[2.](#rom0642) Vermählung Theophanos mit Otto II. in Rom. Benedictus VI. Papst 973. Otto der Große stirbt. Bewegung in Rom. Die Familie der Crescentier. Die Caballi Marmorei. Römische Zunamen in jener Zeit. Crescentius de Theodora. Sturz Benedikts VI. Erhebung des Ferrucius als Bonifatius VII. Seine plötzliche Flucht. Dunkles Ende des Crescentius.

[3.](#rom0643) Benedictus VII. Papst 974. Er befördert die cluniazensische Reform. Er restauriert Kirchen und Klöster. Das Kloster St. Bonifatius und Alexius auf dem Aventin. Legende von St. Alexius. Italienischer Zug Ottos II. Seine Anwesenheit in Rom zu Ostern 981. Sein unglücklicher Feldzug in Kalabrien. Johann XIV. wird Papst. Tod Ottos II. in Rom am 7. Dezember 983. Sein Grabmal in St. Peter.

[4.](#rom0644) Ferrucius kehrt nach Rom zurück. Schreckliches Ende Johanns XIV. Terroristisches Regiment Bonifatius' VII. Sein Sturz. Johannes XV. Papst 985. Crescentius bemächtigt sich der patrizischen Gewalt. Theophano kommt als Regentin des Reichs nach Rom. Sie beruhigt Rom. St. Adalbert in Rom.

  
**Fünftes Kapitel**

[1.](#rom0651) Tiefer Verfall des Papsttums. Invektive der gallischen Bischöfe gegen Rom. Feindliche Stellung der Landessynoden. Crescentius reißt die weltliche Gewalt an sich. Johann XV. entflieht. Die Römer nehmen ihn wieder auf. Er stirbt 996. Gregor V. der erste deutsche Papst. Unterwerfung des Papsttums unter das deutsche Kaisertum. Otto III. Kaiser 21. Mai 996.

[2.](#rom0652) Verurteilung der römischen Rebellen. Crescentius wird begnadigt. Adalbert muß Rom verlassen. Sein Märtyrertod. Otto III. verläßt Rom. Aufstand der Römer. Kampf der Stadt gegen Papsttum und Kaiserum. Crescentius verjagt Gregor V. Umwälzung in Rom. Crescentius erhebt Johann XVI. auf den Päpstlichen Stuhl.

[3.](#rom0653) Die Herrschaft des Crescentius in Rom. Otto rückt gegen die Stadt. Schreckliches Schicksal des Gegenpapsts. Crescentius verteidigt sich in der Engelsburg. Verschiedene Berichte über sein Ende. Der Mons Malus oder Monte Mario. Grabschrift auf Crescentius.

  
**Sechstes Kapitel**

[1.](#rom0661) Folgen des Sturzes des Crescentius. Seine Verwandten in der Sabina. Der Abt Hugo von Farfa. Zustände dieses kaiserlichen Klosters. Merkwürdiger Prozeß des Abts mit den Presbytern von St. Eustachius in Rom.

[2.](#rom0662) Das Justizwesen in Rom. Die Judices Palatini oder Ordinarii. Die Judices Dativi. Einsetzungsformel für den römischen Richter. Formel bei Erteilung des römischen Bürgerrechts. Kriminalrichter, Konsuln und Comites mit richterlicher Gewalt in den Landstädten.

[3.](#rom0663) Die kaiserliche Pfalz in Rom. Kaisergarde. Pfalzgraf. Kaiserlicher Fiskus. Päpstliche Pfalz und Kammer. Abgaben. Verringerung der Einkünfte des Laterans. Verschleuderung der Kirchengüter. Exemtionen der Bischöfe. Anerkennung der Lehnsverträge durch die römische Kirche um das Jahr 1000.

[4.](#rom0664) Otto III. zieht nach Kampanien. Tod Gregors V. im Februar 999. Gerbert. St. Romuald in Ravenna. Gerbert als Silvester II. Phantastische Ideen Ottos III. in bezug auf die Herstellung des Römischen Reichs. Er kleidet sich in die Formen von Byzanz. Das Zeremonienbuch für seinen Hof. Der Patricius.

[5.](#rom0665) Anfang des Pontifikats Silvesters II. Eine Schenkung Ottos III. Erste Ahnung der Kreuzzüge. Ungarn wird römische Kirchenprovinz. Otto III. auf dem Aventin. Sein Mystizismus. Er kehrt nach Deutschland zurück. Er kommt wieder nach Italien im Jahre 1000. Schwierige Lage Silvesters II. Die Basilika St. Adalberts auf der Tiberinsel.

[6.](#rom0666) Tibur oder Tivoli. Empörung dieser Stadt. Ihre Belagerung und Schonung durch Otto III. und den Papst. Aufstand in Rom. Verzweifelte Lage Ottos. Seine Rede an die Römer. Seine Flucht aus Rom. Sein letztes Jahr. Sein Tod am 23. Januar 1002.

  
**Siebentes Kapitel**

[1.](#rom0671) Die Barbarei des X. Jahrhunderts. Aberglauben. Unbildung des römischen Klerus. Invektive der gallischen Bischöfe. Merkwürdige Entgegnung. Verfall der Klöster und Schulen in Rom. Die Grammatik. Spuren von theatralischen Aufführungen. Die Vulgärsprache. Völliger Mangel literarischer Talente in Rom.

[2.](#rom0672) Langsame Rückkehr der Wissenschaften. Gregor V. Das Genie Silvesters II. ein Fremdling in Rom. Boëthius. Die italienische Geschichtschreibung im X. Jahrhundert. Benedikt vom Soracte. Der Libell von der Imperatorischen Gewalt in der Stadt Rom. Die Kataloge der Päpste. Die _Vita_ St. Adalberts.

[3.](#rom0673) Die Stadtbeschreibungen. Der Anonymus von Einsiedeln. Tätigkeit der Sage und Legende in Rom. Die klingenden Statuen auf dem Kapitol. Die Sage vom Bau des Pantheon. Die Graphia der goldenen Stadt Rom. Die Memoria Iulii Caesaris.

[4.](#rom0674) Die Regionen der Stadt im X. Jahrhundert. Die Straßen. Damalige Bauart. Beschreibung eines Palasts. Große Anzahl großer Ruinen. Plünderung Roms durch die Römer.

[5.](#rom0675) Wanderung durch Rom zur Zeit Ottos III. Palatin. Septizonium. Forum. St. Sergius und Bacchus. Infernus. Marforio. Kapitol. S. Maria in Capitolio. Campus Caloleonis. Die Trajanssäule. Die Säule des Marc Aurel. Campo Marzo. Mons Augustus. Die Navona. Farfensische Kirchen. St. Eustachius in Platana. Legende des St. Eustachius. S. Maria im Minervium. Camigliano. _Arcus manus carneae_. Parione. Tiberbrücken. Der Tempel der Fortuna Virilis und der Vesta. Schlußübersicht.
