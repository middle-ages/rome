#### Geschichte der Stadt Rom im Mittelalter

## Drittes Buch

#### Vom Beginn der Regierung des Exarchen bis auf den Anfang des VIII. Jahrhunderts

### Inhalt:

**Erstes Kapitel**

[1.](#rom0311) Rom verfällt. Die römische Kirche steigt aus den Trümmern des Reiches auf. St. Benedikt. Subiaco und Monte Cassino. Cassiodorus wird Mönch. Anfang und Ausbreitung des Mönchtums in Rom.

[2.](#rom0312) Fortschritte der Langobarden in Italien. Sie dringen bis vor Rom. Benedikt I. Papst 574. Pelagius II. Papst 578. Die Langobarden belagern Rom. Zerstörung von Monte Cassino im Jahre 580. Gründung des ersten Benediktinerklosters in Rom. Pelagius II. fordert Hilfe von Byzanz. Gregor Nuntius am Hof des Kaisers. Überschwemmung und Pest im Jahre 590. Pelagius II. stirbt. Sein Bau von S. Lorenzo.

[3.](#rom0313) Wahl Gregors I. zum Papst. Seine Vergangenheit. Die Pestprozession. Die Legende von der Erscheinung des Erzengels Michael über dem Grabmal Hadrians.

  
**Zweites Kapitel**

[1.](#rom0321) Gregor wird zum Papst geweiht. Seine erste Predigt. Bedrängnis Roms durch die Langobarden. Leichenrede Gregors auf Rom. Er erkauft den Abzug der Langobarden.

[2.](#rom0322) Zustand der weltlichen Regierung Roms. Die kaiserlichen Beamten. Völliges Stillschweigen über den römischen Senat.

[3.](#rom0323) Stellung Gregors in bezug auf die Stadt Rom. Seine Sorge für das Volk. Verwaltung der Kirchengüter.

[4.](#rom0324) Gregor schließt mit Agilulf Frieden. Phokas besteigt den Thron von Byzanz und wird von Gregor beglückwünscht. Die Phokassäule auf dem Römischen Forum.

  
**Drittes Kapitel**

[1.](#rom0331) Charakter des sechsten Jahrhunderts. Mohammed und Gregor. Religiöse Zustände. Reliquiendienst. Wunderglaube. Gregor weiht die Gotenkirche S. Agata auf der Suburra.

[2.](#rom0332) Die Dialoge Gregors. Legende vom Kaiser Trajan. Das Forum Traianum. Zustand der wissenschaftlichen Kultur. Anklagen gegen Gregor. Immer tieferer Verfall der Stadt. Gregor bemüht sich, die Wasserleitungen herzustellen.

[3.](#rom0333) Wirksamkeit Gregors in der Kirche. Er sucht das germanische Abendland mit Rom zu verbinden. Er bekehrt England. Sein Tod im Jahre 604. Denkmäler von Gregor in Rom.

  
**Viertes Kapitel**

[1.](#rom0341) Pontifikat und Tod Sabinians und Bonifatius' III. Bonifatius IV. Das Pantheon wird der Jungfrau Maria geweiht.

[2.](#rom0342) Deusdedit Papst im Jahre 615. Aufstände in Ravenna und in Neapel. Erdbeben und Aussatz in Rom. Der Exarch Eleutherius rebelliert in Ravenna. Bonifatius V. Papst. Honorius I. 625. Das Recht, die Papstwahl zu bestätigen, beim Exarchen von Ravenna. Bauten des Honorius. St. Peter. Plünderung des Dachs des Tempels der Venus und Roma. Die Kapelle St. Apollinaris. S. Adriano auf dem Forum.

[3.](#rom0343) St. Theodor am Palatin. Antike Reminiszenzen. Die Kirche _SS. Quatuor Coronatorum_ auf dem Coelius. S. Lucia _in Selce,_ S. Agnese vor der Porta Nomentana. S. Vincenzo und Anastasio _ad Aquas Salvias_. S. Pancrazio.

  
**Fünftes Kapitel**

[1.](#rom0351) Honorius I. stirbt 638. Der Chartular Mauritius und der Exarch Isaak plündern den Kirchenschatz. Severinus Papst. Johannes IV. Papst. Das lateranische Baptisterium. Theodorus Papst 642. Rebellion des Mauritius in Rom. Tod des Exarchen Isaak. Palastrevolution in Byzanz. Constans II. Kaiser. Der Patriarch Pyrrhus in Rom. Die Kirchen St. Valentin und St. Euplus.

[2.](#rom0352) Martinus I. Papst im Jahre 649. Römische Synode wegen der Monotheleten. Anschlag des Exarchen Olympius auf Martins Leben. Theodorus Calliopa führt den Papst gewaltsam hinweg im Jahre 653. Martin stirbt im Exil. Eugenius Papst im Jahre 654.

[3.](#rom0353) Vitalianus Papst 657. Der Kaiser Constans II. kommt nach Italien. Sein Empfang und Aufenthalt in Rom im Jahre 663. Eine Klagestimme über Rom. Zustand der Stadt und ihrer Monumente. Das Colosseum. Constans plündert Rom. Sein Tod in Syrakus.

  
**Sechstes Kapitel**

[1.](#rom0361) Adeodatus Papst im Jahre 672. Erneuerung des Klosters St. Erasmus. Donus Papst 676. Agathon Papst 678. Der Erzbischof von Ravenna unterwirft sich dem Primat von Rom. Das VI. Ökumenische Konzil. Die Pest von 680. St. Sebastian. St. Georg. Die Basilika in _Velo Aureo_.

[2.](#rom0362) Leo II. Papst 682. Benedictus II. Verhältnisse der Papstwahl. Johannes V. Papst. Zwiespältige Wahl nach seinem Tode. Konon. Klerus, Exercitus, Populus. Sergius I. Papst. Der Exarch Platina kommt nach Rom im Jahre 687.

[3.](#rom0363) Die Artikel der Trullanischen Synode werden von Sergius verworfen. Der Spathar Zacharias kommt nach Rom, den Papst aufzusuchen. Die Ravennaten rücken in Rom ein. Verhältnis Ravennas zu Rom und zu Byzanz. Johannitius von Ravenna.

  
**Siebentes Kapitel**

[1.](#rom0371) St. Petrus. Pilgerzüge nach Rom. Der König Kadwalla empfängt die Taufe 689. Die Könige Konrad und Offa nehmen die Kutte. Sergius schmückt die Kirchen mit Weihgeschenken. Grabmal Leos I. im Innern des St. Peter.

[2.](#rom0372) Johann VI. Papst 701. Der Exarch Theophylactus kommt nach Rom. Die italienischen Milizen rücken vor die Stadt. Herstellung des Klosters Farfa. Gisulf II. von Benevent fällt in die Campagna ein. Johann VII. Papst 705. Justinian II. besteigt wieder den Thron von Byzanz. Das Oratorium Johanns VII. im St. Peter. Das Schweißtuch der Veronika. Subiaco hergestellt.

[3.](#rom0373) Sisinnius Papst 707. Constantinus Papst im Jahre 708. Bestrafung Ravennas. Der Papst reist nach dem Orient. Hinrichtungen in Rom. Aufstand Ravennas unter Georg. Erste Städtekonföderation Italiens. Philippicus Bardanes Kaiser 711. Die Römer verwerfen ihn. Der Dukat und Dux von Rom. Bürgerkrieg in Rom. Der Cäsarenpalast. Anastasius II. Kaiser 713. Tod Constantins 715.
