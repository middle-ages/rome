#### Geschichte der Stadt Rom im Mittelalter

## Vierzehntes Buch

#### Geschichte der Stadt Rom in den ersten drei Dezennien des XVI. Jahrhunderts

### Inhalt:

**Erstes Kapitel**

[1.](#rom1411) Cesare Borgia nach dem Tode seines Vaters. Er unterhandelt mit den Kardinälen. Orsini und Colonna dringen in die Stadt. Vertrag Cesares mit den Colonna. Er stellt sich in den Schutz Frankreichs. Er zieht ab nach Nepi. Konklave. Pius III. Papst 22. September 1503. Cesare kehrt nach Rom zurück. Alviano und die Orsini rücken in Rom ein. Sie schließen einen Bund mit Consalvo. Cesare Borgia flieht in die Engelsburg. Pius III. stirbt 18. Oktober 1503.

[2.](#rom1412) Rovere unterhandelt mit Cesare wegen der Papstwahl. Julius II. Papst 1. November 1503. Die Venetianer in der Romagna. Pläne Cesares, sich dorthin zu begeben. Der Herzog Guidobaldo in Rom. Unterhandlungen wegen der Burgen Cesares. Dessen Zusammenkunft mit Guidobaldo. Untergang der französischen Armee in Neapel. Flucht Cesares von Ostia nach Neapel, seine Gefangennahme durch Consalvo und sein Ende. Machiavelli und Cesare Borgia.

[3.](#rom1413) Julius II. und die Lage der Welt und Italiens im Beginn des XVI. Jahrhunderts. Der Kirchenstaat und seine Barone. Die Nepoten des Papsts. Kardinalsernennungen. Bündnis zu Blois September 1504. Johann Jordan vermählt sich mit Felice Rovere, Marcantonio Colotina mit Lucrezia Rovere. Julius unterwirft Perugia 1506. Zug gegen Bologna. Sturz der Bentivogli. Triumphe des Papsts.

[4.](#rom1414) Maximilian in Konstanz Mai 1507. Ankündigung seines Romzugs. Krieg mit Venedig. Liga zu Cambrai. Schlacht von Agnadello. Not der Venetianer. Versöhnung Julius' II. mit der Republik. Plan zur Vertreibung der Franzosen. Julius bannt den Herzog von Ferrara. Zorn Ludwigs XII. Synode in Tours. Chaumont vor Bologna. Julius in Mirandola. Verlust Bolognas. Ermordung Alidosis. Berufung des Konzils in Pisa. Maximilian und das Papsttum. Berufung des Lateranischen Konzils. Krankheit des Papsts. Demokratische Bewegung in Rom. Pompeo Colonna und die römischen Barone.

[5.](#rom1415) Die Heilige Liga Oktober 1511. Das Konzil in Pisa mißglückt. Gaston de Foix entsetzt Bologna. Schlacht bei Ravenna 11. April 1512.

[6.](#rom1416) Eindruck der Schlacht in Rom. Die Schweizer retten den Papst. Das Lateranische Konzil 3. Mai 1512. Neue Liga wider Frankreich. Krieg in der Lombardei. Flucht des Johann Medici. Rückzug der Franzosen. Der Papst gewinnt Bologna. Alfonso in Rom. Julius II. bemächtigt sich Reggios und Modenas. Die Exekution des Bundes gegen Florenz. Rückkehr der Medici. Parma und Piacenza ergeben sich dem Papst. Mißstimmung aller Parteien. Die Schweizer Boten in Rom. Matthias Lang in Rom. Kongreß. Bund zwischen Kaiser und Papst wider Venedig. Der Kaiser anerkennt das Lateranische Konzil. Maximilian Sforza in Mailand eingesetzt. Ende Julius' II. S. 399.

  
**Zweites Kapitel**

[1.](#rom1421) Rom unter Julius II. Sein Verhältnis zur monumentalen Kunst. Straßenbauten. _Via Julia_. Neubau von S. Celso. Die Banken. Lungara. Agostino Chigi. Sein Landhaus (Farnesina). Baldassare Peruzzi. Bramante. Seine Bauten. Giuliano di Sangallo. Der Hof des Belvedere. Der Hof des Damasus. Der Neubau des St. Peter. Seine Grundsteinlegung 18. April 1506. Geschichte seines Baues.

[2.](#rom1422) Erste Sammlung von Antiken im Belvedere. Der Apollo. Die Gruppe des Laokoon. Der Torso. Die Kleopatra oder Ariadne. Andere Antiken und Sammlungen in Palästen. Moderne Bildhauerei. Andrea Sansovino. Michelangelo. Sein David. Sein Plan zum Grabmal Julius' II.

[3.](#rom1423) Die Malerei. Die sixtinischen Deckengemälde Michelangelos. Das jüngste Gericht. Raffael. Seine Gemälde in den Stanzen des Vatikan.

  
**Drittes Kapitel**

[1.](#rom1431) Wahl Leos X. Sein prachtvoller Umzug zum Lateran. Stellung des Papsts zu den Mächten. Krieg mit Frankreich und Venedig. Schlacht bei Novara 6. Juni 1513. Ludwig XII. entsagt dem Schisma. Leo X. und seine Nepoten. Portugiesische Gesandtschaft.

[2.](#rom1432) Leo X. nähert sich Frankreich und bemüht sich zugleich um eine Liga wider dasselbe. Tod Ludwigs XII. 1. Januar 1515. Franz I. König von Frankreich. Julian Medici vermählt sich mit Filiberta von Savoyen. Beitritt Leos zur Liga Spaniens und des Kaisers 17. Juli 1515. Kriegszug Franz' I. nach Italien. Er erobert Mailand. Sein Sieg bei Marignano 14. September 1515. Bestürzung des Papsts. Reise Leos zu Franz I. Zusammenkunft in Bologna Dezember 1515. Dortige Beschlüsse. Tod Julian Medicis März 1516.

[3.](#rom1433) Tod Ferdinands des Katholischen 15. Januar 1516. Sein Erbe und Enkel Karl. Unglücklicher Krieg Maximilians mit Venedig. Leo X. verjagt den Herzog von Urbino und gibt das Land dem Lorenzo Medici. Friede zu Noyon Dezember 1516. Maximilian tritt Verona an Venedig ab. Der Herzog von Urbino bemächtigt sich seiner Staaten wieder. Schimpflicher Krieg des Papsts mit ihm. Verschwörung der Kardinäle Petrucci und Sauli. Prozeß gegen diese, Riario, Soderini und Hadrian von Corneto. Massenhafte Kardinalsernennung Juni 1517. Beendigung des Kriegs mit Urbino.

[4.](#rom1434) Lorenzo Medici vermählt sich mit Madeleine la Tour d'Auvergne. Verbindung Leos mit Frankreich. Schluß des Lateranischen Konzils März 1517. Verderbnis der Kurie. Die Florentiner am Hofe Leos. Dessen Prachtliebe, Verschwendung, Lebensart. Der Ablaß für St. Peter. Luther erhebt sich. Die deutschen Humanisten. Luther in Augsburg. Hutten. Beginn der Reformation.

[5.](#rom1435) Bemühungen Maximilians um die Königswahl seines Enkels. Sein Tod. Wahlkampf. Politik Leos. Kaiserwahl Karls V. 28. Juni 1519. Lorenzo Medici stirbt. Pläne Leos auf Parma, Piacenza, Ferrara. Giampolo Baglione hingerichtet Juni 1520. Karl kommt nach Deutschland. Krönung in Aachen. Fortgang der Reformation. Reichstag zu Worms. Das Wormser Edikt.

[6.](#rom1436) Liga Karls V. mit Leo X. Krieg in der Lombardei. Einnahme Mailands. Tod des Papstes im Dezember 1521.

  
**Viertes Kapitel**

[1.](#rom1441) Heidentum der Renaissance. Skeptik und Unglauben. Weltlichkeit der Bildung. Klassen der römischen Gesellschaft. Mäzenatenkreise. Chigi und Altoviti. Die Diplomaten. Korruption. Kurtisanen. Urbanität und glänzendes Wesen in Rom.

[2.](#rom1442) Die römische Akademie. Angelo Colocci. Goritz. Bibliotheken. Die Vaticana. Inghirami. Beroald. Acciajuoli. Aleander. Die römische Universität. Ciceronianer. Bembo. Sadoleto. Gianfrancesco Pico. Alberto Pio. Antiquare. Albertini. Inschriftensammlung Mazochis. Andreas Fulvius. Pierius Valerianus. Raffaels Stadtplan. Mario Fabio von Calvi. Historiker. Paris de Grassis. Aegidius von Viterbo. Raffael Volaterranus. Paul Jovius. Hellenisten. Karteromachus. Phavorinus. Johann Laskaris. Musurus. Verfall des Humanismus. Schmähschrift Gyraldis und Bekenntnisse des Jovius.

[3.](#rom1443) Neulateinische Poesie. Leo und die Poeten. Die römischen Stadtpoeten des Arsilli. Die Elogia des Jovius. Gyraldis Literaturgeschichte. Valerianus »Vom Unglück der Schriftsteller«. Die Coryciana. Pasquille. Evangelista Maddaleni. Camillo Porzio. Die Mellini. Der Prozeß wider Longolius. Blosio Palladio. Casanova. Hadrian von Corneto. Marcantonius Flaminius. Guido Posthumus Silvester. Sannazar. Vida. Fracastoro. Navagero.

[4.](#rom1444) Italienische Poesie. Verdienste Bembos um die italienische Sprache. Molza. Tebaldeo. Bernardo Accolti. Beazzano. Vittoria Colonna. Veronika Gambara. Berni und die burleske Poesie. Pietro Aretino. Alemanni. Ruccellai. Ariosto. Trissino. Das Drama. Die Komödie. Die Calandra Bibienas. Versuche der Tragödie.

[5.](#rom1445) Malerei. Raffael unter Leo X. Giulio Romano. Andre Schüler Raffaels. Marcantonio Raimondi. Michelangelo unter Leo X. Ehrenbildsäule dieses Papsts. Goldarbeiter. S. Giovanni dei Fiorentini. Beginn des Palazzo Farnese. Raffael als Architekt. Bauten Sansovinos. Fassadenmalereien. Villen. Rom unter Leo X. als Stadt. Neue Stadtviertel. Wachsende Bevölkerung. Verfall des Adels.

  
**Fünftes Kapitel**

[1.](#rom1451) Konklave. Hadrian VI. Papst 9. Januar 1522. Seine frühere Laufbahn. Freude Karls über seine Erwählung. Verwirrung in Italien und Rom. Schlacht bei Bicocca 27. April 1522. Die Franzosen aus Italien verjagt. Plünderung Genuas. Anarchie in Rom. Pest. Das Stieropfer im Colosseum. Romfahrt und Einzug Hadrians VI.

[2.](#rom1452) Wesen Hadrians VI. Enkevoirt und andre Flamländer. Reformversuche. Tiene und Caraffa. Abberufung Manuels. Der Herzog von Sessa Botschafter Karls. Lannoy Vizekönig in Neapel. Unglückliche Lage Hadrians. Seine Reformversuche scheitern. Fortschritte der Reformation. Chieregati. Der Nürnberger Reichstag. Fall von Rhodos. Hadrian sucht seine Neutralität zu bewahren. Intrigen Soderinis. Hadrian tritt der Liga des Kaisers bei. Beginn des Feldzugs Franz' I. Verrat Bourbons. Hadrian VI. stirbt September 1523.

[3.](#rom1453) Clemens VII. Papst 18. November 1523. Giberti und Schomberg. Unglücklicher Feldzug Bonnivets in der Lombardei. Die Kaiserlichen rücken in die Provence. Marseille belagert. Rückzug. Schneller Zug Franz' I. auf Mailand. Er belagert Pavia. Schwankende Politik Clemens' VII. Expedition Stuarts nach Neapel. Bruch zwischen Karl und dem Papst. Schlacht bei Pavia.

[4.](#rom1454) Bestürzung der Kurie in Rom. Die Spanier bekämpfen dort die Orsini und die Franzosen. Clemens schließt ein Bündnis mit dem Kaiser 1. April 1525. Franz I. nach Spanien eingeschifft. Reaktion gegen die Macht des Kaisers. Der Papst sucht, eine Liga wider ihn zu bilden. Die Verschwörung Morones. Tod Pescaras November 1525. Friede zu Madrid 14. Februar 1526. Der König Franz entlassen. Liga zu Cognac 22. Mai 1526.

  
**Sechstes Kapitel**

[1.](#rom1461) Clemens VII. als Führer Italiens im Kampf um seine Unabhängigkeit. Der Kaiser schickt Moncada an den Papst. Clemens verwirft seine Anträge. Pompeo Colonna und die Ghibellinen. Unglücklicher Beginn des Kriegs der Liga. Fruchtlose Unternehmung des Herzogs von Urbino gegen Mailand. Die Colonna überfallen Rom 20. September 1526. Plünderung des Borgo. Clemens wird zu einem schimpflichen Vertrag gezwungen. Manifest des Kaisers an den Papst. Reichstag zu Speyer. Festsetzung der Reformation.

[2.](#rom1462) Clemens bricht den Septembervertrag. Frundsberg sammelt Landsknechte. Ihr Zug nach Italien. Clemens greift die Colonna an. Lannoy landet in Gaëta. Die Colonna und Lannoy rücken bis Frosinone; Frundsberg nach dem Po. Fall des Giovanni Medici. Bourbon in Mailand. Er vereinigt sich mit Frundsberg. Aufbruch dieser Armee gen Parma. Ferramosca als Unterhändler in Rom. Sieg der Päpstlichen bei Frosinone. Expedition der Landarmee und der Flotte nach Neapel. Das Landheer löst sich auf. Marsch der Armee Bourbons. Lageraufstand. Erkrankung Frundsbergs. Vertrag des Papsts mit Lannoy. Lannoy versucht, die Armee Bourbons aufzuhalten. Sie rückt vorwärts gegen Rom.

[3.](#rom1463) Verteidigungsanstalten in Rom. Renzo da Ceri und andere Hauptleute. Verblendung der Römer. Der Prophet Brandano. Kardinalsernennung am 3. Mai. Bourbon vor den Stadtmauern am 5. Mai. Sturm auf die Leonina 6. Mai 1527. Bourbon fällt. Die Leonina erstürmt. Flucht des Papsts in die Engelsburg. Trastevere erstürmt. Die Stadt Rom erstürmt.

[4.](#rom1464) Der »Sacco di Roma«. Fruchtloser Versuch der Bundesarmee, Rom zu entsetzen.

  
**Siebentes Kapitel**

[1.](#rom1471) Kapitulation des Papsts 5. Juni 1527. Schrecklicher Zustand Roms und der Armee. Sie zieht in die Sommerquartiere von Umbrien. Narni geplündert. Clemens VII. als Gefangener in der Engelsburg. Eindruck der Katastrophe Roms auf die Mächte. Wolsey, die Seele der Koalition gegen Karl. Lautrec rückt in Oberitalien ein Juli 1527. Benehmen und Politik Karls V. Frage, ob die weltliche Gewalt des Papsttums fortbestehen solle.

[2.](#rom1472) Rückkehr der Kaiserlichen nach Rom September 1527. Die Geiseln in der Gewalt der Landsknechte. Veyre in Rom. Oktobervertrag in der Engelsburg. Krieg in der Lombardei. Flucht des Papsts nach Orvieto Dezember 1527. Die Liga und der Papst. Kriegserklärung der Mächte in Burgos. Zug Lautrecs nach Neapel. Abzug der Kaiserlichen aus Rom Februar 1528. Krieg um Neapel. Der Papst geht nach Viterbo. Untergang Lautrecs. Der Papst entschließt sich, auf die Seite des Kaisers zu treten.

[3.](#rom1473) Rückkehr des Papsts nach Rom 6. Oktober 1528. Zustand der Stadt. Ein Blick auf die Schicksale der Künstler und Gelehrten während der Plünderung.

[4.](#rom1474) Mahnung Contarinis an Clemens VII. Die Stellung, welche der Papst nimmt. Seine Erkrankung. Sieg der Kaiserlichen bei Landriano Juni 1529. Friede zu Barcelona 29. Juni. Friede zu Cambrai 5. August. Der Prinz von Oranien in Rom; Plan zur Unterwerfung von Florenz. Hippolyt Medici. Karl V. landet in Genua. Oranien vor Perugia und in Toskana. Kongreß von Bologna. Krönung Karls V. 24. Februar 1530. Er kehrt nach Deutschland zurück. Reichstag zu Augsburg.

[5.](#rom1475) Rückkehr des Papsts nach Rom. Sturz Wolseys. Krieg um Florenz. Schlacht von Gavinana 3. August 1530. Tod Oraniens und Ferruccis. Untergang der Freiheit von Florenz. Tiberüberschwemmung Oktober 1530. Alessandro Medici Regent, dann Herzog von Florenz. Zweiter Kongreß in Bologna Dezember 1532. Neue italienische Liga. Clemens weicht dem Konzil aus. Kongreß in Marseille. Vermählung Katharina Medicis mit Heinrich von Orléans. Rückkehr des Papsts nach Rom September 1534. Er erkrankt. Sein Abschiedsbrief an Karl V. Sein Tod 25. Dezember 1534. Schluß der Geschichte der Stadt Rom im Mittelalter.
