# Summary

## Erstes Buch Vom Beginn des V. Jahrhunderts bis zum Untergang des westlichen Reichs im Jahre 476

* Erstes Kapitel

  * [001. Plan dieses Werks](volume01/chapter01/section001.md)

  * [002. Allgemeine Ansicht der Stadt Rom in der letzten Kaiserzeit](volume01/chapter01/section002.md)

  * [003. Die vierzehn Regionen der Stadt](volume01/chapter01/section003.md)

## Neuntes Buch Regierung Innozenz' III. bis zum Jahre 1260

* Sechstes Kapitel

  * [251. Wahl Sinibalds Fieschi zum Papst Innocenz IV. 1243](volume09/chapter06/section251.md)

  * [254. Die Söhne Friedrichs II Konrad IV](volume09/chapter06/section254.md)

* Siebentes Kapitel

  * [255. Brancaleone Senator von Rom 1252](volume09/chapter07/section255.md)

  * [256. Innocenz IV kommt nach Anagni](volume09/chapter07/section256.md)

  * [257. Regierung Brancaleones in Rom](volume09/chapter07/section257.md)

  * [258. Sturz des Emanuel de Madio 1257](volume09/chapter07/section258.md)

## Zehntes Buch Regierung Geschichte der Stadt Rom vom Jahre 1260–1305

* Erstes Kapitel

  * [259. Das Deutsche Reich.](volume10/chapter01/section259.md)

  * [260. Kämpfe in Rom um die Senatorwahl](volume10/chapter01/section260.md)

  * [261. Clemens IV Papst 1265](volume10/chapter01/section261.md)

* Zweites Kapitel

  * [262. Manfreds Brief an die Römer](volume10/chapter02/section262.md)

  * [263. Aufbruch Karls aus Rom](volume10/chapter02/section263.md)

  * [264. Karl legt die Senatsgewalt nieder](volume10/chapter02/section264.md)

* Drittes Kapitel

  * [265. Die Ghibellinen bereiten den Zug Konradins](volume10/chapter03/section265.md)

  * [266. Üble Lage Konradins in Norditalien](volume10/chapter03/section266.md)

  * [267. Konradin flieht vom Schlachtfeld nach Rom](volume10/chapter03/section267.md)
