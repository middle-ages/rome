_4\. Hildeberts Klagestimme über den Fall Roms. Ruin der Stadt in der Epoche Gregors VII._

Die Träne weinte Jahre darauf ein fremder Bischof, Hildebert von Tours, welcher um 1106 die Stadt sah. Dies ist seine rührende Klage über die Ruinen Roms:

> »Nichts ist, Roma, dir gleich, selbst jetzt, da in Trümmern du moderst;  
Was im Glanze du warst, lehren Ruinen im Staub.  
Deine prangende Größe zerstörte die Zeit, und es liegen  
Kaiserpaläste und auch Tempel der Götter im Sumpf.  
Schutt ist worden die Macht, um welche der grimmige Parther  
Zitterte, da sie bestand, klagte, da sie zerfiel.  
Die mit dem Schwert einst Könige, einst Senatoren mit Rechtspruch  
Und die Olympischen selbst machten zur Herrin der Welt;  
Die einst Caesar despotisch, ein Frevler, zu haben begehrte,  
Lieber ihr Herrscher allein, als ihr Vater und Freund;  
Welche mit dreierlei Kunst, mit der Kraft, dem Gesetz und dem Schutze  
Frevler und Feinde bezwang, dauernde Freunde gewann;  
Die als Hort in der Wiege die sorgenden Führer bewachten,  
Deren Wachstum die Lust gastlicher Scharen genährt;  
Der in den Schoß Triumphe die Konsuln und Gnaden das Schicksal,  
Meister die Blüten der Kunst, Schätze geschüttet die Welt:  
Hin ist, wehe! die Stadt! nun schau' ich ihre Ruinen,  
Und nachsinnend bewegt ruf' ich: Roma, du warst!  
Doch nicht Stürme der Zeit, noch Flamme des Brandes, das Schwert nicht  
Haben sie völlig des Schmucks früherer Schöne beraubt.  
So viel steht noch hier, so viel ist gefallen, daß jenes  
Nichts zu vertilgen und dies nichts zu erneuern vermag.  
Reichtest du lebender Kunst auch Gold und Marmor, sie baute  
Selbst mit der Himmlischen Rat keine Ruine mehr auf.  
Solche gewaltige Roma erschuf einst menschliche Bildkraft,  
Daß sie der Ewigen Zorn nimmer zu tilgen vermocht.  
Siehe, die Götter bestaunen ja selbst hier Göttergebilde,  
Wünschend, sie wären zumal gleich wie Statuen schön.  
Konnte Natur doch nimmer den Göttern schaffen ein Antlitz,  
Hold wie der Mensch es dem Gott reizend in Formen geprägt.  
Ja, so blüht das Gebild, daß Kunst wohl eher des Meisters,  
Nicht die Göttlichkeit selbst ihm die Verehrung verleiht.  
Glückliche Stadt! wenn frei du wärest von deinen Tyrannen  
Oder die Herrscher in dir frei von schimpflichem Trug.«

Hildebert sah am Anfange des XII. Jahrhunderts die Verwüstung der Stadt, ihre alten und neuen Ruinen und die noch frischen Spuren des Feindes. Der geistvolle Dichter erschrak über die heidnischen Regungen, zu denen ihn selbst Rom verlockte; er vermischte sie deshalb durch eine zweite Elegie, worin er der trauernden Roma Worte der Tröstung in den Mund legte. Als ich noch, so ließ er diese unglückliche Sibylla sagen, der Idole mich erfreute, waren mein Stolz mein Heer und Volk und meine marmorne Pracht. Die Idole und die Paläste sind gefallen, Volk und Ritterschaft in Sklaverei gesunken, und kaum erinnert sich Rom noch Roms; doch nun habe ich den Adler mit dem Kreuz, den Caesar mit Petrus und die Erde mit dem Himmel vertauscht.

So schöne Betrachtungen konnten jedoch die Römer über den Schutthaufen ihrer Stadt nicht trösten, worin sie als Bettler umhergingen. Um viele Tausende war sie durch Krieg, Flucht, Tod und Gefangenschaft ärmer geworden. Seit Jahrhunderten hatte sie keinen so gewaltsamen Stoß erlitten; zwanzigjährige Parteikriege, Stürme drinnen und draußen, endlich die Feuersbrunst vermehrten ihre Trümmer durch die erste feindliche Zerstörung, welche sie, seit Totila die Mauern niedergeworfen hatte, wirklich erfuhr. Wir können eine Reihe von Monumenten aufzählen, die damals vernichtet worden sind.

Die Stürme Heinrichs auf St. Paul zertrümmerten wahrscheinlich die alte Säulenhalle, die vom Tor zur Basilika führte; der Vatikanische Porticus sank bei der Einnahme des Borgo in Ruinen. Die Leonina hatte Feuer zerstört; der St. Peter selbst mußte beschädigt worden sein. In der Stadt waren Palatin und Kapitol verwüstet, und das Schicksal des Septizonium, des damals schönsten Teiles der Kaiserpaläste, mußten auch andere verschanzte Bauwerke erfahren haben. Die Zerstörung unter Cadalus und Heinrich war jedoch unbeträchtlich, vergleicht man sie mit dem normannischen Brande. Denn Guiscard warf zweimal Feuer in die Stadt, zuerst als er durch das Flaminische Tor eindrang, dann als er von den Römern überfallen wurde. Der Brand zerstörte das Marsfeld, vielleicht bis zur Brücke Hadrians; die Reste der Portiken dieser Gegend und viele andere Denkmäler gingen unter, nur das Grabmal des Augustus schützte seine Beschaffenheit und die Säule des Marc Aurel ihre vereinzelte Lage auf einem völlig freien Platz. Das bisher dicht bewohnte Stadtviertel vom Lateran bis zum Colosseum ging in Flammen auf, und das Lateranische Tor selbst wurde seither das »verbrannte« genannt. Die alte Kirche der »Vier Gekrönten« sank in Asche; der Lateran und viele Kirchen mochten stark gelitten haben, das Colosseum, die Triumphbogen, die Reste des Circus Maximus schwerlich verschont geblieben sein. So viele Chronisten diese furchtbare Katastrophe flüchtig geschildert haben, so viele haben einstimmig berichtet, daß ein großer Teil der Stadt durch sie zugrunde gegangen sei. Ein Geschichtschreiber hat am Ende des XV. Jahrhunderts mit Grund das Urteil gefällt, daß Rom durch normannische Wut zu allererst in den kläglichen Zustand versetzt worden sei, welchen es zu seiner Zeit darbot. Der ehemals stark bevölkerte Coelius (die Region des Colosseum) fuhr zwar fort, noch bewohnt zu sein, aber er sank mehr und mehr in Verödung, und das gleiche Los traf den zu Ottos III. Zeit noch glänzenden Aventin. Wenn man heute in Rom diese beiden alten Hügel besucht, in deren trümmervoller Stille nur uralte Kirchen und Ruinen dastehen, so darf man sich sagen, daß diese Öde von der normannischen Zerstörung sich herschreibt. Jene Teile der Stadt wurden allmählich verlassen, und die Bevölkerung drängte sich nach und nach im Marsfelde, dem neuen Rom, zusammen.

Die Zerstörung der Stadt machte übrigens in dieser Epoche schon durch innere Ursachen reißende Fortschritte. Wenn früher der Bau von Kirchen wesentlich dazu beitrug, so tat dies jetzt die Verwandlung alter Monumente in Burgen und Türme. Außerdem holten selbst fremde Städte aus der Fundgrube Rom Steine und Säulen. Der schöne Dom Pisas, der im XI. Jahrhundert gebaut wurde, die berühmte Kathedrale in Lucca, welche Alexander II. einweihte, schmückten sich sicherlich mit Säulen, die von Rom geschenkt oder hier gekauft worden waren. Als Desiderius seine Basilika baute, erstand er in Rom Säulen und Marmorsteine, die er zu Schiff über Portus fortschaffen ließ, und unter der Beute, welche Robert nach Salerno mitschleppte, mochten sich nicht heidnische Statuen, wohl aber kostbare Ornamente und Säulen befinden, die er zum Bau des Doms St. Matthäus in jener Stadt verwendete. Er hätte indes wie Geiserich auch noch wirkliche Kunstwerke mit sich führen können, denn einige Bemerkungen Hildeberts in seiner ersten Elegie lassen schließen, daß selbst noch die Verwüstung durch die Normannen Statuen von Marmor oder Erz in Rom übrigließ.
