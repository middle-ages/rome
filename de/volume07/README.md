#### Geschichte der Stadt Rom im Mittelalter

## Siebentes Buch

#### Geschichte der Stadt Rom im XI. Jahrhundert

### Inhalt:

**Erstes Kapitel**

[1.](#rom0711) Weltgeschichtliche Stellung der Stadt Rom im XI. Jahrhundert. Wirkung der städtischen Elemente auf das Papsttum. Die Lombarden machen Arduin zum König, die Römer Johann Crescentius zum Patricius. Tod Silvesters II. im Jahre 1003. Johann XVII. und XVIII. Tusculum und seine Grafen. Sergius IV. Ende des Johann Crescentius im Jahre 1012.

[2.](#rom0712) Gregor zum Papst gewählt, wird von Theophylakt oder Benedikt VIII. verjagt. Heinrich entscheidet sich für den tuskulanischen Papst. Romfahrt und Kaiserkrönung Heinrichs II. (1014). Zustand Roms und des Landgebiets, wo erbliche Grafen aufgekommen sind. Der römische Adel als Senat. Romanus Senator aller Römer. Kaiserliches Tribunal. Erdrückter Aufstand der Römer. Rückkehr Heinrichs II. Ende des Nationalkönigs Arduin.

[3.](#rom0713) Kräftige Herrschaft Benedikts VIII. in Rom. Seine Unternehmung gegen die Sarazenen. Erstes Aufblühen von Pisa und Genua. Süditalien. Die Rebellion des Melus gegen Byzanz. Erste Normannenbanden (1017). Unglückliches Ende des Melus. Benedikt VIII. fordert den Kaiser zu einem Kriegszuge auf. Zug Heinrichs II. nach Apulien (1022).

[4.](#rom0714) Anfänge der Reform unter Benedikt VIII. Er stirbt (1024). Sein Bruder Romanus als Johann XIX. Heinrich II. stirbt 1024. Zustand Italiens. Johann XIX. ruft Konrad II. von Deutschland nach Rom. Schauspiel der Romzüge jener Zeit. Kaiserkrönung (1027). Wütender Aufstand der Römer. König Knut in Rom.

[5.](#rom0715) Reskript Konrads II. wegen des römischen Rechts im päpstlichen Lande. Sein glorreicher Zug nach Süditalien, seine Rückkehr. Benedikt IX., ein Knabe aus dem tuskulanischen Hause, wird zum Papst erhoben. Ruchloses Leben dieses Menschen. Schreckliche Zustände der Welt überhaupt. Die Treuga Dei. Benedikt IX. flüchtet zum Kaiser. Soziale Revolution in der Lombardei. Aribert von Mailand. Der Kaiser setzt Benedikt IX. wieder in Rom ein. Er zieht nach Unteritalien; er stirbt 1039.

  
**Zweites Kapitel**

[1.](#rom0721) Die Römer verjagen Benedikt IX. und erheben Silvester III. Benedikt vertreibt ihn. Er verkauft den Heiligen Stuhl an Gregor VI. Drei Päpste in Rom. Eine römische Synode beschließt, Heinrich III. als Befreier nach Rom zu rufen.

[2.](#rom0722) Heinrich III. zieht nach Italien. Konzil zu Sutri (1046). Gregor VI. dankt ab. Heinrich III. erhebt Clemens II., der ihn zum Kaiser krönt. Gemälde der Kaiserkrönung. Übertragung des Patriziats an Heinrich III. und seine Nachfolger.

[3.](#rom0723) Beginn der Kirchenreform. Heinrich III. zieht nach Unteritalien, dann über Rom nach Deutschland heim. Clemens II. stirbt (1047). Benedikt IX. bemächtigt sich des Heiligen Stuhls. Bonifatius von Toskana. Heinrich erhebt Damasus II. Ende Benedikts IX. Tod des Damasus. Ernennung Brunos von Toul zum Papst.

[4.](#rom0724) Leo IX. Papst (1049). Seine Reformtätigkeit. Verderbnis der Kirche. Simonie. Hildebrand. Mittellosigkeit des Papsts. Macbeth in Rom. Süditalien. Erwerbung Benevents durch Leo IX. Seine Kämpfe mit den Normannen. Seine Niederlage bei Civitate und sein Tod (1054).

  
**Drittes Kapitel**

[1.](#rom0731) Hildebrands Programm. Der Kaiser ernennt Gebhard von Eichstädt zum Papst. Gottfried von Lothringen vermählt sich mit Beatrix von Toskana. Heinrich III. kommt nach Italien. Victor II. Papst. Tod des Kaisers (1056). Regentschaft der Kaiserin Agnes. Victor II. Reichsvikar in Italien. Machtvolle Stellung Gottfrieds. Der Kardinal Friedrich. Victor II. stirbt. Stephan IX. Papst.

[2.](#rom0732) Die Eremiten und Pier Damiani. Die Bußdisziplin. Stephan IX. versammelt ausgezeichnete Männer als Kardinäle um sich.

[3.](#rom0733) Stephans IX. Pläne und sein Tod. Benedikt X. Nikolaus II. Hildebrand holt normannische Hilfe. Das neue Wahldekret. Fortschritte der Normannen. Sie leisten dem Papst den Lehenseid. Sturz Benedikts X.

[4.](#rom0734) Erbitterung in Rom gegen das Wahldekret. Nikolaus II. stirbt 1061. Die Römer und die Lombarden fordern den König Heinrich auf, einen Papst zu wählen. Mailand. Die Pataria. Die Cotta und Ariald. Die Hildebrandischen wählen Anselm von Lucca zum Papst. Der deutsche Hof erhebt Cadalus von Parma.

  
**Viertes Kapitel**

[1.](#rom0741) Alexander II. Cadalus geht nach Italien. Benzo kommt als Gesandter der Regentin nach Rom. Parlamente im Circus und auf dem Kapitol. Cadalus erobert die Leostadt. Er zieht nach Tusculum. Gottfried von Toskana diktiert Waffenruhe. Umschwung in Deutschland. Alexander II. als Papst anerkannt (1062). Er zieht in Rom ein.

[2.](#rom0742) Anno wird in Deutschland gestürzt. Cadalus kehrt nach Rom zurück. Zweiter Stadtkrieg um das Papsttum. Fall des Cadalus. Endgültige Anerkennung Alexanders II.

[3.](#rom0743) Hildebrands wachsende Macht. Reformbestrebungen. Die Normannen. Abfall Richards und sein Marsch auf Rom. Gottfried und der Papst führen ein Heer gegen ihn. Neuer Vertrag. Die Kaiserin Agnes nimmt den Schleier in Rom. Kämpfe in Mailand. Erlembald Cotta Miles St. Peters. Tod Arialds.

[4.](#rom0744) Ohnmacht des Papsts in Rom. Auflösung des Kirchenstaats. Die Stadtpräfektur. Cencius, das Haupt der Mißvergnügten. Cinthius Stadtpräfekt. Gottfried von Toskana stirbt. Tod Pier Damianis. Monte Cassino. Fest der Dedikation der von Desiderius neugebauten Basilika (1071).

  
**Fünftes Kapitel**

[1.](#rom0751) Alexander II. stirbt. Hildebrand besteigt den Päpstlichen Stuhl. Seine Laufbahn, sein Ziel. Er wird am 29. Juni 1073 ordiniert.

[2.](#rom0752) Gregor VII. empfängt den Lehnseid der Fürsten von Benevent und Capua. Robert Guiscard verweigert ihn. Plan Gregors, die Fürsten und ihre Reiche zu Vasallen der römischen Kirche zu machen. Sein Aufruf zu einem allgemeinen Kreuzzuge. Mathilde und Gregor VII. Sein erstes Konzil in Rom; seine Reformdekrete.

[3.](#rom0753) Zustände in Rom. Die Gegner Gregors. Wibert von Ravenna. Heinrich IV. Widerstand in Deutschland gegen die Dekrete Gregors. Beschluß gegen die Laien-Investitur. Attentat des Römers Cencius gegen Gregor.

[4.](#rom0754) Bruch Gregors mit Heinrich. Der König läßt zu Worms den Papst absetzen. Sein Brief an Gregor. Heinrich wird zu Rom gebannt und abgesetzt. Aufregung darüber in der Welt. Verhältnis beider Gegner zueinander. Die 27 Artikel Gregors.

[5.](#rom0755) Abfall der Reichsstände von Heinrich IV. Er entkleidet sich der königlichen Macht. Er sucht die Lossprechung vom Bann. Canossa (1077). Moralische Größe Gregors VII. Die Lombarden wenden sich vom König ab. Er nähert sich ihnen wieder. Tod des Cencius. Tod des Cinthius. Tod der Kaiserin Agnes in Rom.

[6.](#rom0756) Heinrich ermannt sich. Rudolf von Schwaben Gegenkönig. Heinrich kehrt nach Deutschland, Gregor nach Rom zurück. Fall der letzten Lombardendynasten in Süditalien. Rückblick auf das Volk der Langobarden. Robert leistet Gregor den Lehnseid. Wilhelm der Eroberer und Gregor. Der Papst anerkennt Rudolf und bannt Heinrich nochmals. Wibert von Ravenna Gegenpapst. Wendepunkt.

  
**Sechstes Kapitel**

[1.](#rom0761) Heinrich IV. rückt gegen Rom (1081). Erste Belagerung der Stadt. Zweite Belagerung im Frühjahr 1082. Abzug nach Farfa. Er rückt nach Tivoli, wo Clemens III. seinen Sitz nimmt. Er verheert die Länder der großen Gräfin.

[2.](#rom0762) Heinrich IV. belagert Rom zum drittenmal (1082-1084). Einnahme der Leostadt. Gregor VII. in der Engelsburg. Heinrich unterhandelt mit den Römern. Festigkeit des Papstes. Jordan von Capua huldigt dem König. Desiderius vermittelt den Frieden. Vertrag Heinrichs mit den Römern. Sein Abzug nach Toskana. Mißglückte Novembersynode Gregors. Die Römer werden dem König eidbrüchig.

[3.](#rom0763) Abzug Heinrichs nach Kampanien. Abfall der Römer von Gregor; sie übergeben die Stadt (1084). Gregor verschließt sich in die Engelsburg. Ein römisches Parlament setzt ihn ab und erhebt Clemens III. Der Gegenpapst krönt Heinrich IV. Der Kaiser erstürmt das Septizonium und das Kapitol. Die Römer belagern den Papst in der Engelsburg. Not Gregors. Der Normannenherzog rückt zum Entsatz heran. Abzug Heinrichs. Einnahme Roms durch Robert Guiscard. Furchtbarer Ruin der Stadt.

[4.](#rom0764) Hildeberts Klagestimme über den Fall Roms. Ruin der Stadt in der Epoche Gregors VII.

[5.](#rom0765) Abzug Gregors VII. aus Rom ins Exil. Sein Sturz. Sein Tod in Salerno. Seine Gestalt in der Weltgeschichte.

  
**Siebentes Kapitel**

[1.](#rom0771) Desiderius wird in Rom als Victor II. gewaltsam erhoben. Er flieht nach Monte Cassino. Er nimmt die Papstwürde wieder an (1087). Er wird in Rom geweiht. Zustände in der Stadt. Victor III. flieht nach Monte Cassino, wo er stirbt (1087). Wahl und Ordination Ottos von Ostia als Urban II. (1088).

[2.](#rom0772) Urban II. Clemens III. im Besitz von Rom. Urban II. wirft sich den Normannen in die Arme, die ihn nach Rom führen. Seine trostlose Lage in der Stadt. Die Vermählung Mathildes mit Welf V. Heinrich IV. kehrt nach Italien zurück (1090). Die Römer rufen Clemens III. wieder in die Stadt. Die Rebellion des jungen Konrad. Urban II. bemächtigt sich Roms.

[3.](#rom0773) Das Phänomen der Kreuzzüge. Kräftigung des Papsttums durch diese allgemeine Bewegung. Urban II. predigt das Kreuz in Piacenza und in Clermont. (1095). Verhältnis der Stadt Rom zu den Kreuzzügen und zum Rittertum. Die Normannen Italiens nehmen das Kreuz. Durchmarsch des französischen Kreuzheeres durch Rom, woraus Clemens III. vertrieben wird. Rückkehr Urbans II.

[4.](#rom0774) Verhältnis Heinrichs IV. zum ersten Kreuzzug. Der Papst stellt sich an die Spitze der Weltbewegung. Welf V. trennt sich von Mathilde. Die Welfen gehen ins Lager Heinrichs über. Heinrich IV. kehrt nach Deutschland zurück (1097). Schluß seiner tragischen Kämpfe. Urban II. stirbt (1099). Der König Konrad stirbt (1101). Heinrich IV. stirbt (1106).

[5.](#rom0775) Kultur Roms im XI. Säkulum. Guido von Arezzo erfindet die Noten. Zustand der Bibliotheken. Die Pomposa. Monte Cassino. Farfa. Gregor von Catino. Subiaco. Anfänge von Sammlungen römischer Regesten. Deusdedit. Mangelhafte Fortführung der Papstgeschichte. Die Regesten Gregors VII. Pier Damiani. Bonizo. Anselm von Lucca. Streitschriften über die Investitur.
