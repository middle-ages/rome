#### Geschichte der Stadt Rom im Mittelalter

## Dreizehntes Buch

#### Geschichte der Stadt Rom im XV. Jahrhundert

### Inhalt:

**Erstes Kapitel**

[1.](#rom1311) Das XV. Jahrhundert: Die Renaissance. Martin V., Rom und der Kirchenstaat. Colonna und Orsini. Verhältnisse Neapels. Johanna II. adoptiert Alfonso von Aragon. Dessen Kampf mit Anjou. Krieg um Aquila. Fall der großen Condottieri Braccio und Sforza 1424. Martin gewinnt viele Provinzen der Kirche wieder. Er weicht erst dem Konzil aus und beruft es dann nach Basel. Sein Tod im Jahre 1431.

[2.](#rom1312) Eugen IV. Papst im Jahre 1431. Die Wahlkapitulation. Die Orsini erheben sich gegen die Colonna. Sturz dieses Hauses durch Eugen. Beginn des Konzils in Basel im Jahre 1431. Der Kardinal Cesarini. Ausbruch des Kampfs zwischen dem Konzil und dem Papst. Sigismund in Italien. Seine lombardische Krönung. Sein Vertrag mit dem Papst. Seine Kaiserkrönung, 31. Mai 1443.

[3.](#rom1313) Fortebraccio und Sforza dringen in die Nähe Roms. Eugen unterwirft sich dem Konzil Dezember 1433. Sforza wird Vikar der Mark und Gonfaloniere der Kirche. Rom stellt die Republik wieder her. Flucht des Papsts nach Florenz Juni 1434. Anarchie in Rom. Die Republik fällt. Vitelleschi besetzt Rom Oktober 1434. Untergang der Präfekten von Vico September 1435. Francesco Orsini Stadtpräfekt. Vitelleschi unterwirft die lateinischen Barone und Palestrina. Er zieht in Rom ein. Palestrina zerstört. Furchtbarer Ruin Latiums.

[4.](#rom1314) Kampf Alfonsos um Neapel. Seeschlacht bei Ponza; Alfonsos Gefangenschaft und Freilassung August 1435. Eugen IV. anerkennt den König René in Neapel. Neuer Streit mit dem Konzil. Das Konzil in Ferrara Januar 1438. Die Union mit den Griechen. Die pragmatische Sanktion Frankreichs. Sigismund stirbt Dezember 1437. Albrecht römischer König. Das Konzil in Florenz. Die Griechen nehmen die Union an Juni 1438. Der Gegenpapst Felix V. Prinzip der Neutralität in Deutschland. Albrecht stirbt November 1439. Friedrich III. römischer König Februar 1440.

[5.](#rom1315) Vitelleschi Tyrann von Rom. Sein Sturz und Tod März 1440. Lodovico Scarampo Regent und Tyrann von Rom. Die Vitelleschi in Corneto. Piccinino bei Anghiari geschlagen Juni 1440. Verwilderung Roms. Krieg der Liga gegen Mailand. Alfonso erobert Neapel Juni 1442. Eugen erklärt Sforza in die Acht. Er verläßt Florenz, schließt mit Alfonso Vertrag und anerkennt ihn als König von Neapel 1443.

[6.](#rom1316) Rückkehr Eugens IV. September 1443. Schrecklicher Zustand der Stadt. Das Konzil im Lateran. Eugen bekriegt Sforza in den Marken. Friedrich III. verbindet sich mit dem Papst. Er wird zum Verräter an der deutschen Kirchenreformation. Die Reichsstände willigen in die Obedienzerklärung. Piccolomini geht mit den deutschen Gesandten nach Rom November 1446. Das Konkordat Deutschlands mit dem Papst. Tod Eugens IV. 23. Februar 1447.

  
**Zweites Kapitel**

[1.](#rom1321) Stefano Porcaro und die römische Demokratie. Das Konklave. Nikolaus V. Seine Vergangenheit. Erlöschen des Schisma und des Basler Konzils 1449. Die Ruhe im Kirchenstaat hergestellt. Tod des Herzogs Visconti 1447. Sforza erringt den Thron von Mailand 1450.

[2.](#rom1322) Das Jubeljahr 1450. Romfahrt Friedrichs III. Seine Vermählung mit Donna Leonora von Portugal. Die letzte Kaiserkrönung in Rom 18. März 1452. Mißachtung des Kaisertums.

[3.](#rom1323) Verschwörung und Ende des Stefano Porcaro 1453. Stimmung in Rom. Klage und Mißtrauen des Papsts Nikolaus' V. Eroberung von Konstantinopel durch den Sultan Mohammed II. Aufrufe zum Türkenkrieg. Italienischer Friede zu Lodi 1454. Abschied Nikolaus' V. von der Welt und sein Tod.

[4.](#rom1324) Calixt III. Papst 1455. Tumulte der Orsini und des Grafen Eversus. Rüstungen zum Türkenkrieg. Der Kardinal Scarampo Admiral. Alfonso von Neapel stirbt. Don Ferrante wird König 1458. Calixt verweigert ihm die Investitur. Nepotismus. Die Borgia am päpstlichen Hof: die Kardinäle Don Luis de Mila und Roderich Borgia. Don Pedro Luis Stadtpräfekt. Calixt III. stirbt 1458. Erster Sturz der Borgia.

[5.](#rom1325) Aeneas Sylvius Piccolomini. Seine bisherige Laufbahn. Konklave. Pius II. Papst 1458. Täuschung der Humanisten. Selbstverurteilung des Papsts in bezug auf seine Vergangenheit. Sein Plan zur Wiedereroberung Konstantinopels. Er beruft den Kongreß der Fürsten nach Mantua.

  
**Drittes Kapitel**

[1.](#rom1331) Pius II. geht nach Mantua Januar 1459. Kongreß zu Mantua. Bulle Execrabilis 18. Januar 1460. Gregor von Heimburg. Johann von Anjou Prätendent in Neapel. Tumulte in Rom. Die Tiburtianer. Rückkehr des Papsts 7. Oktober 1460. Vernichtung des Tiburtius. Krieg gegen die römischen Barone und Piccinino. Krieg gegen Malatesta. Anjou in Neapel überwunden. Nepotismus Pius' II. Die Piccolomini. Sturz des Malatesta 1463.

[2.](#rom1332) Fall Athens im Jahre 1458. Pius II. ermahnt den Sultan, Christ zu werden. Die letzten Paläologen. Der Despot Thomas bringt das Haupt des Apostels Andreas nach Italien. Feierlicher Einzug dieser Reliquie in Rom April 1462. Johannes de Castro entdeckt die Alaunlager von Tolfa. Beschluß Pius' II., sich an die Spitze des Kreuzzugs gegen die Türken zu stellen. Kreuzzugsbulle vom 22. Oktober 1463. Reise des Papsts nach Ancona. Pius II. stirbt daselbst 15. August 1464.

[3.](#rom1333) Paul II. Papst 27. August 1464. Er stößt die Wahlkapitulation um. Seine Eitelkeit und Prachtliebe. Tod Scarampos. Paul setzt die Abbreviatoren ab. Die Römer gewinnt er durch Brot und Spiele. Der Karneval. Revision der römischen Gemeindestatuten im Jahre 1469. Tod des Grafen Eversus und Sturz des Hauses Anguillara Juni 1465. Sturz der Malatesta im Jahre 1468. Robert Malatesta bemächtigt sich Riminis. Friedrich III. in Rom Weihnachten 1468. Krieg um Rimini. Erneuerung der Liga von Lodi 22. Dezember 1470. Borso erster Herzog von Ferrara April 1471. Paul II. stirbt 26. Juli 1471.

[4.](#rom1334) Sixtus IV. Papst 25. August 1471. Tod Bessarions. Borgia Legat in Spanien. Caraffa Admiral im Türkenkrieg. Nepotismus. Pietro Riario. Julian Rovere. Lionardo Rovere. Schwelgerei des Kardinalnepoten Riario. Seine Feste für Leonora von Aragon. Tod dieses Kardinals. Der Nepot Girolamo Riario steigt zu fürstlicher Größe auf. Giovanni Rovere vermählt sich mit Johanna von Urbino.

[5.](#rom1335) Das Jubeljahr 1475. Ermordung des Herzogs Galeazzo in Mailand Dezember 1476. Die Verschwörung der Pazzi. Ermordung Julian Medicis April 1478. Sixtus IV. bannt Florenz. Liga italienischer Mächte und Frankreichs wider den Papst. Krieg gegen die florentinische Republik. Girolamo Riario wird Herr von Forli 1480. Die Türken erobern Otranto. Tod Mohammeds II. Mai 1481. Die Türken verlassen Otranto. Carlotta von Cypern. Cypern venetianisch.

[6.](#rom1336) Girolamo Riario strebt nach dem Besitz der Romagna. Venedig bekriegt Ferrata im Bunde mit dem Papst im Jahre 1482. Orsini und Colonna. Geschlechterfehden in Rom. Sixtus IV. im Kampf mit Neapel. Schlacht bei Campo Morto August 1482. Tod Robert Malatestas in Rom. Tod Federigos von Urbino 1482. Der Papst schließt Frieden mit Mailand. Er wendet sich von Venedig ab. Neuer Streit zwischen Colonna und Orsini. Hinrichtung des Protonotars Lorenzo Colonna 1484. Virginius Orsini und Girolamo Riario bestürmen die Burgen der Colonna. Sixtus IV. stirbt 12. August 1484.

  
**Viertes Kapitel**

[1.](#rom1341) Unruhen in Rom. Girolamo, die Orsini und Colonna kommen in die Stadt. Abzug Riarios. Innocenz VIII. Cibò Papst 29. August 1484. Seine Kinder. Verschwörung der Barone in Neapel. Robert Sanseverino päpstlicher Generalkapitän. Krieg mit Neapel. Friede August 1486. Anarchischer Zustand in Rom. Käuflichkeit der Justiz. Franceschetto Cibò mit Maddalena Medici vermählt. Ermordung Girolamo Riarios in Forli April 1488. Caterina Sforza. Die Nepoten Cibò.

[2.](#rom1342) Kardinalsernennung. Schicksale des Sultan Djem. Die Rhodiser liefern ihn dem Papst aus. Sein Einzug in Rom März 1480. Er residiert im Vatikan. Fall Granadas Januar 1492. Feste in Rom. Der Kardinal Medici zieht in Rom ein März 1492. Lorenzo Medici stirbt April 1492. Die heilige Lanzenspitze wird nach Rom gebracht. Familienverbindung zwischen Neapel und dem Papst. Innocenz VIII. stirbt 25. Juli 1492. Franceschetto Cibò verkauft Anguillara den Orsini.

[3.](#rom1343) Die Kandidaten des Papsttums. Julian Rovere. Ascanio Sforza. Rodrigo Borgia erkauft die Papstwahl. Papst Alexander VI. 11. August 1492. Seine Vergangenheit. Seine Geliebte Vanozza, seine Kinder. Das Krönungsfest am 26. August.

[4.](#rom1344) Beginn Alexanders VI. Nepotismus. Cesare Borgia. Lucrezia Borgia. Spannung zu Neapel. Lodovico Sforza strebt nach dem Herzogtum Mailand. Columbus entdeckt Amerika. Lucrezia Borgia vermählt mit Johann Sforza von Pesaro. Lodovico Sforza fordert Karl VIII. zur Unternehmung gegen Neapel auf. Ferrante sucht diese zu hindern. Er versöhnt die Orsini und Kardinal Julian mit dem Papst. Jofré Borgia und Sancía von Aragon. Kardinalsernennung im September 1493. Cesare Borgia. Alessandro Farnese. Julia Farnese. Julian Cesarini. Hippolyt von Este.

[5.](#rom1345) Friedrich III. stirbt 19. August 1493. Maximilian römischer König. Ferrante stirbt. Alfonso II. vom Papst anerkannt April 1494. Flucht des Kardinals Julian nach Frankreich. Ostia ergibt sich dem Papst. Karl VIII. rüstet den italienischen Feldzug. Alfonso II. und der Papst in Vicovaro Juli 1494. Aufbruch Karls VIII. August 1494. Erste Siege. Mutlosigkeit Alfonsos. Seine und des Papsts Verbindungen mit den Türken. Gian Galeazzo stirbt. Lodovico Herzog von Mailand. Die Colonna nehmen Ostia. Karl VIII. in Pisa und in Florenz. Die Orsini öffnen ihm ihre Burgen. Der Papst unterhandelt. Einzug Karls VIII. in Rom 31. Dezember 1494.

[6.](#rom1346) Karl VIII. unterhandelt mit dem Papst. Vertrag vom 15. Januar 1495. Abzug des Königs. Flucht des Kardinals Cesare. Abdankung Alfonsos, Erhebung Ferdinands II. Karl VIII. in Neapel. Tod Diems. Liga wider Karl März 1495. Dessen Rückzug aus Neapel. Flucht des Papsts nach Orvieto. Karl VIII. in Rom. Sein Sieg am Taro 6. Juli 1495. Seine Rückkehr nach Frankreich. Rückkehr Alexanders VI. nach Rom. Untergang der französischen Armee in Neapel. Tiberüberschwemmung Dezember 1495.

  
**Fünftes Kapitel**

[1.](#rom1351) Lage Italiens nach dem Zuge Karls VIII. Maximilians mißglückter Feldzug gegen Florenz. Alexander VI. beginnt den Kampf mit den Tyrannen im Kirchenstaat. Krieg mit den Orsini. Die Päpstlichen bei Soriano geschlagen Januar 1497. Friede mit den Orsini. Tod des Virginius Orsini. Consalvo erobert Ostia. Giovanni Sforza entflieht aus Rom. Juan von Gandia wird Herzog von Benevent. Seine Ermordung 14. Juni 1497. Eindruck dieses Ereignisses auf den Papst. Untersuchung über den Mörder. Cesare Borgia geht als Legat nach Neapel und krönt Federigo. Er kehrt nach Rom zurück.

[2.](#rom1352) Entsittlichung des Papsttums. Ferrari. Floridus. Savonarola. Karl VIII. † April 1498. Ludwig XII. Krieg und Friede zwischen Colonna und Orsini. Der Papst mit Ludwig XII. verbündet. Lucrezia mit Don Alfonso von Bisceglie vermählt. Cesare geht nach Frankreich und wird Herzog von Valence. Er vermählt sich mit Charlotte d'Albret. Kriegszug Ludwigs XII. Er erobert Mailand. Lucrezia Regentin von Spoleto. Der Papst vernichtet die Gaëtani. Cesare in der Romagna. Fall Imolas 1499.

[3.](#rom1353) Das Jubeljahr 1500. Cesare erobert Sinigaglia. Schicksal der Caterina Sforza Riario. Plötzliche Restauration Lodovicos in Mailand. Cesare zieht in Rom ein. Fall des Hauses Sforza in Mailand. Schreckliche Zustände in Rom. Lebensgefahr des Papsts. Cesare ermordet Don Alfonso. Kardinalsernennungen. Cesare erobert Faenza April 1501. Astorre Manfredi in der Engelsburg. Cesare wird Herzog der Romagna. Seine Unternehmungen gegen Bologna und Florenz. Vertrag der Teilung Neapels zwischen Spanien und Frankreich. Untergang der neapolitanischen Dynastie Aragon 1501.

[4.](#rom1354) Alexander bemächtigt sich der Länder der Colonna. Lucrezia Regentin im Vatikan; Gemahlin Alfonsos von Este. Piombino ergibt sich Cesare. Alexander teilt die Güter der lateinischen Barone unter zwei Kinder Borgia. Vermählung Lucrezias mit dem Erbprinzen von Ferrara und ihre Abreise dorthin Januar 1502. Cesare Tyrann in Rom. Der Papst schifft mit ihm nach Piombino. Astorre Manfredi wird ermordet. Cesare überwältigt Urbino und Camerino. Sein gutes Regiment in der Romagna. Vergiftung des Kardinals Ferrari. Libell gegen den Papst.

[5.](#rom1355) Ludwig XII. in Oberitalien. Die Feinde der Borgia und Cesare eilen zu ihm. Abfall seiner Condottieri. Er überlistet sie. Der Papst setzt den Kardinal Orsini fest. Cesare in Umbrien. Die Kapitäne Orsini hingerichtet. Cesare vor Siena. Aufstand der lateinischen Barone. Cesare im Patrimonium. Der Kardinal Orsini vergiftet. Cesare in Rom. Caere kapituliert. Johann Jordan schließt Vertrag. Der Kardinal Michiel vergiftet. Spannung Frankreichs mit dem Papst. Consalvo vernichtet die Franzosen in Neapel. Unterhandlung der Borgia mit Spanien. Sturz Trochios. Kardinalsernennung. Die französische Armee bricht gegen Neapel auf. Ende Alexanders VI. August 1503.

  
**Sechstes Kapitel**

[1.](#rom1361) Die Renaissance im XV. Jahrhundert. Verhältnis der Stadt Rom zu ihr. Wirksamkeit der Päpste. Die Entdeckung der alten Autoren. Nikolaus V. Die Vatikanische Bibliothek. Sixtus IV. Der Buchdruck kommt nach Rom. Die ersten deutschen Drucker in Rom. Aldus Manutius.

[2.](#rom1362) Die Humanisten, ihr Wesen und ihre Bedeutung. Lateinische Philologen. Bruni. Poggio. Filelfo. Beccadelli. Laurentius Valla. Seine Widerlegung der falschen Schenkung Constantins. Seine Wirksamkeit und Schriften. Griechische Philologie. Die byzantinischen Flüchtlinge. Chrysoloras. Georg von Trapezunt. Theodor Gaza. Johannes Argyropulos. Nicolaus Sagundinus. Bessarion. Orientalische Sprachen. Manetti. Reuchlin.

[3.](#rom1363) Anfänge der Altertumswissenschaft. Die Monumente im XV. Jahrhundert. Erwachen des Sinns für Altertümer. Beginnende Sammlungen. Anfänge des kapitolinischen Museum. Die Auffindung der antiken Mädchenleiche. Livius in Padua. Beginnende Ausgrabungen. Ostia und Portus. Das Schiff des Tiberius im Nemi-See. Pius II. als Altertumsforscher. Aufstellung von Statuen in den Palästen Roms. Auferstehung des Apollo von Belvedere.

[4.](#rom1364) Epigraphensammler. Dondi. Signorili. Cyriacus. Poggio. Petrus Sabinus. Laurentius Behaim. Flavio Biondo als Gründer der Archäologie. Pomponius Laetus. Die römische Akademie. Ihr Prozeß unter Paul II. Filippo Buonaccorsi. Pomponius und Platina. Wirksamkeit des Pomponius. Der Schriftenfälscher Annius von Viterbo. Die ersten deutschen Humanisten in Rom. Cusa. Peurbach und Regiomontanus. Johann Wessel. Gabriel Biel. Johann von Dalberg. Agricola. Rudolf Lange. Hermann Busch. Konrad Celtis. Reuchlin.

[5.](#rom1365) Die Geschichtschreibung. Flavio Biondo. Sabellicus. Pius II. Seine Denkwürdigkeiten. Ammanati. Patrizi. Fortsetzung der Papstgeschichte. Die Humanisten als Biographen der Päpste. Vespasiano. Manetti. Campanus. Canensius. Gasparo von Verona. Platina. Seine Geschichte der Päpste. Jacobus von Volterra. Burkard von Straßburg. Die römischen Tagebücher. Paul Petroni. Der Notar von Nantiportu. Infessura.

[6.](#rom1366) Die humanistische Dichtkunst. Cencio. Loschi. Maffeo Vegio. Correr. Dati. Nicolaus Valla. Gianantonio Campano. Aurelio Brandolini. Giusto de' Conti. Anfänge des Dramas. Die Mysterien und Passionsspiele. Römische Schaugepränge und szenische Aufführungen. Das Theater des Kardinals Raffael Riario. Ferdinandus Servatus. Pomponius Laetus und die Aufführung italienischer Stücke durch die Akademiker.

  
**Siebentes Kapitel**

[1.](#rom1371) Die Kunst der Renaissance. Tätigkeit Martins V., Eugens IV., Scarampos. Der Campo di Fiore. Palastbauten. S. Onofrio. S. Antonio de' Portoghesi. Hospitäler der Engländer und Deutschen. Nikolaus V. Sein Plan zum neuen Vatikan und St. Peter. Seine Restauration. S. Giacomo dei Spagnuoli. S. Salvatore in Lauro. Das Kapitol. Die Aqua Virgo. Pius II. Lariano zerstört. Die Kapelle in Vicovaro. Der Palast Orsini auf der Navona. Torquemada baut die Minerva aus. Paul II. Kirche und Palast S. Marco.

[2.](#rom1372) Bauten Sixtus' IV. Straßen. Ponte Sisto. S. Spirito. Bibliothek und Kapelle. S. Maria del Popolo; della Pace. S. Agostino. S. Pietro in Vincoli. S. Apostoli. Grottaferrata. Die Burg Ostia. Palast des Grafen Riario; des Kardinals Domenico Rovere im Borgo. Palast del Governo Vecchio. Bauten Innocenz' VIII. S. Maria in Via Lata. Fontäne auf dem Petersplatz. Belvedere. Villa Malliana. Bauten Alexanders VI. S. Maria in Monserrato. S. Trinità dei Monti. S. Rocco. S. Maria dell' Anima. Engelsburg. Via Alessandrina. Porta Settimiana. Appartamento Borgia. Sapienza. Palast Sforza-Cesarini. Paläste der Kardinäle Raffael Riario und Hadrian Castellesi.

[3.](#rom1373) Die Skulptur in Rom. Denkmäler der Frührenaissance in Kirchen. Mino und seine Schule. Die Türen Filaretes am St. Peter. Grabmal Martins V. Römisches Monumentalprinzip. Monument Eugens IV. Grabmäler Nikolaus' V., Calixts III., Pauls II., Pius' II. Die bronzenen Monumente Sixtus' IV. und Innocenz' VIII. Grabmäler von Kardinälen. Statuen. Ehrenbildsäulen. Sixtus IV. stellt den bronzenen Marc Aurel her. Büsten. Medaillen. Geschnittene Steine. Juweliere. Die Pietà Michelangelos.

[4.](#rom1374) Die Malerei in Rom. Ihr Wiederaufschwung durch fremde Künstler. Masaccio. Gentile da Fabriano. Fra Giovanni da Fiesole. Benozzo Gozzoli. Tätigkeit der Maler unter Sixtus IV. Melozzo da Forli. Die Malereien in der Sixtinischen Kapelle. Perugino. Mantegna. Filippino Lippi. Pinturicchio.

[5.](#rom1375) Gestalt der Stadt Rom um das Jahr 1500 nach ihren Regionen. Ikonographien der Stadt.
