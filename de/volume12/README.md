#### Geschichte der Stadt Rom im Mittelalter

## Zwölftes Buch

#### Geschichte der Stadt Rom vom Jahre 1355–1420

### Inhalt:

**Erstes Kapitel**

[1.](#rom1211) Florenz und Mailand. Wachsende Macht des Johann Visconti. Alle Parteien rufen Karl von Böhmen nach Italien. Seine Romfahrt. Seine Kaiserkrönung am Ostertag 1355. Sein schimpflicher Abzug aus der Stadt und aus Italien. Tiefste Herabwürdigung der Reichsgewalt. Die Goldene Bulle 1356.

[2.](#rom1212) Albornoz unterwirft den Kirchenstaat. Die Vikare. Die Rektoren. Der doppelte Adelssenat abgeschafft 1358. Johann Conti letzter Senator vom Geschlechteradel. Raimund de Tolomeis erster fremder Senator. Der Adel aus der Republik ausgeschlossen. Sieben Reformatoren der Republik 1358. Rückkehr des Albornoz aus Avignon. Der Ordelaffi unterwirft sich. Bologna kommt an die Kirche. Bernabò Visconti beansprucht diese Stadt. Hugo von Cypern Senator 1361. Genossenschaft der Armbrustschützen und Schildträger. Die Banderesi. Krieg mit Velletri. Plebejische Umwälzung unter Lello Pocadota. Innocenz VI. stirbt 1362.

[3.](#rom1213) Urban V. Papst. Krieg wider Bernabò. Rom huldigt dem Papst. Rosso de Ricci Senator 1362. Friede mit Velletri, mit Bernabò. Staatsmännische Tätigkeit des Albornoz. Revision der Statuten Roms. Fortdauer des Regiments der Reformatoren und Banderesi. Die Soldbanden. Ihre Entstehung und Einrichtung. Der Graf von Landau. Hans von Bongard. Albert Sterz. Johann von Habsburg. Johann Hawkwood. Florenz bemüht sich, eine Liga wider diese Banden zu errichten. Vertrag mit der weißen Kompanie. Bemühungen des Kaisers und Papsts zur Ausrottung der Banden. Liga von Florenz September 1366.

[4.](#rom1214) Urban V. beschließt die Rückkehr nach Rom. Widerspruch der Franzosen und der Kardinäle. Petrarcas Satiren wider Avignon. Sein Ermahnungsbrief an Urban 1366. Seine Apologie Italiens und Roms. Gründe, welche Urban bewogen, Avignon zu verlassen. Seine Romfahrt 1367. Die Flotte im Hafen Corneto. Landung Urbans. Er geht nach Viterbo. Tod des Albornoz. Tumult in Viterbo. Einzug Urbans in Rom, 16. Oktober 1367.

  
**Zweites Kapitel**

[1.](#rom1221) Petrarca beglückwünscht Urban. Frankreich und Italien. Zustand Roms. Urban schafft die Banderesi ab und setzt Konservatoren ein. Karl IV. kommt nach Italien. Er und der Papst ziehen in Rom ein. Abzug des Kaisers aus Italien. Perugia trotzt dem Papst. Der Kaiser von Byzanz in Rom. Urban verkündet seinen Entschluß, nach Avignon zurückzukehren. Bestürzung der Römer. Die heilige Birgitta in Rom. Attest des Papsts von der guten Aufführung der Römer. Einschiffung in Corneto. Urbans Tod in Avignon 1370.

[2.](#rom1222) Gregor XI. Papst 1371. Die Römer bieten ihm zögernd die Gewalt. Das städtische Regiment wird wieder energisch. Letzte Apologie Italiens von Petrarca. Birgitta stirbt 1373. Katharina von Siena. Die Nationalerhebung Italiens gegen das französische Papsttum und die französischen Rektoren. Allgemeine Empörung des Kirchenstaats. Florenz fordert das römische Volk auf, an die Spitze des Nationalkampfs um die Unabhängigkeit Italiens zu treten. Haltung der Römer.

[3.](#rom1223) Bologna empört sich. Bannbulle wider Florenz. Hawkwood plündert Faenza. Die Florentinische Liga wider den Papst. Gregor XI. beschließt die Rückkehr nach Italien, wohin der Kardinal von Genf bretonische Banden führt. Katharina als Gesandte der Florentiner in Avignon. Abzug Gregors XI. aus Avignon 1376. Die Florentiner rufen Rom auf, den Papst nicht anzunehmen. Gregor XI. landet in Corneto. Er schließt Vertrag mit Rom. Er schifft sich ein und landet in Ostia. Einzug Gregors XI. in Rom am 17. Januar 1377.

  
**Drittes Kapitel**

[1.](#rom1231) Das Blutbad in Cesena. Rom widerstrebt der päpstlichen Herrschaft. Verschwörung des Adels. Gomez Albornoz Senator. Gregor XI. in Anagni. Bologna kehrt zur Kirche zurück. Unterhandlungen mit Florenz. Friede zwischen Rom und dem Präfekten. Kongreß zu Sarzana. Trostlose Lage Gregors XI. Er legt sich zum Sterben. Vorgängige Beratungen über das Konklave. Die französischen und die italienischen Kardinäle. Die Vorstellungen der Römer. Gregor XI. stirbt 1378.

[2.](#rom1232) Die Römer fordern einen Römer oder Italiener zum Papst. Das Konklave. Wahl des Erzbischofs von Bari. Der Scheinpapst. Flucht der Kardinäle. Urban VI. als Papst anerkannt. Beleidigung der Kardinäle durch Urban. Beginnende Spaltung. Johanna von Neapel und Otto von Braunschweig. Die Ultramontanen gehen nach Anagni. Honoratus von Fundi. Urban VI. in Tivoli. Das Gefecht am Ponte Salaro. Manifest der französischen Kardinäle gegen Urban. Vermittlung der drei italienischen Kardinäle. Enzyklika der Ultramontanen. Sie wählen Clemens VII. Urban VI. verlassen in Rom. Die heilige Katharina. Wahl neuer Kardinäle in Rom. Bannbulle.

[3.](#rom1233) Die Kirchenspaltung. Die beiden Päpste. Die Länder, welche ihnen anhängen. Karl IV. stirbt 1378. Wenzel römischen König. Das Reich anerkennt Urban VI. Die Engelsburg hält sich für Clemens VII. Alberigo von Barbiano siegt über die Bretonen bei Marino. Die Engelsburg fällt und wird von den Römern zerstört. Urban VI. im Vatikan. Clemens VII. flieht nach Avignon. Prozeß Urbans wider Johanna. Er stellt Karl von Durazzo als Prätendenten Neapels auf. Ludwig von Anjou Gegenprätendent. Urban VI. Herr in Rom. Katharina stirbt 1380. Ihre Verehrung in Rom. Sie wird im Jahre 1866 von Pius IX. zur Schutzpatronin der Stadt erklärt.

[4.](#rom1234) Energisches Regiment Urbans VI. in Rom. Karl von Durazzo Senator und König Neapels. Ludwig von Anjou Gegenkönig. Tragisches Ende Johannas I. Urban VI. in Neapel. Sein Mißverhältnis zu Karl. Urban in Nocera. Verschwörung und grausame Behandlung einiger Kardinäle. Urban in Nocera belagert. Seine Flucht. Urban VI. in Genua. Er läßt die Kardinäle ermorden. Er geht nach Lucca. Ende Karls von Durazzo. Urban geht nach Rom. Fall des Francesco von Vico. Aufstand der Banderesi. Urban VI. stirbt 1389.

  
**Viertes Kapitel**

[1.](#rom1241) Bonifatius IX. Papst 1389. Ladislaus König von Neapel. Das Jubiläum von 1390. Mißbrauch mit den Indulgenzen. Habsucht Bonifatius' IX. Der Kirchenstaat löst sich in Vikariate auf. Vertrag des Papsts mit Rom. Unruhen. Bonifatius geht nach Perugia und Assisi. Er schließt Vertrag mit Rom, wohin er zurückkehrt 1393. Widerstand der Banderesi gegen das päpstliche Regiment. Clemens VII. stirbt. Benedikt XIII. Papst in Avignon 1394. Verschwörungen in Rom. Sturz der Banderesi und der Freiheit Roms durch Bonifatius IX. 1398. Er befestigt die Engelsburg und das Kapitol.

[2.](#rom1242) Jubiläum der Stadt 1400. Geißler-Kompanien. Krieg gegen den Stadtpräfekten. Die Nepoten. Ladislaus erobert Neapel. Ende des Honoratus von Fundi. Bonifatius IX. Herr des Kirchenstaats. Versuche der Colonna auf Rom und ihre Unterwerfung. Viterbo unterwirft sich. Versuche zur Beilegung des Schisma. Untätigkeit des Königs Wenzel. Gian Galeazzo erster Herzog von Mailand. Wenzel wird abgesetzt. Ruprecht König der Römer 1401. Sein ruhmloses Auftreten in Italien. Gian Galeazzo stirbt. Bologna und Perugia kommen wieder an die Kirche. Tod Bonifatius' IX. 1404.

[3.](#rom1243) Tumulte in Rom. Kampf der Colonna mit den Orsini. Innocenz VII. Papst 1404. Die Römer fordern von ihm die Entsagung der weltlichen Gewalt. Ladislaus kommt nach Rom. Die Oktoberkonstitution 1404. Ladislaus kehrt nach Neapel heim. Die Römer bedrängen den Papst. Er ernennt fünf Römer zu Kardinälen. Ermordung der Volksabgeordneten durch Lodovico Migliorati. Vertreibung der Kurie nach Viterbo. Anarchie in Rom. Die Neapolitaner rücken in den Vatikan. Das Volk bekämpft sie. Paul Orsini vertreibt sie. Innocenz VII. kehrt nach Rom zurück 1406. Er schließt Frieden mit Ladislaus. Er stirbt 1406.

  
**Fünftes Kapitel**

[1.](#rom1251) Gregor XII. Unterhandlungen wegen der Union. Verderbnis der Kirche. Der Kongreß zu Savona wird beschlossen. Nikolaus von Clemange. Hindernisse der Union. Die Colonna dringen in Rom ein. Paul Orsini schlägt sie heraus. Er wird mächtig in der Stadt. Gregor XII. geht nach Siena. Ladislaus zieht in Rom ein 1408. Er unterwirft sich die Provinzen der Kirche und regiert als Herr in Rom.

[2.](#rom1252) Plan Benedikts XIII., sich Roms zu bemächtigen. Gregor XII. und Ladislaus. Ränke beider Päpste, die Union zu vereiteln. Benedikt XIII. wird von Frankreich, Gregor XII. von seinen Kardinälen verlassen. Die Kardinäle beider Obedienzen in Pisa. Sie schreiben ein Konzil aus. Balthasar Cossa in Bologna. Gregor XII. geht nach Rimini. Ladislaus zieht nach Toskana, das Konzil zu hindern. Die Florentiner widerstehen ihm. Das Pisaner Konzil 1409. Absetzung der Päpste. Alexander V. Die drei Päpste. Zug Ludwigs von Anjou und Cossas gegen Ladislaus. Die Neapolitaner verteidigen Rom. Revolution in Rom. Die Stadt huldigt Alexander V.

[3.](#rom1253) Alexander V. in Bologna. Die Römer bieten ihm die Gewalt. Er bestätigt ihre Autonomie. Er stirbt 1410. Johann XXIII. Papst. Seine Vergangenheit. König Ruprecht stirbt. Sigismund König der Römer 1411. Johann XXIII. und Ludwig von Anjou ziehen in Rom ein. Expedition gegen Ladislaus von Neapel. Ihr erster Erfolg, ihr kläglicher Ausgang. Bologna rebelliert. Sforza d'Attendolo. Der Papst schließt Frieden mit Ladislaus. Gregor XII. flieht nach Rimini.

  
**Sechstes Kapitel**

[1.](#rom1261) Johann XXIII. und die Synode in Rom. Sigismund in Italien. Johann XXIII. sagt das Konzil an. Ladislaus erscheint vor Rom. Die Neapolitaner dringen in die Stadt. Flucht und Verfolgung Johanns. Ladislaus Herr von Rom 1413. Plünderung Roms. Ladislaus besetzt den Kirchenstaat. Johann XXIII. in Florenz. Konstanz als Ort des Konzils gewählt. Zusammenkunft des Papsts und Königs der Römer in Lodi. Das Konzil wird nach Konstanz ausgeschrieben. Johann XXIII. kehrt nach Bologna zurück.

[2.](#rom1262) Ladislaus rückt über Rom nach Tuszien. Die Florentiner widersetzen sich seinem Vordringen. Er kehrt um. Er wird sterbend nach St. Paul getragen. Er stirbt in Neapel. Johanna II. Königin. Rom vertreibt die Neapolitaner. Sforza dringt in Rom ein und zieht wieder ab. Pietro di Matuzzo Haupt des römischen Volks. Rom unterwirft sich dem Kardinal Isolani. Johann XXIII. reist nach Konstanz. Das Konzil. Schicksale der drei Päpste. Wahl Martins V. Die Familie Colonna. Krönung Martins V. 1417.

[3.](#rom1263) Zustände in Rom. Isolani und die Neapolitaner. Braccio wird Signor von Perugia und andern Städten des Kirchenstaats. Fall des Paul Orsini. Braccio siebzig Tage lang Herr von Rom 1417. Sforza vertreibt ihn. Martin und Johanna II. Schluß des Konzils in Konstanz. Hus. Martin V. geht nach Italien. Ende des Balthasar Cossa. Vertrag Martins mit Johanna II. Vertrag mit Braccio. Bologna unterwirft sich der Kirche. Martin V. zieht in Rom ein am 29. September 1420.

  
**Siebentes Kapitel**

[1.](#rom1271) Die Kultur im XIV. Jahrhundert. Das klassische Heidentum wird in den Prozeß der Bildung aufgenommen. Dante und Virgil. Petrarca und Cicero. Florenz und Rom.

[2.](#rom1272) Unkultur Roms im XIV. Jahrhundert. Zustand der römischen Universität. Ihre Wiederherstellung durch Innocenz VII. Chrysoloras. Poggio. Lionardo Aretino. Die Colonna. Cola di Rienzo. Cavallini de Cerronibus. Anfänge der römischen Altertumswissenschaft. Niccolò Signorili. Cyriacus. Poggio. Römische Geschichtschreibung. Anfänge von Stadtannalen. Papstgeschichte. Dietrich von Niem.

[3.](#rom1273) Verfall der Künste in Rom. Die Treppe von Aracoeli. Das Hospital am Lateran. Restauration von Basiliken. Der Lateranische Palast verfällt. Urban V. beginnt den Umbau der Lateranischen Basilika. Das gotische Tabernakel daselbst. Die Apostelhäupter. Umbau der Engelsburg durch Bonifatius IX. Der bedeckte Gang. Befestigung des Senatspalasts durch denselben Papst. Dortige Wappenschilder. Verfall der Malerei. Pietro Cavallini. Monumentale Skulptur. Grabplatten. Paulus Romanus. Monumente von Kardinälen: Philipp d'Alençon; Petrus Stefaneschi Annibaldi; Marino Vulcani.

[4.](#rom1274) Sitten und Gebräuche im XIV. Jahrhundert. Deren Umwandlung aus Einfachheit zur Üppigkeit. Florenz und Rom. Die Kleidertracht. Die Mode der Frauen. Luxusverbote. Festsinn und öffentliche Aufzüge. Das fragliche Stiergefecht im Colosseum 1332. Die Spiele am Testaccio und auf dem Platz Navona. Beschickung der öffentlichen Spiele Roms durch die Vasallenstädte. Dramatische Vorstellungen. _Ludi Paschales_ im Colosseum.

[5.](#rom1275) Petrarca und die Monumente des Altertums. Deren Zerstörung. Klage des Chrysoloras. Die öffentlichen Bildsäulen in Rom. Auffindung der Gruppe des Nil. Petrarcas Aufzählung der antiken Bauwerke. Uberti. Poggios Bericht von Rom. Tempel. Portiken. Theater. Circus. Fora. Thermen. Wasserleitungen. Triumphbogen. Säulen. Mausoleen. Brücken. Mauern. Tore. Hügel. Gesamtbild Roms. Die dreizehn Regionen, ihre Namen und Wappenzeichen. Neue und alte Straßen. Häuserbau. Das römische Säulenhaus im Mittelalter. Gotik im XIV. Jahrhundert. Einwohnerzahl Roms. Verödung der Campagna.
