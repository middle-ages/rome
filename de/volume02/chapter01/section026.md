_3\. Die Titularbasiliken der Stadt Rom um das Jahr 499._

Es waren dies folgende Kirchen:

  
1\. Titulus Praxidae.

Die Basilika auf dem Clivus Suburanus der Esquilien, der Schwester der Pudentiana geweiht.

  
2\. Vestinae.

Die Kirche ist heute S. Vitale, im Tal des Quirinal; sie war schon von Innocenz I. (zwischen 401 und 417) nach dem Testament der Römerin Vestina, dem heiligen Vitalis und dessen Söhnen Gervasius und Protasius geweiht worden.

  
3\. St. Caeciliae.

Die schöne Kirche in Trastevere, welche im III. Jahrhundert vom Bischof Urban im Wohnhause der Heiligen angelegt worden sein soll.

  
4\. Pammachii.

Dies ist die Basilika St. Johann und Paul auf dem Clivus Scauri hinter dem Colosseum, über dem Hause jener beiden römischen Brüder erbaut, welche dort unter Julianus Apostata den Tod erlitten hatten. Sie kommt zuerst in jenem Konzil mit dem Namen des Pammachius vor, wohl jenes römischen Senators und Gemahls der Paulina, an welchen Hieronymus seinen Trostbrief über deren Tod geschrieben hat. Pammachius gab seine Reichtümer den Armen, wurde Mönch und stiftete jene Kirche. Erst zur Zeit Gregors des Großen wurde sie nach Johann und Paul benannt.

  
5\. St. Clementis.

Die alte, berühmte Kirche zwischen dem Colosseum und dem Lateran, welche schon zur Zeit des Damasus (366–384) bestand.

  
6\. Julii.

Die heutige S. Maria in Trastevere, die auch den Titel Calisti führte, deren Gründung jedoch dein Bischof Julius I. (337–352) angehört. Nach einer späteren Sage soll ein Ölquell, welcher dort, wo die _Taberna Meritoria_ gelegen war, zur Zeit des Augustus entsprang, die Geburt des Weltheilands angekündigt haben und dies der Grund zum Bau der Basilika gewesen sei.

  
7\. Chrysogoni.

Auch die Basilika steht in Trastevere; sie ist einem römischen Märtyrer aus der Zeit Diokletians geweiht. Ihr Erbauer ist unbekannt; sie wird zum erstenmal im Konzil des Symmachus erwähnt.

  
8\. Pudentis.

Die Basilika Pudentiana auf dem Esquilin, die älteste Titelkirche Roms, auch St. Pastor genannt. Ihr ursprünglicher Name war _Pudentis_ oder _Ecclesia Pudentiana_, von Pudens, jenem Senator, der sie in seinem Hause gestiftet hatte.

  
9\. St. Sabinae.

Die schönste und größte Kirche auf dem Aventin wurde entweder unter Coelestin I. oder Sixtus III. in der ersten Hälfte des V. Jahrhunderts erbaut und der Römerin Sabina geweiht, welche unter Hadrian den Martertod erlitten haben soll. Ihr Stifter war der Presbyter Petrus von Illyrien, wie es die musivische Inschrift über der Haupttüre sagt. Die herrlichen Säulen dieser Kirche hat einer der aventinischen Tempel, vielleicht der berühmte der Diana, hergegeben.

  
10\. Equitii.

Es ist die Kirche _S. Martino in Montibus_ auf den Carinen neben den Thermen des Trajan, wo der Papst Silvester im Hause eines Presbyters Equitius sie erbaut haben soll. Daher hieß sie auch Titulus Silvestri, mit dem Zusatz » _ad Orphea«_, vielleicht von einem alten Bildwerk, welches dort stand. Symmachus baute sie neu; er weihte sie jenem Papst und dem heiligen Martin von Tours, aber erst um das Jahr 500, so daß sie im Konzil von 499 noch unter dem Titel Equitii erscheint. Von der alten Kirche Silvesters sieht man noch unter der heutigen Überreste.

  
11\. Damasi.

Die Basilika des St. Laurentius am Pompejustheater.

  
12\. Matthaei.

Eine zwischen S. Maria Maggiore und dem Lateran gelegene Kirche, die von einem antiken Palast den Zunamen _»in Merulana«_ führte. Sie ist untergegangen.

  
13\. Aemilianae oder St. Aemilianae,

wie diese Kirche noch zur Zeit Leos III. genannt wird. Sie ist nicht mehr zu bestimmen.

  
14\. Eusebii.

Die Kirche S. Eusebio steht neben den sogenannten Trophäen des Marius auf dem Esquilin. Ihr Heiliger ist ein römischer Priester, der unter Constantius für das athanasische Glaubensbekenntnis den Tod erlitt.

  
15\. Tigridae oder Tigridis.

Heute St. Sixtus auf der Via Appia innerhalb der Stadt, wo der alte Marstempel gesucht werden mag. Die Veranlassung ihres Titels ist unbekannt. Sie wurde dem Bischof Sixtus II. geweiht, der unter Decius oder Valerian auf der Via Appia enthauptet wurde und dessen Archidiaconus St. Laurentius war.

  
16\. Crescentianae.

Auch diese Basilika ist nicht mehr aufzufinden, wie ihr Titel nicht mehr zu bestimmen. Das Buch der Päpste nennt jedoch im Leben Anastasius' I. (399–401) eine Basilica Crescentiana in der zweiten Legion, in der Via Mamurtini, von der es zweifelhaft bleibt, ob sie die heutige Salita di Marforio ist.

  
17\. Nicomedis.

Eine Kirche dieses Namens wird auf der Via Nomentana erwähnt; da aber unter den Basiliken, die wir hier aufzeichnen, keine vor den Toren Roms genannt wird, so muß jene Kirche wo anders gestanden haben. Sie verfiel schon frühe; ihr Titel wurde von Gregor dem Großen auf die Basilika _St. Crucis in Hierusalem_ übertragen.

  
18\. Cyriaci.

Die untergegangene Kirche _St. Cyriaci in Thermis Diocletiani_, deren Titel Sixtus IV. auf die Kirche der Heiligen Quiricus und Julitta am heutigen Arco de' Pantani übertrug. Die alte Basilika jenes Römers, der unter Diokletian den Tod erlitt, muß im Bezirk der Bäder gestanden haben. Diese waren noch um 466, zur Zeit des Sidonius Apollinaris, im Gebrauch, aber so umfangreich, daß wohl eine Kirche in einem ihrer kleineren Räume erbaut werden konnte. Auch ein Nonnenkloster richtete sich dort ein.

  
19\. St. Susannae.

Diese Kirche hat den Zusatz _»ad duas domos«_, worunter man die Häuser des Gabinus, des Vaters der Heiligen und ihres Oheims, des Bischofs Caius, versteht. Sie stand auf dem Quirinal zwischen den Thermen Diokletians und den Gärten des Sallust, wo sie in veränderter Gestalt noch heute dauert. Schon Ambrosius nennt sie im Jahre 370. Susanna war eine römische Nationalheilige, der Legende nach vom Geschlecht Diokletians. Der brutale Maximian begehrte die junge Fürstin zum Weibe; aber sie verführte alle zu ihr gesendeten Werber zum Christentum. Die vom Kaiser befohlenen Angriffe auf ihre Keuschheit wehrte ein himmlischer Engel ab, und die goldene Bildsäule des Zeus, vor welcher zu opfern man sie zwingen wollte, zertrümmerte Susanna mit dem bloßen Hauche ihres Mundes. Diokletian ließ sie enthaupten, doch seine eigene Gemahlin Serena, eine heimliche Christin, begrub die Tote in einem silbernen Sarg in den Katakomben des Calixtus.

Neben der Kirche St. Susanna bestand der Titulus Caii, von dem es ungewiß ist, ob er mit jenem vereinigt war.

  
20\. Romani.

Diese Kirche ging spurlos unter. Eine Basilika desselben römischen Märtyrers wird vor dem Salarischen Tor, im Ager Veranus neben S. Lorenzo, erwähnt.

  
21\. Vitantii oder Byzantis.

Auch dieser Titel ist ungewiß.

  
22\. Anastasiae.

Die alte Basilika der St. Anastasia, welche schon am Ende des IV. Jahrhunderts bestand, heißt sub Palatio wegen ihrer Lage unter dem Palatin. Ihr Gründer ist unbekannt. Auch Anastasia ist eine römische Nationalheilige. Die Legende nennt sie die Tochter des Chrysogonus, dem sie nach Aquileja gefolgt war. Sie wurde unter Diokletian zuerst auf die Insel Palmaria exiliert, dann in Rom verbrannt.

  
23\. Sanctorum Apostolorum.

Da die heutige Kirche der Apostel an den Thermen Constantins, in der Region Via Lata, erst vom Papst Pelagius I. um 560 gebaut wurde, so ist es fraglich, ob jener Titel zur Zeit des Symmachus schon hier oder an einer andern Stelle zu suchen sei. Die Angabe, schon Constantin habe den Aposteln die Kirche in Rom erbaut, ist ganz unbegründet.

  
24\. Fasciolae.

Eine alte Basilika auf der Via Appia gegenüber S. Sisto. Heute den heiligen Eunuchen Nereus und Achilleus, angeblichen Schülern des St. Petrus, geweiht, erinnert sie durch diese Namen an die untergegangene Mythologie des Altertums. Der Titel Fasciola läßt sich nicht mehr sicher erklären.

  
25\. St. Priscae.

Diese altertümliche Kirche auf dem Aventin ist irrtümlich für das Haus des Aquila und seines Weibes Priscilla gehalten worden, und in ihm soll Petrus gewohnt und aus der Quelle des Faunus Proselyten getauft haben. Jene beiden Heiligen, die treuen Gastfreunde des Apostels, waren die ältesten uns bekannten Mitglieder der römischen Gemeinde; sie wurden unter Claudius durch das Verfolgungsedikt gegen die Juden aus Rom vertrieben und scheinen in Asien gestorben zu sein. Wann die Kirche St. Prisca auf dem Aventin entstand, ist nicht zu ermitteln, doch wahrscheinlich, daß sie eine der ältesten Roms und mit der Pudentiana gleichzeitig ist.

  
26\. St. Marcelli.

Der Bischof Marcellus weihte der Tradition nach das Haus einer Römerin Lucina auf der Via Lata zur Basilika. Er selbst soll dort unter wilden Tieren den Martertod erlitten haben. Es ist derselbe Bischof, welchem die Errichtung von 25 Titeln zugeschrieben wird.

  
27\. Lucinae.

Die berühmte Kirche des St. Laurentius in Lucina, an der Sonnenuhr des Augustus.

  
28\. St. Marci.

Die Kirche des Evangelisten Marcus, in der Via Lata unter dem Kapitol und in der Nähe des Circus Flaminius, soll schon vom Papst Marcus um 336 erbaut worden sein. Der dortige Ort hieß ad Pallacinas von antiken Bädern dieses Namens.
