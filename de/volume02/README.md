#### Geschichte der Stadt Rom im Mittelalter

## Zweites Buch

#### Vom Beginn der Herrschaft des Königs Odoaker bis zur Einrichtung ds Exarchats in Ravenna im Jahre 568

### Inhalt:

**Erstes Kapitel**

[1.](#rom0211) Regierung Odoakers. Simplicius Papst (468-483). Bau neuer Kirchen in Rom. S. Stefano Rotondo. S. Bibiana. Odoaker gebietet die Wahl Felix' III. Theoderich zieht mit den Ostgoten nach Italien. Sturz der Herrschaft Odoakers. Theoderich wird König von Italien 491.

[2.](#rom0212) Streit in Rom um das heidnische Fest der Luperkalien und dessen Ende. Schisma wegen der Wahl des Symmachus oder des Laurentius. Synode des Symmachus vom Jahre 499.

[3.](#rom0213) Die Titularbasiliken der Stadt Rom um das Jahr 499.

[4.](#rom0214) Lokaler Charakter der römischen Heiligen jener Titelkirchen. Deren örtliche Verteilung. Die Titel zur Zeit Gregors des Großen um das Jahr 594. Begriff der Titel. Die Kardinäle. Die »Sieben Kirchen« Roms.

  
**Zweites Kapitel**

[1.](#rom0221) Stellung Theoderichs zu den Römern. Seine Ankunft in Rom im Jahre 500. Seine Rede vor dem Volk. Der Abt Fulgentius. Die Reskripte beim Cassidor. Zustand der Monumente. Sorge Theoderichs um ihre Erhaltung. Kloaken. Aquädukte. Das Theater des Pompejus. Der Palast der Pincier. Der Cäsarenpalast. Das Forum Trajans. Das Kapitol.

[2.](#rom0222) Das Amphitheater des Titus. Schauspiele und Schauspielwut der Römer. Die Tierjagden. Der Circus, seine Spiele und Faktionen.

[3.](#rom0223) Sorge Theoderichs um die Verpflegung des Volks. Roma Felix. Toleranz gegen die katholische Kirche. Die Juden in Rom. Ihre älteste Synagoge. Aufstand des Volks gegen sie.

[4.](#rom0224) Neues Schisma in der Kirche. Synodus Palmaris. Parteikämpfe in Rom. Symachus schmückt den St. Peter aus. Er baut die Rundkapelle St. Andreas; die Basilika des St. Martin, die Kirche St. Pancratius. Hormisdas Papst 514. Johannes I. Papst. Bruch Theoderichs mit der katholischen Kirche.

[5.](#rom0225) Prozeß und Hinrichtung der Senatoren Boëthius und Symmachus. Der Papst Johann übernimmt eine Gesandtschaft nach Byzanz und stirbt in Ravenna. Theoderich befiehlt die Wahl Felix' IV. Tod des Königs im Jahre 526. Darauf bezügliche Sagen.

  
**Drittes Kapitel**

[1.](#rom0231) Regentschaft Amalasunthas. Ihr Genie, ihre Pflege der Wissenschaften in Rom. Ihre versöhnliche Regierung. Wachsendes Ansehen des römischen Bischofs. Felix IV. baut S. Cosma und Damiano. Die dortigen Mosaiken. Motive des Kultus dieser Heiligen.

[2.](#rom0232) Bonifatius II. Papst 530. Schisma zwischen ihm und Dioscorus. Johannes' II. Senatskonsult gegen die simonistische Bewegung um das Papsttum. Erziehung und Tod Athalarichs. Theodahad wird Mitregent. Schicksale der Königin Amalasuntha. Justinians Pläne und Aussichten. Der abendländische Konsulat erlischt im Jahr 535.

[3.](#rom0233) Unterhandlungen Theodahads mit Byzanz. Brief des Senats an Justinian. Aufregung in Rom. Die Römer verweigern die Aufnahme gotischer Truppen. Der Papst Agapitus übernimmt eine Gesandtschaft nach Byzanz. Sein Tod. Abbruch der Friedensunterhandlungen.

[4.](#rom0234) Belisar kommt nach Italien. Fall von Neapel. Die Goten wählen Vitiges zum König. Ende Theodahads. Die Goten ziehen nach Ravenna ab. Belisar rückt in Rom ein am 9. Dezember 536.

  
**Viertes Kapitel**

[1.](#rom0241) Belisar rüstet die Verteidigung Roms. Vitiges rückt mit dem Heerbann der Goten gegen die Stadt. Erster Sturm. Anstalten zur Belagerung. Die gotischen Schanzen. Gegenanstalten Belisars. Vitiges läßt die Wasserleitungen zerbrechen. Schwimmende Tibermühlen. Verzweiflung der Römer. Aufforderung der Goten zur Übergabe. Anstalten zum Sturm.

[2.](#rom0242) Allgemeiner Sturm. Angriff auf das Pränestische Tor. Murus Ruptus. Sturm auf das Grabmal Hadrians. Zerstörung seiner Statuen durch die Griechen. Fehlschlagen des Sturms auf allen Punkten.

[3.](#rom0243) Fortsetzung der Belagerung. Prophezeiungen über den Ausgang des Krieges. Heidnische Reminiszenzen. Der Janustempel. Die Tria Fata. Zwei lateinische Lieder jener Epoche. Belisars Sorgfalt in der Bewachung Roms.

[4.](#rom0244) Der Papst Silverius wird ins Exil geführt. Hungersnot in Rom. Menschlichkeit der Goten. Vitiges besetzt den römischen Hafen. Portus und Ostia. Eintreffen von Verstärkungen in Rom. Die Goten schlagen einen Ausfall zurück. Steigende Not in der Stadt. Die Gotenschanze und die Hunnenschanze.

[5.](#rom0245) Not der Goten. Ihre Gesandtschaft an Belisar. Unterhandlungen. Eintreffen von Truppen und von Proviant in Rom. Waffenstillstand. Sein Bruch. Entmutigung der Goten. Ihr Abzug von Rom im März 538.

  
**Fünftes Kapitel**

[1.](#rom0251) Belisar in Ravenna. Sein schlaues Verfahren mit den Goten. Totila wird König 541. Seine schnellen Erfolge. Sein Zug nach dem Süden. Er erobert Neapel. Er schreibt an die Römer. Er bricht nach Rom auf. Er erobert Tibur. Zweite gotische Belagerung Roms im Sommer 545. Belisar kehrt nach Italien zurück. Der Hafen Portus. Das Gotenlager.

[2.](#rom0252) Der Papst Vigilius wird nach Byzanz berufen. Die Goten fangen die sizilische Getreideflotte auf. Not in Rom. Der Diaconus Pelagius geht als Gesandter in das Gotenlager. Verzweifelter Notschrei der Römer vor Bessas. Entsetzliche Zustände in der Stadt. Belisar kommt nach Portus. Verunglückter Versuch, Rom zu entsetzen. Totila zieht in Rom ein, 17. Dezember 546. Anblick der öden Stadt. Plünderung. Rusticiana. Milde Totilas.

[3.](#rom0253) Rede Totilas an die Goten. Er versammelt den Senat. Er droht, Rom zu zerstören. Brief Belisars an ihn. Sinnlose Behauptungen, daß Totila Rom zerstört habe. Die Prophezeiung Benedikts über Rom. Totila gibt die Stadt auf. Ihre Verlassenheit.

  
**Sechstes Kapitel**

[1.](#rom0261) Belisar rückt in Rom ein. Er stellt die Stadtmauern wieder her. Zweite Verteidigung Roms 547. Totila zieht nach Tibur. Johannes hebt römische Senatoren in Capua auf. Schneller Marsch Totilas nach Süditalien. Belisar verläßt Rom. Seine Denkmäler in der Stadt.

[2.](#rom0262) Belisar irrt in Süditalien umher und kehrt nach Konstantinopel zurück. Totila rückt zum drittenmal vor Rom 549. Zustände in der Stadt. Einzug der Goten. Die Griechen im Grabmal Hadrians. Rom wird wieder bevölkert. Die letzten Circusspiele. Totila verläßt die Stadt. Die Goten zur See. Narses übernimmt den Krieg. Ein römisches Omen. Gleichzeitige Bemerkungen über einige Monumente. Das Forum des Friedens. Myrons Kuh. Die Bildsäule Domitians. Das Schiff des Aeneas. Narses rückt an den Fuß des Apennin. Fall des Totila bei Taginas 552.

[3.](#rom0263) Teja letzter Gotenkönig. Narses nimmt Rom mit Sturm. Das Grab Hadrians kapituliert. Ruin des römischen Senats. Die gotischen Landkastelle werden genommen. Narses rückt nach Kampanien. Heldentat des Teja im Frühling 553. Kapitulation der Goten auf dem Schlachtgefilde des Vesuv. Abzug der tausend Goten unter Indulf. Rückblick auf die Gotenherrschaft in Italien. Unwissenheit der Römer über die Goten wie über die Geschichte der Ruinen Roms.

  
**Siebentes Kapitel**

[1.](#rom0271) Einfall der Horden des Buccelin und Leuthar in Italien und ihre Vernichtung. Triumph des Narses in Rom. Die Goten kapitulieren in Compsa. Zustand Roms und Italiens nach dem Kriege. Die Pragmatische Sanktion Justinians. Erhöhte Stellung des römischen Bischofs. Der Senat. Die öffentlichen Anstalten. Der Papst Vigilius stirbt. Pelagius Papst 555. Sein Reinigungseid.

[2.](#rom0272) Pelagius und Johann III. bauen die Kirche der Apostel in der Region Via Lata. Verfall der Stadt Rom. Zwei Inschriften als Denkmäler des Narses.

[3.](#rom0273) Narses fällt in Ungnade. Er geht nach Neapel und wird vom Papst Johann nach Rom zurückgeführt. Sein Tod im Jahre 567. Ansichten über die Veranlassung des Zuges der Langobarden nach Italien. Albion stiftet das Langobardenreich 568. Entstehung des Exarchats. Die griechischen Provinzen Italiens. Die Verwaltung Roms.
