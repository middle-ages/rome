#### Geschichte der Stadt Rom im Mittelalter

## Fünftes Buch

#### Die Stadt Rom in der Epoche der Karolinger bis zum Jahre 900

### Inhalt:

**Erstes Kapitel**

[1.](#rom0511) Neue Stellung der Stadt Rom zur Welt. Verhältnis des Kaisers und Papsts zu Rom. Leo reist wieder zu Karl. Ardulf von Northumberland in Rom.

[2.](#rom0512) Pippin stirbt im Jahre 810. Bernhard König von Italien. Ludwig I. wird in Aachen zum Mitkaiser der Römer gekrönt. Tod Karls des Großen. Seine weltgeschichtliche Bedeutung. Mangel der Lokalsagen von ihm in der Stadt Rom.

[3.](#rom0513) Tumulte in Rom. Bernhard wird zur Untersuchung in die Stadt geschickt. Leo III. stirbt im Jahre 816. Bauten Leos in Rom. Charakter der damaligen Architektur und Kunst. Die Titelkirchen und die namhaften Klöster Roms.

[4.](#rom0514) Stephan IV. Papst. Seine Reise zu Ludwig. Sein schneller Tod. Wahl und Ordination Paschalis' I. Das Privilegium Ludwigs.

  
**Zweites Kapitel**

[1.](#rom0521) Lothar wird Mitkaiser. Empörung und Fall des Königs Bernhard. Lothar König von Italien. Seine Krönung in Rom. Er schlägt dort sein kaiserliches Tribunal auf. Prozeß mit Farfa. Gewaltsame Hinrichtung römischer Großer. Paschalis weicht dem kaiserlichen Richterspruch aus. Sein Tod.

[2.](#rom0522) Paschalis baut die Kirchen St. Caecilia in Trastevere, S. Prassede auf dem Esquilin, S. Maria in Domnica auf dem Coelius.

[3.](#rom0523) Eugenius II. wird Papst. Lothar kommt nach Rom. Seine Konstitution vom Jahre 824. Eugenius stirbt im August 827.

[4.](#rom0524) Valentinus I. Papst. Gregor IV. Papst. Die Sarazenen dringen ins Mittelmeer. Sie stiften ihr Reich in Sizilien. Gregor IV. baut Neu-Ostia. Verfall der Monarchie Karls. Ludwig der Fromme stirbt. Lothar alleiniger Kaiser. Die Teilung von Verdun im Jahre 843.

[5.](#rom0525) Leidenschaftliche Begier nach dem Besitz von Reliquien. Die heiligen Leichen. Ihre Translationen. Charakter der Pilgerschaften jener Zeit. Gregor IV. baut die Basilika des St. Marcus neu. Er stellt die Aqua Sabatina wieder her. Er baut die päpstliche Villa Draco. Er stirbt im Jahre 844.

  
**Drittes Kapitel**

[1.](#rom0531) Sergius II. Papst. Der König Ludwig kommt nach Rom. Seine Krönung; seine Zerwürfnisse mit dem Papst und den Römern. Siconolf in Rom. Die Sarazenen überfallen und plündern St. Peter und St. Paul. Sergius II. stirbt im Jahre 847.

[2.](#rom0532) Leo IV. wird Papst. Brand im Borgo. Liga von Rom, Neapel, Amalfi und Gaëta gegen die Sarazenen. Der Seekrieg bei Ostia im Jahre 849. Leo IV. erbaut die _Civitas Leonina_. Ihre Mauern und Tore. Die Distichen auf ihren Haupttoren.

[3.](#rom0533) Leo IV. ummauert Portus und übergibt den Hafen einer Korsenkolonie. Er baut Leopolis bei Centumcellae. Civitavecchia. Er stellt Horta und Ameria her. Seine Kirchenbauten in Rom. Seine Weihgeschenke. Reichtum des Kirchenschatzes. Frascati.

[4.](#rom0534) Ludwig II. wird zum Kaiser gekrönt. Absetzung des Kardinals Anastasius. Ethelwolf und Alfred in Rom. Prozeß gegen den Magister Militum Daniel vor dem Tribunal Ludwigs II. in Rom. Leo IV. stirbt im Jahre 855. Die Fabel von der Päpstin Johanna.

  
**Viertes Kapitel**

[1.](#rom0541) Benedikt III. wird zum Papst gewählt. Tumult in Rom wegen der Papstwahl. Invasion des Kardinals Anastasius. Festigkeit der Römer gegenüber den kaiserlichen Legaten. Benedikt III. wird am 29. September 855 ordiniert. Ludwig II. alleiniger Kaiser. Freundliche Beziehung Roms zu Byzanz.

[2.](#rom0542) Nikolaus I. wird Papst. Er unterwirft sich den Erzbischof Ravennas. Das griechische Schisma des Photius bricht aus. Beziehungen Roms zu den Bulgaren. Die Gesandten des Königs Boris in Rom. Formosus geht als Missionar nach Bulgarien. Versuch Roms, dieses Land zu seiner kirchlichen Provinz zu machen. Die bulgarische Konstitution Nikolaus' I.

[3.](#rom0543) Der Streit wegen Waldrada. Nikolaus verdammt die Synode von Metz und setzt Gunther von Köln und Theutgaud von Trier ab. Ludwig II. kommt nach Rom. Exzesse seiner Truppen in der Stadt. Trotz der deutschen Erzbischöfe; Festigkeit und Sieg des Papsts.

[4.](#rom0544) Sorge Nikolaus' I. für die Stadt Rom. Er stellt die Jovia und die Trajana her. Er befestigt Ostia von neuem. Seine geringen Bauten und Weihgeschenke. Zustand der Wissenschaften. Das Schuledikt Lothars vom Jahre 825. Die Dekrete Eugens II. und Leos IV. wegen der Parochialschulen. Griechische Mönche in Rom. Die Bibliotheken. Die Codices. Die Münzen.

[5.](#rom0545) Unwissenheit in Rom. Der Liber Pontificalis des Anastasius. Seine Entstehung, sein Charakter. Übersetzungen des Anastasius aus dem Griechischen. Das Leben Gregors des Großen von Johannes Diaconus.

  
**Fünftes Kapitel**

[1.](#rom0551) Beginnende Suprematie des Papsts. Der Kirchenstaat. Die pseudoisidorischen Dekretalen. Nikolaus I. stirbt im Jahre 867. Hadrianus II. Lambert von Spoleto überfällt Rom. Die Feinde Hadrians in Rom. Frevel des Eleutherius und Anastasius und ihre Bestrafung.

[2.](#rom0552) Erneuerter Streit um Waldrada. Meineid Lothars. Sein demütigender Empfang in Rom, sein schneller Tod. Der Kaiser Ludwig in Unteritalien. Begriff des Imperium in jener Zeit. Brief Ludwigs an den Kaiser von Byzanz. Schändung des Kaisertums durch den Überfall in Benevent. Ludwig kommt nach Rom. Er wird noch einmal gekrönt. Die Römer erklären Adalgisus von Benevent zum Feind der Republik.

[3.](#rom0553) Johann VIII. Papst im Jahre 872. Tod des Kaisers Ludwig II. Die Söhne Ludwigs von Deutschland und Karl der Kahle streiten um den Besitz Italiens. Karl der Kahle Kaiser im Jahre 875. Verfall der imperatorischen Gewalt in Rom. Karl der Kahle König Italiens. Die deutsche Faktion in Rom. Exzesse des Adels. Formosus von Portus.

[4.](#rom0554) Die Sarazenen verwüsten die Campagna. Klagebriefe Johanns VIII. Liga der Sarazenen mit den süditalienischen Seestädten. Glänzende Tätigkeit Johanns VIII: er stellt eine Flotte auf, er unterhandelt mit den unteritalischen Fürsten, er besiegt die Sarazenen am Kap der Circe. Zustände in Süditalien. Johann VIII. baut Johannipolis bei St. Paul.

  
**Sechstes Kapitel**

[1.](#rom0561) Schwierige Stellung Johanns VIII. zu Lambert und zum Kaiser. Er bestätigt noch einmal die Kaiserwürde Karls des Kahlen. Die Synoden von Rom und Ravenna im Jahre 877. Dekrete Johanns wegen der Patrimonien. Die päpstlichen Kammergüter. Fruchtlose Versuche, das Lehnswesen abzuwehren. Tod Karls des Kahlen. Triumph der deutschen Partei. Drohende Haltung Lamberts und der Exilierten. Überfall Roms durch Lambert und Gefangennahme des Papsts. Johann VIII. flieht nach Frankreich.

[2.](#rom0562) Johann auf der Synode von Troyes. Der Herzog Boso wird sein Günstling. Er begleitet ihn nach der Lombardei. Seine Pläne scheitern. Karl der Dicke wird König Italiens, auch in Rom zum Kaiser gekrönt, im Jahre 881. Ende Johanns VIII. Seine kühnen Entwürfe. Sein Charakter.

[3.](#rom0563) Marinus I. Papst. Er stellt Formosus wieder her. Er stürzt Guido von Spoleto. Hadrian III. Papst 884. Die ihm fälschlich zugeschriebenen Dekrete. Stephanus V. Papst. Gebrauch, nach dem Tode eines Papsts das Patriarchium zu plündern. Luxus der Bischöfe. Hungersnot in Rom. Absetzung und Tod Karls des Dicken. Ende des karolingischen Kaisertums. Kampf Berengars und Guidos um die Krone. Guido erneuert das fränkische Kaisertum. Tod Stephans V.

  
**Siebentes Kapitel**

[1.](#rom0571) Formosus Papst 891. Die Faktion Arnulfs und die Faktion Guidos. Der Gegenkandidat Sergius. Formosus fordert Arnulf zum Römerzuge auf. Arnulf in Italien. Guido stirbt. Lambert Kaiser. Arnulf zieht nach Rom. Er nimmt die Stadt mit Sturm. Er wird zum Kaiser gekrönt im April 896. Die Römer schwören ihm Treue. Seine unglückliche Rückkehr. Tod des Formosus im Mai 896.

[2.](#rom0572) Verwirrung in Rom. Bonifatius VI. Papst. Stephanus VI. Papst. Die Leichensynode; das Totengericht über Formosus. Die Basilika des Lateran stürzt ein. Ursachen jenes empörenden Frevels. Der Libell des Auxilius. Die Invektive gegen Rom. Schreckliches Ende des Papsts Stephanus VI.

[3.](#rom0573) Romanus Papst. Theodorus II. Papst. Nach dessen Tode sucht Sergius sich des Papsttums zu bemächtigen und wird vertrieben. Johannes IX. Papst 898. Sein Dekret wegen der Konsekration des Papsts. Seine Bemühung, das Kaisertum Lamberts zu bekräftigen. Tod Lamberts. Berengar König Italiens. Die Ungarn in Italien. Ludwig von der Provence Prätendent. Tod Johanns IX. im Juli 900.
