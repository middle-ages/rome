#### Geschichte der Stadt Rom im Mittelalter

## Achtes Buch

#### Geschichte der Stadt Rom im XII. Jahrhundert

### Inhalt:

**Erstes Kapitel**

[1.](#rom0811) Paschalis II. Tod Wiberts. Neue Gegenpäpste. Der rebellische Adel. Ursprung des Geschlechts Colonna. Aufstand der Corsi. Maginolf Gegenpapst. Graf Werner von Ancona zieht nach Rom. Unterhandlungen Paschalis' II. mit Heinrich V. Konzil in Guastalla. Der Papst reist nach Frankreich. Neue Empörung des Kirchenstaats.

[2.](#rom0812) Romfahrt Heinrichs V. Hilflose Lage Paschalis' II. Schwierigkeit der Lösung des Investiturstreits. Der Papst faßt den Entschluß, die Bischöfe zur Rückgabe ihrer Krongüter zu zwingen. Unterhandlungen und Verträge. Einzug Heinrichs V. in die Leonina und sein kühner Staatsstreich.

[3.](#rom0813) Die Römer erheben sich, Paschalis zu befreien. Überfall und Schlacht in der Leonina. Heinrich V. zieht mit den Gefangenen ab. Er lagert bei Tivoli. Er erzwingt vom Papst das Privilegium der Investitur. Kaiserkrönung. Heinrich V. zieht von Rom ab. Schreckliches Erwachen Paschalis' II. im Lateran.

[4.](#rom0814) Die Bischöfe erheben sich gegen Paschalis II. Ein Konzil im Lateran kassiert das Privilegium. Die Legaten bannen den Kaiser. Alexius Komnenus und die Römer. Belehnung des Normannenherzogs Wilhelm. Tod der Gräfin Mathilde. Die mathildische Schenkung.

  
**Zweites Kapitel**

[1.](#rom0821) Paschalis II. verdammt das Privilegium. Die Römer empören sich wegen der Wahl des Stadtpräfekten. Pier Leone. Seine Burg am Marcellustheater. Die Diakonie St. Niccolò in Carcere. Abfall der Campagna. Heinrich V. kommt nach Rom. Flucht des Paschalis. Burdinus von Braga. Ptolemäus von Tusculum. Rückkehr und Tod Paschalis' II. Seine Denkmäler in Rom.

[2.](#rom0822) Wahl Gelasius' II. Die Frangipani überfallen das Konklave. Gefangennahme und Rettung des Papsts. Heinrich V. kommt nach Rom. Gelasius entflieht. Der Kaiser erhebt Burdinus als Gregor VIII. Er kehrt nach dem Norden zurück. Gelasius II. als Schutzflehender in Rom. Die Frangipani überfallen ihn zum zweiten Mal. Er flieht nach Frankreich. Tod dieses unglücklichen Greises in Cluny.

[3.](#rom0823) Calixtus II. Unterhandlungen mit Heinrich V. Konzil zu Reims. Calixt kommt nach Italien. Sein Einzug in Rom. Sturz des Gegenpapsts in Sutri. Das Wormser Konkordat. Heilsame Erschütterung der Welt durch den Investiturstreit. Friedliche Herrschaft Calixts II. in Rom. Denkmäler im Lateran verewigen das Ende des großen Streits. Tod Calixts II.

[4.](#rom0824) Wahlkampf. Das Geschlecht der Frangipani. Honorius II. wird Papst. Tod Heinrichs V. Der Papst anerkennt Lothar als deutschen König. Die Hohenstaufen erheben die Waffen. Roger von Sizilien bemächtigt sich Apuliens. Er zwingt Honorius, ihn zu belehnen. Tod Honorius' II.

  
**Drittes Kapitel**

[1.](#rom0831) Die Pierleoni. Ihre jüdische Abkunft. Die römischen Juden-Synagoge im XII. Jahrhundert. Petrus Leo und sein Sohn, der Kardinal Petrus. Schisma zwischen Innocenz II. und Anaklet II. Innocenz flieht nach Frankreich. Brief der Römer an Lothar. Anaklet II. verleiht Roger I. den Titel des Königs von Sizilien.

[2.](#rom0832) St. Bernhard wirkt für die Anerkennung Innocenz' II. in Frankreich. Lothar verspricht, ihn nach Rom zu führen. Romzug des Papsts und Lothars. Mutige Haltung Anaklets II. Kaiserkrönung Lothars. Er kehrt heim. Zweite Vertreibung des Papsts Innocenz. Konzil in Pisa. Roger I. bezwingt Apulien. Zweiter Zug Lothars nach Italien. Streitigkeiten zwischen dem Papst und dem Kaiser. Heimkehr und Tod Lothars.

[3.](#rom0833) Rückkehr Innocenz' II. nach Rom. Tod Anaklets II. Victor IV. Gegenpapst. Rom unterwirft sich Innocenz II. Das Zisterzienserkloster ad Aquas Salvias. Lateranisches Konzil im Jahr 1139. Krieg Innocenz' II. gegen Roger I. Gefangen, anerkennt er die Monarchie Sizilien. Friedliche Tätigkeit des Papsts in Rom. Krieg der Römer mit Tivoli. Innocenz nimmt Tivoli in Schutz. Die Römer erheben sich, setzen den Senat auf dem Kapitol ein, und Innocenz II. stirbt.

  
**Viertes Kapitel**

[1.](#rom0841) Innere Zustände der Stadt Rom. Der Bürgerstand. Die Bannerschaften der Miliz. Popolanen-Adel. Patrizier-Adel. Landadel. Verfall der römischen Landgrafen. Oligarchie der _Consules Romanorum_. Erhebung des Bürgerstandes. Stiftung der städtischen Kommune. Der große Lehnsadel hält zum Papst.

[2.](#rom0842) Das Kapitol in den dunklen Jahrhunderten. Seine allmähliche politische Wiedergeburt. Blick in seine Trümmer. Wo stand der Jupitertempel? S. Maria in Aracoeli. Die Legende von der Vision Oktavians. Das Palatium Octaviani. Der erste Senatspalast des Mittelalters auf dem Kapitol.

[3.](#rom0843) Arnold von Brescia. Sein erstes Auftreten; seine Verbindung mit Abälard. Seine Lehre von der Säkularisierung der Kirchenstaaten. Seine Verurteilung durch den Papst. Seine Flucht und sein Verschwinden. Cölestin II. Lucius II. Kampf des Papsts und der Konsuln gegen den Senat. Der Patricius Jordan Pierleone. Die senatorische Ära. Lucius II. und Konrad III. Unglückliches Ende Lucius II.

[4.](#rom0844) Eugen III. Seine erste Flucht aus Rom. Abschaffung der Präfektur. Arnold von Brescia in Rom. Errichtung des Ritterstandes. Wirkung der Vorgänge in Rom auf die Landstädte. Eugen III. anerkennt die Republik. Charakter der römischen Stadtverfassung. Zweite Flucht Eugens. Kampf des Volks mit dem Adel. Rebellion des niederen Klerus gegen die hohe Geistlichkeit. St. Bernhard schreibt an die Römer. Verhältnis Konrads III. zu Rom. Eugen III. in Tusculum.

[5.](#rom0845) Schreiben des Senats an Konrad III. Politische Ansichten der Römer. Rückkehr Eugens III. Sein neues Exil. Anträge der Römer an Konrad. Er rüstet sich zur Romfahrt und stirbt. Friedrich I. besteigt den deutschen Thron. Brief eines Römers an diesen König. Rom, das römische Recht und das Reich. Die Konstanzer Verträge. Aufregung der Demokraten in Rom. Rückkehr Eugens in die Stadt. Sein Tod.

  
**Fünftes Kapitel**

[1.](#rom0851) Anastasius IV. Hadrian IV. Er legt das Interdikt auf Rom. Vertreibung Arnolds von Brescia. Friedrich I. kommt zur Krönung. Gefangennahme Arnolds. Der Steigbügelstreit. Rede der Senatoren vor dem König und dessen Antwort. Zug nach Rom.

[2.](#rom0852) Krönung Friedrichs I. Erhebung des römischen Volks. Schlacht in der Leonina. Hinrichtung Arnolds von Brescia. Sein Charakter und seine Bedeutung. Abzug Friedrichs in die Campagna. Heimzug nach Deutschland.

[3.](#rom0853) Hadrian IV. bekriegt den König Wilhelm. Er wird gezwungen, ihm die Belehnung zu geben. Orvieto wird päpstlich. Friede Hadrians mit Rom. Mißstimmung zwischen Papst und Kaiser. Die Städte Lombardiens. Hadrian unterhandelt mit ihnen; er überwirft sich mit Friedrich. Die Römer nähern sich dem Kaiser. Tod Hadrians IV. Seine Wirksamkeit. Seine Klage über das Unglück, Papst zu sein.

[4.](#rom0854) Schisma zwischen Victor IV. und Alexander III. Das Konzil zu Pavia anerkennt Victor IV. Mutiger Widerstand Alexanders III. Er schifft nach Frankreich. Zerstörung Mailands. Tod Victors IV. 1164. Paschalis III. Christian von Mainz. Alexander III. kehrt nach Rom zurück. Tod Wilhelms I. Der griechische Kaiser. Friedrich kommt wieder nach Italien. Der lombardische Städtebund. Rainald von Köln rückt in die Nähe Roms.

[5.](#rom0855) Tusculum. Verfall der Grafen dieses Hauses. Rainald von Köln rückt in Tusculum ein. Die Römer belagern ihn. Christian von Mainz zieht zum Entsatz heran. Schlacht bei Monte Porzio. Furchtbare Niederlage der Römer. Friedrich belagert die Leonina. Sturm auf den St. Peter. Unterhandlungen mit den Römern. Alexander III. entflieht nach Benevent. Friede zwischen dem Kaiser und der Republik Rom. Die Pest verschlingt Friedrichs Heer. Sein Abzug von Rom.

  
**Sechstes Kapitel**

[1.](#rom0861) Kampf der lombardischen Städte mit Friedrich. Paschalis III. in Rom. Calixt III. Tusculum ergibt sich der Kirche. Die Römer lassen Alexander nicht in die Stadt. Sieg der Lombarden bei Legnano. Unterhandlungen Friedrichs mit dem Papst. Kongreß und Friede zu Venedig. Alexander III. schließt Frieden mit Rom. Sein triumphierender Einzug in den Lateran.

[2.](#rom0862) Die Landbarone setzen das Schisma fort. Der Stadtpräfekt Johann hält Calixt III. aufrecht. Krieg der Römer mit Viterbo. Calixt III. unterwirft sich. Lando von Sezza Gegenpapst. Konzil in Rom. Tod Alexanders III. (1181).

[3.](#rom0863) Lucius III. Krieg der Römer um Tusculum. Tod Christians von Mainz. Lucius III. überwirft sich mit dem Kaiser; er stirbt in Verona. Urban III. Die sizilianische Heirat. Heinrich VI. rückt in die Campagna. Gregor VIII. Clemens III. Friede mit der Republik Rom (1188).

[4.](#rom0864) Der Kreuzzug. Richard Löwenherz zieht Rom vorbei. Tod Friedrichs I. Cölestin III. Heinrich VI. begehrt die Kaiserkrone. Seine Krönung. Die Römer zerstören Tusculum. Fall der tuskulanischen Grafen. Verhältnis des Adels zur Republik in Rom. Änderung der Verfassung. Benedikt Carushomo Senator. Giovanni Capoccio Senator. Giovanni Pierleone Senator. Heinrich VI. vernichtet die normannische Dynastie in Sizilien. Sein schnelles Ende. Tod Cölestins III.

  
**Siebentes Kapitel**

[1.](#rom0871) Unkultur Roms im XII. Jahrhundert. Das justinianische Recht. Das kanonische Recht. Die Sammlung des Albinus. Der _Liber Censuum_ des Cencius. Die Fortsetzungen des Buchs der Päpste. Mangel an römischen Geschichtschreibern. Die Beschreibung des St. Peter von Mallius, des Lateran von Johann Diaconus.

[2.](#rom0872) Die _Mirabilia Urbis Romae_.

[3.](#rom0873) Römische Bildsäulen-Sagen. Virgil im Mittelalter. Seine Gestalt als Prophet und als Nekromant. Der Zauberer Virgilius in Rom und in Neapel. Berichte darüber aus dem Ende des XII. Jahrhunderts. Schilderung des Rabbi Benjamin aus Tudela von Rom im XII. Jahrhundert.

[4.](#rom0874) Die Monumente und ihre Eigentümer im XII. Jahrhundert. Der römische Senat beginnt für die Erhaltung derselben zu sorgen. Die Säule des Trajan. Die Säule des Marc Aurel. Privatarchitektur im XII. Jahrhundert. Der Turm des Nikolaus. Die Türme in Rom.

[5.](#rom0875) Kirchliche Architektur. Ihr Wiederaufleben im XII. Jahrhundert. S. Maria in Cosmedin. S. Maria in Trastevere. Die Malerei in Rom. Anfänge der Bildhauerkunst. Die ersten Cosmaten. Eugen III. und Cölestin III. beginnen den Bau des Vatikanischen Palasts.
