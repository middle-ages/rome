#### Geschichte der Stadt Rom im Mittelalter

## Neuntes Buch

#### Geschichte der Stadt Rom im XIII. Jahrhundert von der Regierung Innozenz' III. bis zum Jahre 1260

### Inhalt:

**Erstes Kapitel**

[1.](#rom0911) Das XIII. Jahrhundert. Das Reich, die Kirche, das Bürgertum, die Stadt Rom. Wahl Innocenz' III. Das Haus Conti. Geldausteilung des kaum gewählten Papsts an die Römer. Seine Weihe und Krönung. Schilderung des päpstlichen Krönungsrittes zur Besitznahme des Lateran.

[2.](#rom0912) Innocenz III. macht aus dem Stadtpräfekten einen päpstlichen Beamten. Verhältnisse der Stadtpräfektur. Die Präfekten vom Haus Vico. Verhältnisse des Senats. Scottus Paparone Senator. Innocenz III. erlangt das Recht auf die Senatswahl. Schwurformel des Senators. Die Stadtgemeinde Rom bleibt autonom. Erste römische Podestaten in auswärtigen Städten.

[3.](#rom0913) Zerfall der Lehnsfürstentümer Heinrichs VI. nach seinem Tode. Philipp von Schwaben, Herzog von Toskana. Markward, Herzog von Ravenna. Konrad, Herzog von Spoleto. Der tuszische Städtebund. Wiederherstellung der Patrimonien der Kirche. Die Volkspartei in Rom erhebt sich. Johann Capocci und Johann Pierleone Rainerii. Kampf Roms mit Viterbo wegen Vitorchiano. Pandulf von der Suburra Senator. Viterbo unterwirft sich dem Kapitol.

[4.](#rom0914) Die Orsini. Ihre Erbfehde mit den Verwandten Innocenz' III. Richard Conti und das Haus Poli. Die Güter Poli kommen an Richard. Stadtkrieg. Flucht Innocenz' III. nach Anagni 1203. Kampf der Faktionen um den Senat. Innocenz kehrt zurück 1204. Gregor Pierleone Rainerii Senator. Erbitterter Verfassungskampf. Charakter solcher Bürgerkriege. Innocenz erlangt nochmals die Anerkennung des päpstlichen Rechts auf die Senatswahl 1205.

  
**Zweites Kapitel**

[1.](#rom0921) Verhältnisse Siziliens. Innocenz III. wird Vormund Friedrichs. Markward. Walther von Brienne. Die germanischen Landbarone in Latium. Die Kommunen in Latium. Richard Conti wird Graf von Sora. Rückkehr des Papsts aus Latium nach Rom.

[2.](#rom0922) Innocenz III. in seinem Verhältnis zum deutschen Kronstreit. Otto vom Hause Welf und Philipp von Schwaben. Die Kapitulation von Neuß. Der reichsrechtlich anerkannte Kirchenstaat und dessen Umfang. Proteste der Partei Philipps gegen die Einmischung des Papsts in die Königswahl. Krönung des Petrus von Aragon in Rom.

[3.](#rom0923) Umschwung in Deutschland zu Gunsten Philipps. Dessen Unterhandlungen mit dem Papst. Die Ermordung König Philipps. Die Anerkennung Ottos als König in Deutschland. Ottos IV. Romfahrt und Kaiserkrönung. Kampf in der Leonina.

[4.](#rom0924) Bruch Ottos IV. mit dem Papst. Enttäuschung Innocenz' III. Völlige Verwandlung des Welfenkaisers in einen Ghibellinen. Einmarsch Ottos in Apulien. Der Bannstrahl des Papsts. Die Deutschen rufen Friedrich von Sizilien auf den Thron. Otto IV. kehrt nach Deutschland heim.

  
**Drittes Kapitel**

[1.](#rom0931) Friedrich entschließt sich, nach Deutschland zu gehen. Er kommt nach Rom. Er wird in Aachen gekrönt 1215. Er gelobt einen Kreuzzug. Lateranisches Konzil. Tod Innocenz' III. Sein Charakter. Weltherrliche Größe des Papsttums.

[2.](#rom0932) Bewegung der Ketzer. Doktrin von der christlichen Armut. Stiftung der Bettelorden. St. Franziskus und St. Dominikus. Die ersten Klöster ihrer Orden in Rom. Wesen und Einfluß des Bettelmönchtums. Die Sekte der Spiritualen.

[3.](#rom0933) Honorius III. wird Papst. Das Haus Savelli. Krönung Peters von Courtenay zum Kaiser von Byzanz in Rom 1217. Friedrich verzögert den Kreuzzug. Tod Ottos IV. 1218; Wahl Heinrichs von Sizilien zum Nachfolger Friedrichs in Deutschland. Unruhen in Rom unter dem Senator Parentius. Romfahrt und Krönung Friedrichs II. 1220. Kaiserliche Konstitutionen.

[4.](#rom0934) Rückkehr Friedrichs II. nach Sizilien. Friedlicher Besitz des Kirchenstaats durch Honorius III. Die Romagna durch einen kaiserlichen Grafen regiert. Mißverhältnisse in Spoleto. Rom und Viterbo. Demokratische Bewegungen in Perugia. Rom und Perugia. Flucht des Papsts aus Rom. Parentius Senator. Unterhandlungen wegen des mehrmals verschobenen Kreuzzuges. Angelo de Benincasa Senator. Feindliche Stellung der Lombarden zum Kaiser. Spannung zwischen Kaiser und Papst. Bruch zwischen Friedrich und Johann von Brienne. Tod Honorius' III. 1227.

  
**Viertes Kapitel**

[1.](#rom0941) Hugolinus Conti wird Papst Gregor IX. Er fordert den Kreuzzug vom Kaiser. Abfahrt, Wiederausschiffung und Exkommunikation des Kaisers 1227. Manifeste von Kaiser und Papst. Die kaiserliche Faktion vertreibt Gregor IX. aus Rom. Kreuzzug des Kaisers. Invasion Apuliens durch den Papst 1229. Rückkehr des Kaisers und Flucht der Päpstlichen.

[2.](#rom0942) Tiberüberschwemmung 1230. Die Römer rufen Gregor IX. zurück. Friede zu S. Germano 1230. Erstes massenhaftes Ketzergericht in Rom. Der Senator Annibaldo erläßt ein Edikt wider die Ketzerei. Ketzerverfolgung und Inquisition überhaupt.

[3.](#rom0943) Neue Unruhen in Rom. Johann von Poli Senator 1232. Die Römer wollen die Campagna der päpstlichen Herrschaft entreißen. Der Kaiser vermittelt den Frieden zwischen Rom und dem Papst. _Vitorchiano fedele_. Neue Rebellion der Römer. Ihr politisches Programm. Sie erheben sich im Jahre 1234 zu dem ernstlichen Versuch, sich freizumachen.

[4.](#rom0944) Luca Savelli Senator 1234. Die Römer erklären das Patrimonium St. Peters für Eigentum der Stadt. Der Papst bietet die Christenheit gegen sie auf. Der Kaiser leistet ihm Hilfe. Niederlage der Römer bei Viterbo. Angelo Malabranca Senator 1235. Rom unterwirft sich durch Vertrag dem päpstlichen Regiment.

  
**Fünftes Kapitel**

[1.](#rom0951) Friedrich II. in Deutschland und Italien. Er beschließt den Krieg gegen den Lombardenbund. Die Kommunen und der Papst. Der umbrisch-toskanische Städtebund. Ansicht des Papsts von seinem Recht auf Italien und auf die Weltherrschaft. Der Prokonsul-Titel der Römer. Petrus Frangipane. Johannes Poli und Johannes Cinthii Senatoren. Rückkehr des Papsts 1237. Schlacht bei Cortenuova. Das Mailänder Carrocium in Rom. Johannes de Judice Senator.

[2.](#rom0952) Unmaß des Kaisers den Lombarden gegenüber. Der Papst bannt ihn 1239. Friedrich schreibt an die Römer. Sein Manifest an die Könige. Gegenmanifest des Papsts. Schwierige Stellung Friedrichs in seinem Verhältnis zur Zeit. Widersprüche in seinem eigenen Wesen. Eindruck seiner Briefe auf die Welt. Die Kurie durch ihre Gelderpressung verhaßt. Gruppierung der Parteien. Friedrich trägt den Krieg nach dem Kirchenstaat.

[3.](#rom0953) Die Städte des Kirchenstaats gehen zu Friedrich über. Er residiert in Viterbo. Verzweifelte Lage des Papsts. Warum Rom guelfisch blieb. Die große Prozession Gregors IX. Abzug Friedrichs II. Waffenstillstand. Abbruch desselben durch den Papst. Abfall des Kardinals Johann Colonna. Gregor schreibt ein Konzil aus. Die Priester bei Monte Cristo gefangen 1241. Die Tartaren. Erfolglose Unterhandlungen. Annibaldi und Oddo Colonna Senatoren. Mattheus Rubeus Orsini alleiniger Senator. Friedrich schließt Rom ein. Tod Gregors IX. 1241.

[4.](#rom0954) Friedrich II. kehrt ins Königreich zurück. Wahl und schneller Tod Cölestins IV. Die Kardinäle zerstreuen sich. Die Kirche bleibt ohne Haupt. Bund zwischen Rom, Perugia und Narni 1242. Die Römer rücken gegen Tivoli; Friedrich nochmals gegen Rom. Bau von Flagella. Friedrich wieder auf dem Lateinergebirg. Die Sarazenen zerstören Albano. Verhältnisse des Lateinergebirgs. Albano. Aricia. Die Via Appia. Nemi. Civita Lavinia. Genzano. Das Haus Gandulfi. Orte auf der tuskulanischen Seite des Gebirgs. Grottaferrata. Dortige Statuen von Bronze.

  
**Sechstes Kapitel**

[1.](#rom0961) Wahl Sinibalds Fieschi zum Papst Innocenz IV. 1243. Friedensunterhandlungen. Der Papst kommt nach Rom. Abfall Viterbos vom Kaiser, welcher von dieser Stadt zurückgeschlagen wird. Annibaldi und Napoleon Orsini Senatoren. Präliminarfrieden in Rom. Der Kaiser tritt von ihm zurück. Flucht des Papsts nach Genua 1244.

[2.](#rom0962) Innocenz versammelt das Konzil in Lyon 1245. Absetzung des Kaisers. Folgen dieser Sentenz. Friedrichs Aufruf an die Fürsten Europas. Gegenmanifest des Papsts. Die Stimmung in Europa. Was der Kaiser wollte. Innocenz IV. beschließt den Krieg auf Leben und Tod wider das hohenstaufische Geschlecht.

[3.](#rom0963) Verschwörung sizilianischer Barone gegen den Kaiser und ihre Unterdrückung. Waffenglück Friedrichs. Viterbo und Florenz kommen in seine Gewalt. Zustände in Rom. Mahnbrief des Senators an den Papst zur Rückkehr. Päpstliche Belehnung der Frangipani mit Tarent. Der Kaiser will gegen Lyon ziehen. Abfall Parmas; Unglück des Kaisers. Enzius von den Bolognesen gefangen. Fall des Petrus de Vineas. Tod Friedrichs II. 1250. Seine Gestalt in der Geschichte.

[4.](#rom0964) Die Söhne Friedrichs II. Konrad IV. Rückkehr des Papsts nach Italien. Dortige Verhältnisse. Manfreds Lage als Stellvertreter Konrads. Konrad IV. kommt nach Italien und nimmt Besitz vom Königreich. Innocenz IV. trägt die Investitur desselben erst Karl von Anjou, dann einem englischen Prinzen an. Der Senator Brancaleone zwingt ihn, seinen Sitz wieder in Rom aufzuschlagen, 1253. Der Prinz Edmund wird mit Sizilien vom Papst belieben. Verhängnisvoller Tod Konrads IV. 1254.

  
**Siebentes Kapitel**

[1.](#rom0971) Brancaleone Senator von Rom 1252. Näheres über das Amt des Senators und die Einrichtung der römischen Republik jener Zeit. Widerstand der römischen Barone und kraftvolles Auftreten des neuen Senators.

[2.](#rom0972) Innocenz IV. kommt nach Anagni. Tivoli unterwirft sich dem Kapitol. Der Papst rüstet sich, vom Königreich Sizilien Besitz zu nehmen. Manfred wird sein Vasall. Einzug Innocenz' IV. in Neapel. Flucht Manfreds. Sein Sieg bei Foggia. Innocenz IV. stirbt 1254. Alexander IV. kehrt nach Rom zurück.

[3.](#rom0973) Regierung Brancaleones in Rom. Aufstreben der Zünfte. Ihre Verhältnisse in Rom. Verfassung der Zunft der Kaufleute. Die Stiftung des Populus. Brancaleone, der erste Kapitän des römischen Volks. Sein Sturz und seine Gefangennahme 1255. Bologna mit dem Interdikt belegt. Emanuel de Madio Senator. Befreiung Brancaleones und Rückkehr desselben nach Bologna.

[4.](#rom0974) Sturz des Emanuel de Madio 1257. Der Demagog Mattheus de Bealvere. Zweiter Senat Brancaleones. Bestrafung des Adels. Zerstörung der Adelstürme in Rom. Tod Brancaleones 1258. Sein ehrenvolles Andenken. Seine Münzen. Castellano degli Andalò Senator. Sein Sturz und seine Gefangennahme. Napoleon Orsini und Richard Annibaldi Senatoren. Fall des Hauses der Romano. Das Phänomen der Flagellanten.
