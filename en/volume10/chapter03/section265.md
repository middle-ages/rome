# CHAPTER III.

### 265. The Ghibellines prepare for Conradin's Expedition

*1. The Ghibellines prepare for Conradin's Expedition — Charles, as Head of the Guelf League, goes to Florence — Revolt of Sicily AND Apulia — Don Arrigo espouses the Cause of the Ghibellines — Guido of Montefeltre, Prosenator — Conradin descends on Italy — Galvan Lancia in Rome — The Senator seizes the Guelf Leaders — League between Rome, Pisa, Siena and the Ghibellines of Tuscany.*


[](p.411)Conradin sent letters and manifestos to Italy and even to Rome, in which he announced that he was coming to claim the rights of his ancestors and called himself King of Sicily. The Pope consequently instituted a suit against him in Viterbo; he published this document and at the same time a bull, in which he forbade the electors of Germany ever to elect Conradin King of the Romans and threatened all his adherents with excommunication.[^411.1]


" I do not lay much stress," wrote Clement IV. in October 1266, " on the envoys whom the Ghibellines [](p.412)have sent to their idol, the boy Conradin ; I am too well acquainted with his position ; it is so pitiable, that he can do nothing either for himself or his adherents."[^412.1] But in the spring of 1267 the reports became more decided, the demeanour of the Ghibellines in Tuscany more threatening. On April 10 the Pope wrote to the Florentines. " A poisonous basilisk has arisen from the stock of the dragon, which already fills Tuscany with his pestilential breath ; he sends forth to cities and nobles a brood of vipers, men of destruction, the accomplices of his schemes, traitors to us and to the vacant empire as to the illustrious King Charles ; with his lying artifices he parades himself in glittering splendour and endeavours, now by entreaties, now by gold, to seduce men from the ways of truth. Such is the heedless boy Conradin, the grandson of Frederick, sometime Emperor of the Romans, excommunicate by the just sentence of God and of His vicar; his instruments are the infamous men Guido Novello, Conrad Trincia, and Conrad Capece, with several others, who desire to set up this vile idol in Tuscany, and who acquire German mercenaries both secretly and openly in order to form alliances and conspiracies."[^412.2] On April 14 the Pope issued a second citation to Conradin, requiring him to defend himself before his tribunal.[^412.3]


[](p.413)The GhibelHnes in truth displayed great activity. Conrad Capece, returned from Swabia, already entered Pisa as vicar of Conradin, in whose name he issued letters. Pisa and Siena were willing to further the courageous enterprise ; the conspirators in Apulia and Sicily stood ready, and the Romans showed themselves favourable. As the danger assumed a more serious character, the Pope and Charles discussed together how to meet it. Apulian troops under Guido de Montfort at once marched to Tuscany and occupied Florence, whither they were summoned by the Guelfs. Charles came himself at the end of April to Viterbo, where he held long consultations with the Pope, and then followed his troops to Florence.[^413.1] Florence, Pistoja, Prato, and Lucca at once confided the signory to his hands for six years. The great increase of his power was highly inconvenient to the Pope, who was nevertheless obliged to accept it with a good grace ; and in order to palliate by a title the illegal invasion of Tuscany (an imperial territory), he even named Charles " Restorer of the Peace," as if during the vacancy of the empire he had the right to do so.[^413.2]


While the Ghibellines defended themselves against [](p.414)Charles's troops in Poggibonzi and other fortresses, the growing rebellion in Sicily and Apulia raised their courage. Capece, who had hastened in a Pisan vessel to Tunis, had persuaded Frederick of Castile, brother of the Senator Arrigo and a former adherent of Manfred, who had remained behind there, to risk an attack on Sicily. These courageous men landed with some hundred companions at Sciacca in the beginning of September 1267. The greater part of the island rose on their arrival and proclaimed Conradin King. The revolt spread to Apulia ; the Saracens of Luceria, who had already raised the banner of Swabia on February 2, 1267, awaited Frederick's grandson with impatience. The skilfully laid plans of the Ghibellines thus prevented Charles from going to Lombardy to intercept Conradin's progress.


With consternation he saw Rome, where he had been Senator but a short time before, in the power of Don Arrigo, the cousin who was his enemy, and who had already openly declared in favour of the Ghibellines.[^414.1] The Capitol would serve Conradin (who was approaching) as a basis of operations against Sicily, as well as it had served himself for the same purpose against Manfred. He advised the Pope by artificial disturbances to overthrow the Infante of Castile ; [](p.415)but Clement found no one in Rome disposed to further such a scheme ; on the contrary, he admitted that the powerful Senator was dreaded on all sides " like a thunderbolt."[^415.1] Don Arrigo ruled with energy and ability, supported by his vicar, whom (following Charles's example) he had installed on the Capitol. The vicar was Guido of Montefeltre, lord of Urbino, like his ancestors a zealous Ghibelline, a man who soon made Italy re-echo with his name, who was esteemed the greatest general of his age, and to whom at Augsburg in August Conradin had promised rich fiefs in Sicily.[^415.2] Several fortresses in Roman territory were occupied by the civic militia ; in August Don Arrigo seized the important stronghold of Castro on the frontiers of the kingdom ; he strove to acquire influence in Corneto, and in September took Sutri, whence he could stretch out a hand to the Ghibellines of Tuscany. The Pope strove in vain to effect a reconciliation between the Senator and Charles, and no less ineffectual were his [](p.416)exhortations to the barons of the patrimony to remain faithful to the Church.[^4l6.1]


In the beginning of October the report arrived in Rome that Conradin had departed for Italy. And so indeed it was. The young prince had converted his hereditary possessions into money, had with difficulty equipped an army, and had set forth on his march through Tyrol. His hazardous enterprise was precisely the reverse of that which his grandfather had undertaken in the beginning of his glorious career. For the youthful Frederick had left Sicily to fight against a Guelf emperor for the German crown of his ancestors ; his grandson now came from Germany to Sicily to wrest Frederick's Italian crown from a usurper. Frederick had torn himself from the arms of a wife who sought to dissuade him ; Conradin from the arms of a mother who foresaw misfortune. The Church, however, had lent her support to Frederick, but the papal bull had forbidden Conradin to enter Italy, or to lay claim to the heritage of his grandfather. Conradin left Bavaria in September 1267, accompanied by his uncle Duke Lewis, his step-father Meinhard of Tyrol, Rudolf of Habsburg, the cup-bearer Conrad of Limpurg, Conrad of Frundsberg, Albert of Neififen, Kroff of Flüglingen, and several other nobles from Germany and Tyrol. Finally he was attended by Frederick, son of Hermann of Baden, the last of the Babenberg [](p.417)claimants to the dukedom of Austria. The similarity of the circumstances of his orphaned youth, his misfortunes, and his enthusiastic friendship made him Conradin's brother-in-arms. On October 21 Frederick's grandson, with 3000 knights and other soldiers, entered Ghibelline Verona, where his father Conrad IV. had been received by Ezzelino and Obert Palavicini fourteen years before.


Two days earlier, on October 18, Galvan Lancia, Manfred's uncle, had entered Rome with the banner of the house of Swabia. He came as Conradin's plenipotentiary to conclude an alliance with the city. The Ghibellines received the representative of the Hohenstaufen emperor with great rejoicings; the Senator greeted him with public honours, assigned him a dwelling in the Lateran, and at a solemn sitting on the Capitol received Conradin's embassy. The Pope, on hearing of these occurrences, broke into a passion. " I have learnt," he wrote to the Roman clergy, on October 21, "news that fills me with astonishment and horror, that Galvan Lancia, the son of damnation and formerly one of the most ruthless persecutors of the Church, entered Rome on the feast of S. Luke ; that in scorn of the Pope he dared to unfold the banner of Conradin, of the poisonous race of Frederick, and with insolent pomp has occupied the Lateran, which even just men are scarcely worthy to enter." He consequently commanded that Galvan should be cited before the tribunal of the Church.[^417.1] Neverthe[](p.418)less Conradin's legate was honoured in every way ; he was ceremoniously invited to the public games at Monte Testaccio, which were given with unwonted splendour.[^418.1]


In order to silence every contradictory voice, the Senator resolved to rid himself of all the heads of the Guelf party at one stroke. Such were esteemed Napoleon, Matthew and Raynald Orsini, the brothers Pandulf and John Savelli, Richard Petri Anibaldi, Angelo Malabranca, Peter Stephani, some of whom were brothers or nephews of cardinals. He invited these men to a conference on the Capitol in the middle of November ; when they appeared they were taken prisoners. Napoleon and Matthew were conveyed to the rock fortress of Saracinesco ; John Savelli, formerly Senator, a just and noble man, gave his son Luca as hostage, and was released. Raynald Orsini alone did not respond to the invitation, but fled. Terror seized the Guelfs ; many escaped to their fortresses, but Rome remained [](p.419)quiet and obedient to the Senator.[^419.1] The Pope protested ; he placed the prisoners, the cardinals who were related to them and their estates, under the protection of the Church ; and he demanded — although with prudence and moderation — satisfaction from the Senator and from the city commune.[^419.2]


Don Arrigo also banished the families of these nobles, caused their houses to be torn down, and fortified the Vatican, in which he placed Germans.[^419.3] The alliance with Conradin was publicly proclaimed from the Capitol.[^419.4] The Senator himself invited the prince to Rome. A brave warrior and a troubadour at the same time, Don Arrigo addressed some vigorous verses to Conradin, and it was possibly during these days, and amid the din of Ghibelline arms, that he wrote the song which is still preserved. He therein expresses his hatred of Charles, the robber of [](p.420)his property, and his hope for the fall of the French lilies ; he exhorts Conradin to take possession of the beautiful garden Sicily, and by a bold and truly Roman deed to seize the crown of empire.[^420.1]


PLnvoys from Pisa, Siena, and the Ghibcllinc league of Tuscany had arrived in Rome to conclude a formal alliance with the city. On November i8 the great and little Councils, the Consuls of the Merchants, and the Priors of the Guilds assembled in Aracceli, under the presidency of the Prosenator Guido of Montefeltre. Jacopo, chancellor of the city, was elected Syndic of the Romans, and entrusted with the power of concluding the treaty with the Tuscan commissioners.[^420.2] The same day the [](p.421)Pope pronounced the excommunication against Conradin, Pisa, Siena, and the Ghibellines of Tuscany, and sent the sentence to the Roman clergy, in order that it might be proclaimed ; but he did not venture to impose either the interdict on Rome or the ban on the Senator. " I avoid," he wrote, on November 23, " as much as I can, war with the Romans, but I fear that to me and the King of Sicily nought else will remain."


On December I, the offensive and defensive alli ance between Rome, Pisa, Siena, and the Ghibelline party in Tuscany was concluded in the palace of the Quattro Coronati, where the Senator dwelt at this time. The express purpose of the treaty, in which Conradin's rights were preserved, was the ruin of Charles and of his power in Tuscany. After the Guelf cities there had made him signor for six years, after the Pope had named him the Prince of Peace, the Ghibellines put forward Don Arrigo as a rival, appointing the Spaniard captain-general of their confederation for five years. They undertook to pay his retinue, — 200 mounted Spaniards, — and the [](p.422)Senator promised to place 2000 men at the service of the Ghibelline league.[^422.1]


The heads of the Roman Guelfs were meanwhile in prison or banishment; Raynald Orsini alone had escaped to Marino in the Latin Mountains, and was there besieged by the Senator with a military force. But as the Senator met with no success, all whom he suspected, laymen as well as clergy, had to expiate his wrath. He required money to prepare for Conradin. He took the deposita from the Roman convents, in which, according to a very ancient custom, not only Romans but also foreigners kept their valuables. He broke open the treasuries of several churches, and robbed them of their vestments and vessels. Much property was thus accumulated. As the rumour now arose that Don Arrigo had [](p.423)determined to make an armed inroad into Apulia, the Pope summoned Charles to return promptly, while he himself thought of leaving Viterbo and repairing to Umbria.[^423.1] He now voluntarily expressed a wish that Charles should again become Senator of Rome, in which case he would release him from his former oath. He wrote to Don Arrigo, complaining of the reception of Galvan, of the alliance with the Ghibellines of Tuscany, and of the outrages committed on the Roman nobles, and threatened him with the severest penalties of the Church.[^423.2]



[^411.1]: Primus Processus contra Conradiniun^ Viterbii in cathedrali ecci., November 18, 1266. Simultaneously, the bull Fundata domus. The Pope calls Conradin unica scintilla of the house of Frederick. Potthast, n. 19,815. Posse, Anal. Vat., n. 556, and text in the Appendix, p. 141.

[^412.1]: Ep. 392, Viterbo, October i6, to the legate in the March.

[^412.2]: De radice cohibri venenostis egressus regulus, suis jam inficit ßatibus partes TuscicE. . . . Ep. 450, Viterbo, April 10, 1267. Such was the shape in which the noble grandson of Frederick II. appeared to the Pope.

[^412.3]: Posse, n. 569.

[^413.1]: On May 27, 1267, in Viterbo, he concluded a treaty with the ex-emperor Baldwin, who ceded Achaia to him. Philip, Baldwin's son, was to marry Charles's daughter Beatrix.

[^413.2]: Paciarium generalem ... as early as April 10, Ep. 450, to the Florentines. Pacts restauraior in Tuscia (Ep. 512, Viterbo, July 28, 1267). On May 2, 1268, he informed William of Thuringia, the preacher monk, of this appointment, Böhmer, Acta Imp. Sel.^ 987. Pisa and Alfonso X. protested. On May li the Pope wrote that Charles had passed by Viterbo on his way to Florence, and had assumed the rectorship of the Guelf cities (Ep. 464).

[^414.1]: Arrigo once exclaimed : per lo cor Dio^ el 7ni niatrh^ io ilmatrö (Villani, vii. c. 10). He also gave vent to his passionate hatred in a song, of which we shall later have to speak : —
    Mora per Dio cht ma trat lata mortte
    E chi tiene lo mio acquisto in sua Ballia
    Cofne guideo. . . .

[^415.1]: Quamvis — ttii nuncii dixerint, quod parandtim esset in Urbe dissidium : scias tarnen nos adhuc nulluni aditum invenisse. Pars enim non confidit de parte, et ainbce ti77ient Senatorem ut fulgur. To Charles, Viterbo, September 17, 1267, Ep. 532.

[^415.2]: In a document of the Capitol of November 18, 1267, he is called Egregius vir Do7n. Guido Comes de Alontefereti-o et Gazolo, Vicarius in urbe pro superillustri viro Domino Henrico . . . Senatore (Archives of Siena, n. 869). I do not believe that Guido entered Rome for the first time with Galvan Lancia on October 18, since nowhere are the two mentioned together. As early as August 1267 Conradin calls him nunc alma urbis vicaritis. See the Privilegium granted to him, dated ap, civ. Augusta a. 1267 m. Atig. Ind. X., published by Wüstenfeld (Pflugk-Hartung, Iter. Ital., section ii. p. 688).

[^4l6.1]: Ep. 518 to Charles, Viterbo, August 13, 1267. Ep. 523 to the people of Corneto, August 20, 1267. Ep. 532 to Charles, September 17, concerning Sutri. Ep. 534 to Peter of Vico, September 2i. He calls him, as does Saba Malaspina, Petrus Romani Proconsul.

[^417.1]: Cod. VaHcan., 6223, fol. 149. Rectorib. Romanensis Fraterni talis : De Vidtu glor. Ap, Principis rubor injurie — consurgit. . . . This Fraternitas Romana comprised the entire clergy of the city under twelve rectors : they assembled in S. Tommaso in Parione, and in S. Salvator in Pensilis by the Circus Flaminius. M, Armellini, Le Chiese di Rotna^ Rome, 1887, p. 24 f. The Pope was all the more indignant because he had protected Galvan when a fugitive from Calabria and, through the Bishop of Terracina, had given him absolution, under condition that he would serve him in the East. Reports in the Vat. Cod., fol. 148. The decree of the bishop was issued A. 1267, Ind. X. temp, D. dementis IV. PP. Pont, ejus a. II. m. Febr. die V.

[^418.1]: Again in the following year the Pope complained : prafatum Galvanum ad eornm iudos, ut ipsis illuderet, venientem non solum pari, sed j?iajori fastu — receperunt et munificentius honorarunt. Raynald, ad an)i. 1268, n. 21.

[^419.1]: Saba, pp. 834, 835 ; ad itistar piscium — uno tractu retium capiuntur. This took place before November 16, 1267, when the Pope protested against it, but not before November 13, when he still wrote in friendly terms to the Senators (Ep. 554). Ep. 558, November 20, to Charles ; Ep. 559 to the Cardinal of S. Adrian, November 23 ; Ep. 561, 563, November 26.

[^419.2]: Ep. 556, Viterbo, November 16, 1267, in which he already says of Henry : publicum Ecclesia et — Caroli — hostem, ac manifeslum ejusdem Corradini se fautorem exhibuit.

[^419.3]: On April 11, 1271, Charles of Anjou, as Senator of Rome, ordered the houses and towers of the brothers John and Pandulf Savelli, which (at the instigation of Peter Romani de Cardinali and of Stephen Alberti Normanni) Arrigo of Castile had caused to be pulled down, to be restored at the expense of the guilty. C. Min. Riccio, Sagg. di Cod. Dipl., i. n. 83.

[^419.4]: This happened after November 16, and not as Cherrier, iv. 168, following the Reg. Clem. IV., lib. iv. n. 3, fol. 248, asserts, in the beginning of November.


[^420.1]: 1
> Alto valore chagio vis to impart te
>
> Siati arimproccio lo male chai sofertto.
>
> Pemsati in core che te rimasto impartte.
>
> E cofne te chiuso cio che tera apertto.
>
> Raquista in tutio lo podere ercolano.
>
> Nom prendere partte se puoi avere tut to.
>
> E membriti come fecie male frutto
>
> Chi male contiva terra chce a sua mano,
>
> Alto giardino di loco Ciciliano
>
> Tal giardinetto ta preso in condotto,
>
> Che tidra gioia di cio cavei gran hit to,
>
> A gran corona chiede da romano.

> The canzone (five strophes and an envoi) is found in the Cod. Vat., 3793, fol. 533, a celebrated collection of romances of sac. 13 and 14 ; it is inscribed donnarioo.


[^420.2]: Concerning this, see two remarkable documents in the Archives of Siena, n. 869 : In nom. dorn. Am. Ann. a nativ. ejusd. 1267 die Veneris XVIII. Novbr. Ind, XI. more Romano generale et spec, consilium commutiis Rome factum fuit in Ecc. S. Marie de Capitolio per vocem preconum et sonuvi campane de hominib. ipsor. consilior. more solito congregatmyi convocatis etiam convenientib. ad diet, consilium consulibus mercatorum et capitibus artium Urbis Ro?ne. In quo quid. consilio seu quib. Egregius vir Dom. Guido comes de Monteferetro et Gazolo vicarius in urbe pro superill. viro D. Henr, filio qnd. D, Fernandi seren. Castelle regis Senatore ipsius urbis. , . . The Parliament approves the league with Siena, Pisa, and the Ghibellines of Tuscany, and the granting of full powers to a Roman Syndic : Act. Rome In Eccl. S. M. de Capitolio. Ibi vero D. Azo Guidonis Bovis prothojudex et consiliarius dicti D. Senatoris. D. Angelus Capucius. D. Rofredus de Parione. D. Crescentius leonis. Jokes Judicis et alii plerique interfuerunt rogati testes. Et Ego Palmerius de monticello civis parmensis Imp, Auct, notarius . . . scripsi — Nro, 870 : under the same d^ie Jacobus cancellarius urbis is elected nuncius, procurator, actor et sindicus of the Roman people.

[^422.1]: Archives of Siena, n. 871 ; a large parchment in a very neat hand. The syndics of Pi-a and Siena, and the pars Ghibellina de Tuscia (Pistoja, Prato, Poggibonzi, San Miniato, &amp;c.) appoint in Tuscia Capitanetwi gen. Excel. ATagnif. et III. Vir, D. Henrigtim — nunc A. U. Senat orem — per spatitini quinque annor. salvis in omnib. pred. honorib. ill. Regis Corradi. The league between Pisa and Venice is secured : Act. Urbi in palatio SS. qziatiwr Coronatorum^ ubi idem D. Capitan. morabatur, presentib. D. Accone Judice. Guidoni Bov. de Parma. D. Uguiccione Judice, D. Janni Mainerio. Magistro Vitagli de Aversa, Mariscopto notario. D. Marito de Florentia, D. Ormano de Pistorio. D. Ugolino Belmonti et de Uberto Judice de Senis sub A.D. Mill. CCLXVII. Ind. XL, prima die Kal, Dec. sec. curs sum Ahne Urbis. Ego Usimbardus olim Boninsegne. . . . In a second document the cities pledge themselves to defend their rights, and Henry and his adherents, et ad domanium Imperii in Tuscia acquirendum et occupandut7i. . . . Henry pledges himself to tolerate no exercise of Charles's authority in these cities. Actum ut supra. A third document contains the treaty between these cities and Rome, measures for the security of trade, guarantees of their rights, and the abolition of reprisals. Actum ut supra.

[^423.1]: Bonincontrius {Hist. Sic, p, 5) says that Henry actually made the expedition, occupied Aversa and captured the Abruzzi as far as Aquila ; but since the Pope is silent on the subject, the statement may be taken for what it is worth.

[^423.2]: Ep. 568, December 17, to Charles : scias fili, quod si potes senatum Urbis acquirere ad te77ipus competens, tolerabimus. Ep. 569, December 19 : threatening letter to Henry ; he writes still more strongly in Ep. 572, December 28 ; Ep. 573, December 30, but always with the superscription dil.filio nob, viro , . . Senatori urbis.
