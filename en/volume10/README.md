# BOOK TENTH

## HISTORY OF THE CITY OF ROME FROM 1260 UNTIL 1305.

### PREFACE

In the earlier part of the translation I have only given an Index at the end of Vols. II. and IV. I think it therefore necessary to say a few words in explanation of the Index I have added to Vol. V., lest the reader, unacquainted with Gregorovius in the original, might be led to congratulate himself on having reached the end of the work. Such is far from being the case. Three more substantial volumes still await him. But to save myself trouble in the future, while revising the proofs of Vol. V. I compiled an Index as I went along ; and having done so, I thought I should consult the reader's convenience better by adding it to the volume to which it belonged, instead of reserving it for another year in order to piece it together with the Index to Vol. VI.

<p style="text-align: center;">A. H.</p>

November 1897.
