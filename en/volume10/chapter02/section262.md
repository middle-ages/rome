# CHAPTER II.

## 262. Manfred's Letter to the Romans

*1. Manfred's Letter to the Romans — His March INTO Roman Territory — First Hostile Encounter — Charles's pitiable Position in Rome — The Proven(5;al Army marches through Italy and enters Rome — Charles crowned King of Sicily in S. Peter's.*


[](p.372)Manfred was in Foggia at the time when Charles made his entry into Rome. On May 24 he thence issued a lengthy manifesto to the Romans, in which he said that he was the descendant of glorious emperors, who had ruled the world ; that he was even justified in aiming at the imperial crown, but that the avaricious Church had made war upon him in his country and, after having been defeated there, had summoned Count Richard and the King of Castile to the empire. In order to defend his rights he had again subjugated Tuscany and the Marches ; he was greater, both in wealth and power, than other princes, since he ruled over nearly the whole of Italy, over the sea as far as Tunis and Sardinia, as also over the greater part of Romania. Nevertheless the Pope had summoned the Count of Provence against him. To punish this arrogance he had sent his troops into S. Peter's patrimony, where they had been received by Peter of Vico. The avaricious [](p.373)Church endeavoured to prevent his restoration of the empire, although she denied the fact, and resembled a widow who in public weeps the death of her husband, but secretly rejoices because she has succeeded to his property. Manfred further told the Romans, that the Church strove to annex the empire, and persecuted Frederick's race, in order to reign over all kings and countries, and that she traced her right to supremacy from the valueless donation of Constantine. He reproached the Romans that, owing to their want of courage, they themselves were to blame for these usurpations, for to them belonged the election and coronation of the emperor, and from them would he receive the empire, although he might take it against the will of the Senate, as Julius Caesar and his grandfather Frederick had taken it. In conclusion, he summoned the Romans to remove the vicar of the Count of Anjou ; he himself was approaching with force to take the imperial diadem from the Senate and people.[^373.1]


This memorable manifesto reveals the height of Manfred's consciousness of power ; he sums up in it his whole life. The position which he had acquired in Italy, as also the strength and the flourishing condition of his kingdom, justified him in regarding himself as Frederick's lawful heir, and at the same time in continuing the war with the Papacy. He [](p.374)openly admitted that the restoration of the empire was his object, and that he would take the crown in Rome from the Roman people.


Learning that Charles was in Rome, Manfred was obliged to try to crush him before the arrival of the Provencal army. The undertaking was difficult, and with Apulians and Saracens was scarcely practicable. That he could not rely upon the Ghibclline party was soon made clear by the defection of many of its members, for Ostia and Civita Vecchia were surrendered to Charles, and even Peter of Vico, hitherto the most zealous leader of the Ghibellines in Roman Tuscany, deserted to the enemy's camp.[^374.1] Manfred resolved to march into Roman territory. In the hope of drawing Charles out of the city and then annihilating his forces, he determined to push from the Abruzzi to Tivoli ; in July he advanced to Celiac, the present Carsoli, after having commanded Count Guido Novello, his vicar-general in Tuscany, to proceed at the same time with all his strength against Rome.[^374.2] The troops of the two opponents encountered each other for the first time in the mountains of Tivoli. The attempt to force an entrance into Tivoli itself failed and ended in an insignificant skirmish.[^374.3]


[](p.375)Manfred encamped, as formerly Frederick II. had done, on the field of Tagliacozzo, where only two years later the last of his house, whom he himself had deprived of the crown of Sicily, was to be overthrown by the same Angevin.[^375.1] He had determined that since he could not gain Tivoli, he would push into Spoletan territory, when news from Apulia decided him to return to the kingdom. This he did in haste, after having strengthened the garrison of Vicovaro.[^375.2]


Charles's impatience to encounter his rival was meanwhile checked by circumstances. Whether or not he advanced in person from Rome to the Liris in September is uncertain.[^375.3]


Treason began its sinister work in the Sicilian kingdom. Several barons held secret negotiations with Charles. Report related that 60,000 Provencals had opened a way through Lombardy, while the Crusade was preached with success against Manfred in every country. The populace, long accustomed to hear the Crusade preached against one and the same German family, against father, son, and grand[](p.376)son, listened thoughtlessly to the summons of Clement IV., who announced that in the Count of Provence the Church had appointed a champion against " the poisonous brood of a dragon of poisonous race," and exhorted the faithful to take the cross under the same banner and above all things to give money, in return for which every sin would be forgiven.[^376.1] As in the time of Frederick II. swarms of mendicant monks spread themselves over Italy and Apulia, disseminating hatred against the existing government, encouraging treason, and filling the mind of the populace with superstitious dread.


The King, who well knew the financial difficulties in which Charles in Rome and Clement in Perugia were involved, never doubted that their plans would be shattered. Seldom has a great undertaking been equipped with such wretched means. The expenses of the conquest of Sicily were literally collected by begging or were raised from usurers. The destitution of the debt-ridden Charles was so great that he knew not how to provide for his daily expenses (which amounted to 1 200 pounds Tours). He assailed the Pope, the King of France, and the bishops with piteous demands for money. We still read the [](p.377)many letters of the Pope, — dreary monuments of an enterprise which never redounded to the honour of the Church. " My treasury is completely empty ; the reason whereof is shown by a glance at the confusion of the world. England resists, Germany will not obey, France sighs and grumbles, Spain has enough to think of in her own affairs, Italy pays nothing, but devours. How can the Pope find money for himself and others without resorting to godless means ? Never in any undertaking have I found myself in such straits." Thus Clement wrote to Charles.[^377.1] The Church tithes of the first year were spent in preparations. France would give no money, and the Pope believed the enterprise lost. Charles tried to raise a loan from the Roman merchants ; they, however, demanded the ecclesiastical property in Rome as security. With sighs Clement agreed to the bond. For, as he admitted, unless the loan is raised the count must either fly, or die of hunger. Thirty thousand pounds were raised with difficulty on these securities ; Manfred, it was said, had by his gold prevented the Roman banks from giving more.[^377.2] Usurers from the south of France, [](p.378)Italy, and Rome made use of Sicilian affairs to drain the Pope and the count. " Ask," the Pope wrote to Cardinal Simon, " ask the count himself how wretched his life is ; he begs clothing and keep for himself and his people with the sweat of his brow, and always looks to the hands of his creditors, who suck his blood. They make him pay a solidus for that which is not worth two pence, and even this he only acquires by flattery and humble request."[^378.1] Clement never lived through more trying days than these, when the political undertakings of the Church forced him to descend to the petty cares to which a priest of Christianity should ever remain a stranger.


With growing impatience the Pope awaited the arrival of the army. "If thy troops do not come,"he wrote to Charles, " I do not know how thou wilt await them and how thou wilt manage to live, how thou wilt maintain the city or assist the approach of the army if it be delayed. Should it, however, arrive, as we hope, still less do I know how we shall feed so many men."[^378.2]


Everything, in fact, depended on whether the Provencal army reached Rome or not. Should it be defeated by the Ghibellines in northern Italy, Charles was lost and Manfred triumphed. The cardinallegate in France had equipped the crusading army assembled in Provence in case of emergency, and had [](p.379)set it in motion in June. Here were found barons of renowned name, brave knights in whom still glowed a spark of the fanaticism that had kindled the Albigensian wars ; all thirsting for glory, gold and land : Bocard, Count of Vendome, and his brother John ; Jean de Neelle, Count of Soissons ; the Constable Gilles le Brun, Pierre of Nemours, Grand-Chancellor of France, the Marshal of Mirepoix, William I'Estendard, Count Courtenay, the warlike Bishops Bertrand of Narbonne and Guy de Beaulieu of Auxerre, Robert of Bethune, the young son of Guy de Dampierre, Count of Flanders ; the whole house of Beaumont, several noble families from Provence ; finally, Philip and Guido of the celebrated house of Montfort.[^379.1] This army of rapacious adventurers, decorated by the Pope himself with the cross of the Redeemer in order that they might conquer a foreign and Christian country amid streams of blood, crossed the Alps of Savoy in June, to the number of about 30,000 men. Treaties which Charles had made with the counts of these territories and with some cities opened him a way through Piedmont ; the Margrave of Montferrat joined him at Asti and the Margrave of Este with other Guelfs stood in arms near Mantua.[^379.2]


In vain Palavicini, Jordan of Anglano and Boso [](p.380)of Doara hoped to hold the river Oglia ; their exertions failed. Palavicini finally threw himself into Cremona, and the French continued their march to Bologna amid terrible devastation.[^380.1] Four hundred Guelfs, who had been banished from Florence, joined them in Mantua and promised them greater reinforcements. Thus from party hatred the Italians of this age, Guelfs as well as Ghibellines, admitted a foreign conqueror into their country and opened the way to the French for centuries to come. The sense of freedom and of patriotism had become enfeebled in the exhausted cities ; no tie bound the ancient federations, no great national sentiment rose above petty party aims and domestic quarrels. Milan, Brescia, Verona, Cremona, Pavia, and Bologna were rent asunder by the fury of factions or bowed under tyrants, while the ports of Genoa and Venice, and even Pisa, followed their own commercial advantage.


The Ghibellines in Tuscany did not prevent the advance of the enemy, when, avoiding this territory, the Provencal army proceeded through the Marches and the dukedom of Spoleto to advance against Rome. Recanati, Foligno, Rimini, and other places [](p.381)raised the Guelf standard. Manfred found himself bitterly deceived. His power over so many cities, extending as far as the Po, had only been a splendid delusion, and it was soon seen that his rule even in Apulia was a delusion also. In October he attempted an unsuccessful expedition to the Marches and finally, restricting himself merely to defence, he called Jordan of Anglano from Lombardy.


Charles, who, in order that he might be endowed with the prestige of legality, demanded his coronation as King of Sicily, had implored the Pope to crown him in Rome in person ; the pride of the Romans, he said, would be offended by a coronation in Perugia or anywhere outside the city. The Pope replied indignantly that the Romans had no cause to be troubled concerning his election.[^381.1] Many difficulties arising from the position in which the Pope found himself, Charles's arrogant demeanour as Senator, his financial distress, the horrors perpetrated on the march by the Provencal army, were represented to the King by Clement IV. in his irritation. It was with reluctance that the Pope had ratified Charles's investiture on November 4, reluctantly he issued a bull on December 29 fixing the date of the coronation, a ceremony which he, however, delegated to five cardinals, his representatives.


[](p.382)Charles On January 6, 1266, Charles of Anjou and his wife Beatrix were accordingly crowned in S. Peter's as King and Queen of Sicily, and the custom that none but emperors and popes should receive coronation in the cathedral sacred to the apostles, and on the spot where Charles the Great had received the crown of empire, was thus for the first time set aside. Tournaments and popular festivals celebrated the ominous act.[^382.1]


For a moment Manfred had hoped to gain the Pope to his side ; this hope had now vanished for ever. Hearing of Charles's coronation, he sent envoys to the Pope ; he entered a protest; in kingly language he invoked Clement to prevent the robber, whom Clement himself had armed, from making an attack upon his kingdom. We cannot read unmoved the terribly severe and prophetic answer of the Pope. " Let Manfred know," said Clement, " that the time for grace is past. Everything has its time, but time has not everything. The hero in arms has already issued from the gate ; the axe is already laid to the roots."[^382.2]


The Provencals meanwhile entered Rome soon after Charles's coronation. After a tedious march of seven months through the middle of Italy, they arrived in the longed-for city, exhausted, in rags and without pay. They hoped to find abundance of [](p.383)everything, and beheld the King, their master, oppressed with debts and restless with despair. He offered them nothing beyond the prospect of an immediate campaign, where they would be called on to ford rushing torrents, traverse pathless territory, to attack strong fortresses and to overcome armies skilled in war.



[^373.1]: *Letter Armonia celestis ... in Capasso, Hist. Dipl. Reg. Sec., n. 274, abridged in Böhmer-Ficlcer, 4760. Quum pro jam dicta restauracione Imperii ac rei pub. romanor. ad sacri sumendttni dyadema Imperii, auctoritate tui senatus populi et communis^ maxime nostre potencie comitiva, christi nomine evocato, advenire te Rofiiam matrem et capiul Imperii properamus.*

[^374.1]: Ep. 90. Clement wrote from Perugia on July 1 1 to the Rector of the Patrimony concerning this occurrence : Peter was probably appointed Prefect on this account.

[^374.2]: Böhmer, Acta Imp. Sei., 980. Manfred to Guido Novelle, Benevento, June 7, 1265.

[^374.3]: On December 10, 1265, Charles, who was then in Rome, settled a pension on Jacobus Rusticus de Audemario, because he had lost a hand fighting in partibus tiburtinis. Del Giudice, i. n. 28.

[^375.1]: Castrametatus in confinio territorii urbis apud Tallacocium, Mansit ibi cum toto exercitu suo circa duos menses — then he went to Arsoli. This account is certainly most inexact {Descr. Vict., p. 833).

[^375.2]: Ep. 96, Perugia, July 13. Ep. 137, ibid., August 25 : venii ad matricem — (Amatrica in Abruzzo) — in regnum rediit festinanter, di^nissa militia Vicovari.

[^375.3]: The Diarium of M. of Giovenazzo is spurious : Matteo di Giovenazzo, eine Fälschung des 16 Jahrh., by W. Bernhardi, Berlin, 1868. Charles's Regesta show that he was in Rome on July 15 and 16, August 9, and September 7. Then on September 23 (Del Giudice, Cod. Dipl., yo\. i.). Manfred was in Capua on August 25, Reg. Imp.

[^376.1]: Bull of indulgence : De venenoso genere velut de radice colubri virulenta progenies Manfredus qd. princeps Tarentinus egressus — visus est quantum potuit paterna?n savitiam superare . . . oportuit nos pro Ecd. defensione Athlet am assumere. Ep. 145, without a date. Cardinal Simon is empowered to absolve : manuum injectores in clericos — incendiarios — sacrilegos sortilegos — clericos concubinaries — nee non presbyteros et religiosos quosl. qui contra constitutionent Eccl, leges vel physicam aiidierint — dum tarnen pro hujusmodi tugntio recipiant sigttum crucis.

[^377.1]: Ep. 105 : licet nunquam in negotio aliquo major per plexii as nobis occurrerit. Ep. 135, a piteous letter to the King of France : Moveant igitur te viscera pietatis adfratrem^ moveant et ad matrem. . . . The many letters of the same tenor show the deplorable position of Charles and of the Pope.

[^377.2]: Et si non ßat^ regem oportet vel fame deficere^ vel aufugere. Ep. 118 and 120. The loan amounted to 100,000 pounds Provins. Ep. 181 to Lewis, from Perugia, November 17, according to which only 30,000 pounds were raised. On October 4, 1265, Charles admitted that his debt to the merchants, contracted on the mortgage of the ecclesiastical property, had been contracted for the conquest of Sicily (Del Giudice, n. xx. ). There were wealthy houses in Rome : in Urhe — sunt p lures abundantes in scsculo mult as divitias obtinentes, wrote the Pope to Charles. Ep. 89.

[^378.1]: Ep. 165, Perugia, October 19,

[^378.2]: Ep. 173, Perugia, October 30

[^379.1]: William de Nangis, p. 374 : Descriptio Victorice, p. 834 ; Villani, vii. c. 4 : Papon, iii. 17.

[^379.2]: On August 9, 1265, the league was concluded between Charles, Obizzo of Este, Lewis, Count of Verona, Mantua and Ferrara against Manfred, Palavicini, and Boso : AcHim Rome in Palatio Capitolii . . . in the presence of the witnesses Robertus de Lavena, Robert de Baro, Riccardus Petri Anibaldi, Anibaldus Domini Trasimundi. Verci, ii. 88.

[^380.1]: There was suspicion of treachery on the part of Boso. Dante saw his shade in the icefields of the lowest depth of hell (Inferno, xxxii. 115) : —


    E piange qui Pargento de Franceschi ;
    lo vidiy potrai dir, quel de Duera
    La dove i peccatori stanno freschi.

    Boso, banished from Cremona by the papal party, died in misery. Schirrmacher, Die letzten Hohenstaufen^ pp. 518, 519, tries, however, to show that the accusation of treachery is unfounded.


[^381.1]: Ep. 195, Perugia, December 20. Di Cesare says (p. 201), that, soon after his arrival, Charles visited the Pope at Perugia, and that Clement accompanied him to Rome. This is a mistake. In Papain numq. Romam intravit, says Herm. Altahensis, Annal., p. 406, Vitoduramis Chron, (Leibnitz, Accession.^ i. 23) is also mistaken in saying that the Pope went in procession through Rome, accompanied by the Emperor Baldwin and King Charles.

[^382.1]: Saba Malaspina (p. 819). The date of the coronation is given in Bernardus Guidonis, p. 595.

[^382.2]: Jam in publicum prodiit fortis armatus, ad radicem posita est securis. Ep. 266. These letters — invaluable documents — develop, scene by scene, this harrowing tragedy.
