# 第九卷

# 从英诺森三世统治到1260年的十三世纪的罗马城历史

* [Chapter-I]()

* [Chapter-II]()

* [Chapter-III]()

* [Chapter-IV]()

* [Chapter-V]()

* [Chapter-VI 251. Sinibald Fieschi elected Pope as Innocent IV 1243](chapter06/section251.md)

* [Chapter-VII 255. Brancaleone, Senator of Rome, 1252](chapter07/section255.md)
