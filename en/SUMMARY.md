# Summary

## BOOK IX. History of the City of Rome in the Thirteenth Century FROM the Reign of Innocent III. until 1260.

* CHAPTER-VI

  * [251. Sinibald Fieschi elected Pope as Innocent IV 1243](volume09/chapter06/section251.md)

  * [254. The Sons of Frederick II — Conrad IV](volume09/chapter06/section254.md)

* CHAPTER-VII

  * [255. Brancaleone, Senator of Rome, 1252](volume09/chapter07/section255.md)

  * [256. Innocent IV goes to Anagni](volume09/chapter07/section256.md)

  * [257. Brancaleone's Government in Rome](volume09/chapter07/section257.md)

  * [258. Fall of Emmanuel de Madio](volume09/chapter07/section258.md)

## BOOK X. History of the City of Rome from 1260-1305

* CHAPTER-I

  * [259. The German Empire](volume10/chapter01/section259.md)

  * [260. Fighting in Rome concerning the Senatorial Election](volume10/chapter01/section260.md)

  * [261. Pope Clement IV, 1265](volume10/chapter01/section261.md)

* CHAPTER-II

  * [262. Manfred's Letter to the Romans](volume10/chapter02/section262.md)

  * [263. Charles leaves Rome](volume10/chapter02/section263.md)

  * [264. Charles resigns the Senatorial Authority](volume10/chapter02/section264.md)

* CHAPTER-III

  * [265. The Ghibellines prepare for Conradin's Expedition](volume10/chapter03/section265.md)

  * [266. Evil plight of Conradin in North Italy](volume10/chapter03/section266.md)

  * [267. Conradin Flees to Rome From The Fleld of Battle](volume10/chapter03/section267.md)
