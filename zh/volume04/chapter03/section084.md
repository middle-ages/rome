_3\. 保罗一世于 767 年去世。伪教皇君士坦丁。罗马反革命。克里斯托弗斯和塞尔吉乌斯在伦巴第人的帮助下出其不意地攻占了罗马。伦巴第人将菲利普安置在拉特兰。斯蒂芬三世成为教皇。罗马的恐怖主义 对篡夺者的审判。768年皮平之死。 769年拉特兰会议。

保罗一世被他的传记作者描述为一个性格温和、乐善好施的人。然而，他最后时刻和死后发生的暴风雨事件证明，他作为罗马的统治者并不受欢迎。但这是教皇相对于罗马城的地位发生变化的结果。当罗马城采取世俗形式，与希腊帝国的政治联系被切断后，市政本能就像从长眠中苏醒过来。罗马人为抵御伦巴第人和希腊人的进攻而拿起了武器，他们逐渐感受到了自己的力量，因此城市自治的需求开始显现出来。从那时起，罗马共和国就有了贵族统治的历史；城市内部的争斗、教皇与贵族的斗争开始了，教皇们很快发现自己不得不为不情愿的罗马换一个皇帝，而他们自己却仍然没有能力统治罗马。与此同时，由于一个世俗公国与教皇地位联系在一起，教皇地位在罗马权贵眼中的身价也随之上升。在教皇选举中具有决定性影响力的乐天派，从此开始努力从自己的家族中任命教皇。

教皇奄奄一息地躺在城墙外的圣保罗修道院里，消息还没传开，城里就爆发了轩然大波。一个由贵族组成的强大党派起兵实施他们的野心计划。他们的首领是托托，显然是托斯卡纳的公爵，居住在内皮；他在托斯卡纳乡村拥有许多庄园和仆人殖民地，在罗马也有一座宫殿。这里有些宫殿的历史可以追溯到古代，并有过去的纪念碑；前主人的记忆，如塞梯吉安人、德西安人、普罗比人、西玛奇人、马克西米人，也许已成为国内的传说，也许附着在古老的大理石像上；但宫殿本身也经历了罗马的蜕变，在这里或那里变成了修道院和医院，或变成了城堡式的住所，一个血统可疑的野蛮家族就在其中度过了一生。

早在保罗去世之前，托托公爵就带着武装人员和他的兄弟君士坦丁努斯、帕西弗斯和帕斯卡利斯从尼皮出发，从圣潘克拉修斯的大门进入罗马，并在那里自投罗网。教皇于 767 年 6 月 28 日去世，被他身边所有的人无情地抛弃；只有一位长老或红衣主教斯蒂法努斯忠实地陪伴着他。第二天，托托让他的弟弟君士坦丁当选为教皇，并在一片喧哗声中把他领到了拉特兰。这场动荡的选举只能由这些伟人在罗马神职人员中组成的党派促成。他们的名字一部分是拉丁文，一部分是拜占庭文。君士坦丁是个普通人，这更增加了篡位的厚颜无耻；但托托强迫普拉内斯特的乔治主教将他的弟弟转变为神职人员，并先后授予他亚迪克努斯和迪克努斯的圣职。蜕变从未如此迅速：当选教皇让罗马人在他兄弟的威慑下宣誓效忠于他，并于 7 月 5 日星期日前往圣彼得大教堂，在那里，同一位乔治主教与阿尔巴诺的尤斯特里乌斯主教和波图斯的西托纳图斯主教一起为他祝圣。

就这样，一个戴着墨镜的术语地主获得了圣彼得的主教职位，并被允许担任一年。没有人敢阻挠他的暴力升迁；甚至连法兰克使节的反对也没有听说过；当时在场的一位法兰克使节带着君士坦丁的第一封信平静地启程前往弗兰西亚，此外，这样的使节只是临时出现在罗马，而且经常被教皇亲自召见，这反而证明了弗兰肯国王和罗马人的帕特里西乌斯还没有在罗马城直接行使任何最高权力。在篡夺的整个过程中，没有听说皮平进行过干预或派出过授权代表；只有罗马各方，首先是教皇宫殿的权贵们采取了行动。

然而，入侵者君士坦丁还没坐上教皇宝座，就发现有必要赢得皮平的青睐。像他的前任一样，他宣布自己被提升为罗马人的帕特里西乌斯，请求继续与罗马保持保护关系，并向他保证自己将继续忠于教会的捍卫者。他告诉皮平，保罗死后，罗马人和邻近城市的人民推举他为保罗的继任者，但他隐瞒了自己被提升的情况。皮平没有回信，君士坦丁又寄来了第二封信。他哥哥的不幸傀儡，为了自己统治罗马而被授予了扁担，发出了更多焦虑的叹息。他在信中说，他是被无数众望所归的人以狂风骤雨般的力量推上教皇的可怕高位的，这是半真半假的，也是他垮台的预兆。他再次恭敬地表示问候，并请求国王不要听信诽谤者。皮平没有回答。

教会的首任官员对这些暴力状况做出了反应。在保罗统治时期，克里斯多福斯曾是公证人和执政官的首席长官，也就是他的第一任总理或今天所说的国务秘书；他徒劳地反抗了篡位，然后带着儿子们逃到了圣彼得大教堂的高高祭坛，君士坦丁在那里许诺给他生命和自由，让他住在自己的房子里直到复活节。克里斯托弗斯是罗马的最高贵族，在教堂空置期间负责管理教堂，而他的儿子谢尔盖（Sergius）则担任着重要的圣职（sacellarius 或 sacristan）。两人都与其他罗马人密谋推翻篡位者。他们假装向往修道院生活，君士坦丁很高兴能摆脱他们，或者说相信了他们的誓言：他允许他们离开罗马，到里耶蒂附近的圣萨尔瓦多修道院隐居。但这些人匆忙赶往斯波莱托公爵狄奥蒂修斯那里，并在他的陪同下前往帕维亚。

德西德里乌斯很高兴地回应了流亡者的抱怨；他同意借给他们武器去征服罗马，但他要求他们履行义务以换取他的帮助。塞尔吉乌斯和瓦里佩特率领伦巴第大军前往罗马。768 年 7 月 28 日，他们占领了萨拉里安桥，次日清晨渡过米尔维安河，直抵潘克拉修斯城门。守卫被同谋者说服，放他们进去了。但由于害怕，伦巴第人不敢下贾尼库卢斯河。听到城中有敌人的呼声，托托和帕西弗斯匆匆赶往城门，与他们同行的还有同谋者和叛徒塞孔迪塞利乌斯-德米特里厄斯（Secundicerius Demetrius）和沙图尔-格拉蒂奥苏斯（Chartular Gratiosus）。一名身材高大的战士拉辛佩特（Rachimpert）冲向托托，但在公爵强有力的攻击下倒下了，伦巴第人看到他倒下，已经开始逃跑，这时那两个叛徒用长矛刺穿了托托。帕西弗斯随后逃到拉特兰宫殿去营救他的弟弟，因为他们的事业已经失败了。君士坦丁带着他和他的副主教西奥多逃到了圣约翰拉特兰大教堂；他们把自己关在圣凯萨留斯讲堂里，在祭坛前坐了几个小时，而宫殿里回荡着武器的声音和搜查者的喊叫声。他们被抓了起来，扔进了地牢。

在这场骚乱中，瓦尔德佩特瞒着塞尔吉乌斯，将伦巴第人聚集到罗马人中间。德西德留为这样的党派支付了费用，并希望通过他们培养出一位忠于自己的教皇。他搬到了埃斯基莱纳河畔的圣维特修道院，并从那里带出了长老菲利普。惊讶的罗马人看到一位新教皇正走向拉特兰，并听到伦巴第人高喊："菲利普爸爸，圣彼得选中了他。" 在那里还找到了一位主教，他为菲利普举行了祝圣仪式；新当选的教皇坐在教皇宝座上，向人们祝圣，并按照惯例举行了宴会，教会的要人和民兵的优等人都出席了宴会。不幸的是，因不明原因耽搁的克里斯托弗斯（Primicerius Christophorus）先于罗马抵达。罗马方面立即拿起武器；他们的领袖查图拉里乌斯-格拉蒂奥苏斯（Chartularius Gratiosus）随后迫使篡位者菲利普返回修道院。

第二天，也就是 8 月 1 日，克里斯托弗斯以教皇代理婚姻的身份召集神职人员和民众举行集会：集会地点还是旧广场上的_in tribus fatis_，在帝国末期，民众集会曾多次在这里举行。普里米塞里乌斯提名长老斯蒂芬为候选人。这位红衣主教是西西里人奥利维斯之子，曾是保罗一世最热情的支持者之一，只有他在保罗一世临终时没有离开他。他被从特拉斯提维尔的圣凯西莉亚教堂带走，在拉特兰宣布斯蒂芬三世即位。

罗马所陷入的野蛮状态在最狂热的复仇欲望中显露无遗。被囚禁的主教和红衣主教的眼睛和舌头被挖了出来；篡位者君士坦丁像一个嘲笑者一样被带着穿过罗马，带到了阿汶丁山的塞拉诺瓦修道院。8月6日，主教会议废黜了他，斯蒂芬三世被祝圣。

杀害托托的凶手格拉蒂奥苏斯（Gratiosus）后来成为了军队或某个城市的副官作为奖赏，他率领战士们对所有被推翻党派的支持者进行了猛烈的攻击。他们中的一个人，阿拉特里的护民官格拉西利斯（乡下城镇也有军事护民官），仍然坚守在这座由基克洛皮古城墙加固的城市里，直到它被攻破。这个拉丁山地国家的乡下人匆忙赶到罗马，把护民官从监狱里拉了出来，并在斗兽场弄瞎了他的双眼。不久之后，格拉蒂奥苏斯进入了塞拉诺瓦修道院，君士坦丁在那里遭到了拜占庭式的残杀。

现在，罗马人的复仇目标转向了伦巴第人瓦尔德佩特，他曾帮助推翻君士坦丁，但却让菲利普坐上了教皇的宝座。有人爆料说他想把罗马出卖给斯波莱托公爵。瓦利珀特徒劳地抓着万神殿中的圣人像，他在那里寻求庇护；他被投入可怕的监狱，并被残酷地处死。

正是在这样的暴行下，斯蒂芬三世开始了他短暂的教皇生涯。他是在违背德西德留斯的意图、与他彻底决裂的情况下成为教皇的。因此，他立即求助于法兰克王公，要求他们从本国派遣主教到罗马，他必须在罗马召开会议。塞尔吉乌斯（Sergius）本人，也就是现在的塞孔迪切里乌斯（Secundicerius），将教皇的信带到了法国，但他没有在皮平的生前找到他。这位大名鼎鼎的国王于 768 年 9 月 24 日去世，他的王国被他的两个儿子瓜分。已经是罗马贵族的查理和查尔斯接待了斯蒂芬的信使，并向罗马派出了十二位主教，其中包括兰斯的图尔平。

4月12日，斯蒂芬三世召开了拉特兰宗教会议；会议讨论了对君士坦丁的谴责、对他所授的圣职的调查，最后制定了教皇选举规则。瞎了眼睛的君士坦丁被带到了第一次会议上。他被问到为什么敢以一个普通人的身份登上彼得的宝座。"罗马人民，"这个不幸的人回答道，"因为他们曾经在教皇保罗一世的手中遭受过压迫，所以他们用武力把我推上了教皇的宝座。他摊开双手，趴在地上乞求宽恕。他没有受到审判就被释放了。第二天，审讯继续进行。被告巧妙地以拉文纳的塞尔吉乌斯和那不勒斯的斯蒂芬等几位主教为榜样，他们也都是从教友直接升任主教的。这一事实激起了法官们的愤怒：教士们扑向君士坦丁，将他打倒并扔到教堂门外。他的结局被蒙上了一层阴影。

随后，宗教会议焚毁了假教皇的记录；会议决定，从今以后，没有从教会最底层晋升到执事或长老-枢机主教级别的人不得被提升为教皇。教友被排除在教皇选举之外，他们只有鼓掌的权利。关于君士坦丁按立的主教，会议决定，所有以前曾担任过长老或执事的人都应再次降级，但如果他们已受到其教众的爱戴，则可在重新选举后在罗马接受祝圣。大公会议结束时颁布了关于维持对圣像崇拜的法令。会议法令签署后，游行队伍前往圣彼得大教堂，在那里宣读了各项决议。至此，斯蒂芬三世肃清了教会中的僭主，但却没有巩固他在罗马的教皇权力。
