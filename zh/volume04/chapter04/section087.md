_3\. 查理曼对意大利的军事行动。围攻帕维亚。查理曼在罗马庆祝复活节。皮皮尼捐赠的确认。774年帕维亚和伦巴第王国灭亡。

查理再次敦促德西德里乌斯国王求和，并向他提出了赔偿，但没有成功，于是他于773年9月率军前往意大利。他途经日内瓦，准备越过西尼山，但阿尔卑斯山口已被伦巴第人封锁，难以进入，可能还有弗兰肯人的怨言，这促使查理再次通过使节向国王宣布，只要有三个德高望重的人质向他保证归还城市，他就心满意足了。德西德留拒绝了这一提议。但是，他的儿子阿德尔吉斯（Adelgis）因惊慌失措而突然出逃，叛徒们又使法兰克军队得以越过阿尔卑斯山，这一切迫使他不得不放弃营地，把自己关在帕维亚。阿德尔吉斯和奥查尔惊慌失措，带着卡尔曼的遗孀和儿子们投奔了坚固的维罗纳城。阿尔博因的子民在进行了微弱的抵抗后就倒下了，由于内部不和，尤其是司铎的阴谋诡计，抵抗的时间缩短了。不是伦巴第人的失败让查理获得了 "大帝 "的称号，但历史上几乎没有其他任何征服能如此轻松地取得成功，并产生如此巨大的影响，持续数百年之久。

查尔斯国王包围了帕维亚城；他预感到围城将旷日持久，于是派人去找他的妻子希尔德加德和他的孩子们。另一支法兰克军队出现在维罗纳城前，奥夏和卡尔曼的遗孀很快就带着小王子们投降了胜利者。帕维亚英勇地保卫了自己六个月；复活节即将来临，查理曼决定在罗马庆祝复活节。对当时的信徒来说，前往殉教者陵墓朝圣似乎是通往天堂的最可靠途径；两个世纪以来，朝圣者在复活节期间一直涌向罗马，在整个中世纪，我们经常可以看到皇帝和国王在罗马庆祝复活节。德国国王前往罗马的悠久历史始于法兰克国王。

查理带着他的一些军队和由主教、公爵和伯爵组成的豪华随从，从帕维亚的营地出发。他迅速穿过托斯卡纳，于复活节周日晚上（774 年 4 月 2 日）抵达罗马。教皇派出了所有的法官和民兵旗帜在 24 英里外迎接他，他们在布拉奇亚诺湖下的诺瓦斯车站迎接他，并护送他进城。在马里奥山脚下，迎接他的是所有带着赞助人的民兵队伍、手持棕榈树和橄榄枝的儿童学校，以及无数的民众，他们一见到查尔斯就举起赞美之词，喜庆地高呼："向法兰克人的国王和教会的捍卫者致敬！" 编年史作者明确指出，按照迎接大主教的惯例，甚至连罗马大教堂的十字架和旗帜都派人来迎接他。查理下马时还没来得及看到这些东西，就在随从的簇拥下谦恭地向圣彼得大教堂走去。那是复活节周日傍晚的清晨；教皇正在门廊的台阶上等待他的客人，周围站满了神职人员，广场上则是人山人海。查尔斯跪在楼梯最底层的门槛上，双膝跪地爬上楼梯，虔诚地亲吻每一级台阶，直到走到教皇面前。世界上最强大的王公们就是以这样的姿态走近罗马圣殿的。当国王们沉沦为教皇的附庸和奴仆，当他们大胆地将福森的双脚放在教皇的脖子上时，难道不是时候到了吗？查尔斯和哈德良相互拥抱；国王牵着教皇的右手，向右边的大殿走去。他们一进门，司铎们就高声诵经： 查理和他的弗兰肯人匍匐在使徒墓前。完成虔诚的礼拜后，国王亲切地请求允许他进入罗马参观其他主要教堂；他们都事先下到了使徒墓前，国王和教皇、罗马人和弗肯兰人的法官们相互宣誓保证安全。

毫无疑问，查理让他的军队在尼罗河畔安营扎寨，但他本人却穿过哈德良桥进入了这座城市，而这座城市并没有意识到，第一位进入它的法兰克国王也将迎来日耳曼民族的第一位皇帝。奥古斯都未来的继任者带着无知的惊讶看着他走过的古典废墟，因为尽管他喜欢听古人讲历史，但他对罗马圣人的事迹比对罗马政治家和英雄的事迹了解得更多。当时的罗马虽然经历了三个世纪的荒凉，但仍带有古老的印记。它仍然是古罗马人的城市，一个由辉煌废墟组成的巨大世界，在它的宏伟面前，基督教的一切都消失了。

罗马人把国王领到了拉特兰教堂；他们自己也惊奇地注视着教会保护者近乎巨大的英雄形象，或是他那身披矿石的野蛮骑士。他在洗礼堂参加了教皇主持的洗礼仪式，然后谦恭地徒步返回圣彼得大教堂。他没有在城中居住，凯撒宫也不再有任何提及，自从希腊王后离开后，凯撒宫最后的可居住部分也已破败不堪。查尔斯无疑是住在使徒彼得的主教公寓里。复活节周日，他在选帝侯和民兵的护送下来到圣玛丽教堂（马焦雷），教皇在那里做了弥撒。随后，他在拉特兰的餐桌前用餐。周一，他参加了在圣彼得大教堂举行的庆祝活动，周二在圣保罗教堂，复活节的活动就此结束。与今天相比，这些庆祝活动在古代没有那么华丽，也更具有教会性质，但正如古老的礼仪书籍所证明的那样，并没有简单多少。

4 月 6 日星期三，查尔斯应邀参加了在使徒彼得教堂举行的会议，教皇与所有神职人员和民兵的法官都出席了会议。在这次会议之前，哈德良向法兰克国王发表了讲话，没有比使徒陵墓及其大殿附近更适合向他捐赠的地方了，那里仍然弥漫着复活节的香火气息。由于预见到伦巴第帝国即将灭亡，教皇作为其主要继承人之一行事；因此，他提醒查理注意将意大利的某些城市和省份赠与圣彼得的旧条约和誓言，最后他还宣读了皮皮尼安文件。哈德良的传记作者向我们保证，国王和他的法官们不仅确认了文件的内容，而且查理还让他的公证人埃瑟里乌斯重写了文件。这份文件被放置在使徒彼得的墓中，并以 "可怕 "的誓言宣誓。

根据哈德良的传记作者的说法，这份所谓的查理曼大帝的捐赠是对基尔齐的皮平捐赠的确认，但这份捐赠也从拉特兰的档案中消失了，据说国王随身携带的副本也没有在德国或法国找到。根据该副本，这位宽宏大量的君主几乎将整个意大利都送给了教皇，包括他从未征服过的省份，如科西嘉、威尼斯、伊斯特拉和贝内文托公国。哈德良的传记作者在世时，要么发现这份文件是伪造的（如果他亲眼看到的话），要么自己伪造了文件中的信息。皮平的捐赠无法从文件中得知，但无疑与主教辖区有关，显然得到了查理的确认，他为自己保留了这片土地的主权，并通过遗产和收入逐年增加。与此同时，他自己与罗马的关系地位也通过一项条约得以确立：他要求获得帕特里西乌斯的所有权利，从 774 年起，"防御者"（Defensor）这一光荣称号被赋予了更充分的含义：罗马、杜卡特、主教辖区各省的最高管辖权被授予罗马人的帕特里西乌斯。教皇在这些土地上只接受行政管理，成为了弗兰肯国王的臣民。

与罗马的关系解决后，查理就离开了，而教皇则下令在所有教堂祈祷，以加快围攻帕维亚的成功。回到营地后，法兰克人的国王奋力追击；瘟疫也与被围城中的叛徒串通一气，帕维亚于 774 年 6 月投降。最后一位伦巴第统治者以王朝和帝国的灭亡为自己的鲁莽行为赎罪；他作为俘虏无条件投降。德西德留斯在科比修道院结束了自己的生命，正如人们所说，他是一个虔诚的奇迹创造者。然而，查理却戴上了铁冠，从 774 年起自称为弗兰肯和伦巴第国王，罗马人的帕特里修斯，而德西德乌斯的逃亡之子阿德尔吉斯则匆匆来到拜占庭宫廷，开始了一个王位请求者的悲惨生活。
