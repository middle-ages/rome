### 261. 1265年的克莱门特四世教皇

*3. 1265年的克莱门特四世教皇. 他鼓动查理征服西西里. 曼弗雷德的备战. 归尔甫在罗马处境艰难. 查理离开法兰西胜利登陆. 进入罗马. 被逐出拉特兰宫. 接管元老院. 接受教皇使节的授职.*

乌尔班死后，枢机主教们好不容易才达成了共识。他们中具有爱国思想的一方希望与曼弗雷德和解，并希望阻止普罗旺斯对意大利的入侵：这是弥足珍贵的时刻，因为他们作出的决定关系到未来晦暗不确的命运，不仅对意大利，也是对教廷。这种时候本来应该有一个天才可以将教会从迷宫中解救出来，可惜没有这样的人出现。归尔甫派和法兰西派占了上风；选举结果最终落在了一个普罗旺斯人身上，一个安茹查理的臣民，乌尔班四世的政策就这样得到了继承和延续。朗格多克(Languedoc)地区圣吉尔的圭多·勒格罗斯·富尔科迪(Guido Le Gros Fulcodi von St. Gilles)以前只是个普通人，是他之前合法婚姻中几个孩子的父亲。富尔科迪曾是一位有声望的律师(Advokat)，是法兰西国王路易九世内阁的秘密成员。妻子去世后，他厌倦了这个世界，遁入卡尔特会修道院(Kartäusermönch)，先后成为普伊主教(Bischof von Puy)和纳博讷大主教(Erzbischof von Narbonne)，1261年被乌尔班四世提升为圣萨比纳(S. Sabina)枢机主教，现在于1265年初被选为教皇。当接到自己当选的消息时，他人已经回到法国去了，因为惧怕吉伯林派报复，所以他的当选是秘密进行的。这位心思缜密的老人没有野心，饱经风霜的生活和这种生活形成的哲学浇灭了他的任何野心，此时他对接不接受头衔犹豫不决。然而，他去了佩鲁贾之后，屈服于那边枢机主教们的殷切恳求，最终于1265年2月22日在翁布里亚大教堂被祝圣为克莱门特四世。

新教皇别无选择，只能继续其前任的工作，希望对付曼弗雷德这件事快点有个了结。他批准了查理的选举；命令使节西蒙加快缔结条约，号召路易九世国王支持弟弟的事业，把十字军东征的誓师转变为与曼弗雷德作战的誓师。金钱是这项事业的支柱，而筹集资金却很困难。基督教世界的枢机主教团已经被罗马榨干了，但法兰西教会以什一税的形式继续提供十字军东征的花费，乌尔班四世已经索要三年了。甚至连不情不愿的英格兰和苏格兰的主教也被催促缴纳同样的税收。克莱门特四世和他的前任一样，向整个欧洲征税，以便维持教皇对西西里的封建统治；不过，他没有得到贪财和裙带的骂名，对这两种恶习他都很陌生。

查理的妻子以她的珠宝手饰作抵押，向法兰西的贵族们乞讨，并接受高利贷。冒险家们扛起了十字架的责任，而法兰西的骑士们，渴望拥有领地，也准备参加这次远征，号称能在世界上最富庶的国家拥有城市和郡县。在法兰西为这项事业奔忙的同时，曼弗雷德也采取了措施，准备在意大利与他们对决。对于查理走陆路的军队，曼弗雷德希望即使不关闭阿尔卑斯山的通道，至少也要在伦巴第为打败他们做好准备，在那里，帕拉维奇诺、博索·德·多拉(Boso de Doara)、兰齐亚藩侯、安格拉诺的乔丹和一些友好城市为他发布了军队招募令(Heerbann)。一支西西里与比萨的帆桨战船(Galeere)组成的舰队将从海上阻击敌人。托斯卡纳仍然在曼弗雷德的控制之下；曼弗雷德的代执，行宫伯爵(Pfalzgraf)圭多·诺韦洛(Guido Novello)，以国王名义管理着强大的吉伯林城市联盟，卢卡1264年夏天加入了该联盟；教皇则通过阿雷佐主教威廉的活动，企图把放逐的归尔甫联合在一起，信誓旦旦但收效甚微。维科的佩特鲁斯和安尼巴尔迪控制了罗马的伊特鲁里亚区域；沿海地区设有守卫，曼弗雷德甚至封锁了台伯河河口。他下令动员了王国的所有人，收编了非洲的萨拉森人，在德意志也组建了部队，加固了坎帕尼亚的堡垒，并推进到拉丁姆边境，以威胁罗马，在罗马附近，曼弗雷德的军队与雅科波·拿波利·奥尔西尼(Jacopo Napoleon Orsini)领导的罗马吉伯林人一起，占领了维科瓦罗(Vicovaro)的要塞，这是瓦莱利亚大道的隘口，其他人则在各自堡垒中等待机会，以便能强行进入城市打击敌人。

罗马城里的归尔甫越来越不耐烦了。他们的元老之前保证要在五旬节(Pfingstfest)时到罗马的；然而是否能来现在却受到了质疑。查理的代执甘特尔米(Cantelmi)捉襟见肘，以至于都受到了蔑视。“罗马人”，克莱门特在给查理的信中说道，“名声显赫，性格傲慢，他们召唤你来管理这座城，渴望目睹你的面庞；请一定审慎仔细，因为罗马人（教皇讽刺地补充道）期待他们的领袖要有威严的外表，铿锵的演讲和雷厉的行动，认为这样才是主宰世界的样子。我对你的代执甘特尔米及其陪同人员表示赞赏，但他们人数太少，寡廉鲜耻，降低了你们的威望。”甘特尔米有一天撬开了拉特兰的金库，拿走了能找到的所有东西；沦落在佩鲁贾的克莱门特都提出了抗议，解释说没有义务自掏腰包为查理伯爵维持罗马的治安；尽管如此，他还是从托斯卡纳和翁布里亚城市的银行贷款，每天被普罗旺斯人和罗马人折磨着提供资金。城市变得越来越不满；被流放的吉伯林人秘密潜回城市并制造骚乱；安全问题已经到了爆发的边缘；抢劫和谋杀猖獗；街道被设置各种路障。归尔甫给教皇写了紧急信件，恳求他催促查理赶紧到来；如果查理再拖延，他们的财产就会被剥夺，他们日夜守卫，已疲惫不堪，无法再保卫城市。痛苦的教皇劝说他们保持忍耐，哀叹自己既没有金钱也没有武器，他声称依靠法国教会的资助，能保证伯爵会迅速抵达。与此同时教皇恳求查理赶快行动，因为罗马有可能重新回到敌人手中；查理终于宣布了他的到来。查理的骑士费雷里乌斯(Ferrerius)带着一队普罗旺斯人安全抵达；这个加斯科涅指挥官在维科瓦罗(Vicovaro)对吉伯林人进行了一次愚蠢的攻击，结果被俘虏了，押送到曼弗雷德的营地。因此，法兰西人的第一次行动很不幸，而这个对西西里有利的预兆大大提高了军队的士气。可怜的查理伯爵成了人们嘲笑的对象；有人断言，如果他到达罗马，就等于进入坟墓。

安茹查理对西西里的远征属于这一时期的冒险活动和比较成功的十字军东征，都主要由法兰西人主导。西西里的第一批征服者是从诺曼底出发的；查理的先祖威廉公爵就是从那里出发攻占了英格兰。第一次和最后一次十字军东征也是法兰西完成的：是法兰西骑士攻克的拜占庭。查理在东方的十字军表现中很抢眼（在曼苏拉(Mansura)和他的哥哥一起被俘），他想通过加冕王冠来实现自己的野心，摆脱自己的贫困和债务。没有任何事情能阻挡这位亲王向一位从未伤害过他的国王开战；在自己和那些嗜血的普罗旺斯人眼中，这项事业完全是骑士精神的体现，是十字军东征的延续。教皇本人则将他与皮平的儿子查理[^1]相提并论，后者曾经也是从法兰西出发来到这里拯救教会。似曾相似的情形让人想起教皇曾召唤法兰西国王到意大利，把他们从伦巴第人的枷锁下解救了出来。然而，在查理大帝的时代，以神圣的十字军东征为名，对一个基督教国家进行征服，显得对神不敬。安茹查理阴冷地出现在罗马人和日耳曼人的古老战场上，就像大将纳尔塞斯(Narses)出征一样，而曼弗雷德则呈现出托提拉(Totila)的悲情。历史重复了一个循环；因为尽管国家之间关系不同，但形势却基本上是相同的；教皇召唤一个外国征服者进入意大利，想把自己从德意志人的枷锁中解救出来。士瓦本王朝的灭亡就像东哥特人曾经的王朝。在同一个熟悉的舞台上，两个国家及其英雄的覆灭给历史带来了双重悲剧，第二个悲剧似乎只是第一个一模一样的复制。

安茹伯爵把大部分军队留在了普罗旺斯，这些军队要穿过北意大利，他自己则于1265年4月在马赛登船。他的远征，盲目伴随着蛮干。在皮萨诺港(Porto Pisano)的海岸上，风暴吹得他只剩下三艘船，这场风暴也吹散了曼弗雷德的舰队，当圭多·诺韦洛以曼弗雷德的名义在比萨指挥德意志骑兵前去抓捕查理时（如果及时赶到，他肯定会抓到查理的），查理已再次起航。查理奇迹般地接近了敌人的舰队，并成功地驶过了阿根塔罗角(Kap Argentaro)和科尔内托(Corneto)。

电闪雷鸣中查理终于发现自己正对着罗马的奥斯提亚(Ostia)海岸。海水很高，登陆困难，这片海岸还没被登陆过；没有人知道该怎么办。然而，查理果断地跳上一条小舟，穿过汹涌的波涛，成功冲上了对岸。奥斯蒂亚的守卫没有抵抗，敌人也没有出现。在欢呼声中查理被迎到了圣伯多䘵大教堂；1265年5月21日，即圣灵降临节前的星期四，他在教堂前下了车，准备进入罗马。他的帆桨战船很快就到达了台伯河口；河中的路障都被拆除了，整个普罗旺斯舰队沿着河道前进到了没有城墙的圣伯多䘵教堂。

罗马人挤在一起观看未来的西西里国王，同时也是他们选出的元老。查理是一个四十六岁的男人，有着强健的体魄和王者的风范。他橄榄色的脸庞严厉而坚毅；他令人敬畏的目光冷峻而锋利。粗犷的天性中蕴藏着一颗不安分的灵魂；他哀叹睡眠缩短了人类行动的时间，他几乎从来不笑。所有能使一个雄心勃勃但缺乏天份的人成为暴君和征服者的特质，查理都最大程度地具备了，因此他是被精心挑选的，最合适教皇的工具。

圣灵节的前夕（5月23日），他从圣保罗门进入罗马。随身只带了1000名没有战马的骑士，受到了教士、市民、贵族和骑士的热烈欢迎。为了向他们的元老表示敬意，罗马装扮了特别的装饰，还举行了战舞；唱着感恩的歌来赞美查理殿下。同时代的人断定，自人类有史以来，罗马人从未以如此隆重的方式迎待过任何一位他们的统治者。新的元老在普罗旺斯人的簇拥下，骑马穿过了盛装华饰的城市，但穷人在地上找不到一个第纳尔(Denar)，因为司库(Kämmerer)根本没撒钱。安茹伯爵空着手来到罗马，他不但没有给人民送礼，反而让归尔甫朝他进贡。

按照王室的惯例，查理在圣伯多䘵教堂的宫殿前下的车，他没有多说什么就直接在拉特兰宫住下了。克莱门特对这个客人的无礼行为表示诧异，查理甚至没有申请许可，就在教皇的宫殿里住下了。教皇给查理写了一封引人侧目的信。“你大胆地做了任何一件基督教国王都不会允许自己做的事。和所有应当的礼节相反，你的追随者在你的授意下进入了拉特兰宫。你应当知道，这座城市的元老，无论身份多么显赫和尊贵，在教皇的宫殿中居住，对我来说都是非常不明智的。我希望能避免将来可能的肆意妄为：教会的优先权不能被任何一个人冒犯，特别是你，我们已经把你抬到了非常尊崇的位置。希望你不要让这个决定看起来是错误的。你可以到城里的其他地方去住，那里有很多宽敞的宫殿。此外，不要说我们粗暴地把你赶出了宫殿；相反，我们非常注意维护你的尊严。”于是伯爵离开了拉特兰宫，并提醒自己，他不过是教皇的一个玩物。他也没有住进元老院的元老殿，那里住着他的代执，而是住在科利安(Coelius)的四冠圣徒殿(Vier Gekrönten)。

6月21日，查理在阿拉塞利修道院(Kloster Aracoeli)接受了元老权杖(Insignien)，他立即在硬币上印上了自己的名字，让人们记住这一事实。根据罗马的法规，他带来了自己的法官；还保留了在元老院的代表(Stellvertreter)，因为他自己需要处理比城市管理或公民诉讼更重要的事务。拥有元老院的职位无疑是一个不可估量的优势，很快他就让人明白，他准备作为罗马共和国的主权领袖来行使职权，就像布兰卡利奥内曾经做的那样。教皇精准地觉察到伯爵超越职权范围的行为；于是回应了查理的试探，即查理只能获得早期元老那样的权利，并告诉查理，召集他不是为了效仿前任的不体面行为，也不是为了篡夺教会的权利。

给查理举行授职是6月28日举行的。四位枢机主教，即十二使徒的安尼巴尔多、圣安杰洛的理查德(Richard von S. Angelo)、圣尼古拉的约翰(Johann von S. Niccolò)和科斯米丁(Cosmedin)圣玛丽亚的雅克波(Jacobus von S. Maria)，受教皇委托在拉特兰的教堂大厅(Basilika)进行了授职。伯爵在他们手中宣誓对教会效忠，并接受了圣伯多䘵的旗帜作为授职象征。克莱门特起初试图在这种苛刻的条件下将王国强加给他，使伯爵仅仅承担一个临时雇佣的仆人(Dienstmann)角色。然而，经过艰苦的谈判，查理得以提出更有利的条件。在承诺完全豁免神职人员义务的条件下，为了回报支付的8000盎司和偿还的预付款，查理将获得一个完整的意大利王国（贝内文托除外），作为教会分封给家族的世袭领地。他再次发誓，一旦征服了阿普利亚，就把罗马的权力交还教皇手中。

查理从此就自认西西里的国王，尽管磨磨蹭蹭的教皇在11月4日才批准了这一任命。而在7月份，查理就发布了皇家法令，会于1265年10月14日下令建立一所大学，作为他罗马元老权力的永久纪念碑，他是被上帝召唤而来的，为了这座荣耀之城的利益。然而，现在要迈出关键的一步；在纸面上获得的王国需要实际征服，而无数的障碍似乎都使这项任务根本无法完成。

[^1]: 即[查理大帝](https://zh.m.wikipedia.org/wiki/%E6%9F%A5%E7%90%86%E6%9B%BC)(742-814)，曾统一了欧洲。丕平是加洛林王朝创建者[丕平三世](https://zh.m.wikipedia.org/zh/%E7%9F%AE%E5%AD%90%E4%B8%95%E5%B9%B3)，也叫矮子丕平。
