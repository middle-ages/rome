###266.康拉丁在意大利北部的糟糕处境

*2.康拉丁在意大利北部的糟糕处境。他到达帕维亚。查理在维泰博觐见教皇。逐出教会诏书 康拉丁在比萨受到接见。查理攻打罗马失败。康拉丁的第一次胜利 进军罗马 华丽的接待 吉贝林首领。离开罗马。塔利亚科佐之战。康拉丁的胜利和失败*。


与此同时，在维罗纳，康拉德想方设法为他的军队提供粮食，与各城市结盟，使进军托斯卡纳成为可能。他所面临的困难并不亚于查理。他的一些未领军饷的部队离开了他；他的叔叔路易和继父迈哈德（Meinhard）欠了他一大笔钱，被迫抵押了他的遗产，他们于 1268 年 1 月返回了德意志，霍亨斯陶芬党的事务也把他们召回了那里。康拉丁坚定不移地克服了如此巨大的困难，证明他无愧于祖先。事与愿违，他成功地穿越了敌人的领土，就像查理的陆军之前成功穿越意大利一样。整个努力似乎是查理的回归，他现在不得不采取曼弗雷德的态度。康拉丁于1268年1月20日抵达帕维亚，在那里一直困惑不解，直到3月22日才离开。

查理急不可耐地想要与他会合；经过长时间的围攻，他已经使吉贝尔人的主要城堡波吉邦西投降，甚至还迫使比萨求和；如果他能在康拉丁到达罗马之前迫使他进行一场野战，那么他就有可能结束波河上的战争。但教皇因害怕失去西西里而备受煎熬，因为光明起义已经夺取了卡拉布里亚、阿普利亚和阿布鲁佐，教皇劝他回到自己的王国；因为如果他失去了王国，就不要指望教会会再次为他承担西西弗的任务；相反，教会会让他在普罗旺斯流亡，自取其辱。国王看到自己的王国在身后陷入一片火海，于是带着一些部队返回了家乡，在托斯卡纳留下了约翰-德-布雷塞尔维（John de Braiselve）内廷大臣。4 月 4 日，他来到维泰博觐见教皇。

在这里，他重申了对巴伐利亚的康拉丁和路德维希二世、蒂罗尔伯爵以及吉贝尔派所有首领的开除教籍命令；他甚至还对那些曾经或将要庇护敌人的国家和城市下达了禁令。比萨、锡耶纳、维罗纳和帕维亚受到了禁止教务的打击；卡斯蒂利亚的亨利、蒙特费尔特罗的圭多一世、元老宫的地方官、所有接受过康拉丁信使的罗马人都被逐出教会；城市受到了禁止教务的威胁，而罗马人则被解除了对元老的誓言，查理被授权，如果他在一个月内不屈服，就可以再次接管城市政府，为期十年。

当这些禁令在维泰博宣布时，比萨响起了无数的欢呼声：腓特烈二世的年轻孙子乘坐共和国的船只，带着五百名骑士，兴高采烈地驶进了比萨港。康拉丁从帕维亚出发，途经腓特烈一个亲生女儿的丈夫卡雷托侯爵的领地，抵达萨沃纳附近海边的瓦多利古雷，并于 3 月 29 日登船。他把部队托付给了巴登的腓特烈，腓特烈愉快地率领部队翻越蓬特雷莫利的山脉，穿过卢尼贾纳，于 5 月初到达比萨。在这个共和国，年轻的王位请求者第一次得到了庄严的承认，并拥有了一支装备精良的舰队，可以驶向罗马或意大利南部沿海。查理不确定康拉丁的下一步计划，现在决定返回王国，以便制服那里的叛军，尤其是卢塞拉的萨拉森人，并像曼弗雷德曾经做的那样，在自己的国家等待敌人的进攻。他仍然试图从维泰博发动一场针对罗马的政变；他的一些部队与流亡的安圭拉家族，包括安圭拉伯爵和马特乌斯-鲁贝乌斯-奥尔西尼家族，甚至进入了罗马城，但被元老打了出来；这说服了查理放弃罗马。4 月 30 日，在教皇任命他为帝国在图西亚的总督后，他离开了维泰博；这一尊荣和元老院的更新都是重要的让步，在未来会给他带来丰硕的成果。

现在，康拉丁在比萨和锡耶纳得到了强有力的支持；元老宫的使节召他去罗马，加尔瓦诺在那里等着他，元老的支持来源保证他的实力会得到稳固的增长。费尔莫和马尔凯正处于叛乱之中；只要再取得决定性的优势，意大利大部分地区都会宣布支持康拉丁。在这位年轻王子的眼皮底下，一支舰队在比萨装备完毕，将与弗雷德里克-兰齐亚、理查德-菲兰吉里、马里诺-卡普斯和其他勇士一起出海攻打那不勒斯和卡拉布里亚；这确实发生了，因为这些吉卜赛人在八月攻打了伊斯基亚岛。康拉丁本人于 6 月 15 日从比萨迁往锡耶纳，在那里一直待到 7 月中旬，受到了这座富裕城市的市民们的热烈欢迎，并愉快地得到了战争资金的支持。6月25日，他的部队在瓦莱桥（Ponte a Valle）取得胜利，查理的内廷大臣约翰-德-布雷斯维尔（John de Braiselve）在那里被俘，这使他的希望大增。通往罗马的道路已经畅通无阻。克莱门特四世只是从佩鲁贾和阿西西调集部队到维泰博保护他，并等待最后一批霍亨斯陶芬人通过。他曾徒劳地警告罗马人不要离开教会；他的信中第一次透露出他的严重忧虑，但恐惧也没有动摇这位司铎。他说，康拉丁会像烟一样逝去，他还把康拉丁比作吉贝尔人正引向屠杀的羔羊。从维泰博的城墙上，他可以看到战士们的队伍于 7 月 22 日穿过图斯卡尼亚附近的平原，而不会对他造成威胁。

康拉德在卡西亚道上前进，途经韦特腊拉、苏特里、蒙特罗西，经过老维吉前往罗马；五千名装备精良的骑兵紧随其后；与他同行的有巴登的康拉德、比萨的格哈德-多拉蒂科伯爵、安条克的康拉德、意大利的许多吉贝尔派首领。青年醉眼朦胧的目光从马里奥高地游移到罗马的大坎帕尼亚，坎帕尼亚庄严肃穆地铺展开来，被一排排美丽的山峦框住，闪闪发光的台伯河横穿而过，台伯河沿着残破的土瓦山流向米尔维亚桥，而天空的蓝色球体似乎喜庆地停留在高耸入云的罗马上。在萨比纳山麓，人们看到了提布尔白色的房屋；康拉丁被告知，那里是腓特烈和曼弗雷德行军的剧场。他看到了远处古老的普拉内斯特：仅仅五个星期后，他就带着锁链坐在了那里的凯克洛奔堡上！在阿尔班丘陵和亚平宁山脉之间闪现出一片宽阔的塔勒，他看到了拉丁姆的气候，并被告知这是安茹的查理通往里里斯的道路。

帝国一长串的皇帝呈现在康拉丁激动的灵魂面前，而这座城市的崇高形象，就像罗马人民的壮丽景象一样，从米尔维奥桥到凯旋大道的斜坡上，到处都是罗马人民欢迎他的呼喊声，这让他像奥托二世或三世曾经那样欣喜若狂。 元老给了他帝国般的接待，而根据圭尔夫-马里斯皮纳的供述，罗马是一座天生的帝国之城。罗马人经常顽强地与日耳曼皇帝作战，但帝国的理念仍然对他们施展着魔力。他们真诚地欢迎伟大的腓特烈的孙子成为帝国权力的合法代表。所有能够拿起武器的罗马人都头戴钢盔，成群结队地在尼禄战场上等待着他，人们挥舞着鲜花和橄榄枝，唱着欢快的歌曲。7 月 24 日，当康拉丁通过天使桥从要塞大门进入罗马时，他发现罗马变成了一片欢庆胜利的景象。街道上人头攒动，家家户户都用绳子吊着地毯、稀有服装和珍贵珠宝，罗马妇女合唱团在齐瑟和凯特鼓声中跳起了民族舞蹈。圭尔夫-马拉斯皮纳承认，查理曼的接待远不及康拉丁受到的欢庆。罗马吉卜赛人是出于自由的感情才向他表示敬意的。这个狂热的男孩在人间的辉煌之巅站立了一分钟。

他被带往元老宫，被誉为未来的皇帝。他要么住在那里，要么住在拉特兰宫殿。阿普利亚流亡者吉贝尔派的首领们蜂拥而至，要求得到未来的封地。曾经被查理或教皇大赦过的罗马贵族们也加入了他的行列。无原则的维科的彼得，先后是曼弗雷德和查理的支持者，出现在元老宫向他致敬；雅各布斯-拿破仑-奥尔西尼家族提供了真诚的服务。年轻的理查德和其他安尼巴尔迪家族、圣尤斯塔奇奥的阿尔切鲁乔伯爵、诺曼人斯蒂芬、阿洛蒂家族约翰、苏尔迪家族、曼弗雷德时代忠诚的吉贝尔家族带来了金钱和武器，而元老则在准备最后的盔甲准备离开。其他的奥尔西尼家族和安尼巴尔迪家族，萨维利家族全体成员仍然站在查尔斯一边，弗兰吉帕尼、科隆纳和孔蒂则在自己的城堡里静候事态的发展。

在查理曼冒险之后仅仅两年，一个奇怪的转折使罗马成为了征服阿普利亚的中心，并使这个篡夺者处于曼弗雷德的位置。然而，从塞普拉诺到卡普亚的防线防御得更好；因此罗马的战争委员会决定，有必要从瓦莱里亚河进入阿布鲁佐；他们想一直推进到苏尔莫纳，在卢塞拉与萨拉森人联合，全力攻击他们仍然认为在那里的敌人。这个计划完美无瑕。

8 月 18 日（1268 年），康拉丁从罗马出发，蒙特费尔特罗家族的圭多一世作为元老院的助理司祭留守罗马。与他同行的还有卡斯蒂利亚的亨利和几百名西班牙人、巴登的腓特烈、加尔瓦诺、安条克的康拉德以及其他大人物。这支装备精良的军队约有 10,000 人，他们的勇气令人欢欣鼓舞。罗马人远远地跟随着圣劳伦斯门外的行军队伍，全城的民兵都想加入军队，但康拉丁在行军两天后就遣散了其中的大部分人；只有吉贝尔家族的首领带着他们最精锐的部队留在了他身边，他们是圣尤斯塔奇奥的阿德奥西奥、斯特凡-阿尔贝蒂、年迈的约翰-阿德奥达托二世、雅各布-奥尔西尼家族的儿子年轻的拿破仑、里卡德鲁斯-安尼巴尔迪、彼得-阿洛蒂家族和维科。他们沿着阿尼奥河经蒂沃利来到维科瓦罗，吉贝利家族的奥尔西尼在那里招待了康拉丁。他们经过萨拉奇内斯科，加尔瓦诺的女儿、安条克的康拉德的妻子在这里迎接了她的皇室表亲。这座岩石城堡位于 X. 属于康拉德，因为他的父亲安条克的腓特烈从一位高贵的罗马女子玛格丽塔那里获得了它作为嫁妆。两个被俘虏的奥尔西尼家族还在那里，康拉德很快就会因此获救。

与此同时，查理得知康拉德抵达罗马并即将进军王国的消息后，解除了对卢塞拉的围困。他向福贾进军，并从那里向福基诺湖推进；8 月 4 日，他已经到达阿尔巴和斯库尔科拉马尔西卡纳。然后，他在富奇诺湖和拉奎拉之间来回行军数日，不确定敌人打算走哪条路去阿普利亚，而他自己又必须堵住哪条路。想到康拉丁会试图通过拉奎拉渗透到苏尔莫纳，他再次向奥维努洛和拉奎拉推进。然而，康拉丁的军队在里奥弗雷多（Riofreddo）越过了崎岖的边境地带，穿过了卡索利（Carsoli）的山口，下到了萨尔托河的塔勒。这里是马尔森地区，高耸的维利诺峰和其他山峰位于富奇诺湖之上。周围有塔利亚科佐、斯库尔科拉马尔西卡诺、切拉诺和阿尔巴等城镇，阿尔巴在古代是马其顿国王皮洛士的地牢，当时是马尔西郡的总部，其头衔仍由他的父亲安条克的康拉德持有。有几条道路穿过这片海域，通过山路向西通往罗马，向南通往索拉，向北通往拉奎拉和斯波莱托，向东通往苏尔莫纳。

康拉丁经塔利亚科佐前往斯库尔科拉马尔西卡纳，并于 8 月 22 日在这里的 Villa Pontium 扎营。听说敌人正沿着通往湖边的道路前进，查尔斯穿过奥维努洛山口向敌人发起进攻。8月22日，当他自己带着3000名疲惫的骑兵和徒步的士兵在离阿尔巴两英里的马利亚诺附近的山上扎营时，他看到了他；因此，这场战役一定是在这里打的，而且对查理和康拉丁的命运都具有决定性的意义。萨尔托河把敌军的两个营地隔开了一个晚上，一个在阿尔巴附近的帕伦廷田野上方，另一个在斯库尔科拉马尔西卡纳附近现已被摧毁的庞特城堡。次日清晨，康拉丁的军队组成了两个战斗群，第一战斗群由参议员、加尔瓦诺伯爵和托斯卡纳吉贝尔人的首领、比萨的格哈德-多拉蒂科指挥；第二战斗群主要是德国骑士，由康拉丁和腓特烈这两个年轻人指挥。敌方最优秀的督军雅各布-甘特尔米、库朗斯的亨利元帅、克拉里的约翰、威廉-勒斯坦达德、维勒哈杜因的威廉二世、作为查理附庸从那里为他提供了 400 名装备精良的骑士的维勒哈杜因大臣、圭多-蒙特福德和国王本人率领战斗队形。据传说，刚从东方归来的著名战将埃拉德-瓦勒里曾建议他隐藏第三个军团作为预备队；但这样一位经验丰富的将军不需要别人的暗示就能保留一个军团以备不时之需。除了伦巴第人和托斯卡纳贵族外，查理的军队中还有罗马人、奥尔西尼家族的巴塞洛缪-鲁伯斯、侯爵安尼巴尔德斯、两个萨沃尼人约翰和潘杜夫以及其他贵族，因此，同一部落的战友就像敌对的兄弟一样站在一起对抗。

8 月 23 日上午，卡斯蒂利亚的亨利首先渡河，绕过桥头的普罗旺斯人，拉开了战斗的序幕。当康拉丁的部队扑向可恨的敌人时，他们仿佛是贝内文托的复仇之灵。没有背叛玷污了战士们的荣誉。他们的冲锋击溃了敌人的组织；普罗旺斯人的第一道防线和法国骑士团的第二道防线被击破。当身着国王盔甲的库朗斯（Courances）内廷大臣举着战旗从马上摔下并被击毙时，雷鸣般的胜利呐喊宣告了篡夺者的死亡。法军在当天的英雄卡斯蒂利亚的阿里戈的追击下狂奔而去。日耳曼人和托斯卡纳人扑向敌营，大肆抢掠，战场上的一切秩序都荡然无存，年轻人沉醉在胜利的喜悦中，将手掌紧握在手中。幸运之神在清晨将他捧上帝国的盾牌，又在傍晚让他陷入无名的荒凉。

查理站在山丘上，俯瞰着军队的逃亡；这场战役的失利注定了他王位的坍塌。圭尔夫编年史记载，这位国王泪流满面地向圣母祈祷，瓦莱里则向他喊道，是时候冲出埋伏圈了。八百名骑士突然冲到了战场上，那里看不到法国的旗帜。当这支生力军冲出来时，他们严密的方阵足以冲散并摧毁康拉丁的残兵败将，而四散的法军则围绕着核心重新集结。由于缺乏后备力量，或许也由于卡斯蒂利亚的亨利率领的西班牙人过于急躁，追击敌人过远，康拉丁失去了这场辉煌的胜利。当因凡特从这里返回战场时，他以胜利者的身份离开了康拉丁，他看到战士们在他的营地前列队，他急忙与他们打成一片，并向他们致以欢乐的问候。"蒙乔埃！"的战斗呐喊声向他响起，百合花旗帜的景象让他愣住了；他英勇镇定地扑向敌人；他两次试图突破敌人，但与命运的决定作斗争是徒劳的。

当夜幕降临战场时，阴郁的查理坐在帐篷里，向教皇口述了一份胜利报告，这份报告完全是贝内文托战场上那封信的翻版，其中似乎只改动了几个名字。"圣父，我现在像焚香一样，向您献上全世界所有信众盼望已久的喜讯，我恳求您： 天父，起来吃您儿子的猎物吧....。我们杀敌无数，相比之下，在贝内文托的失败显得微不足道。我们并不清楚康拉丁和亨利参议员是战死还是逃脱，尤其是这封信是在战后不久写的。参议员所骑的那匹马是在没有骑手的情况下逃走的。让教会，我们的母亲，兴高采烈地赞美万能的主，是他通过他的战士给了她如此伟大的胜利；因为现在主似乎已经结束了她所有的烦恼，把她从迫害者的魔爪下解救出来。第十一征税诏告第四年八月二十三日于帕伦丁球场"。

这就是圣巴塞洛缪之夜的可怕猎人的语言，他带着偏执的虚伪，将他的牺牲品献给教皇，就像一盘美味的捕获的猎物。同一个暴君迅速取得了双重胜利，先是战胜了曼弗雷德，然后又战胜了康拉丁，这激起了人们的道德感；因为事实上，在这里，邪恶第二次战胜了善良，非正义第二次战胜了正义。在塔利亚科佐的战场上，也许是战士们从战斗的瓮中抽出的最不公正的一签。如果说正义和复仇，如果说武器的力量和勇气、英雄主义和热血青年能够保证胜利，那么康拉丁在那里必须获得胜利；但不可抗拒的命运却将胜利拱手让给了查理。看到成千上万的人被杀，胜利者的仇恨得到了满足，但他仍想复仇。他砍掉了许多被俘罗马人的脚；当他意识到看到残缺不全的脚会激起太多仇恨时，他下令将他们全部烧死在一座建筑物里。在高贵的罗马人中，阿尔贝蒂家族的斯蒂芬、英勇的圣尤斯塔奇奥家族的阿尔切鲁西奥和老阿德奥达托二世都死了。康拉丁的内廷大臣 Kropf von Flüglingen 也倒下了。维科的彼得身负重伤，拖着疲惫的身躯来到罗马，又从罗马回到了自己的城堡，并于 12 月在那里去世--他是一个毫无原则的人，是维科凶悍的吉贝尔王朝的始祖之一。
