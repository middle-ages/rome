### 第五章

_1\. 奥诺留斯四世-潘杜尔夫-萨维利家族参议员。与西西里和帝国的关系。空缺一年。尼古拉斯四世查理二世在里耶蒂加冕。科隆纳。红衣主教雅各布-科隆纳。约翰-科隆纳和他的儿子红衣主教彼得和伯爵斯特凡。罗马涅的叛乱。奥尔西尼家族反对科隆纳。贝尔托德-奥尔西尼家族参议员。约翰-科隆纳参议员 1289 年 维泰博臣服于元老宫。潘多夫-萨维利家族参议员 1291 年 斯特凡-科隆纳和马特乌斯-雷纳尔迪-奥尔西尼家族参议员 1292 年 尼古拉斯四世逝世 1292 年__________________________________________________________________________。

教会从查理的长期保护下解放出来后，罗马人迅速升任罗马教廷主教：1285 年 4 月 2 日，科斯梅丁圣玛丽亚德高望重的老红衣主教雅各布-萨维利家族在佩鲁贾当选为教皇。他匆忙赶往罗马，于5月15日被祝圣为霍诺留四世。这是他为了纪念霍诺留三世而自称的，霍诺留三世是他自己家族中的第一位教皇，当时他已经权倾朝野。他本人是参议员卢卡-萨维利（Luca Savelli）和圣菲奥拉伯爵家族的乔安娜-阿尔多布兰德斯卡（Joanna Aldobrandesca）的儿子。他的兄弟们曾在塔利亚科佐为查理而战，其中约翰已经去世，潘杜夫仍是罗马的元老，与安尼瓦尔德斯并肩作战。霍诺留四世刚刚被选为教皇，罗马人就将元老院的权力终身授予了他，他随即确认了潘杜尔夫在元老院的地位。

看到这两兄弟，一个在阿汶丁山靠近圣萨比纳的宫殿里当教皇，另一个在元老宫里当元老，统治着这座城市，两人都痛风缠身，动弹不得，真是奇怪。霍诺里乌斯手脚瘫痪，既不能站立，也不能自由行走；当他在高高的祭坛上举行庆典时，只能借助机械装置升起主祭；而波达格里安-潘多夫则必须用椅子抬着。但是，这些值得尊敬的人都拥有健全的头脑，充满智慧和力量。元老宫里，潘多夫拄着拐杖一瘸一拐地走着，他领导的军团非常严格，罗马享受着最好的安宁；街道很安全，因为强盗都被绞死了，野蛮的贵族也不敢骚乱。元老萨维利家族在其兄担任教皇期间作为其代理婚姻统治着这座城市。

霍诺留四世在位时间短暂，但他对教皇国的和平和西西里岛的事务十分关注。他从维泰博收回了马丁四世用来惩罚选举犯罪的禁止教务令，但这座城市却要拆除城墙；它失去了管辖权，它的校长职位归教皇所有；它不得不将一些城堡移交给奥尔西尼家族。从那时起，这座曾经繁荣昌盛的城市逐渐衰落。在蒙特费尔特罗家族的伟大战士放下武器流亡之后，霍诺留成功地平定了罗马涅。1286 年，教皇任命他的堂弟、资深执政官佩特鲁斯-斯特凡内斯基（Petrus Stefaneschi）为那里的伯爵。他更关心的是那不勒斯，在查理二世被囚禁期间，阿图瓦的罗伯特和教皇使节格哈德管理着那不勒斯。1285年11月11日彼得国王去世后，西西里岛的统治权传给了他的次子唐-贾科莫（Don Giacomo），他无视教皇的禁令，当着母亲康斯坦茨的面在巴勒莫加冕。伟大的海军将领罗杰-德-洛里亚（Roger de Loria）在海上处处取得胜利；贝尔纳多-达-萨里亚诺（Bernardo da Sarriano）率领的西西里舰队甚至在1286年9月4日登陆罗马海岸，西西里人为了给康拉丁报仇，将阿斯图拉烧成灰烬，并杀死了叛徒弗兰吉帕尼的儿子。

霍诺留与鲁道夫一世-哈布斯堡关系友好；罗马国王一再要求的帝国加冕仪式宣布于1287年2月2日举行，但查理曼大帝的王冠却从未戴在哈布斯堡一世的头上。1287年4月3日，霍诺留四世在阿汶丁山的宫殿中去世；他在这座山上建造了自己的居所，只在蒂沃利度过夏季，可能是为了使用_Aquae Albulae_的硫磺浴。他给家人留下了财富和尊敬。从他作为红衣主教所立遗嘱以及作为教皇所确认的遗嘱中可以看出，萨维利家族当时已经是拉丁山区甚至是奇维塔卡斯特拉纳地区的强大领主。在罗马，他们在阿汶丁山拥有一座宫殿和城堡，在帕里奥内地区拥有一座宫殿和塔楼，那里的_Vicolo de' Savelli_至今仍在纪念他们，后来他们又在马塞卢斯剧院的废墟上建造了现在被称为奥尔西尼家族的大宫殿。

红衣主教们在死者的家中举行了会议，但未能就选举做出决定；罗马教廷空缺了将近一年。炎热的季节来临了：六位红衣主教死于热病，其他人则在逃亡中寻求救赎。只有普拉内斯特的红衣主教在圣萨比纳贫瘠的房间里忍受着孤独和发烧的空气，对死亡充满了蔑视，并因此获得了头冠。冬天，当红衣主教们回到阿汶丁山时，他们选举他为教皇，但直到1288年2月22日才当选。来自阿斯科利的杰罗姆出身卑微，是小修道院的修士，当时是他们的将军，在格里高利十世时期就已作为东方使节表现出色，被尼古拉三世提升为拜占庭宗主教，随后又被任命为普拉内斯特主教。他以尼古拉四世的身份登上了罗马教廷--第一位成为教皇的方济各会教徒，他是一位虔诚的修道士，没有私心，致力于为世界带来和平、十字军东征和根除异端。

罗马人还将元老院的权力终身授予了他。教皇被任命为教区长在其他城市也很常见。他们试图接管地方行政长官的选举，然后任命自己的代理婚姻。教皇与教皇国公社的关系与最高封建主与与其签订条约的诸侯的关系没有任何区别。这些城市承认教皇的主权，服兵役，缴纳土地税，在某些情况下服从使节法庭的管辖，但它们保持着自己的法规、行政管理和国家自治。每个省都是一个拥有特殊习俗和特权的共和国。这种强大的市镇精神使教皇们无法成为真正的君主，他们为了限制贵族阶层而不得不放弃这种精神；但他们明智地利用了公社之间的差异和嫉妒，通过不和来削弱公社。他们剥夺了一些公社选举议长的权利，而把这种权利给予了其他公社，以换取每年的税收。他们禁止城市间的政治联盟，但往往以彼之道还施彼身。他们有时是君主制，有时是民主制；他们的政府软弱而温和，常常是宗法式的，总是摇摆不定；由于无法建立普遍的法律，以及使节们对没有物质力量活力的市政制度的不明智的敌视，最后是教皇宝座上无继承人的迅速更迭，产生了教皇国所特有的那种仅仅是机械组成和反复瓦解的奇怪状态。

尼古拉四世在位的第一年，罗马一直很平静，直到1289年春天，党派纷争把他逼到了里耶蒂。他在那里加冕查理二世为西西里国王。在英格兰的爱德华和教皇的努力下，这位安茹的查理的软弱儿子于 1288 年 11 月从西班牙的监禁中获释，现在他来到了里耶蒂，并于 5 月 29 日在这里举行了加冕仪式。在一份文件中，他宣布自己和他的父亲一样是教会的附庸，受到教会的恩典，并援引了封建条款，发誓不在罗马或教皇国担任参议员或议长职务。阿拉贡一党可能对查理二世的加冕不以为然，但罗马的动荡更多是因为贵族家族之间的猜忌。五十年来，萨维利家族和与他们有姻亲关系的奥尔西尼家族一直是罗马贵族中最有影响力的成员，并取代了曾经的统治者安尼巴尔迪家族。新教皇也曾是奥尔西尼家族的朋友，因为尼古拉三世曾让他担任红衣主教，这也是他出于感激而冠以奥尔西尼之名的原因；但他很快就转而投靠了吉贝尔家族，并专门投靠了科隆纳家族。

在腓特烈二世时期，红衣主教约翰和他的侄子奥多反对教会，这个名门望族在恢复教皇统治期间受到挫折，从而弥补了其吉贝尔派的过失，直到十三世纪末，它才成为罗马最有权势的家族，并在随后的几个世纪里一直占据着罗马城的第一把交椅。尼古拉三世为了削弱安尼巴尔迪家族，再次对科隆纳青睐有加；他将奥多之子雅各布提升为红衣主教。尼古拉四世为他们的家族带来了新的辉煌。作为帕莱斯特里纳的主教，他曾与他们有过亲密接触；他的头冠可能是受了他们的影响。他让雅各布枢机主教的弟弟约翰-科隆纳（1280 年已成为元老）担任马尔凯罗马涅侯爵府的院长；在约翰的儿子中，他将彼得提升为圣尤斯塔奇奥枢机主教，将斯蒂芬提升为罗马涅伯爵。这位罗马资深执政官自此成为家族中最伟大的人物之一，后来成为彼特拉克的赞助人和朋友，并因护民官科拉-迪-里安佐时期其家族的悲惨命运而闻名。斯蒂芬当时初长成，热情奔放，充满活力。作为罗马涅伯爵，他因干涉各公社的章程而得罪了该省的贵族和城镇。因此，1290 年 11 月，波伦塔的圭多的儿子们在拉文纳袭击了他，并将他和他的宫廷囚禁起来。里米尼、拉文纳和其他城市纷纷起义，于是教皇派遣阿雷佐主教伊尔德布兰德-德-罗梅纳（Ildebrand de Romena）以校长的身份前往罗马涅平息起义，并将斯特凡从监狱中释放出来。

奥尔西尼家族的一位成员，即鲜花广场的乌瑟卢斯（Ursellus of Campo di Fiore）也参加了起义，他是当时里米尼的波德斯塔（Podestà of Rimini）马特乌斯的儿子。奥尔西尼家族以嫉妒的眼光看待科隆纳家族的壮大，尤其是这些领主还将他们赶出了元老院。潘多夫-萨维利家族辞职后（可能发生在新教皇上任后不久），尼古拉四世仍然对奥尔西尼家族有利，先是任命乌尔苏斯，然后又任命前罗马涅第一伯爵贝尔托德为元老院议员。然而，早在 1290 年，科隆纳家族就成功地推翻了他们的对手：红衣主教彼得、斯蒂芬伯爵和其他四个权贵子弟的父亲约翰在尼古拉斯-孔蒂和卢卡-萨维利家族下台后成为了元老。权倾朝野的科隆内斯是真正的坎帕尼亚王子，也是那不勒斯查理二世的密友，他以不同寻常的辉煌出现在罗马。人们甚至在凯旋时用战车把他引到了元老宫，在向维泰博和其他城市进军之前，人们称颂他为凯撒。这种闻所未闻的游行是对古代的一种回忆，它表明罗马人中已经再次涌动起热烈的情绪或观点。

尼古拉四世通常住在萨比纳、翁布里亚或维泰博，实际上他对罗马没有任何权力；他只能听之任之，1290 年 7 月和 8 月，罗马人对维泰博发动了一场激烈的战争，维泰博拒绝成为罗马城的附庸。教皇随后促成了和平。1291 年 5 月 3 日，仍是罗马唯一元老和统治者的约翰-科隆纳以罗马人民的名义在元老宫缔结了和约，维泰博的使节们在佩鲁贾、阿纳尼、里耶托、奥尔维耶托和斯波莱托的辛迪加的见证下，向罗马城重申了附庸誓言，并承诺支付巨额赔偿，因为他们在那场战争中俘虏或杀死了几位高贵的罗马人。这一庄严的国事行动之后，5 月 5 日元老又对维泰博进行了重新确认，这表明在强大的约翰-科隆纳的政府统治下，元老宫上的共和国就像在布兰卡隆时期一样拥有了完全的主权。然而，科隆纳的统治在贵族中激起了激烈的反对。教皇被谩骂，因为他将自己完全置于一个家族的权力之下；讽刺诗嘲笑他；他被刻画在一根柱子上，这是该王朝的纹章，柱子上只有他的头和头冠，而另外两根柱子，即科隆纳的两位红衣主教，则站在他的身边。奥尔西尼家族最终也成功地从他们的党派中填补了元老院的席位；首先是潘杜夫-萨维利家族在 1291 年再次成为元老院议员，但在第二年，前罗马涅伯爵斯特凡-科隆纳和马修斯-雷纳尔迪-奥尔西尼分享了元老院的权力。

尼古拉四世于 1292 年 4 月 4 日在他为自己在圣玛丽亚-马焦雷附近建造的宫殿中去世。就在他去世前不久，1291 年 7 月 15 日，鲁道夫一世-哈布斯堡在没有戴上帝国王冠的情况下入土为安；与此同时，5 月 18 日，叙利亚最后的基督教领地阿克失守，为十字军东征的世界大战画上了句号。与古罗马的东方战争一样，欧洲长达两百年的军事行动成为教皇统治世界的有力杠杆。教会与帝国的伟大斗争的结束以及这些十字军东征的消亡，使教皇的视野变得狭窄。教皇的巨大建筑上一块又一块的石头倒下了；世界从教皇那里退缩了，英诺森三世的权杖开始从教皇疲惫的手中落下。
