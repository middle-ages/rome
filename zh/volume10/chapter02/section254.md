#### 264 查尔斯辞去参议员职务

*3 查尔斯辞去元老院职务。Conrad Beltrami Monaldeschi 和 Luca Savelli 参议员 1266 安吉勒斯-卡波奇（Angelus Capocci）领导下的罗马民主政府。卡斯蒂利亚的亨利 1267 吉贝尔派聚集在托斯卡纳。使节们匆匆赶往德意志，邀请康拉丁前往罗马。他决定接受这一邀请*。

曼弗雷德的倒台也是吉贝尔派在整个意大利的失败，意大利的大多数城市现在都承认查理是他们的守护神。罗马教廷希望重新成为罗马的唯一统治者，现在要求查理通过条约辞去元老院的职务；国王犹豫了一下，希望继续留任一段时间，最后带着掩饰不住的不满向罗马人宣布，为了不触怒声称有权拥有元老院的教会，他将辞去自己的尊严。他于1266年5月底辞去了教皇职务，教皇很快就后悔了。

克莱门特四世现在希望不费吹灰之力就恢复他在罗马的主权，因为查理已经承诺通过条约帮助他。然而，罗马城并没有试图将元老院移交给教皇，甚至也没有邀请他返回。他已于四月离开佩鲁贾前往奥尔维耶托，然后前往维泰博，希望进入拉特兰，并一直留在那里。当时，罗马与罗马教廷的关系并不比佛罗伦萨共和国或卢卡共和国更亲密；罗马人认为教皇的权利已经消灭，而查理却没有做出任何努力来捍卫这些权利。既然元老院要重新任命，罗马人民又按照旧制度选出了两名元老，他们是奥尔维耶托的康拉德-贝尔特拉米-莫纳尔代斯基和罗马的卢卡-萨维利家族。他们立即要求罗马商人支付教会货物的抵押款，教皇称他们是罗马内外的强盗和小偷。

大赦令让一些吉贝尔人回到了罗马城，在那里他们再次与圭尔夫人一起参加了议会。曼弗雷德的一些支持者，如奥尔西尼家族的雅各布斯-拿破仑，向教皇屈服了，但这只是假装的。当战败的党派从惊愕中恢复过来后，它在罗马和托斯卡纳、那不勒斯和伦巴第到处组织起来，以意大利人特有的技巧组成秘密社团。 格尔夫贵族难以容忍的傲慢深深地激怒了罗马人民，他们在 1267 年上半年起义，成立了由 26 名信任者组成的民主政府，并任命吉贝利党人安杰洛-卡波奇家族为庶民统领官。克莱门特不得不承认这场动乱；当贵族们（正如罗马人所说的那样）从维泰博骚动起来，开始与新政府对抗时，人民队长甚至向他求助，这时教皇强调自己是无辜的，并派了两位主教去建立和平。

与此同时，受人民委托任命元老的卡波奇家族将目光投向了西班牙的一位元老，他就是卡斯蒂利亚的亨利，卡斯蒂利亚国王斐迪南三世的儿子，罗马名义上的国王阿方索的弟弟，一位才华横溢、雄心勃勃的冒险英雄。他曾因叛乱被流放到英格兰的法国南部，早在 1257 年就想参加亨利三世对曼弗雷德的战役，但未能如愿。1259年，他在弟弟弗雷德里克和其他西班牙流亡者的陪同下，乘坐英国船只前往非洲，此后他一直为突尼斯统治者效力，与摩尔人作战。意大利的动荡促使他为自己的野心寻找新的舞台。1267 年春，他带着几百名卡斯蒂利亚人来到了他的堂兄查理的宫廷。查理欠了他一大笔钱，他不愿偿还，于是他想用一种好的方式摆脱这个麻烦的债主。Infante与阿拉贡的詹姆斯一起争夺撒丁岛的王位，教会宣布撒丁岛是其财产，并与比萨共和国发生争执。他来到维泰博的教皇宫廷，用他的突尼斯黄金征服了红衣主教们；但克莱门特四世更倾向于用阿拉贡联姻来解决他的问题，而不是把撒丁岛赐给他，而查理也申请了撒丁岛。这位国王欺骗了自己的堂兄，暗中阻挠他实现愿望。

Infante在罗马是一个比较幸运的候选人，他的元老宫向他敞开了大门。人民领袖卡波奇家族主持了他的选举，罗马人欣然接受了一位卡斯蒂利亚王子担任元老，他以武功荣耀和财富著称，他们期望从他那里得到强大的保护，抵御贵族的傲慢和教皇的要求。贵族、大多数红衣主教和教皇本人都反对这次选举，但都无济于事。安茹的查理一登上西西里的王位，罗马的气氛就又变成了吉贝尔派。1267年6月，Infante从维泰博来到罗马担任威权统治者，于是，奇怪的巧合发生了，两个西班牙兄弟同时当选国王，其中一个还是罗马元老。

唐-阿里戈对这座城市的治理很快就变得不亚于他的前任安茹的查理。因为Infante刚刚上任，他与教皇的分歧就开始了；他想让整个坎帕尼亚臣服于元老宫，剥夺神职人员的管辖权，镇压贵族。教皇提出抗议，但元老不听。人们尊重这位王子，他起初对吉贝尔派和吉贝尔派都表现得很公正；但他对查理的强烈仇恨和突发事件很快就促使他公开宣布自己是教会党派的敌人。

曼弗雷德和施瓦本家族的支持者聚集在托斯卡纳。在这个国家里，这两个旧派别的新龙种已经萌芽，他们之间不可调和的争斗给意大利的历史打上了狂野和伟大激情的英雄印记，当教会和帝国之间的大争斗已经结束时，意大利人仍在以他们的形式和货币进行战斗。在当时的想象中，这场激烈的党争似乎是两个恶魔--盖尔法（Guelfa）和盖贝利亚（Gebellia）--的罪恶杰作，而这两个恶魔确实是中世纪的复仇女神。它们并非首次出现在曼弗雷德时代；它们的起源要更久远一些，但它们的疯狂行为具有派系战争的可怕特征，主要是在霍亨斯陶芬统治衰落之后，这种战争将意大利的省份和城市撕裂成敌对的两半。曼弗雷德倒台后，比萨和锡耶纳、波吉本西和圣米尼亚托-阿勒泰德斯科一直是霍亨斯陶芬或吉贝尔家族的天下。圭多-诺韦洛-吉迪伯爵沮丧地离开了佛罗伦萨，他在普拉托和其他城堡聚集了围绕斯瓦比亚旗帜的德国雇佣兵和朋友。曼弗雷德的一些战地指挥官逃离了贝内文托附近的战场或阿普利亚的地牢，比如加尔瓦诺和弗雷德里克-兰齐亚兄弟、安条克的康拉德（腓特烈皇帝的孙子、加尔瓦诺的女婿）、康拉德和马里努斯-卡普斯、那不勒斯贵族以及福里尼奥的康拉德-兰齐亚。西西里王国在新统治者的枷锁下呻吟；法国税务员、法官和巴利夫将其踩在脚下，查理的专制剥夺了它的一切权利和自由，它发现自己处于这样一种状态，与之相比，曼弗雷德的统治似乎是一个黄金时代。曾经背叛过他的人民现在想起了他的仁慈，白白地召回了他。克莱门特四世在著名的书信中以父亲的训诫和善意的建议的形式，巧妙地描绘了一个令人憎恨的暴君的形象。

阿普利亚的流亡者逃到了托斯卡纳，并说王国造反的时机已经成熟。曼弗雷德的追随者看到他的孩子们在枷锁中煎熬，无法捍卫自己继承的权利；因此，他们将希望转向了康拉德，西西里岛最后一位合法继承人，圭尔夫家族曾邀请他前往意大利反对篡夺者曼弗雷德。

康拉德四世的儿子，1252 年 3 月 25 日出生于兰茨胡特附近的沃尔夫施泰因城堡，当他的叔叔倒台、征服者登上王位时，他才十四岁，根据继承法，王位是他无可争议的财产。他受到严厉的叔叔巴伐利亚的路德维希二世和他的母亲伊丽莎白（公爵的妹妹，1259 年第二次嫁给戈里齐亚及蒂罗尔伯爵迈因哈德）的保护。帝国的王冠在康拉丁的头上盘旋了片刻，但教皇为了让德国被各方势力拖垮，让意大利没有皇帝，并没有就阿方索和理查德之间的王位之争做出决定，他禁止选举霍亨斯陶芬王朝最后一位真正的子孙。康拉丁只剩下耶路撒冷国王这个虚无缥缈的头衔和完全被削弱的斯瓦比亚公国。他在巴伐利亚长大，当地歌手的歌声和关于其家族的英雄伟业和衰落的激动人心的画面滋养了他的精神。

政治史上很少有像这位年轻人的命运这样扣人心弦的事情，世袭的环境将他从故乡引向意大利，将他作为其英雄血脉的最后一位牺牲在祖先的坟墓上。早在 1266 年，来自比萨、维罗纳、帕维亚和锡耶纳、卢塞拉和巴勒莫的吉贝利派使节就来到了康斯坦茨、奥格斯堡和兰茨胡特；次年，兰齐亚兄弟和卡普斯前来将这只 "羽翼未丰的雄鹰 "托起飞翔。根据圭尔夫-马拉斯皮纳（Guelph Malaspina）的美丽寓言，他们就像那些给即将到来的国王带来黄金、乳香和没药的使者。他们向他许诺，如果他能在阿尔卑斯山上再次展开帝国的旗帜，来把他光荣的祖先的土地从可恨的暴政中拯救出来，他就会得到意大利的支持。

当伟大的腓特烈的孙子看到这些意大利人在他脚下顶礼膜拜时，当他听到他们精彩的演讲，收到他们丰厚的礼物和承诺时，他被狂喜冲昏了头脑。海妖的声音把他引向那片美丽而命运多舛的土地，那片日耳曼人向往的历史天堂，在那里，他显赫的父辈们似乎在召唤他走出不公正的坟墓。他的母亲不愿意，但他的家人和朋友都同意。一个谣言传遍了阿尔卑斯山，说康拉德四世的小儿子正在准备一支军队，准备降临意大利，推翻暴君查理的王位，恢复施瓦本人的统治。
