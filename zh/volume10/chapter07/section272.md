_3\. 教堂建筑 使徒彼得和梵蒂冈。圣保罗教堂 拉特兰教堂 小教堂_Sancta Sanctorum. _S. 洛伦佐。S. Sabina. 医院 S. Spirito. 圣托马斯 拉特兰医院。圣安东尼奥阿巴特。哥特式艺术原则。米涅瓦山顶的圣玛丽亚。卡萨马里，福萨诺瓦。哥特式帐篷。科斯马塔家族的艺术家。瓦萨勒图斯。墓穴。罗马纪念碑文字的特点_。

那个时代的教皇中也有艺术赞助人。最慷慨的莫过于英诺森三世。在他捐赠的礼物中，几乎没有一座罗马教堂漏掉，而且他一般都会对大教堂进行全面修复。在圣彼得大教堂，他用马赛克装饰了护民官，这些马赛克与旧大教堂一起消失了，他还修复了被巴巴罗萨毁坏的前院。霍诺留三世和格里高利九世完成了他的修复工作，他们用马赛克画装饰了大教堂的正面，画中的基督位于圣母和圣彼得之间，四位福音书作者和他自己都在救世主的福森脚下。在梵蒂冈宫，英诺森三世延续了前任的工作，建造了一座更大的建筑，并在周围修建了围墙和入口塔楼。由于罗马局势动荡，拉特兰成为激烈的城市战争的场所，教皇们有必要在圣彼得大教堂拥有一个坚固的住所，因此从十三世纪起，他们就在那里建立了住所。从里昂返回后，英诺森四世继续修建梵蒂冈宫，随后爱好华丽的尼古拉三世从 1278 年起继续修建，并从佛罗伦萨请来建筑大师弗拉-西斯托和弗拉-里斯托罗为他服务。他打通了通往梵蒂冈的通道，并在那里修建了花园，用围墙和塔楼将其包围起来。这些花园被称为_viridarium novum_，圣彼得大教堂的大门由此得名_Porta viridaria_。这也重新唤起了人们的自然意识；几个世纪以来，罗马第一次出现了公园。尼古拉三世是历史上梵蒂冈官邸的首创者。

圣保罗大教堂也多次得到修复和装饰。13 世纪上半叶，这里修建了宏伟的回廊庭院，这是罗马最美丽的建筑，可能是科斯马特人的杰作。与之相似，同一时期拉特兰教堂的庭院更加美丽。

拉特兰主教堂在教皇转到阿维尼翁后不久就被大火烧毁，如今只剩下一些十三世纪的古迹。尼古拉三世修复了它以及那里的宫殿，并在那里重建了 Sancta Sanctorum 小教堂。读过这些故事的人都知道，这是教皇们的私人礼拜堂，在这里举行最庄严的仪式，尤其是复活节。这里存放着最尊贵的圣物，"非人手所造 "的圣萨尔瓦多像和使徒王子们的头颅。尼古拉三世优雅的新建筑内部铺设了大理石，哥特式花饰下装饰着蜿蜒曲折的圆柱，并镶嵌着马赛克和绘画，这是旧拉特兰宫的唯一遗迹。格列高利九世对拉特兰宫进行了重建和加固。但在他之后，尼古拉三世继续修建。然而，教皇们不再满足于梵蒂冈和拉特兰的宫殿，奥诺留四世在圣萨比纳为自己修建了一座宫殿，尼古拉四世则在圣玛丽亚-马焦雷修建了另一座宫殿。教皇们甚至还在蒙特菲亚斯科、特尔尼、维泰博、索里亚诺等地建造了别墅和宫殿，这种对华丽的日益喜爱招致了某些方面的批评，因为这被视为过于世俗或过于任人唯亲。

霍诺留三世在城墙外建造了圣劳伦斯大教堂，他将两座古老的大殿合二为一，建造了神圣的长老会，并加建了门廊，这是非常了不起的。人们也注意到了修道士修道院的出现。但即使是这些建筑，也只是现有建筑的扩建，也许多米尼克创建的圣萨比纳修道院是个例外，那里还有一个罗马风格的庭院。

教皇们最值得称道的活动属于慈善机构。英诺森三世创建了圣斯皮里托医院和弃儿医院，这是他在梦中的影像或罗马人的嘲弄下促成的，罗马人斥责他为自己的房子建造了巨大的孔蒂塔，以达到野心勃勃的目的。他在萨西亚的圣玛丽亚旁边建造了这座塔，伊纳国王曾在那里建立了一个朝圣者安养院（_schola Saxonum_），1204 年将其移交给普罗旺斯人吉多管理，吉多是蒙彼利埃一个以圣灵为名的医院组织的创始人。于是，这座古老的盎格鲁-撒克逊房屋被改建成了圣灵医院，而教堂本身也沿用了这一名称。后来的教皇扩大了该机构的规模，使其成为世界上最伟大的此类机构。

几年前，圣托马斯医院建在多拉贝拉拱门附近的科埃利乌斯河上，当地的引水渠将其称为_in Formis_；英诺森三世将其赠与了尼卡德-约翰-马塔，他曾为赎回基督徒奴隶而成立了三位一体骑士团。小教堂以改建后的形式依然存在，而医院则只剩下马特伊别墅入口处的古老入口。1216 年，红衣主教约翰-科隆纳（John Colonna）在拉特兰（Lateran）建立了第三家医院，现在仍在那里；红衣主教佩特鲁斯-卡波奇（Petrus Capocci）在圣玛丽亚-马焦雷（S. Maria Maggiore）附近建立了第四家医院，即安东尼奥-阿巴特医院（S. Antonio Abbate）。圣安东尼大火的患者由在法国南部成立的一个修道会的修士们照顾。这家医院已不复存在，只有圆拱门式的古老大理石门表明它曾是一座规模宏大的建筑。

总的来说，十三世纪的罗马教会建筑并没有显示出任何重大意义。没有必要建造新的建筑，而修复旧的大教堂则让这座城市忙得不可开交。当资产阶级的强势崛起催生了佛罗伦萨、锡耶纳和奥尔维耶托的宏伟大教堂时，罗马再也没有大型教堂可建了。然而，十三世纪中叶以后，哥特式风格也在这里出现，我们第一次看到它是在 Sancta Sanctorum 教堂。这种来自法国北部的神秘风格被修道士们所采用，他们已经在阿西西的圣人墓中使用了这种风格，并根据意大利人的艺术感觉进行了调整；但哥特式风格并没有在古典罗马得到发展，只有圣玛丽亚教堂（S. Maria sopra Minerva）例外，尼古拉三世于 1280 年开始由佛罗伦萨新圣玛丽亚教堂的建筑师 Fra Sisto 和 Fra Ristoro 进行修建。这座半哥特式教堂是许多世纪以来基督教世界首都唯一一座具有重要意义的独立新建筑。相比之下，拉丁姆的卡萨马里（Casamari）和福萨诺瓦（Fossanova）修道院教堂早在 13 世纪初就已经建成了精美的哥特式建筑。

只是在祭坛和坟墓上方的帐篷中，哥特式风格才与罗马音乐装饰相结合，在该世纪末成为罗马的主流风格。罗马城的教堂中仍保留着其中一些优美的作品，它们是中世纪最具吸引力的古迹之一。这些作品部分出自托斯卡纳大师之手，如圣保罗教堂中精美的帐幕，据说是尼科洛-皮萨诺（Niccolò Pisano）的学生阿诺尔弗-迪-坎比奥（Arnolfo di Cambio）于 1285 年制作的；部分出自罗马艺术家之手，如科斯梅丁圣玛丽亚教堂中的帐幕，是红衣主教弗朗切斯科-加塔尼（Francesco Gaëtani）请德奥达图斯（Deodatus）制作的。自十一世纪以来，罗马大理石工人一直活跃在意大利中部和南部。他们自称为_Marmorarii_或_arte marmoris periti_，这是罗马特有的称呼。因为这座城市到处都是可口的大理石碎石，即使对于外国城市来说也是名副其实的卡拉拉。因此，这里形成了一种用大理石片镶嵌马赛克的特殊艺术，并不断借鉴古代房屋和寺庙马赛克的模式。大理石板从古代建筑上撕下，华丽的柱子被锯断，以获得装饰材料，特别是教堂的地板，上面艺术性地铺满了斑岩、蛇纹石、大理石、白色和黑色大理石。修道院庭院中的帐幕、安博尼、祭坛、坟墓、主教椅、复活节烛台、圆柱、拱门和门楣都被镶上了马赛克。所有这些作品，其中有些是装饰性的，尤其是教堂的地板，都是罗马古代辉煌不断被掠夺的罪魁祸首，罗马丰富的大理石每天都在消耗，却从未枯竭。大理石工人还掠夺地下墓穴以满足自己的需要，导致许多铭文丢失。

这种罗马石制品（_opus romanum_）从十二世纪末开始催生了出色的石匠行业。从十二世纪末开始，优秀的科斯马茨石匠家族崛起，并成为当地重要的石匠艺术。这个家族的活动跨越了整整一个世纪，是劳伦提斯大师的后裔，他和他的儿子雅各布斯（Jacobus）在 1180 年左右首次出现。之后，该家族子孙繁衍，历经数代，分别以科斯马斯（Cosmas）、约翰内斯（Johannes）、卢卡斯（Lucas）和迪奥达特（Deodat）为名。虽然科斯马斯这个名字在这个艺术家家族中只出现过一次，但他们却奇迹般地以他的名字命名。虽然科斯马埃家族的作品没有尼科洛和乔瓦尼、阿诺尔弗、契马布埃和乔托那样声名显赫，但他们却以独创的艺术流派为罗马增光添彩，他们的作品充斥着拉丁姆、托斯卡纳甚至翁布里亚，这些作品本质上结合了建筑、雕塑和艺术绘画，如帐幕、安博尼、坟墓、门廊和修道院庭院。科斯马特家族和流派在罗马消亡的同时，开始推广艺术的教皇也离开罗马前往法国，他们和他们的作品被阿维尼翁流放后罗马的黑暗遗弃所吞没。在罗马与科斯马特斯画派同时兴盛的另一个画派也遭遇了同样的命运。它的首领是巴萨莱图斯（Bassallectus）或瓦萨莱图斯（Vassalletus），拉特兰教堂美丽的修道院庭院就是出自他的手笔。

罗马的陵墓非常突出，当然这些陵墓大多属于高级神职人员。古代石棺的使用仍在继续，但由于皮桑学派的兴起，也出现了独立的陵墓。英诺森五世去世时，查理命令他在罗马的司库询问能否找到一个斑岩石棺来安葬教皇，如果找不到，就为他制作一个精美的陵墓。在罗马，十三世纪上半叶的名人纪念碑无一幸存，罗马的许多陵墓，尤其是圣约翰和圣彼得大教堂的陵墓被毁，令人扼腕叹息。在圣洛伦索，从红衣主教威廉-费斯奇（William Fieschi，† 1256 年）的墓开始，他是阿普利亚的使节，曾被曼弗雷德遣送回国。他躺在一个古老的大理石石棺中，石棺上的浮雕描绘的是一场罗马式婚礼--这对红衣主教来说是一个了不起的象征！只有简洁笔直的石棺属于中世纪艺术，石棺上的绘画描绘了登基的基督、英诺森四世和圣劳伦斯，以及红衣主教和他身边的圣司提反。冗长而夸张的碑文讴歌了逝者。

接着是红衣主教理查德-安尼巴尔迪的墓，他是著名的圭尔夫家族领袖和安茹的查理的支持者。这座简朴的纪念碑靠墙竖立在拉特兰中殿的左侧，与碑文一样现代，但大理石雕像仍是原物。这座陵墓让人想起了伟大的霍亨斯陶芬王朝时期和王位继承时期，因为理查德作为红衣主教经历了从格里高利九世到格里高利十世的整个时代。他于1274年在里昂去世。

该时代另一位年轻的红衣主教，特鲁瓦的安切鲁斯（† 1286 年），长眠于普拉塞德圣地的一座保存完好的纪念碑中，这座纪念碑已经显示出罗马雕塑的巨大进步，肯定是科斯马特人的作品。死者躺在一张床上，床顶是精美的大理石，悬挂在小圆柱上。柱子之间的地面是马赛克。

在阿拉科利，我们发现了萨维利家族的坟墓。这个贵族家族在这里建造了一座小教堂，并用绘画装饰。如今，这里仍有两座坟墓，一座是教皇本人埋葬的霍诺留四世的母亲的坟墓，另一座是元老院的陵墓。第一个墓穴是一个独立制作的石棺，在一个直角形的墓室下面有金色背景的马赛克装饰。上面安放着霍诺留四世的大理石雕像，他面容英俊，没有胡须；这是保罗三世首次从梵蒂冈带来的，并安放在霍诺留的母亲瓦娜-阿尔多布兰德斯基（Vana Aldobrandeschi）已经安葬的石棺上。第二座纪念碑以一种奇异的方式将古代和中世纪的形式结合在一起；一个带有罗马艺术衰落时期巴克斯浮雕的大理石瓮是瓮的基础，上面是一个带有哥特式顶部的马赛克石棺。正面三次出现了家族的盾徽；不规则地镌刻着不同时期的铭文。因为这里安息着几位萨维利家族的成员；首先是参议员卢卡斯，他是霍诺留四世、约翰和潘杜夫的父亲，这座墓是这几个儿子为他修建的；然后是著名的参议员潘杜夫和他的女儿安德烈亚；还有阿加皮图斯-科隆纳的妻子马比亚-萨维利家族，以及后来的其他家族成员。

密涅瓦宫中安葬着红衣主教拉蒂努斯-马拉布兰卡（Latinus Malabranca），塞莱斯廷五世就是在他的建议下成为教皇的，与他一起成为教皇的还有红衣主教马修斯-奥尔西尼家族（Mattheus Orsini）。石棺的形状像一张安息床，死者的遗像就沉睡在上面。科斯马特学派最精美的作品属于博尼法斯八世时期。正是在这个时期，第二代科斯马斯之子约翰在乔托的监督下制作了几座具有奇妙发明的坟墓，这些石棺装在哥特式帐篷中，其中的缪斯画描绘了死者上方的圣母和圣人，而死者则由两个大理石天使守护着--这种优美的形象后来再也没有出现过。约翰内斯大师最著名的作品是密涅瓦宫中的威廉-杜兰提斯纪念碑，这是一件精雕细琢的作品。1299 年在圣玛丽亚马焦雷（St Maria Maggiore）建造的阿尔巴诺红衣主教贡萨尔维斯（Gunsalvus of Albano）之墓也与之类似。艺术家将自己的名字刻在了第三件此类作品上，即位于圣巴尔比纳的波尼法斯八世的神甫助手、吉卜力家族苏尔迪的斯蒂芬的华丽纪念碑上。梵蒂冈石窟中的博尼法斯八世墓是否也是圣约翰的作品还不确定。我们在那里看到了教皇的石棺和他的大理石雕像；这件作品简洁有力，却没有前几件作品的精美。

科斯马特人的艺术以 1302 年逝世的阿拉科埃利（Aracoeli）阿夸斯帕塔（Acquasparta）方济会将军马修斯（Mattheus）之墓而告终，该墓不再刻有约翰的名字，也没有任何铭文，但属于该艺术家的流派。同年，红衣主教杰拉德-帕尔马去世；他的纪念碑位于拉特兰大教堂的左侧过道，现在高高地砌在墙壁上，是一个简单的石棺，上面刻着用莱昂尼诗句写成的冗长而野蛮的碑文。棺盖上只有死者的雕像，后来在墙壁上加高了棺盖，使其清晰可见。

我们再来看看罗马教堂中常见的墓碑，这些奇怪的石制死亡日历曾像马赛克一样铺满大殿的地板，如今已逐渐消失。从八世纪开始，死者就被埋葬在教堂里。世纪起，死者被埋葬在教堂里。在很长一段时间里，他们的埋葬地只是在地板上挂一块牌匾，上面写着他们的名字、死亡日期和 "愿他们的灵魂安息 "的字样。后来，除了碑文外，石碑上还刻有蜡烛的图像；再后来，特别是从 13 世纪开始，人们用浮雕或轮廓的方式描绘出死者本人，他沉睡在枕头上，双手交叉放在胸前，头部左右两侧是家族盾徽；拉丁文碑文刻在石板边缘。这些纪念碑中最古老的大多已被毁坏，但在阿拉科埃利（Aracoeli）、圣塞西莉亚（S. Cecilia）、米涅瓦顶上的玛丽亚（Maria sopra Minerva）、普拉塞德（Prassede）、萨比纳（Sabina）、帕尼斯佩尔纳的洛伦佐（Lorenzo in Panisperna）和其他教堂中仍有一些十三世纪的纪念碑。有时，面板上还镶有马赛克。最精美的此类艺术作品是 1300 年在圣萨比纳的多明我会将军穆尼奥-德-萨莫拉的墓碑，这是大师雅各布斯-德-托里蒂的作品。

这种纪念碑在十四世纪越来越常见，也是当时服饰的杰出代表。它们还展示了剧本中人物的逐渐变化，对此我们只能指出这一点。在十三世纪上半叶，罗马仍然盛行古老的书法字体；到了该世纪末，这些字体变得躁动不安；人们注意到，尤其是_E_、_M_、_N_和_V_的画法完全是随意的。罗马字母的线条变成了弧形，_E_和_C_开始用一笔收尾。在本世纪末，字体变成了斜体。新字体的特点是_T_，它将横线的钩深深地向下拉，呈弧形。这种绘画原则使字体看起来色彩斑斓、奇特怪异。这种字体在整个十四世纪都占据主导地位，直到复兴时代才消失，被称为哥特式字体。虽然这些文字与哥特人的艺术风格关系不大，但它们与十三世纪末在意大利形成的哥特式艺术风格有关。它们在碑文中与哥特式艺术风格相协调，就像阿拉伯文字与摩尔建筑风格相协调一样完美。它们表达了人类审美感受的转变，同时也与当时日益复杂的服饰有关。它们与古罗马文字的贵族形式相联系，就像哥特式教堂与大教堂相联系，就像粗俗的民族语言与拉丁语相联系。
