### 第三章

_1\. 格列高列七世的方案。皇帝任命格布哈特-冯-艾希施泰特为教皇。洛林的戈特弗里德与托斯卡纳的贝娅特丽克丝结婚。亨利三世来到意大利。维克托二世成为教皇。皇帝驾崩（1056 年）。艾格尼丝皇后摄政。维克多二世在意大利的帝国总督。戈特弗里德的强势地位。红衣主教弗雷德里克。维克托二世去世。斯蒂芬九世成为教皇_。

教会的深刻变革也掩盖或主导了这一时期罗马城的历史。长期以来，罗马城一直是政教斗争的场所和中心，只是好不容易才获得并发展了自己的市政独立性：罗马城沦为教皇或皇帝的服务对象，而教皇或皇帝又是罗马城的党派。

在奥托王朝统治结束后，罗马贵族征服了教皇，并以一种动荡的方式维持了一段时间的贵族统治；但它的城市权力没有永久的形式，一旦帝国或教皇采取强有力的行动，它就会瓦解。亨利三世摧毁了图斯克兰人的暴政，他也将教皇选举与贵族制一起带到了德国，并通过他的德国教皇为教会注入了新的活力；在德国的帮助下，教会刚刚恢复了力量，就又向它的救世主索要选举权，并最终要回了完全的自由。格列高列七世不仅成为罗马第一人，也成为所有民族和时代最伟大的政治家之一。作为改革运动的领袖，他很快就把所有其他人都变成了自己的工具：圣人和僧侣，他煽动了他们的狂热；教皇，他为他们指明了方向；伦巴第的教父们，他把他们派到战场上作为反对贵族和蔑视主教的煽动者；热情的托斯卡纳侯爵们，他赢得了他们的友谊；贪婪的诺曼人，罗马教会在他们那里赢得了附庸和捍卫者。这位大胆的司铎手中举着的旗帜上，最初只有两条从惩戒教规中借来的禁令： 姘居和殉夫；这两项禁令都是当时的现实伤痛，但凭借高超的技巧，它们最终变成了教皇渗透国家的突破口，从德意志王室手中夺回了贵族权，并获得了对世界的精神统治权。

在格列高列七世的计划中，长期以来受帝国权力限制的自由选择教皇尚未被放在首位。由于对一个强大皇帝的恐惧和罗马的不确定性（贵族会再次夺取选举权），牧师党不得不耐心地服从帝国法律。与德意志决裂的想法也远离了每个人的头脑。

据说，利奥九世在临终前为了教会的利益推荐了格列高列七世；所有的目光都已聚焦在这一位修道士身上，狂热者们大声要求他成为教皇。然而，他还是去了亨利的宫廷，以便从德意志（如果可以的话）引进另一位教皇，他不能缺少这位具有改革思想的皇帝的支持。伟大的日耳曼人惊讶地看着这位修道士，他是作为罗马神职人员的代表之一来干涉教皇选举的。在具有日耳曼思想的贵族们的罗马使节也来到帕特里修斯的宫廷后，亨利应格列高列七世和罗马人的迫切要求，将艾希施泰特主教提升为教皇。格布哈特出身于卡尔乌伯爵家族，是他的亲戚，此人国事经验丰富，精神饱满，力大无穷，而且还很年轻，是他信任的参谋；因此，他付出了巨大的牺牲才把他辞退，但他希望能在意大利很好地利用这位忠实朋友的洞察力，在那里，帝国的一个叛逆者刚刚获得了一个非常强大的地位。

托斯卡纳的博尼法斯被暗杀（1052 年 5 月 6 日），他的遗孀贝娅特丽克丝两年后嫁给了洛林公爵戈特弗里德。后者是帝国的敌人，以难民身份来到意大利，现在，他不顾皇帝的反对，夺取了博尼法斯的大片土地，并从此以博尼法斯三个未成年孩子的名义统治这些土地。他因此成为全意大利最有权势的王子。这片土地一直是外国财富的舞台；外国人是他的皇帝和公爵；外国人是他的教皇和许多最受尊敬的主教；外国人是诺曼人，他们刚刚建立起自己的南意大利帝国。如果像戈特弗里德这样勇敢而聪明的人与他们联合起来，如果他将整个意大利中部统一在自己的权杖之下，他难道不能赢得意大利和罗马的王位，并随心所欲地拥立教皇吗？

格布哈特接受教皇职位的条件是，皇帝承诺帮助罗马教廷获得其所有的财产；亨利还允许他在罗马举行补选。艾希施泰特主教于三月离开雷根斯堡，匆匆赶往罗马。皇帝本人于3月启程前往意大利，但他没有抵达罗马。他以一如既往的干劲在意大利北部组织帝国事务，那里的大人物们很快就服从了他；贝阿特里克斯还出面维护她与前叛乱者的婚姻，反对国家的专制原则，但愤怒的皇帝将她和她的孩子玛蒂尔达拘禁了起来。她那逃亡的丈夫仍然无法摆脱皇帝的怒火；他甚至在洛林再次拿起武器，迫使皇帝很快回国。亨利在返回德意志之前，于6月在佛罗伦萨宗教会议上会见了教皇。他授予维克托二世意大利助理司祭的权力，让他在意大利牵制戈特弗里德公爵。高特弗里德公爵的弟弟弗雷德里克在利奥九世时期曾在教会任职，目的是为自己开辟仕途；他被利奥九世提升为红衣主教和议长，最后被派往拜占庭担任教皇使节，在那里他以外交才能和强大的人格魅力赢得了声誉。当他带着许多珍宝回国时，皇帝命令教皇逮捕他，但腓特烈早有预兆，他逃过一劫，在蒙特卡西诺戴上了头巾，躲在这里或特雷米蒂岛上，以躲避远方皇帝的愤怒。

维克多二世在罗马居住了一年，致力于教会改革。和他的前辈们一样，他在这里感到很不开心，渴望回到德意志。1056 年夏天，他被召回德意志，处理教会和国家事务，很快他就能在他的帝国朋友的尸体前感叹，荣耀、权力、主权和幸福在他面前荡然无存。1056年10月5日，伟大的亨利三世驾崩，年仅39岁；他结束了法兰克王朝强大的帝王世系，这些帝王曾将德意志推向世界权力的顶峰。这位王子的突然去世震撼并改变了世界，对德意志本身来说也是最大的灾难。妻子作为监护人留在他的灵柩前，孩子作为国王，德意志和意大利陷入了无政府的混乱之中，但新兴的教会却看到自己从帝国的独裁统治中解放了出来。当维克多二世在他朋友的灵柩前哭泣时，就像西尔维斯特二世曾经在奥托三世临终前哭泣一样，修道士格列高列七世能感觉到他对毫无防备的皇权继承人的胜利。

阿格尼丝皇后是阿基坦公爵威廉的女儿，伦巴第人曾将王冠献给了威廉，她成为了儿子亨利四世的摄政王，亨利四世当时还不到六岁，但却比西奥法诺曾经拥有的更多困难和更少的天赋。她的顾问起初是教皇，因为垂死的皇帝向他推荐了帝国和继承权；他仁慈地安排了德意志事务，并确保了孩子的继承权，但很快他就不得不返回罗马，并被解除了意大利帝国代牧的职务。在这里，教皇以皇帝的名义统治（这种情况很少见！）所有的王室土地，只要他还活着，他还以公爵的身份管理着斯波莱托和卡梅里诺。只有戈特弗里德的权力不受任何更高权力的限制。维克多不得不赶紧拉拢他；事实上，他已经在1056年12月的科隆帝国议会上让他与女皇和解了。

高特弗里德得以带领妻子和继女玛蒂尔德返回意大利，并被帝国承认为博尼法斯侯爵所有领地的主人。从那时起，他的王权使他对教会事务的影响力超过了之前的斯波莱托公爵。他认为自己是罗马的帕特里西乌斯（Patricius），主持教皇选举或介绍教皇都属于他；毫无疑问，阿格尼丝皇后即使没有授予他教皇的头衔，至少也授予了他罗马主教的永久权力以及在科隆保护教皇的权力。博尼法斯公爵之前也曾担任过同样的职务。

当维克多二世于1057年春天回到佛罗伦萨时，他试图联合这个洛林家族。戈特弗里德的弟弟弗雷德里克已经被维克多二世任命为卡西诺山修道院院长，现在维克多二世又于6月14日任命他为佛罗伦萨特拉斯提维尔的圣克里索戈努斯枢机主教。格列高列七世选择了洛林作为未来的教皇；他将这个强大的家族置于罗马和德意志之间，这个家族似乎只与德意志帝国和解，他希望在这个家族的帮助下争取教会的独立。

新任红衣主教隆重地迁往罗马，在那里，他作为意大利第一位王子的兄弟受到了隆重的接待；他接管了自己的名义教堂，并在帕拉拉的圣玛丽亚修道院（S. Maria in Pallara）的废墟上居住下来，当时那里已经住着来自蒙特卡西诺的本笃会教徒。他刚到那里不久，就传来了维克多二世去世的消息。帝国在意大利的唯一支柱倒下了，洛林家族突然发现自己已经接近了辉煌的顶峰。现在，在最后一位帝国教皇去世后，在一个弱女子的统治时期，可以尝试选举一位自由教皇了。当然，这只能由来自洛林的红衣主教来完成，因为只有他才拥有反抗德意志王室的权力。

虽然格列高列七世为了体面而亲自提名为教皇候选人，但他尚未到任。8月2日，贵族、神职人员和民众迫不及待地赶往罗马堡；这位强人被领到圣彼得罗-罗马诺堡，在那里他匆忙当选，并被宣布为斯蒂芬九世。凯旋后，他被带往拉特兰占领，并于8月3日在圣彼得大教堂被授予圣职。许多罗马人的声音欣然汇聚到了这位受到德意志皇帝迫害的王子身上，他们在他身上实现了长期以来第一次自由的教皇选举。

斯蒂芬的崛起使洛林家族在意大利的影响力不受限制。托斯卡纳侯爵现在还夺取了斯波莱托（Spoleto）和卡梅里诺（Camerino），从而将从曼图亚（Mantua）和费拉拉（Ferrara）直到罗马领土的几乎所有土地联合起来。还有什么能比新教皇将帝国的王冠留给他的弟弟，戈特弗里德让他当教皇只是出于这个目的更自然的呢？

德意志宫廷听到维克多的死讯后悲痛不已，听到斯蒂芬的自由当选后也很不高兴；但它太软弱了，无法有力地收回被剥夺的贵族权利，而罗马人民不仅赋予了亨利三世，也赋予了他的继任者们这些权利。一段时间后，斯蒂芬九世派格列高列七世作为他的圣座大使前往德意志，让这位娴熟的外交官在那里道歉和安抚。斯蒂芬九世任命希尔德布兰德为大主教（Archidiaconus），让他在教廷中位居第一。看到德国宫廷和罗马教廷之间即将出现裂痕，他急忙召集身边最勇敢的斗士。格列高列七世是改革派真正的政治领袖，而被斯蒂芬带到罗马担任红衣主教的奥斯蒂亚主教皮埃尔-达米亚尼则是改革派狂热的先知。这位修道士的出现、他的指导和他的工作值得关注，因为它们代表了当时生活潮流的一个重要元素，而当时城市的历史也离不开这些元素。
