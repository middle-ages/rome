_4\. 教皇在罗马无能为力。教皇国的解体。城市长官。森西乌斯（Cencius）不服气的首领。辛西乌斯（Cinthius）城市长官。托斯卡纳的戈特弗里德去世。皮埃尔-达米亚尼之死。卡西诺山。庆祝德西德里乌斯（Desiderius）重建的大教堂落成（1071 年）_。

改革之争使亚历山大二世的统治充满了狂热的骚乱，教皇制度经历了自圣像崇拜之争以来最激烈的时期。教皇经常在罗马城外活动，特别是在托斯卡纳和他的卢卡教区，即使作为教皇，他也没有为了收入而放弃卢卡教区。虽然贵族派别已经平息，但这座躁动不安的城市的状态仍然不稳定，亚历山大喜欢尽可能多地离开这里。他的世俗权力被限制在最低限度；教皇对坎帕尼亚的伯爵们完全无能为力。教皇们在卡洛林时代曾派遣他们的校长、执政官和公爵到最遥远的城堡，甚至是潘塔波利斯和罗马涅担任法官、将军和财政官员，而在这个时代，教皇们在罗马附近几乎不拥有这样的权力。卡洛林王朝的教皇国已经解体；教会的前官员或佃户伯爵成为了城市的世袭领主，他们在城市中任命自己的副主教，而在流放的主教区和修道院中，教长们自己掌握着伯爵的旗帜，并任命他们的行政官员和法官。当时幸存下来的教皇国中，拉丁姆、滨海马萨、萨比纳河部分地区和罗马图西亚只是在概念上构成了教会的统治；实际上，这些省份被分割成了上百个小男爵领地。

在罗马，大王朝嘲笑教皇的主权。贵族或元老院以传统的形式行使城市管理和司法权。然而，人们仍然看到教皇主持民事法庭，或者他派遣自己的代理婚姻主持民事法庭。在这个时代，城市长官不仅在民事司法中扮演重要角色，而且作为刑事司法系统的主席，他还主持罗马和城市地区的死刑裁判权。他的职位变得比以往任何时候都更加重要；大人物们争相竞聘，他的任命通常会让罗马充满动荡。自尼古拉二世以来，罗马人就被剥夺了教皇选举权，他们顽固地保留着选举他们最重要的城市行政长官的权利；他们在议会中选举省长，但皇帝只要能维护他的宗法权力，就会给他叙任权，或者他授权教皇代替他授予叙任权。当然，教皇们也努力将都督府从帝国的职位降为教皇的职位；至少在这一时期，他们经常成功地设立都督，而不考虑帝国的叙任权。

在亚历山大二世的最后几年，这位官员的人选引起了激烈的争论。罗马人岑西乌斯甚至在卡达卢斯倒台后仍继续蔑视教皇；他一定属于拥有圣天使城堡（卡达卢斯之塔）的克里森特家族，但他已不再拥有这座重要的城堡，因为在卡达卢斯倒台后，这座城堡已被夺走。他向往城市权力，但既没有继承祖先的权力，也没有继承转瞬即逝的财富。他的父亲斯特凡（Stefan）曾担任过城市长官，并没有被格列高列七世党人赶下这个职位；他去世后，他的儿子希望成为他的继任者，但改革党人提升了一位虔诚的人担任长官，他就是辛西乌斯（Cencius 或 Cinthius），他是约翰内斯-提尼奥斯（Johannes Tiniosus）的儿子，格列高列七世曾在 1058 年任命他为长官。当时的报道称，斯蒂芬之子辛西乌斯是一个不信神的强盗和通奸犯，是第二个卡蒂利纳，他们可能并没有夸大这位卡达卢斯派系首领的罪行。当他没有赢得省长职位时，他就用自己在那里建造的一座高塔从城市一侧封锁了哈德良大桥；他在桥上安排了卫兵，向所有过桥的人收取过桥费。如果允许一个罗马伟人像强盗男爵一样在圣彼得大教堂入口处安营扎寨，就可以判断教皇们在这座城市的权力有多小了。如果教皇能够让城里的民兵服从于他们，那么他们就能把罗马的贪婪贵族清除出去；但他们并不总是掌握着民兵，这些公民连队往往是独立存在的，为他们的党派或代表他们的大人物服务。罗马没有一个统一、全面的教皇政府；相反，罗马和米兰一样，被分为两大阵营，并被分隔成受人尊敬的王朝集团及其附庸。教皇们除了那些通过劝说和黄金赢得的支持者，或者那些他们将教会财产以封地的形式给予的附庸之外，没有其他的支持者，而且由于此时使徒彼得的财产几乎耗尽，他们的好战分子人数只能极少。

格列高列七世可能已经尽其所能，将市府交到了一位改革之友的手中。约翰的儿子辛修斯（Cinthius）将在罗马接管埃勒姆博德在米兰执行的十字军东征任务。如果说他的对手辛西乌斯是以魔鬼的形象出现的，那么后者则被他的党派视为圣人。他是格列高列七世和米兰的两位改革者的密友，和他们一样，他也被一种狂热的激情所鼓舞，但这种激情并不具有黑暗狂热的特征，因为罗马是殉道者的贫瘠土壤。罗马人惊讶地看着在圣彼得大教堂公开宣讲忏悔布道的市政长官；甚至达米亚尼也一定很惊讶，共和国的一位官员竟然在宣讲和尊崇第一批基督徒的原则，根据这一原则，每个基督徒都是司铎；而这一格言与格列高列七世的制度几乎是格格不入的。达米亚尼不得不告诫他的朋友，不要忽视人民的暂时拯救而忽视自己灵魂的拯救，因为他说，主持正义无异于祈祷。没有什么比两个罗马人的对比更能说明当时罗马的状况了：辛西乌斯在天使之桥的塔楼上抢劫杀人，辛提乌斯在圣彼得大教堂布道却忽视了正义。

亚历山大二世的最后时期也发生了一些奇怪的事件。两位名人在他之前去世，托斯卡纳的戈特弗里德和皮埃尔-达米亚尼。侯爵于 1069 年死在洛林；这块土地由他第一次婚姻所生的儿子驼背戈特弗里德继承；他娶了贝阿特丽斯的独生女玛蒂尔德，因此洛林和意大利的继承权仍然属于这个家族。出于软弱，德意志国王没有主张重新占领托斯卡纳侯爵领地的权利；即使在女系中，世袭原则也得到了默认，寡妇得到了她第一任丈夫留下的帝国封地，然后她又将这些封地遗赠给了她的女儿，明智的罗马教会继续受到贝娅特丽克丝和玛蒂尔达这两位杰出女性的保护。

在这个被宗教狂热深深激起的时代，意大利有一些杰出的女性脱颖而出。在更早的世纪，我们注意到了狄奥多拉和马罗齐亚、贝尔塔和伊尔明加德的身影，她们作为派别的领袖帮助决定了意大利和罗马的命运。十一世纪中叶，又是女性对她们的时代产生了巨大的影响，但她们的意义与她们的前辈根本不同。除了贝娅特丽克丝和她的女儿，皮埃蒙特苏萨的阿德莱德侯爵夫人也早已凭借其智慧、财富和权力大放异彩。与贝娅特丽克丝一样，她也曾两次结婚、两次守寡，先是嫁给了斯瓦比亚公爵赫尔曼，后来又嫁给了奥多侯爵。1065年，她的女儿贝尔塔将她嫁给了年轻的亨利。 亨利厌倦了她，想要休掉她，但罗马教会阻止了离婚；1069年，皮埃尔-达米亚尼作为她的使节前往沃尔姆斯，国王第一次服从了教皇的命令。

这是达米亚尼在意大利以外为罗马服务的最后一次公使。他于 1072 年 2 月 22 日在法恩扎去世，享年 66 岁，被誉为当时教会中最虔诚的人，也是怀着最纯洁的愿望为教会改革最热心的斗士之一。不久前，他经历了意大利有史以来最辉煌的教会节日。1071 年 10 月 1 日，德西德留修道院长在蒙特卡西诺建成的大教堂举行了祝圣仪式。

这座修道院是当时意大利最辉煌的修道院。那里居住着 200 名修道士，其中许多人热衷于世俗和精神科学的研究。从这里走出了许多名人。斯蒂芬九世曾在 1057 年担任该修道院院长，但他的继任者德西德留斯（Desiderius）的文学才华或他在修道院学院中团结的学者们的学术成就比他更耀眼。当伦巴第诸国分崩离析时，卡西诺山仍在其怀抱中汇聚着这个日耳曼民族最后的精神之花。德西德留斯（Desiderius）或道费留斯（Dauferius）出身于伦巴第的贝内文托家族。意大利的大多数修道院都变得一贫如洗，但卡西诺山的财富却始终保持着巨大的增长；这个修道院共和国的领土坐落在贫瘠的石灰岩山脉上，在诺曼人或垂死的伦巴第人的年轻国家中，它本身就是一个繁荣的国家。虽然伦巴第人和诺曼人都时不时地掠夺修道院的领地，但他们还是被迫再次交出这些领地，厚颜无耻的征服者们也许更害怕拉特兰的诅咒，而不是修道院院长手中的禁令，他就像高耸入云的卡西诺山或开罗山上的小朱庇特，时不时地把禁令扔到他们 "不以为然 "的头上。卡西诺山是南方伦巴第人和狂野的诺曼人的麦加；他们抢劫，但他们热衷于崇拜圣本笃，并唱着诗歌到他的墓前朝圣。他们急于摆脱所有道德和政治上的罪恶，把几个世纪以来积累的忏悔戒律转化为金银财宝，于是修道院明智地把他们和其他王公们获利的罪恶，连同希腊帝国的礼物一起收藏在宝库中。教皇和红衣主教们可以羡慕地看着满满一箱拜占庭黄金，或者欣赏他们拥有的珠宝和锦缎地毯。他们不得不将拉特兰宫的凋敝与德西德里乌斯用五年时间建成当时意大利南部的奇迹--新大殿--的巨大财富相比较，心中充满了悲哀。

来自四面八方的贵宾齐聚一堂，共同庆祝祝圣仪式。教皇与格列高列、达米亚尼和其他许多红衣主教一同前来；下意大利的十位大主教和 44 位主教也出席了仪式。诺曼伯爵和最后几位伦巴第王公也到场了：卡普亚的理查德和他的儿子乔丹以及他的兄弟雷诺夫（Rainulf），他们最近是罗马的敌人，现在却成了和解的附庸；萨莱诺的吉索夫（Gisulf）、仍是贝内文托领主的兰多夫（Landulf）、那不勒斯公爵塞尔吉乌斯（Sergius）、索伦托的塞尔吉乌斯（Sergius）、马尔西伯爵、无数的骑士和贵族都来了；只有罗杰和罗伯特-吉斯卡尔（Robert Guiscard）没有到场，因为他们当时正在攻打巴勒莫。华丽的集会就像罗马和南意大利的大议会，很少有如此多的名人聚集在一起。人们不难想象，伟大的格列高列七世很快就会接替衰弱的亚历山大担任教皇，但人们很难想象德西德留修道院长也会戴上教皇的头冠。

庆典持续了整整八天；意大利从未见过类似的庆典，即使是今天，当了解情况的人在蒙特卡西诺（著名的德西德里乌斯大教堂已不复存在）拿起那份羊皮纸大文件时，也不禁肃然起敬，亚历山大二世、皮埃尔-达米亚尼、格列高列七世、德西德里乌斯、卡普亚的理查德、乔丹、雷努尔夫、贝内文托的兰道夫和萨勒诺的吉苏尔夫在就职典礼当天都在上面刻下了自己的名字，其中有些还是亲笔所书。

这个节日既是罗马与诺曼人之间的政治联盟，也是意大利全国性的教会庆典，从任何意义上讲都是一次反对德意志帝国的伟大示威。格列高列七世的努力在其中庆祝了罗马教会历史上出现的新时代的第一次胜利，就像一个象征。
