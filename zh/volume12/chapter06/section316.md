### 第六章

_1\. 约翰二十三世与罗马宗教会议 西吉斯蒙德在意大利。约翰二十三世宣布召开大公会议。拉迪斯劳斯出现在罗马。那不勒斯人入侵罗马城。约翰的逃亡和迫害。拉迪斯劳斯（Ladislaus）1413 年罗马领主。拉迪斯劳斯占领教皇国。约翰二十三世在佛罗伦萨。康斯坦茨被选为大公会议的会址。教皇与罗马国王在洛迪会晤。大公会议在康斯坦茨召开。若望二十三世返回博洛尼亚。

大公会议是否在三年内继续举行仍在比萨决定；教会的腐败无边无际，其教义的统一受到了威克里夫异端日益强大的威胁，这就要求进行彻底的改革，而这只能是大公教会会议的工作。约翰二十三世在担任教皇的最初几年里一直只处理世俗事务。虽然他于 1412 年 4 月在罗马召集了一次会议，但令他自己满意的是，这次会议的参加人数少得可怜，不能被视为教会大会。在那次宗教会议上发生了一个滑稽的巧合，没有什么比这个故事更能说明当时人们对这个人的亵渎本质的看法了。当圣约翰在梵蒂冈小教堂做晚祷，吟唱_Veni Creator Spiritus_时，一只毛茸茸的夜猫子代替圣灵出现了，并用火热的目光注视着教皇。在第二次会议上，它又回来了，红衣主教们或大笑或惊愕，用棍棒将它打死。许多历史学家都注意到了这一事件。

与此同时，人们不断敦促圣约翰召开会议。巴黎大学的使者敦促他亲自在罗马召开会议。西吉斯蒙德决定恢复帝国与意大利长期中断的关系，并已于 1411 年年底在此露面，当时他因为察拉的缘故对威尼斯人发动了战争，并在伦巴第占据了指挥位置，起初他并不高兴，后来取得了胜利。在各方的压力下，约翰最终于 1413 年 3 月 3 日向基督教世界宣布，他已经平定了罗马，将格里高利十世从那不勒斯赶走，并与那不勒斯王国媾和，同时宣布于次年 12 月在一个尚未确定的地方召开大议会。他的决定是虚伪的，但一连串奇怪的事件（最初始于那不勒斯）迫使他执行了他想避免的事情。

拉迪斯劳斯放弃了格里高利，与约翰媾和只是为了欺骗他。他渴望惩罚安茹的战争活动，因为它使他濒临毁灭。他的心思一直集中在意大利的王权上，他最初希望通过联合教皇国和那不勒斯来实现这一目标。但是，意大利的统一一时似乎无法通过那不勒斯从南方实现，而拉迪斯劳斯，这位那不勒斯王国最有进取心的君主，却在另一个方向上成为了时代的重要工具，因为他对罗马的攻击和即将到来的死亡加速了结束分裂的大议会的召开。

当约翰宣布他打算在罗马之外召开教会大会时，国王以此为借口破坏了他的条约。他宣称，在教皇外出期间，他必须保护罗马不发生动乱。流亡的罗马人引诱他再次夺取罗马城。虽然他的老盟友约翰-科隆纳已于 1413 年 3 月 6 日去世，但他仍然找到了足够的支持者。罗马人本身也憎恨教皇，急不可耐地要求改变他们的处境。拉迪斯劳斯以最无耻的不忠行为违背了他勉强宣誓的义务。5月，他派遣一支军队前往马尔凯，斯福尔扎将他的对手、教会督军保罗-奥尔西尼（Paul Orsini）围困在罗卡-康特拉达（Rocca Contrada），阻止他赶往罗马。同月底，一支那不勒斯舰队驶入台伯河口，拉迪斯劳斯本人也启程前往罗马。塔利亚科佐的奥尔西尼家族伯爵娶了约翰二十三世的侄女为妻，他的抵抗很快就被克服了，国王从格罗塔费拉塔向罗马城进发，没有遇到任何抵抗。在这里，有人欢喜有人忧。国王的背信弃义似乎让神职人员们大惑不解，他们怀疑国王与教皇达成了秘密协议。教皇们在公众舆论中被贬低得如此之深，以至于他们被视为自己城市的叛徒，而他们的前任曾如此频繁、如此顽强地捍卫这座城市，对抗国王和皇帝。如果像科萨这样的渎神者真的采取了这种手段，通过推翻罗马和教皇国来制造对自己有利的混乱，那么在格里高利十二世的程序之后，这就不再奇怪了；但在这种情况下，约翰二十三世就会显示出自己是最不可理喻的人。这些事实并不完全与最初与拉迪斯劳斯达成的协议相矛盾，但它们确实表明约翰允许自己被国王严重欺骗。

当拉迪斯劳斯逼近罗马城门时，教皇采取了防御措施。为了安抚人民，他取消了压迫性的葡萄酒税，甚至还给了罗马人自由。6 月 5 日，他将市政府交到了市政官和地方长官的手中，并用华丽的辞藻劝说他们不要害怕国王，因为他自己也准备与他们共赴黄泉。在朱利亚诺山伯爵费尔西诺-德-赫尔曼尼斯参议员的主持下，人民于次日在元老宫集会。这个议会同样夸张地发誓，宁死也不屈服于那不勒斯。这些罗马人喊道，我们要先吃掉自己的孩子，然后再向巨龙拉迪斯劳斯投降。现在，每个明智的人都知道这出喜剧要说什么了。罗马人最后的共和公民美德已经泯灭，他们任由国王摆布。人们怀疑拉迪斯劳斯是带着教皇的旨意和背叛而来的，这种怀疑甚至使那些爱国荣誉感仍在蠢蠢欲动的人也瘫痪了。

6 月 7 日，教皇和他的全体幕僚离开梵蒂冈，前往台伯河这边马努佩洛的奥尔西尼伯爵家族的宫殿，他在那里过了一夜，向人民表明他对他们有信心。那不勒斯人已经到了门口。预计会有一场暴风雨。然而，6月8日早上却传来了敌人已经进驻罗马的消息。拉迪斯劳斯在夜间攻破了圣克罗切的城墙，他的战地指挥官塔塔利亚从这个缺口进入了罗马。他在拉特兰犹豫不决，直到天亮。当他没有受到攻击时，当少数几个向前迎击他的民兵害怕地回头时，塔尔塔利亚带着嘹亮的吼声向城市中心进军。从未有过如此迅速的征服。约翰二十三世立即率领他的宫廷逃离罗马，而拉迪斯劳斯则进驻拉特兰。他的骑兵沿着卡西亚道追击逃亡的蜂群九英里；一些传教士累死在路上；教皇的亲兵劫掠了教区。他好不容易逃到了苏特里，又从那里与英诺森七世在同一天晚上逃到了维泰博。

在此期间，拉迪斯劳斯以征服者的傲慢态度对待罗马。他的士兵大肆掠夺，放火焚烧房屋；档案被毁，教堂遭劫；圣殿被无礼地嘲弄；醉醺醺的士兵用教堂里的金杯与妓女狂欢；巴里的红衣主教被拖进监狱，圣彼得大教堂的圣器室被清空；神圣的大教堂里被放进了马匹。国王违背诺言，没收了佛罗伦萨商人的所有货物，并将许多罗马人俘虏到了王国。他任命尼古拉斯-德-迪亚诺（Nicolaus de Diano）为元老，领导新政府。他用自己的名字铸造了一枚硬币，并给自己加上了一个奇怪的头衔："城市杰出的照明师"。他向饥寒交迫的罗马人提供粮食，并将粮食分发下去。这座城市已经陷入了如此贫困的境地，似乎居住着一个乞丐民族；事实上，在中世纪历史的废墟下，它能引起与托提拉时代相同的怜悯。

城市地区的所有城镇再次向国王致敬，奥斯提亚于 6 月 24 日投降。他还在彼得领地任命了自己的官员，并任命罗马人克里斯托弗罗-卡波-迪-费罗（Cristoforo Capo di Ferro）为那里的首领。他把军队交给了他的督军，任命卡普亚的朱利奥-切萨雷（Giulio Cesare）为梵蒂冈的最高长官，特洛伊伯爵（Troy Count）为特拉斯特维尔（Trastevere）的助理司祭，多米尼克-阿斯塔利（Dominico Astalli）为芬迪（Fundi）的主教，然后于 7 月 1 日经奥斯蒂亚返回那不勒斯。 圣天使城堡仍然只为教皇而守，直到 10 月 23 日才投降。当时举行了庆祝活动；人们举着火把走街串巷，高呼 "拉迪斯劳斯国王万岁"！

现在，约翰二十三世像格里高利十世一样四处逃亡。罗马被征服的打击使他连根拔起，像风中的枯叶一样继续前行。他没有冒险去佛罗伦萨，因为那里舆论纷纭，人们担心国王会报复；他像个流亡者一样，不得不在圣安东尼附近的郊区寻找住处，直到佛罗伦萨人勉强收留了他。他在那里一直待到初冬，而那不勒斯人则征服了锡耶纳的所有土地。他写信向基督教会控诉，并向国王们求助。他派红衣主教查兰特前往伦巴第，请求西吉斯蒙德的支持。罗马王的使者来到佛罗伦萨，要求教皇召开会议。会议安排在洛迪举行。

就这样，那不勒斯国王把约翰二十三世推向了罗马王的怀抱，经过漫长的中断，帝国的权力重新回到了教皇的手中。在帝国本身丧失了所有权利的时候，西吉斯蒙德看到自己被要求凭借这些古老的帝国权利成为教会的恢复者。自值得纪念的里昂大公会议以来，教皇制度的历史已经过去了150年，在这之后，教皇制度的历史又回到了原来的轨道上；教皇制度本身很快就在德国的一个城市召开了一次会议，这次会议与里昂会议正好相反。教皇国的重心转移到法国，然后又因分裂而失去了道义和政治上的权力，在这之后，教皇国又回到了早期德意志皇帝召集宗教会议审判不称职和争吵不休的教皇的时代。

对于狡猾的科萨来说，选择一个不会让他受皇帝控制的地方召开会议非常重要。除了不惜一切代价夺取头冠，他别无他法，但他的能力已经耗尽。约翰二十三世的故事是最杰出的例子之一，它说明了环境的力量可以通过债务来奴役个人的意志，从而使他无可救药地陷入他为自己编织的网中。他给西吉斯蒙德的使节、圣塞西莉亚的安东尼奥斯-查兰特枢机主教和圣科斯马和达米亚诺的弗朗西斯库斯-扎巴雷拉枢机主教都是如此。在著名的希腊人曼努埃尔-克里索拉斯（Manuel Chrysoloras）的陪同下，他曾就意大利城市的选择发出过指示，但后来又收回了这些指示，并授权他们与罗马国王达成协议。他认为他们会采取对他有利的行动；但当这些全权代表在洛迪出现在西吉斯蒙德面前时，罗马国王要求将德国城市康斯坦茨作为召开会议的最合适地点。他们向教皇报告了此事。教皇控告他们叛国，并服从了西吉斯蒙德的意愿。

11 月 12 日，他前往博洛尼亚。由于佩波利（Pepoli）、本提沃格利（Bentivogli）和伊索拉尼（Isolani）家族领导的贵族革命，这座城市于1413年9月22日再次臣服于教会，现在却不情愿地欢迎它曾经的暴君。约翰希望能在那里站稳脚跟，摆脱即将召开的议会的圈套；但这只是个妄想。西吉斯蒙德召见了他；红衣主教们要求他离开；11 月 25 日，他任命佩特鲁斯-斯特凡内斯基-安尼巴尔迪家族为他在罗马的副主教，同日离开博洛尼亚，迈着不确定的步伐向罗马国王走去。两人在洛迪会面。虽然受到了尊贵的款待，但这是一种保留，预示着约翰的未来。他试图把国王争取到一个意大利城市，但徒劳无功。西吉斯蒙德的态度依然坚定，教皇不得不于12月10日在洛迪向基督世界宣布，在与罗马国王达成协议后，大公会议将于11月1日在康斯坦茨召开。西吉斯蒙德已于10月30日通过御书宣布了大公会议的召开，并邀请了所有王公、领主、牧师、医生和任何有资格参加会议的人前往康斯坦茨，同时保证了他们的安全。现在，他还邀请了本笃十三世和格里高利十二世前来；他还致函阿拉贡和法国国王，罗马国王作为基督教的领袖和教会的合法守护者的声音在很长时间里第一次被听到。

圣诞节后，西吉斯蒙德和约翰去了克雷莫纳，据说这座城市的暴君加布里诺-方达罗后来后悔没有把他的客人从他带领他们去的克雷莫纳塔楼上扔下去，因为那样他就能在同一时刻摧毁基督教的两位首领--如果这是真的，这种恶魔般的行为足以说明当时精神的堕落，当时世界上所有曾经的伟大都已堕落。西吉斯蒙德和约翰在克雷莫纳分手后，后者经曼图亚和费拉拉返回博洛尼亚，他于 1414 年 2 月抵达博洛尼亚，以其惯用的伎俩夺取了该城的兵团，并寻找逃离在康斯坦茨等待他的毁灭的方法。
