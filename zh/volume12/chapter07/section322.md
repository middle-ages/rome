_4\. 十四世纪的习俗和传统。从简朴到奢华的转变。佛罗伦萨和罗马。服饰。女性的时尚。禁止奢侈。节日气氛和公众游行 1332 年在斗兽场举行的备受质疑的斗牛比赛。 在特斯塔西奥和纳沃纳广场举行的比赛。附庸城市对罗马公共运动会的贡献。斗兽场的戏剧表演_Ludi Paschales_。

关于十四世纪罗马人习俗和传统的一些消息，使我们对罗马人的知识生活有了更全面的了解。如果一些编年史作者的话是可信的，那么意大利人在十三世纪仍然生活在原始简单的父权制条件下。但丁口中的卡西亚吉达对佛罗伦萨人的淳朴的赞美和里科鲍德对腓特烈二世时期所有意大利人的淳朴的赞美可能有些夸张，但可以肯定的是，意大利社会更丰富的发展只是在共和国发展出强大的国家生活和暴君宫廷中出现王公贵族的辉煌时才开始的。早在安茹的查理一世时期，人们就注意到法国习俗对意大利的渗透。维拉尼（Villani）认为，佛罗伦萨在1342年左右出现的华丽服饰是受法国人的影响，法国人随雅典公爵来到佛罗伦萨。习俗和时尚的转变不能仅用可见的历史原因来解释。每个国家都有一批非常保守的习俗，尤其是与教会崇拜有关的习俗，而其他形式的习俗则会在一夜之间发生变化。为了确定社会的蜕变，我们应该能够清楚地追溯到属于它的所有元素的混合。由于不可能做到这一点，因此通常只有整个世纪才能作为一个时间模型来展示。

佛罗伦萨的风俗发生变化的同时，罗马也发生了变化。罗马的一位编年史家说，人们开始改变自己的衣着，变得像加泰罗尼亚人那样紧身；人们开始在头巾上戴帽子，在腰带上系一个朝圣者式的包，原本只属于隐士和西班牙人的满脸胡须变得时髦起来。在十四世纪，宽大的服装被认为是体面的，维拉尼将其称为长袍的时尚，但在佛罗伦萨的古画中，人们看到的紧身和色彩鲜艳的服装却让位于宽大的服装。这就是所谓的塞浦路斯时尚。甚至妇女也穿这种衣服。她们的裙子底部很宽，从腰带开始往上很窄，裁剪得很宽，胸部几乎裸露出来。只有墓室的石板才能让我们看到罗马人的平民服饰。但在这些十四世纪和十五世纪的墓板中，没有一块是留胡子的，这告诉我们，留胡子这种被认为不雅的习俗在任何死者肖像中都是罕见的，或者是不允许的。也没有一个殡葬人物穿着窄小的服装，每个人都穿着宽大的衣服，通常从上到下都扣上纽扣，这绝不是裹尸布，而是真正的生活服装，因为任何男性人物身上都没有缺少贝雷帽。

妇女们佩戴着奢华的黄金、宝石和珍珠首饰，就连裙子也是如此。面料有布、亚麻、丝绸和天鹅绒，颜色鲜艳夺目。地方官对奢华的禁止是徒劳的，因为习俗是法律永远无法战胜的力量。早在十三世纪，红衣主教拉蒂努斯作为罗马涅的使节，就禁止在失去赦免权的情况下乘坐长火车。"这对女人来说比死亡更痛苦" 他命令她们举止得体。她们大声喧哗，然后戴上了最漂亮的金边面纱，比以前更加诱人。佛罗伦萨的威权统治者禁止妇女们在脸上戴上白色和黄色丝绸的假粗辫子，（1326 年）她们一直缠着卡拉布里亚公爵夫人，直到在她的说情下，禁令才被解除。为了保持共和国的节制，防止贫困化，佛罗伦萨和其他共和国通过了禁止普遍消费的法律。罗马人以他们为榜样，同时采用了他们的时尚和奢侈品禁令。顺便提一下，罗马贵族妇女的服饰是如此华丽，以至于匈牙利王后（路易的母亲）在 1343 年来到罗马时对其赞不绝口。然而，由于缺乏财富，罗马的奢华无法与其他城市相比。科拉为人们举办的奢华盛宴当然不同寻常。但是，罗马人的浮华和绚丽超过了所有其他意大利人。在中世纪，罗马也是唯一一个具有浓厚节日气氛的城市，而皇帝和教皇的加冕仪式以及对教会的崇拜使这种气氛得以延续。

由于罗马的光环，即使是罗马行政官的游行也比其他共和国的类似游行更加庄严肃穆。科拉时代的罗马阅兵式，在我们这个军装统一的时代将是一个壮观的场面。我们在阿维尼翁时代就有关于罗马行政长官游行的详细描述。身着紫色、天鹅绒和金色华丽长袍的官员经常骑马游行，这仍然给公民留下了共和国整体秩序良好的印象。在接待教皇、皇帝或其他王公贵族和元老的使节时，或者在庆祝公共运动会时，都会举行这样的游行。

当然，中世纪罗马人的比赛并不能让人对他们的文化或权力有一个很高的概念。竞技比赛是最美丽的骑士庆典。即使教会没有多次禁止，竞技比赛也不会在罗马兴盛起来；在文明的意大利，竞技比赛根本没有兴盛起来。关于 1332 年 9 月 3 日罗马贵族在斗兽场举行的斗牛，我们有一段奇怪的记载。据说，圆形剧场的一排排座位是用木制品重建的，并按照古代的等级分布。贵族妇女坐在红色覆盖的阳台上，由三位女士按区域引领。骑士战士们头戴头盔，头盔上印有各自夫人的颜色和座右铭，例如："我独自一人，就像霍拉提乌斯；我是拉维尼亚的埃涅阿斯；我是罗马卢克蕾齐娅的奴隶"。他们徒步进入竞技场，不穿盔甲，手持长剑和长矛。各自攻击自己的公牛。美丽的女人们欣赏着她们崇拜者的愚蠢英勇，哀悼着躺在竞技场上被牛角刺穿的十八个高贵的年轻人。他们被庄严地安葬在圣玛丽亚-马焦雷和拉特兰。然而，这一报道有许多不真实的痕迹，可能是十五世纪文艺复兴时期的发明，当时在罗马，罗维雷家族的西克斯图斯四世的侄子们举行了公牛狩猎和竞技比赛。我们怀疑，在 1332 年，罗马斗兽场上的一排排座椅是否还能修建起来，满是瓦砾和废墟的竞技场是否还能用来进行这样的格斗。罗马和整个意大利一样，每年都会举行不同类型的格斗比赛。与此同时，在那不勒斯的宫廷前也上演着血腥的角斗士格斗，彼特拉克看到并厌恶地描述了这些场面。十三世纪初，特雷维索的紫色城堡非常引人注目，在那里，美丽的女人欢天喜地地保卫着她们的珠宝和自己，抵御年轻男人的侵犯，这些男人用花束、糖果、香膏和对生活的热情征服了这些珍宝。更迷人的是佛罗伦萨的盛宴，维拉尼和小说家们经常提到的弦乐演奏、舞蹈和盛宴。

罗马有一年一度的大众运动会。狂欢节期间，人们会在特斯塔乔山和纳沃纳广场举行庆祝活动，有时也会在其他场合举行。在中世纪，罗马的狂欢节已经远离了让这个假面节日如此著名的特征。古罗马人也会惊讶地看着这些可怕的粗俗庆祝活动，他们的马戏团比赛已经堕落到这种地步，并对元老院感到惊叹，他们盛装前往 "舍本贝格"，在草地上庄严地插上罗马的旗帜，并发出这种比赛开幕的信号。一群卫兵和刽子手一起走在前面，刽子手手持木块和斧头威慑恶人。猪被绑在四辆披着围巾的大车上，犹太人必须把它们运走；它们被滚下泰斯塔克西奥河（Testaccio），在那里，呐喊的人们为争夺战利品而大打出手。每个地区都会带来一头加冕的公牛。这些牲畜也是猎物；如果丈夫和情人从猎场回来时没有一块肉，罗马妇女就会侮辱他们。然后是比武和摔跤比赛，最后是争夺一块布（_pallium_）的获胜奖品（_bravium_）的比赛，这在整个意大利都很常见。自古以来，特斯塔乔山就属于阿汶丁山的圣玛丽修道院，罗马人每年都要向其支付弗罗林金币作为使用费。周围的平原是牛群的牧场；游乐场一直延伸到阿汶丁山的一座古塔。

纳沃纳广场（Navona）上的比赛，即古老的阿戈纳利斯马戏团（Circus Agonalis），也包括比武，尤其是假面游行。

在这两个节日中，各地区都会提供训练有素的表演者。根据 1580 年的章程，表演者人数为 72 人，另外还有来自其他城市的表演者。与古代一样，这些节日对罗马也具有政治意义。来自元老宫附庸城市的代表们手持旗帜和帛书，依然向罗马人展示着古代拉丁统治的影子，以及臣民和盟友的可贡性。被征服的地方必须签订合同，承诺参加罗马运动会。例如，图斯卡尼亚自 1300 年以来每年都要派出八名选手，而元老宫也要求元老、蒂沃利、科尔内托、泰拉奇纳和罗马领土上的其他社区缴纳同样的贡品。他们抵制这种象征屈从的昂贵贡品，教皇也一再禁止元老院强制向罗马的比赛提供战车。节日的费用相当可观；除了臣服的城镇外，他们还要按地区缴纳贡金，罗马的犹太人每年要缴纳 1130 金弗罗林作为节日贡金；30 金弗罗林明确表示是对犹大工资的惩罚性提醒。

有时，这些游戏还包括精神性的戏剧表演，即所谓的 "再现"。据一位罗马编年史家记载，1414 年 2 月 18 日，蒙蒂地区的演员（_jocatores_）在特斯塔乔表演了使徒彼得受难和斩首圣保罗的场面。这些演员几乎不是真正的演员，而是排演这些场景的市民。罗马的_Ludi Paschales_起源于兄弟会，尤其是贡法隆兄弟会（Confaternity del Gonfalone）。据推测，早在 1250 年，斗兽场就上演过类似的激情戏。至少在圆形剧场落入兄弟会之手后一直如此。它有一个供奉玛丽亚-德拉-皮亚的小教堂，建在古老的平台上。它的屋顶由以前的几排座椅组成，在很长一段时间里，每年复活节都会在这里上演耶稣受难记。由于观众人数众多，斗兽场也像古代一样挤满了人。在哥摩多斯或特拉扬为富裕的罗马人举办盛大宴会的时代，没有人想到会有这么一天，成千上万的人挤满了这座被毁坏的宏伟建筑，怀着虔诚的心情观看犹太救世主被钉在十字架上，而剧场里只有几排座位。
