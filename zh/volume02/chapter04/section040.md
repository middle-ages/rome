_4\. 教皇西尔维乌斯被流放 罗马的饥荒 哥特人的人性 维提格斯占领罗马港口。波图斯和奥斯提亚 援军抵达罗马。哥特人击退了一次进攻。罗马城的困境日益严重。哥特人的碉堡和匈奴人的碉堡_。

贝利撒留有理由对一些元老的忠诚表示不满，当他将一些贵族流放出城时，没有人会指责他的苛刻；但他对西尔维乌斯采取的行动不能轻易被指控为与哥特人串通叛国，因为正是这位教皇鼓励罗马人接纳贝利撒留进城。普罗科皮乌斯用简短而谨慎的文字描述了这一不愉快的事件："由于怀疑该城的大祭司西尔维乌斯与哥特人密谋叛国，他立即将他送往希腊，不久后又任命了另一位名叫维吉里乌斯的主教。然而，西尔维乌斯的倒台是西奥多拉皇后阴谋的结果，她希望新教皇能够撤销卡尔西顿会议的决定，在君士坦丁堡安插被谴责的宗主教安提穆斯，因为西尔维乌斯本人坚决拒绝这样做。她利用罗马的困境，与她的心腹执事维吉利乌斯（Vigilius）谈判，维吉利乌斯是一个野心勃勃的罗马人，曾在君士坦丁堡担任教会代表（apocrisiarius），她写信给贝利萨（Belisar），敦促他以虚假的借口罢免西尔维乌斯，将维吉利乌斯提升为彼得座主教。后者曾向她许诺承认安提穆斯，并拒绝接受卡尔西顿会议，支持教皇制。

软弱的贝利萨听从了两个可耻女人的命令，一个是叱咤风云的狄奥多拉，另一个是狡猾的安东尼娜--他自己的妻子；因为这两个女人都结交了同样出身低微、放荡不羁的知己，尽管她们彼此恐惧和憎恨。他没有勇气招惹这些女人的怒火，却让自己成为她们阴谋的执行者，这对他自己也是一种伤害。安东尼娜和维吉利乌斯提出了假证人，她们发誓说西尔维乌斯曾写信给维提杰斯："到拉特兰旁边的阿西纳里亚门来，我将把这座城市和帕特里修斯交给你。受到威胁的教皇逃到了阿汶丁山的圣萨比纳教堂，但贝利萨让人把他从那里召到了平西尔宫，围城期间他本人就住在那里。不幸的西尔维乌斯相信了他曾发誓提供的安全保障，离开了他的庇护所。随行的神职人员留在了第一道和第二道帷幕处，西尔维乌斯与维吉利乌斯一起进入了宫殿的内室，指挥官坐在安东尼娜的脚边，安东尼娜斜躺在床上。当她看到他时，这位出色的女演员喊道："告诉我，西尔维留斯教皇，我们对您和罗马人做了什么，您要把我们交到哥特人的手中？" 就在她责备教皇的时候，第一区的副主教约翰走了进来，从教皇的脖子上取下了披带，把他领进了一间寝宫。在那里，他剥掉了教皇的长袍，给他穿上了修道士的衣服，然后另一名执事去向等在外面的神职人员宣布，教皇已被废黜，并已成为一名修道士。听到这个消息，教士们都逃走了。然而，在希腊权力斗争的恐怖气氛下，维吉利乌斯被元老院和教士们推选为教皇，而此时他的前任已经被带走到了利西亚的帕塔拉。贝利萨以暴力手段废黜西尔维乌斯的事件发生在 537 年 3 月，维吉利乌斯可能是在同月 29 日被祝圣的。帝国将军对他们神职的专制干预清楚地向罗马人表明，哥特人的统治容易承受，但拜占庭人的枷锁会给他们带来沉重的负担。维吉利乌斯是出身高贵的罗马人，是约翰执政官的儿子，也就是他的赞助人、教皇博尼法斯二世敢于选择作为他的继任者的红衣主教迪亚科努斯。当然，这一不符合教规的行为已被废除，但从那时起，维吉里乌斯就渴望成为教皇。作为拜占庭的圣座大使，他在拜占庭的宫廷中赢得了有权势的朋友，现在他以一种动荡的方式篡夺了罗马教廷。

当时，意大利正在发生最可怕的饥荒，罗马也开始遭受重创。这迫使贝利萨将所有不适合保卫城墙的人赶出城外。这些不幸的人成群结队地离开罗马，分散到全国各地，或在台伯河港口上岸，乞求那不勒斯的款待。哥特人让他们安然离去。在整个围攻期间，他们的人道精神甚至得到了敌人的尊重，敌人明确称赞他们没有触碰圣彼得大教堂或圣保罗大教堂，尽管这两座教堂都在他们的领地内。然而，城市中的其他圣殿却遭受了与战争相关的各种破坏。教皇维吉利乌斯指责哥特战士破坏了地下墓穴和众多墓地，并打碎了大马士革的大理石碑文。维吉利乌斯本人在围城期间似乎也是人民的恩人。

维提吉斯只是让自己陷入了血腥的仇恨之中：他派信使前往拉文纳，命令杀死那些被他从罗马绑架来作为人质的元老。最后，为了更严密地包围这座城市，彻底切断它的补给，他占领了波图斯。台伯河分两臂流入大海，形成圣岛。左岸的奥斯提亚港早在古代就已经淤塞，因此克劳狄乌斯皇帝在右岸开凿了一个港口和运河，并向海里投掷了一个木螺。这就是著名的_Portus Romanus_或_Urbis Romae_的由来。特拉扬用一个六角形的内港扩建了这个宏伟的建筑群，并在其周围修建了宏伟的建筑。与此同时，他还开凿了一条新的运河，即_Fossa Traiana_，这条运河今天仍然可以在菲乌米奇诺的台伯河右支流上看到，波尔图斯从此成为一个重要的港口城市；在基督教的最初几个世纪，它已经是一个主教区。在异教的最后时期，甚至在五世纪中叶，罗马人经常率领城市长官或执政官前往波尔图斯和奥斯提亚之间的小岛，向卡斯托尔和波吕克斯献祭，享受清新的绿色。因为无论酷暑还是严冬，那里的花朵都不会凋谢，春天岛上长满了玫瑰和香脂灌木丛，所以罗马人称它为维纳斯花园。后来，狄奥多里克将重要的港口办公室委托给了一位科米人，以确保港口的保护。即使在普罗科皮乌斯时代，波尔图斯仍是一座有坚固城墙环绕的体面城市，而位于河左岸的老奥斯提亚则早已荒废，没有城墙；因为尽管河的两侧仍可停泊船只，但船只仍前往波尔图斯。从波尔图门通往港口有一条极好的道路，沿路的河上有许多船只，用绳子牵引着公牛，将西西里的粮食和货物从东方运往罗马，十分繁忙。

维提盖斯率领 1000 人占领波尔图斯后，没有遇到任何抵抗，他切断了罗马人与海洋的联系，因此运输船只只能从安提乌姆走艰苦而不安全的路线。

然而，二十天后，1600 名匈奴和斯拉沃尼亚骑兵的到来减轻了这次损失对士气的影响，这些援军使贝利萨得以在城门前与敌人发生小规模冲突，在这些冲突中，萨尔马特弓箭手的技巧战胜了只装备长矛的哥特骑兵。小胜激起了被围困者的勇气，他们要求对敌人的堑壕发起总攻，贝利萨屈服于他们的冲动。最多的部队将从平齐亚纳门和萨拉利亚门突入；较少的部队将从奥里利亚门突入尼禄战场，以阻止哥特人通过米尔维安桥；第三支部队将从圣潘克拉修斯门突入。

但是，哥特人已经为叛逃者的进攻做好了准备，他们以整齐的战斗队形迎接希腊人，步兵在中央，骑兵在两翼。经过数小时的激战，他们的英勇取得了彻底的胜利；希腊人既无法夺取米尔维安桥（这座桥可以切断另一侧的阵营），也无法攻克这一侧的堑壕；他们从四面八方被击退，幸亏城垛上的弹弓发挥了强大的作用，他们才得以幸免于难。

这次进攻失败后，被围困者只能进行一些小规模的战斗，而哥特人则试图通过更加严密的封锁来加剧城内的饥荒。他们占据了拉丁大道和阿皮亚大道之间的一个地方，距离城市 50 英里，那里有两条相交的水渠，可以在此修建堡垒。他们在这些水渠的拱门上筑起城墙后，建立了一个可容纳 7000 人的坚固营地，阻止了来自那不勒斯方面的任何补给。城墙周围的草药不足以喂饱马匹，骑兵们在夜间（当时已进入夏至）获得的粮食只能满足富人的短暂饥饿。各种动物都成了食物；元老们用黄金称量士兵们用倒下的骡子肉做成的香肠。炎热的气候让饥饿更甚，罗马闷热的街道上到处都是未埋葬的尸体。

人民不堪忍受这种痛苦，纷纷起义，通过使者要求贝利萨进行最后的决战。但这位将军以他的镇定自若平息了人们的呼声，并让他们等待即将到来的救援和补给舰队。他派普罗科皮乌斯甚至安东尼娜前往那不勒斯，尽可能多地为船只装载粮食。终于，拜占庭军队在下意大利登陆了；尤塔利乌斯带着军饷来到泰拉奇纳，在一百名骑兵的保护下，兴高采烈地进城了。为了确保粮食的运输，贝利萨占领了阿尔巴努姆和蒂布尔要塞，而围攻者竟然没有注意到这两个要塞。为了扰乱阿皮亚大道上的敌军，他让匈奴骑兵先行，并在圣保罗大教堂附近安营扎寨。从东门沿着台伯河还有一个门廊通向大教堂，这为他们提供了一个稳固的立足点。从这里，从提布尔和阿尔巴努姆，阿皮亚大道上的营地受到了突袭的威胁，贝利萨的轻骑兵阻止了哥特人在坎帕尼亚收集食物。然而，低温意味着哥特人和希腊人都无法在营地坚守阵地。匈奴人的防御工事被取消，哥特人的守军也从水管中撤出。显然，这些水管的破坏导致城前的整片土地变成了沼泽地，甚至几个世纪后，罗马附近仍有沼泽地。
