### 第六章

_1\. 贝利撒留入侵罗马 他重建了城墙 547年罗马第二次保卫战。 托提拉迁往提布尔。约翰在卡普亚起用罗马元老。托提拉迅速向意大利南部进军。贝利撒留离开罗马。他在罗马城的纪念碑_________________________。

托提拉刚前往阿普利亚，贝利撒留就试图进入这座无人占领的城市。他带着一千人冒险离开了波尔图斯，但从阿尔西姆赶来的骑兵在一场激战后迫使他折回。他等待更有利的时机，只在港口要塞留下少数部队，巧妙地欺骗了哥特人，然后带着所有剩余部队离开，从奥斯提安门进入城市。那是 547 年的春天，这位伟大的将军几乎还没有回到他辉煌的地方，他的天才和运气似乎又加倍地回来了。

他首先要做的是修建城墙。由于他既没有足够的劳动力、材料，也没有足够的时间来彻底重建如此大面积的城墙，他只能尽力而为。城墙是用瓦砾堆砌而成的，许多珍贵的大理石或石灰华都是从邻近的古迹中不假思索地使用的。这些石头并没有捆绑在一起，只是在外面用桩子支撑着，之前在它们周围挖的壕沟经过清理和加深后，成为了最好的防御工事。经过二十五天的加速施工，贝利萨得以在翻修过的城墙周围走一圈，亲眼看到它们至少看起来像剧场布景。四散的罗马人从坎帕尼亚（Campagna）向城市进军，使城市重新呈现出人烟稀少的景象。

托提拉没有听说，他从阿普利亚急行军一回来，敌人就进入了罗马，像汉尼拔一样不安分地来回穿梭，速度也和他一样快。这一举动看似杂乱无章，因为它并不是一个令人愉快的举动，而且由于哥特国王没有先将贝利撒留赶出波尔图斯就放弃了罗马，它可能第一次显得应受谴责。毫无疑问，他曾想象过，如果贝利撒留再次进入这座几乎被城墙围住的城市，他一定会无法忍受。事实上，他发现希腊人仍在修缮城门，因为他自己曾把城门的两翼搬走或毁坏，而贝利撒的木匠还没有完成更换。现在堵住入口的不是他们，而是手持盾牌和长矛的战士们自己。哥特人在台伯河上的营地过夜，早上他们怒气冲冲地向城墙扑去，只要维提格斯的撞锤轻轻一击，城墙就会倒塌。但经过一整天的战斗，当夜幕降临时，他们发现自己又被扔回了台伯河上的营地，他们羞于承认自己在开放的罗马面前吃了败仗。第二天早上，当他们再次发起进攻时，发现城墙上弓箭手严阵以待，城门前有许多木制机械，由四根成直角连接的杆子组成，可以随意转动或倒转，而不改变其形状或用途。贝利萨的天才似乎天生就是为了保卫罗马而生的，他在这里独当一面，所向披靡，而哥特人在围城方面经验不足，他们在罗马的城墙上不断消耗自己的力量，仿佛是命运的驱使。黑夜也终结了第二次风暴，托提拉在数日后进行的第三次风暴也同样不幸。他的皇家旗帜刚刚从敌人手中夺回。

在营地里，他的战士们对他大加斥责；那些曾称赞他部分或全部推倒被征服城市防御工事的原则是明智之举的人，则痛心疾首地斥责他为何没有夺取罗马，或者，如果他认为这样做不明智的话，为何没有将罗马夷为平地。即使在遥远的地方，哥特人在半开的罗马城前的不幸遭遇和贝利萨的幸运抵抗也让人深感震惊。过了一段时间，托提拉仍因此遭到法兰克国王的谩骂；当他要求娶他的女儿为妻时，泰奥德贝特给了他一个严厉的答复：他不相信一个人是意大利的国王，也不相信他将永远是意大利的国王，他没有能力坚守被征服的罗马，而不得不将部分被摧毁的城市拱手让给敌人。

托提拉将他的部分战甲和大部分财产留在了罗马的城墙外；现在他推倒了阿尼奥河上的桥梁，全力向他筑起防御工事的提布尔进发。这样，贝利撒留就有闲心用布满矿石的翅膀关闭罗马城门了，而且他还能第二次将罗马城的钥匙作为战利品送往君士坦丁堡，这使他获得了更大的荣耀。至此，普罗科皮乌斯结束了哥特战争的冬季和第十二年。因此，托提拉解除对罗马的围困应该是在 548 年春天左右；但历史学家似乎把时间提前得太快了。围攻可能只持续了一个月。

在此期间，国王又遭受了一次惨痛的损失，这使他在罗马面前的不幸更加沉重。约翰将军在下意大利的小规模战争中不屈不挠，率领一支大胆的骑兵队来到了坎帕尼亚。在那里，也许是在卡普亚，罗马元老和他们的妻儿都被哥特人囚禁；托提拉从他们那里逼出的信件起到了唤回省民服从的作用。约翰袭击了卡普亚，斩杀了哥特守卫，释放了元老，并愉快地将战利品运往卡拉布里亚。当然，他在那里只能抓到几个贵族，因为他们中的大多数人在托提拉攻占罗马后就已经逃之夭夭了，但许多元老的妻子都落入了他的手中；他把她们都送到了西西里，在那里她们现在可以作为人质为皇帝服务。

托提拉听到政变的消息后，急忙从正在围攻的佩鲁贾赶往意大利南部。他越过卢卡尼亚山脉，袭击了约翰将军的营地，并在这些地区的森林和山脉中驱散了希腊人。然后，他前往布伦迪辛，在那里歼灭了一队刚刚登陆的希腊军队。现在，他将战场转移到了下意大利，迫使贝利撒留再次离开罗马，亲自前往卡拉布里亚。皇帝亲自命令他在那里担任最高指挥官。贝利撒只带了 700 名骑兵和 200 名步兵上船，将城市的防务交给了科诺将军，并在 547 年冬天左右永远离开了罗马，从此在意大利南部海岸徘徊，既无荣耀，也无运气。

城墙是贝利萨在罗马的纪念碑；城墙使他的名字永垂不朽，不是因为他重建了城墙，而是因为他以令人钦佩的天才两次保卫了城墙。据说他还修建了引水渠，并恢复了罗马浴场的使用权；但只有特拉亚纳（Trajana）这条引水渠似乎是他真正修复的，因为它是磨坊运转所不可或缺的。因此，如果不考虑特拉亚纳水渠和后来修建的几条不完善的水渠，那么在 537 年被哥特人摧毁之后，水渠就不再向罗马供水了，而这座世界上水源最丰富的城市几个世纪以来只能依靠台伯河、蓄水池和几处泉水，就像它最初的童年时代一样。

教皇纪事》中记载，贝利萨在拉塔大街建立了一个济贫院，除了两个大烛台外，他还向使徒彼得献上了一个重达 100 磅、用宝石装饰的金十字架，并在上面刻下了他的胜利。这件艺术品很可能是用雕刻的图像装饰的，因此它的遗失令人惋惜。据说他将这件祭品交到了教皇维吉利乌斯的手中，因此他是在击败维提吉斯之后捐赠的。他从汪达尔人和哥特人的战利品中获得的财富肯定是无法估量的，如果他在罗马的短暂停留或当时的战争混乱允许的话，罗马会从他那里得到许多恩惠，并用许多纪念碑来装饰他的荣耀。
