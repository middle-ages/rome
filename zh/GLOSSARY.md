# 专有名词

## Stellvertreter

[代理](https://de.wikipedia.org/wiki/Stellvertreter)或副职是一个组织中拥有替代权的自然人，当主管不在时接管实际职位持有人的职能。如果职位持有人具有管理职能，可以在分工的意义上将管理能力永久性地转移给副手。

## Gegenkönig

[对立国王](https://de.wikipedia.org/wiki/Gegenk%C3%B6nig)是选举君主制下，由对立派拥立的国王。

## Hirtenstab

[主教牧杖](https://de.wikipedia.org/wiki/Krummstab)

## Troubadour

[吟游诗人](https://zh.wikipedia.org/wiki/%E9%81%8A%E5%94%B1%E8%A9%A9%E4%BA%BA)指[中世纪](https://de.wikipedia.org/wiki/Troubadour_%28K%C3%BCnstler%29)宫廷歌曲的诗人、作曲家和歌手。

## Freibrief

[特许状](https://de.wikipedia.org/wiki/Freibrief)，又称自由特许状，通常是皇家或诸侯的文件，在中世纪时通过它授予某些特权。

## Vikar

[助理司祭](https://de.wikipedia.org/wiki/Vikar)或者助理神甫。

## Galeere

帆桨大战船又叫[桨帆船](https://zh.wikipedia.org/wiki/%E6%A7%B3%E5%B8%86%E8%88%B9)，通常使用在战争与贸易中。

## Prätendent

请求者指的是[王位请求者](https://zh.wikipedia.org/wiki/%E7%8E%8B%E4%BD%8D%E8%A6%AC%E8%A6%A6%E8%80%85)或王位觊觎者(en: [Pretender](https://en.wikipedia.org/wiki/Pretender), de: [Thronprätendent](https://de.wikipedia.org/wiki/Thronpr%C3%A4tendent))

## Landesverweserin

一般写作[Reichsverweser](https://de.wikipedia.org/wiki/Reichsverweser)，帝国行政官在王位空缺期间，即在国王长期不在期间或在国王去世和继任者登基期间代表君主。Verweser一词来自于旧高阶德语firwesan，意思是 "代表某人的位置/存在"。

## Königsstatthalter

帝国总督，来源[Statthalter](https://de.wikipedia.org/wiki/Statthalter)

## Schutzverwandtin

一般写作[Schutzverwandter](https://de.wikipedia.org/wiki/Schutzverwandter)，保护性亲属（又称：保护性同志）是一种来自中世纪的法律制度，直到19世纪在德国土地上基本有效。

## Senat

在中世纪，有不同的人自称是罗马的 "元老"，1143年，在罗马重建了一个"[元老院](https://de.wikipedia.org/wiki/Senat#Mittelalter)"，它得到了罗马城市人口的广泛支持。它主要代表罗马社会中教皇、高级神职人员和大贵族家族的利益。

## Kapitol

[卡比托利欧山](https://zh.wikipedia.org/wiki/%E5%8D%A1%E6%AF%94%E6%89%98%E5%88%A9%E6%AC%A7%E5%B1%B1)，或指代山上的议事宫殿，本文译作元老宫。

## Anwälte

权益代表指在法律事务中[代表某人的人](https://de.wikipedia.org/wiki/Anwalt)，特别是私人、公司或机构对国家、当局、法院或公司的代表。另一个含义是[举证人(Advokat)](https://de.wikipedia.org/wiki/Advokat)。

## Priester

罗马天主教的[司铎](https://zh.wikipedia.org/wiki/%E7%A5%AD%E5%8F%B8)。

## Vizekönig

字面意思为[副王](https://de.wikipedia.org/wiki/Vizek%C3%B6nig)，一般意译为“[总督](https://zh.wikipedia.org/wiki/%E7%B8%BD%E7%9D%A3_%28%E6%AE%96%E6%B0%91%E5%9C%B0%E5%AE%98%E8%81%B7%29)”。

## Kreuzzug

(十一至十三世纪的)[十字军东征](https://de.wikipedia.org/wiki/Kreuzzug)

## Freistädte

[自由城市](https://zh.wikipedia.org/wiki/%E5%B8%9D%E5%9B%BD%E8%87%AA%E7%94%B1%E5%9F%8E%E5%B8%82)本来是主教的驻地城市，这些城市在13世纪和14世纪从他们的主教的手中解放出来，名义上它们依然是主教的领地，向主教纳税和提供军队，但实际上它们不受它们的领主的命令。

## Podesta

(it: Podestà)（中世纪意大利的城镇）长官，县长；[执法官](https://zh.wikipedia.org/wiki/%E6%89%A7%E6%94%BF%E5%AE%98_%28%E4%B8%AD%E4%B8%96%E7%BA%AA%E6%84%8F%E5%A4%A7%E5%88%A9%29)
在中世纪的意大利社会，[市政官](https://www.britannica.com/topic/podesta)是最高的司法和军事行政长官。该职位是由神圣罗马帝国皇帝腓特烈一世·巴巴罗萨设立的（德意志史. 张载扬），目的是为了治理反叛的伦巴第城市。从12世纪末开始，公社在某种程度上变得更加独立于皇帝，他们开始选举自己的市政官，后者逐渐取代了执政官的合议制政府。市政官通常从另一个城市或遥远的封建家族中选出，以确保他在地方争端中的中立性，他通常是一个受过法律培训的贵族，任期一年（后来是六个月）。他召集议会，领导公社军队，并管理民事和刑事管辖权。尽管该办公室受到严格的法定限制，但它有时也是建立专制政府或称 "signoria "的起点。13世纪后，该职位的重要性有所下降；在15世纪的佛罗伦萨，其主要职能是司法。

## Kommune

[公社](https://zh.wikipedia.org/wiki/%E4%B8%AD%E4%B8%96%E7%BA%AA%E5%85%AC%E7%A4%BE)或城邦([中世纪意大利](https://de.wikipedia.org/wiki/Kommune_%28Mittelalter%29)的)11至13世纪间的一种城镇组织形式。

## Plebejer

[古罗马平民](https://zh.wikipedia.org/wiki/%E5%B9%B3%E6%B0%91_%28%E5%8F%A4%E7%BE%85%E9%A6%AC%29)，后来罗马公民与罗马平民的差别消失。

## Kapitän

[督军](https://zh.wikipedia.org/wiki/%E7%9D%A3%E5%86%9B_%28%E6%8B%9C%E5%8D%A0%E5%BA%AD%29)语源来自[katepánō](https://en.wikipedia.org/wiki/Katepano)。这一混合词后来演变为英语中的captain及其在其他语言中Capitan、Kapitan、Kapitän、El Capitán、Il Capitano、Kapudan Pasha等词。
这个职位和市政官类似，除了军事，还管辖民事，司法。而督军区只出现在意大利南部，因此在意大利中北部出现译作“统领官”，在西西里作“督军”。

## Fossalta

指[福萨尔塔战役](https://de.wikipedia.org/wiki/Schlacht_bei_Fossalta)，发生在1249年春意大利北部。腓特烈二世企图征服米兰，不想被博洛尼亚人偷袭成功，结果皇帝大败而归，腓特烈二世的儿子恩齐奥就是在此役中被俘。

## Goldflorene

[弗罗林](https://zh.wikipedia.org/wiki/%E5%BC%97%E7%BD%97%E6%9E%97)是1252至1533年间铸造的一种金币。

## Dukaten

[杜卡特](https://zh.wikipedia.org/wiki/%E8%BE%BE%E5%85%8B%E7%89%B9)或达克特是欧洲从中世纪后期至20世纪期间，作为流通货币使用的金币或银币。

## Taler

(十八世纪还通用的德国银币)[塔勒](https://zh.wikipedia.org/wiki/%E5%A1%94%E5%8B%92)

## Marschall

中世纪的[内廷大臣](https://de.wikipedia.org/wiki/Marschall)

## Seneschall

宫廷总管，与德语Truchsess等同。

## Justitiar

[司法长](https://de.wikipedia.org/wiki/Justiziar#Geschichte)是诺曼底西西里王国的最高省级官员，主要负责刑事司法。该头衔和职位应该源自诺曼英国。

## Körperschaft

公会或行会是代表专业人员的利益的组织，似与Korporation一样。德语的Körperschaft就是[Standesvertretung](https://de.wikipedia.org/wiki/Standesvertretung)，对应中文的[专业协会](https://zh.wikipedia.org/wiki/%E4%B8%93%E4%B8%9A%E5%8D%8F%E4%BC%9A)。

## Herold

[传令官](https://de.wikipedia.org/wiki/Herold)

## Investitur

(宗教的)授职礼，另指[叙任权](https://zh.wikipedia.org/wiki/%E5%8F%99%E4%BB%BB%E6%9D%83)

## Doge von Venedig

[威尼斯总督](https://zh.wikipedia.org/wiki/%E5%A8%81%E5%B0%BC%E6%96%AF%E6%80%BB%E7%9D%A3)

## Syndikat

原指法律顾问职务或行业(分的)联合组织。

## Piazza di Araceli

[天坛广场](https://zh.wikipedia.org/wiki/%E5%A4%A9%E5%9D%9B%E5%B9%BF%E5%9C%BA)坐落于[卡比托利欧山](https://zh.wikipedia.org/wiki/%E5%8D%A1%E6%AF%94%E6%89%98%E5%88%A9%E6%AC%A7%E5%B1%B1)的斜坡。

## Basilika Araceli

[天坛圣母堂](https://zh.wikipedia.org/wiki/%E5%A4%A9%E5%9D%9B%E5%9C%A3%E6%AF%8D%E5%A0%82)。

## Tempel der Concordia

[康考迪亚神庙](https://zh.wikipedia.org/wiki/%E5%BA%B7%E8%80%83%E8%BF%AA%E4%BA%9E%E7%A5%9E%E5%BB%9F_%28%E7%BE%85%E9%A6%AC%29)，经历了多次重建和破坏，1450年完全倒塌。

## Pierleoni

[皮耶罗尼家族](https://en.wikipedia.org/wiki/Pierleoni_family)

## Capocci

[卡波奇家族](https://en.wikipedia.org/wiki/Capocci)

## Frangipani

[弗兰吉帕尼家族](https://en.wikipedia.org/wiki/Frangipani_family)

## Savelli

[萨维利家族](https://en.wikipedia.org/wiki/Savelli_family)

## Orsini

[奥尔西尼家族](https://zh.wikipedia.org/wiki/%E5%A5%A7%E7%88%BE%E8%A5%BF%E5%B0%BC%E5%AE%B6%E6%97%8F)

## Eidgenossenschaft

同盟,联盟

## Signore

中世纪表示意大利城邦国家的[威权统治者](https://en.wikipedia.org/wiki/Signoria)。是一种处于市民统治衰落，家族统治兴起的中间阶段。用于市民(commune)与共和对立的语境。
[专政官](https://www.britannica.com/topic/signoria)即在中世纪和文艺复兴时期的意大利城邦中，由领主（即专制者）管理的政府，通过武力或协议取代共和体制。从13世纪中叶到16世纪初，它是意大利特有的政府形式。Signoria的发展标志着意大利城邦演变的最后阶段。共和制内部激烈的派系斗争，使得将权力转移到一个足以维护和平的人手中似乎是可取的。通常情况下，一个人从担任公共职务开始，如庶民统领(Captain of the People)，并逐渐扩大他的权力，直到它成为永久性的并在他的家庭中世袭。然后，他将寻求教皇或帝国的头衔以使其地位合法化。

## Kardinalskollegium

[枢机团](https://zh.wikipedia.org/wiki/%E6%A8%9E%E6%A9%9F%E5%9C%98)

## Prälatur

[教长](https://zh.wikipedia.org/wiki/%E6%95%99%E9%95%B7_%28%E5%9F%BA%E7%9D%A3%E5%AE%97%E6%95%99%29)，典型的教长是管理所辖教区的主教。

## Conti

[孔蒂家族](https://en.wikipedia.org/wiki/Counts_of_Segni)

## Annibaldi

[安尼巴尔迪家族](https://en.wikipedia.org/wiki/Annibaldi_family)

## Septizonium

[七神大殿](https://en.wikipedia.org/wiki/Septizodium)是古罗马的一座建筑。名称意思是七个太阳的神殿，并且可能以七个行星神（土星、太阳、月亮、火星、水星、木星、金星）命名。8 世纪，这座大厦已经被毁坏，16世纪被拆除。

## Tempel des Quirinus

[奎里努斯神庙](https://de.wikipedia.org/wiki/Tempel_des_Quirinus)

## Carrocium

## Praetor

[民选官](https://de.wikipedia.org/wiki/Praetur)

## Doppelwahl

双重选举

## Scylla

斯库拉，希腊神话中吞吃水手的六头女海妖。

## Charybdis

卡律布迪斯，希腊神话中该亚与波塞冬的女儿，荷马史诗中的女妖。

## Vier Gekrönten

[四冠圣徒殿](https://de.wikipedia.org/wiki/Quatuor_coronati)

## Solidus

苏勒德斯（一种古罗马金币）。

## Albigenserkriege

[阿尔比十字军](https://zh.wikipedia.org/zh-cn/%E9%98%BF%E5%B0%94%E6%AF%94%E5%8D%81%E5%AD%97%E5%86%9B)，亦称卡特里派十字军，是由教宗英诺森三世为铲除法兰西南部朗格多克的卡特里派所发动的长达二十年的军事讨伐。

## Connetable

该词最早出现在墨洛温王朝时期，最开始仅负责管理皇家马厩，所以可称为司马。卡佩王朝下是法兰西军队的最高统帅可称为将军，权力很大但等级上低于[宫廷总管(Sénéchal)](https://fr.wikipedia.org/wiki/S%C3%A9n%C3%A9chal_de_France)。在腓力二世·奥古斯都时期，和[宫廷内侍(Chambellan)](https://fr.wikipedia.org/wiki/Grand_chambellan_de_France)共享宫廷总管的权力。

## Volturnus

[沃勒图努斯](https://zh.wikipedia.org/wiki/%E6%B2%83%E5%8B%92%E5%9B%BE%E5%B0%94%E5%8A%AA%E6%96%AF)是古罗马神话与宗教中的神祇之一。
同时有一条[沃尔图诺河（Volturno）](https://zh.wikipedia.org/wiki/%E6%B2%83%E5%B0%94%E5%9B%BE%E8%AF%BA%E6%B2%B3)

## Indiktion

[征税诏告](https://de.wikipedia.org/wiki/Indiktion)，古罗马每隔15年一次的财产评价公告

## Savelli

[萨维利家族](https://it.wikipedia.org/wiki/Savelli_(famiglia))（最初也是Sabelli家族）是中世纪至18世纪罗马历史上一个古老而高贵的家族。

## Kapitän des Volks

一般作Volkskapitan，对应意大利语[Capitano del Popolo](https://de.wikipedia.org/wiki/Capitano_del_Popolo)，与Podesta职能类似的执政官，一般为外部人员担任。Podesta一般由贵族家族担任，Capitano则一般由市民选出担任。

## Schenk

德国职业姓氏，指的是中世纪的侍酒师或葡萄酒侍者（后来也指酒馆老板）职业。

## Prokurator

(古罗马)省长;(古威尼)执政官；(修道院)司库

## Generalkapitän

古代没有军衔概念，通译为总指挥，见[wiki](https://de.wikipedia.org/wiki/Generalkapit%C3%A4n)。

## Admiral

德语Admiral对应的意大利语为[Ammiraglio](https://it.wikipedia.org/wiki/Ammiraglio)，来源于中世纪的拉丁文Admiralis或admirallus，是一个用于诺曼时期西西里王国的希腊－阿拉伯海军指挥官的术语，不是现代意义上的海军上将。

# 人名

## Konstanze von Aragon

[阿拉贡的康斯坦莎](https://de.wikipedia.org/wiki/Konstanze_von_Arag%C3%B3n)(1154-1222)，1209年与腓特烈二世结婚，1212–1220年是西西里摄政。

## Jolanthe von Jerusalem

[耶路撒冷的约兰达](https://zh.wikipedia.org/wiki/%E4%BC%8A%E8%8E%8E%E8%B4%9D%E6%8B%89%E4%BA%8C%E4%B8%96_(%E8%80%B6%E8%B7%AF%E6%92%92%E5%86%B7))(1212-1228)，也叫耶路撒冷的伊莎贝拉二世，耶路撒冷国王[布里昂的约翰](https://zh.wikipedia.org/wiki/%E5%B8%83%E9%87%8C%E6%98%82%E7%9A%84%E7%BA%A6%E7%BF%B0)的女儿。

## Isabella von England

[英格兰的伊莎贝拉](https://zh.wikipedia.org/wiki/%E8%8B%B1%E6%A0%BC%E8%98%AD%E7%9A%84%E4%BC%8A%E8%8E%8E%E8%B2%9D%E6%8B%89)(1214-1241)，英格兰国王[约翰](https://zh.wikipedia.org/wiki/%E7%BA%A6%E7%BF%B0_%28%E8%8B%B1%E6%A0%BC%E5%85%B0%E5%9B%BD%E7%8E%8B%29)的女儿。

## Enzius

[撒丁岛的恩齐奥](https://de.wikipedia.org/wiki/Enzio_von_Sardinien)(1218-1272)，1238年被腓特烈二世封为撒丁岛国王，生母可能是士瓦本的女贵族阿德莱德(Adelaide)。

## Friedrich von Antiochien

[安条克的腓特烈](https://de.wikipedia.org/wiki/Friedrich_von_Antiochia)(1223?-1256?)，1246－1250年为托斯卡纳的[帝国司祭](https://de.wikipedia.org/wiki/Reichsvikar)，生母是安条克的玛蒂尔德(Matilda)。

## Pallavicini

即[奥贝托二世·帕拉维奇诺](https://it.wikipedia.org/wiki/Oberto_II_Pallavicino)(1197-1269)曾与埃泽林一起是支持皇帝的吉伯林派，后加入归尔甫与埃泽林反目。

## Ezzelin

即[埃泽利诺·达·罗马诺三世](https://it.wikipedia.org/wiki/Ezzelino_III_da_Romano)(1194-1259)。

## Hildebrand

即教皇[格列高列七世](https://zh.wikipedia.org/wiki/%E9%A1%8D%E6%88%91%E7%95%A5%E4%B8%83%E4%B8%96)(1020-1085)，与神圣罗马帝国皇帝[亨利四世](https://zh.wikipedia.org/wiki/%E4%BA%A8%E5%88%A9%E5%9B%9B%E4%B8%96_%28%E7%A5%9E%E5%9C%A3%E7%BD%97%E9%A9%AC%E5%B8%9D%E5%9B%BD%29)展开了[叙任权斗争](https://zh.wikipedia.org/wiki/%E5%8F%99%E4%BB%BB%E6%9D%83%E6%96%97%E4%BA%89)。

## St. Peter

[使徒彼得](https://zh.wikipedia.org/wiki/%E5%BD%BC%E5%BE%97_％28%E4%BD%BF%E5%BE%92529)，也叫圣伯多禄。

## Manfred Lancia

[曼弗雷德·兰齐亚](https://zh.wikipedia.org/wiki/%E6%9B%BC%E5%BC%97%E9%9B%B7%E8%BF%AA_%28%E8%A5%BF%E8%A5%BF%E9%87%8C%E5%9B%BD%E7%8E%8B%29)(1232-1266)，按意大利语写法发音为曼弗雷迪(Manfredi)。

## Blanca Lancia

[比安卡·兰齐亚](https://de.wikipedia.org/wiki/Bianca_Lancia)(1200?-1246?)。

## Amadeus von Savoy

即萨伏依伯爵[阿玛迪斯四世](https://en.wikipedia.org/wiki/Amadeus_IV,_Count_of_Savoy)(1197-1253)，1233年成为萨伏依伯爵。

## Berthold von Hohenburg

即[霍恩堡藩侯贝特霍尔德](https://de.wikipedia.org/wiki/Berthold_von_Hohenburg)(1215-1256|1257), 他先是腓特烈二世的内侍。1239年，被任命为科莫上尉，1244年担任副总督，1246－1247被任命为尼西亚宫廷特使。1254年，康拉德四世死后，贝特霍尔德成为康拉丁的西西里王国摄政王。

## Richard von Cornwall

[康沃尔的理查](https://baike.baidu.com/item/%E7%90%86%E6%9F%A5/56766371)也叫[康瓦尔的理查](https://zh.wikipedia.org/wiki/%E5%BA%B7%E7%93%A6%E7%88%BE%E7%9A%84%E7%90%86%E6%9F%A5)属英格兰的金雀花王朝。
英格兰国王[亨利三世](https://zh.wikipedia.org/wiki/%E4%BA%A8%E5%88%A9%E4%B8%89%E4%B8%96_%28%E8%8B%B1%E6%A0%BC%E5%85%B0%29)的弟弟。

## Edmund von Lancaster

即[驼背埃德蒙](https://en.wikipedia.org/wiki/Edmund_Crouchback)(1245-1296), 兰开斯特伯爵。

## Brancaleone von Andalo

[安达洛的布兰卡利奥内](https://de.wikipedia.org/wiki/Brancaleone_degli_Andal%C3%B2)(?-1258)，

## Brancaleone degli Andalo

同Brancaleone von Andalo

## Salinguerra

即[萨林圭拉二世](https://it.wikipedia.org/wiki/Salinguerra_II_Torelli)(1164-1244)，意大利贵族与政治家。

## Jacopo von Carrara

[卡拉拉的雅科波](https://www.geni.com/people/Jacopo-de-Carrara/6000000008266000023)(1207-1242)

## Azzo von Este

[埃斯泰的阿佐七世](https://de.wikipedia.org/wiki/Azzo_VII._d%E2%80%99Este)(1205-1264)

## St. Petrus

同St. Peter

## Ambrosius

[圣安博](https://zh.wikipedia.org/wiki/%E5%AE%89%E6%B3%A2%E7%BE%85%E4%BF%AE)(340－397)，旧译作“安博罗削”，任米兰总主教，4世纪基督教著名的拉丁教父之一。

## Martinus della Torre

[马蒂诺·德拉·托雷](https://it.wikipedia.org/wiki/Martino_della_Torre)(?-1263)，米兰的第一任领主。

## Uberto Pallavicini

即[奥贝托二世·帕拉维奇诺](https://it.wikipedia.org/wiki/Oberto_II_Pallavicino)(1197-1269)，有的书籍也写作Hubert Pelavicini，曾与埃泽林一起是支持皇帝的吉伯林派，后加入归尔甫与埃泽林反目。在[阿达河畔卡萨诺](https://zh.wikipedia.org/wiki/%E9%98%BF%E8%BE%BE%E6%B2%B3%E7%95%94%E5%8D%A1%E8%90%A8%E8%AF%BA)的[卡萨诺之战](https://it.wikipedia.org/wiki/Battaglia_di_Cassano_d%27Adda_%281259%29)中大胜伦巴底联盟，成为米兰等城市的执政。

## Michael Angelus Ducas

即[米海尔二世·科穆宁·杜卡斯](https://zh.wikipedia.org/wiki/%E7%B1%B3%E6%B5%B7%E5%B0%94%E4%BA%8C%E4%B8%96%C2%B7%E7%A7%91%E7%A9%86%E5%AE%81%C2%B7%E6%9D%9C%E5%8D%A1%E6%96%AF)(1230–1266/68)

## Guido Novello

即[圭多·诺韦洛·吉迪](https://it.wikipedia.org/wiki/Guido_Novello_Guidi)(1227-1293)，13世纪托斯卡纳地区重要的吉伯林派封建领主。

## Wilhelm von Arezzo

即[古列尔米诺·乌贝蒂尼](https://it.wikipedia.org/wiki/Guglielmino_Ubertini)(1219-1289)，1248年起任阿雷佐的第59任主教。

## Narses

[纳尔塞斯](https://zh.wikipedia.org/wiki/%E7%B4%8D%E7%88%BE%E5%A1%9E%E6%96%AF)（478—573）与贝利萨留同为东罗马帝国查士丁尼大帝麾下大将，灭亡东哥德王国。

## Totila

[托提拉](https://zh.wikipedia.org/wiki/%E6%89%98%E6%8F%90%E6%8B%89)（？－552）541年至552年东哥特王国倒数第二任国王，出色的军事和政治领导人。552年败于东罗马帝国，战死。

## Hause Montfort

蒙特福德的[菲利普二世](https://de.wikipedia.org/wiki/Philipp_II._von_Montfort)(?-1270)，他参与了安茹查理夺取西西里的军事行动。[圭多·蒙特福德](https://de.wikipedia.org/wiki/Guy_de_Montfort)(1244-1292)是[第六代莱斯特伯爵西蒙·蒙特福德](https://zh.wikipedia.org/wiki/%E7%AC%AC%E5%85%AD%E4%BB%A3%E8%8E%B1%E6%96%AF%E7%89%B9%E4%BC%AF%E7%88%B5%E8%A5%BF%E8%92%99%C2%B7%E5%BE%B7%E5%AD%9F%E7%A6%8F%E5%B0%94)(Simon von Montfort)的第四个儿子，菲利普·蒙特福德(Philipp von Montfort)的堂弟。

## Markgraf von Montferrat

即[蒙费拉托藩侯威廉(1240-1292)](https://de.wikipedia.org/wiki/Wilhelm_VII._%28Montferrat%29)也叫蒙费拉托(Monferrato)的[威廉七世](https://en.wikipedia.org/wiki/William_VII,_Marquis_of_Montferrat)或[古列尔莫七世](https://it.wikipedia.org/wiki/Guglielmo_VII_del_Monferrato)，后联合卡斯蒂利的阿方索反对安茹查理。

## Konrad von Antiochia

[安条克的康拉德](https://en.wikipedia.org/wiki/Conrad_of_Antioch)(1240/1241-1312)是[安条克的腓特烈](https://en.wikipedia.org/wiki/Frederick_of_Antioch)的儿子，而安条克的腓特烈又是腓特烈二世的私生子，因此他是腓特烈二世的孙子，曼弗雷德的侄子，康拉丁的堂兄。

## Guido Guerra

即[圭多·格拉五世](https://it.wikipedia.org/wiki/Guido_Guerra)(1220-1272)，也出现在但丁的《神曲·地狱篇》第十六章。

## Kaiser Heinrich VI

神圣罗马帝国皇帝[亨利六世](https://zh.wikipedia.org/wiki/%E4%BA%A8%E5%88%A9%E5%85%AD%E4%B8%96_%28%E7%A5%9E%E5%9C%A3%E7%BD%97%E9%A9%AC%E5%B8%9D%E5%9B%BD%29)(1165-1197)1194年起成为西西里国王。腓特烈二世的父亲。

## Kaiserin Konstanze

[科斯坦察一世](https://zh.wikipedia.org/wiki/%E7%A7%91%E6%96%AF%E5%9D%A6%E5%AF%9F_%28%E8%A5%BF%E8%A5%BF%E9%87%8C%E5%A5%B3%E7%8E%8B%29)，西西里王国的女继承人，与亨利六世的婚姻使霍亨斯陶芬获得了西西里的继承权。

## Ferdinand III. von Kastilien

[卡斯蒂利亚国王斐迪南三世](https://zh.wikipedia.org/wiki/%E8%B2%BB%E7%88%BE%E5%8D%97%E5%A4%9A%E4%B8%89%E4%B8%96_%28%E5%8D%A1%E6%96%AF%E8%92%82%E5%88%A9%E4%BA%9E%29)（1199－1252）。

## Don Arrigo

即[卡斯蒂利亚的亨利](https://de.wikipedia.org/wiki/Heinrich_von_Kastilien)(1230-1303)，外号[元老亨利](https://en.wikipedia.org/wiki/Henry_of_Castile_the_Senator)，卡斯蒂利亚国王费迪南三世的第四子，第二子为[弗雷德里克](https://en.wikipedia.org/wiki/Frederick_of_Castile)。

## Ludwig von Bayern

巴伐利亚的路德维希，就是指[路德维希二世](https://zh.wikipedia.org/wiki/%E8%B7%AF%E5%BE%B7%E7%BB%B4%E5%B8%8C%E4%BA%8C%E4%B8%96_%28%E4%B8%8A%E5%B7%B4%E4%BC%90%E5%88%A9%E4%BA%9A%29)公爵。

## Meinhard von Götz

[戈里齐亚及蒂罗尔伯爵迈因哈德](https://de.wikipedia.org/wiki/Meinhard_III._%28G%C3%B6rz%29)(1193|1194-1258)

## Guido von Montefeltre

[蒙特费尔特罗的圭多一世](https://de.wikipedia.org/wiki/Guido_I._da_Montefeltro)(1220-1298)。

## Montefeltre

[蒙特费尔特罗家族](https://zh.wikipedia.org/wiki/%E8%92%99%E7%89%B9%E8%B2%BB%E7%88%BE%E7%89%B9%E7%BE%85%E5%AE%B6%E6%97%8F)是一个中世纪至文艺复兴时期的意大利贵族世家，该家族于13世纪起获封乌尔比诺领主并持续统治著乌尔比诺公国直到1508年绝嗣为止。

## Rudolf von Habsburg

后来成为神圣罗马帝国皇帝的[鲁道夫一世·哈布斯堡](https://zh.wikipedia.org/zh-cn/%E9%B2%81%E9%81%93%E5%A4%AB%E4%B8%80%E4%B8%96_%28%E5%BE%B7%E6%84%8F%E5%BF%97%E5%9B%BD%E7%8E%8B%29)(1218-1291)。

## Kroff von Flüglingen

弗吕格林根的路德维希·康拉德·克罗夫, 可能是([Konrad Kropf von Flüglingen](https://second.wiki/wiki/burg_flc3bcglingen))

## Pyrrhus von Makedonien

[皮洛士](https://de.wikipedia.org/wiki/Pyrrhos_I.)(前319/318-前272)，后来成为叙拉古国王（前278年–前275年）及马其顿国王（前288年–前284年）。

## Nikolaus III

教皇[尼古拉三世](https://zh.wikipedia.org/wiki/%E5%B0%BC%E5%90%84%E8%80%81%E4%B8%89%E4%B8%96)，本名乔瓦尼·加埃塔诺·奥尔西尼。

## Paulet von Marseille

[马赛的保莱特](https://en.wikipedia.org/wiki/Paulet_de_Marselha)，即保莱特·德·马赛(Paulet de Marselha)，是来自马赛的普罗旺斯吟游诗人。

# 地名

## Apulien

[阿普利亚](https://zh.wikipedia.org/wiki/%E6%99%AE%E5%88%A9%E4%BA%9A%E5%A4%A7%E5%8C%BA)

## Rhone

[罗讷河](https://zh.wikipedia.org/wiki/%E7%BD%97%E8%AE%B7%E6%B2%B3)

## Riviera

里维埃拉指[利古里亚海岸](https://zh.wikipedia.org/wiki/%E5%88%A9%E5%8F%A4%E9%87%8C%E4%BA%9A%E6%B5%B7%E5%B2%B8)

## Piemonte

[皮埃蒙特](https://zh.wikipedia.org/wiki/%E7%9A%AE%E5%9F%83%E8%92%99%E7%89%B9%E5%A4%A7%E5%8C%BA)

## Goito

[戈伊托](https://zh.wikipedia.org/wiki/%E6%88%88%E4%BC%8A%E6%89%98)

## Pola

[波拉地区](https://en.wikipedia.org/wiki/Pola_%28Italian_province%29)

## Siponto

[西潘托](https://en.wikipedia.org/wiki/Siponto)

## Casalecchio

[卡萨莱基奥](https://zh.wikipedia.org/wiki/%E9%9B%B7%E8%AF%BA%E6%B2%B3%E7%95%94%E5%8D%A1%E8%90%A8%E8%8E%B1%E5%9F%BA%E5%A5%A5)

## Imola

[伊莫拉](https://zh.wikipedia.org/wiki/%E4%BC%8A%E8%8E%AB%E6%8B%89)

## Modena

[摩德纳](https://zh.wikipedia.org/wiki/%E6%91%A9%E5%BE%B7%E7%B4%8D)

## Latium

[拉丁姆](https://zh.wikipedia.org/wiki/%E6%8B%89%E4%B8%81%E5%A7%86)是意大利中西部的一个区域，对应今天的[拉齐奥](https://zh.wikipedia.org/wiki/%E6%8B%89%E9%BD%90%E5%A5%A5%E5%A4%A7%E5%8C%BA)的拉丁语。

## Terracina

[泰拉奇纳](https://zh.wikipedia.org/wiki/%E6%B3%B0%E6%8B%89%E5%A5%87%E7%BA%B3)

## Maritima

[滨海马萨](https://zh.wikipedia.org/wiki/%E6%BB%A8%E6%B5%B7%E9%A9%AC%E8%90%A8)

## Tivoli

[蒂沃利](https://zh.wikipedia.org/wiki/%E8%92%82%E6%B2%83%E5%88%A9)离罗马30公里。

## St. Salvator in Pensilis

可能是位于圣天使区的[Santo Stanislao dei Polacchi](https://it.wikipedia.org/wiki/Chiesa_di_Santo_Stanislao_dei_Polacchi)教堂。

## Circus Flaminius

[弗拉米尼乌斯广场](https://zh.wikipedia.org/wiki/%E5%BC%97%E6%8B%89%E7%B1%B3%E5%B0%BC%E7%83%8F%E6%96%AF%E5%BB%A3%E5%A0%B4)

## Kap Argentaro

即现在的[蒙泰阿真塔里奥](https://zh.wikipedia.org/wiki/%E8%92%99%E6%B3%B0%E9%98%BF%E7%9C%9F%E5%A1%94%E9%87%8C%E5%A5%A5)。

## Porta Maggiore

[马焦雷门](https://zh.wikipedia.org/wiki/%E9%A9%AC%E7%84%A6%E9%9B%B7%E9%97%A8)，最初称为普奈勒斯蒂门（Porta Prenestina），是罗马古老然而保存完好的3世纪奥勒良城墙东侧的一座城门。

## Rocca Secca

[罗卡塞卡](https://zh.wikipedia.org/wiki/%E7%BD%97%E5%8D%A1%E5%A1%9E%E5%8D%A1)是中世纪哲学家托马斯·阿奎那的出生地。

## Telesia

[特莱西亚](https://it.wikipedia.org/wiki/Telesia)

## Alife

[阿利费](https://zh.wikipedia.org/wiki/%E9%98%BF%E5%88%A9%E8%B4%B9)

## Castel dell' Uovo

即[蛋堡(Castel dell'Ovo)](https://zh.wikipedia.org/wiki/%E8%9B%8B%E5%A0%A1)

## Schloß Wolfstein

[沃尔夫施泰因城堡](https://de.wikipedia.org/wiki/Burgruine_Wolfstein)

## Veii

[维爱](https://zh.wikipedia.org/wiki/%E7%B6%AD%E6%84%9B)

## Ponte Molle

[米尔维奥桥](https://zh.wikipedia.org/wiki/%E7%B1%B3%E7%88%BE%E7%B6%AD%E5%A5%A7%E6%A9%8B)

## Campo di Fiore

[鲜花广场(Campo de' Fiori)](https://zh.wikipedia.org/wiki/%E9%B2%9C%E8%8A%B1%E5%B9%BF%E5%9C%BA)

## Aventin

[阿汶丁山](https://en.wikipedia.org/wiki/Aventin)

## Tusculum

[塔斯库勒姆](https://zh.wikipedia.org/wiki/%E5%A1%94%E6%96%AF%E5%BA%AB%E5%8B%92%E5%A7%86)，也作图斯库卢姆，是一个古罗马时期的城市遗迹。

## Genazzano

[杰纳扎诺](https://zh.wikipedia.org/wiki/%E6%9D%B0%E7%BA%B3%E6%89%8E%E8%AF%BA)

## Palestrina

[帕莱斯特里纳](https://zh.wikipedia.org/wiki/%E5%B8%95%E8%8E%B1%E6%96%AF%E7%89%B9%E9%87%8C%E7%BA%B3_%28%E7%BD%97%E9%A9%AC%E9%A6%96%E9%83%BD%E5%B9%BF%E5%9F%9F%E5%B8%82%29)

## Subiaco

[苏比亚科](https://zh.wikipedia.org/wiki/%E8%8B%8F%E6%AF%94%E4%BA%9A%E7%A7%91)

## Castel del Monte

[蒙特城堡](https://zh.m.wikipedia.org/zh/%E8%92%99%E7%89%B9%E5%9F%8E%E5%A0%A1)
