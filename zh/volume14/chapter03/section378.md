_6\. 查理五世与利奥十世的联盟 伦巴第的战争 攻占米兰。1521年12月教皇逝世。

1521年5月8日，也就是路德发表《八分之一宣言》的当天，唐-胡安-曼努埃尔在罗马签署了教皇与查理之间的联盟草案。其条款如下 米兰和热那亚将从法国手中夺回，并在其 "真正的王子 "查理的宗主权下恢复合法政府。弗朗切斯科-马里亚-斯福尔扎（Francesco Maria Sforza）被任命为米兰公爵，安东尼奥托-阿多诺（Antoniotto Adorno）被任命为热那亚总督。一万名瑞士人得到了 20 万杜卡特，其中一半由教皇支付。驱逐法国人后，查理承诺将帕尔马和皮亚琴察交给教会，并帮助教会征服费拉拉。他将佛罗伦萨和美第奇家族置于他的保护之下，并承诺全力追击所有反对天主教信仰和罗马教廷的人。另一方面，教皇发誓要把查理封给那不勒斯，接受他的加冕礼，并在对威尼斯的战争中支持他。瑞士人和英国国王在这个联盟中仍有一席之地。这项条约的缔结是朱利叶斯-美第奇枢机主教的功劳；皇帝答应给他西班牙的保护国、一个主教职位和一万杜卡特的俸禄作为奖赏。

主要目的是购买瑞士士兵。根据早先的条约，由法国出钱在卢塞恩成立的党派已经授权弗朗西斯国王招募士兵；但锡永红衣主教，这位为教皇目的不遗余力地煽动祖国的人，在苏黎世成功地开展了同样的士兵生意。茨温利是瑞士的改革者，他坚决反对这种可耻的贩卖人口行为。"很公平，"这位高贵的公民说，"这些罗马红衣主教也穿着宽大的斗篷，戴着红色的帽子；摇一摇，杜卡特就会掉下来，扭一扭，自己的血就会流下来。" 瑞士人答应了教皇的要求，教皇向他们进贡了 3.5 万个杜卡特和几千名士兵，战争就此定局。威尼斯使节仍试图维持和平，改变教皇的想法；5 月底，教皇甚至又开始动摇了。

曼努埃尔向皇帝通报了与当时的法国特使卡尔皮伯爵的新谈判，甚至建议他用召开会议的威胁来吓唬利奥。随后，教皇做出了决定：5 月 29 日，他签署了同盟文件。

威尼斯人对利奥将查理带入意大利感到惊讶，因为利奥一直将皇权置于意大利之外。他们可能会指责他宁可选择战争也不选择和平，以便用几个城市来扩张他的教会国家。他的对手在佛兰德斯和纳瓦拉煽动起了他的敌人。他让安德烈亚斯-德-福瓦（Andreas de Foix）入侵了毫无防备的纳瓦拉。当皇帝听到这个消息时，他感叹道："上帝为我作证，这场战争不是我挑起的；法国想让我变得比我更强大"。他在美因茨对威尼斯特使加斯帕罗-孔塔里尼说："要么国王消灭我，要么我成为欧洲的主宰。" 事实上，当时法国向正在崛起的查理五世的帝国势力发起战争的原因与今天法国向在普鲁士领导下再次崛起的德国发起战争的原因相似。1521 年 5 月 20 日，伊格内修斯-洛约拉在纳瓦拉的潘佩洛纳被法国子弹击伤。就在查理五世对路德下达帝国禁令的同一个月，这个可怕的西班牙人在时间的背景中显现出来。这股恶魔般的力量形成了，它注定要让宗教改革远离罗曼人和梵蒂冈，但又不能让它在我们的祖国完全民族化。

法国人还在意大利进行了第一次战争运动。莫罗内在雷焦聚集了许多米兰的流亡者，当时吉恰尔迪尼（Guicciardini）是教皇的副将，他与这些流亡者一起制定了进攻雷焦和公国其他城市的计划。这一阴谋促使米兰省督劳特里克的兄弟托马斯-德-福瓦-莱斯昆（Thomas de Foix）内廷大臣试图发动政变，夺取雷焦。政变于 6 月 23 日失败。教皇随后宣布教皇国受到攻击，并宣布与皇帝结盟。他禁止法国国王，甚至免除了臣民的效忠宣誓，除非弗朗西斯在一定期限内放下武器，交出帕尔马和皮亚琴察。皇帝和教皇的联军由普罗斯佩罗-科隆纳指挥，他手下站着年轻的佩斯卡拉侯爵费尔南多-达瓦洛斯（Fernando d'Avalos）。教皇的战地指挥官是曼图亚的费德里戈-贡萨加（Federigo Gonzaga），他是红衣主教美第奇的使节。盟军首先试图攻占帕尔马，但阿方索通过向摩德纳进军洗劫了这座城市。费拉拉公爵得知利奥和查理的联盟内容后，站到了法国一边，弗朗西斯一世的盟友只有他、无力的本蒂沃格里和威尼斯人。这位王子很快就意识到，他仓促发动了战争；他试图在8月的加莱请求英国调停，但徒劳无功；相反，英国也于8月25日在布鲁日与查理缔结了反对他的同盟。在本国的压力下，国王无法向意大利派遣救援部队，而一万瑞士人则越过波河向米兰进发。人们看到两位红衣主教，西滕和朱利叶斯-美第奇，身着紫色长袍，骑着马，前面带着银色十字架，这是对基督教的嘲弄。当联盟的将军们与瑞士人联合之后，在瓦普里奥战败的劳特累克再也无法坚持下去了。他撤回米兰，烧毁了郊区，处死了被怀疑是吉贝尔派的市民，激怒了不幸的人民，他们召唤他们的解放者--新的瘟疫之灵。1521 年 11 月 19 日，普洛斯彼罗和佩斯卡拉从米兰城墙上追击威尼斯人，劳特里克内廷大臣随即撤回科莫。夜里，红衣主教公使美第奇进入了这座不设防的城市。公国的大部分城市都投降了，只有克雷莫纳以及米兰、诺瓦拉、阿罗纳和亚历山德里亚的城堡仍被法军占领。

11 月 24 日，利奥十世在他的马格里亚纳别墅收到了米兰被攻占的好消息。他说，这对我来说比我的教皇职位更重要。因此，政治和军事上的成功是当时教皇们最重要的事情和最大的喜悦；教会在世界上的道德力量已经萎缩到了悲惨的领土关系上。有传言说红衣主教美第奇将成为米兰公爵，而斯福尔扎将取代他成为红衣主教。11月25日，利奥怀着无比激动的心情回到了米兰城；人们手持橄榄枝向他涌来，唱诗班用音乐向他致意。人们欢庆了三天。教皇本想召开宗教会议，但激动的心情让他病倒了；他自己也取消了前往人民圣母玛利亚教堂的感恩游行。

不久后，他听说皮亚琴察陷落了，他的军队正在骚扰费拉拉公爵。法国人战败后，阿方索发现自己陷入了绝境；他击败了帕尔马，激怒了盟军，可以预料盟军现在会根据条约对他发动进攻。利奥对他实施了新的禁令，并对费拉拉实施了禁止教务。公爵固守首都，决心光荣下台，他在一份宣言中向全世界揭露了教皇推翻他的应受谴责的手段。

12 月 1 日，利奥听说帕尔马也去世了；同一天，他也去世了。他的突然死亡引起了毫无根据的中毒怀疑。利奥的敌人们欢欣鼓舞；在死者死后，人们高呼着关于博尼法斯八世的著名箴言："你像狐狸一样登上王位，像狮子一样统治，像狗一样死去"。所有憎恨利奥的人都因为他的不诚实、他在购买职位时的欺骗以及他的财技而对他进行讽刺。据当时来自罗马的报道称，无数人、教皇的债权人身败名裂。比尼银行有20万杜卡特的债权，加迪家有3.2万，斯特罗兹银行面临倒闭的危险。里卡索利借给教皇1万个杜卡特，红衣主教萨尔维亚蒂为了用这些钱创造财富而放弃了自己的恩惠，他借给教皇8万个杜卡特；红衣主教桑蒂-夸特罗和阿尔梅利尼每人有15万个杜卡特需要偿还。"总之，利奥的仆人或宠臣没有一个不是身败名裂的；事实上，他为了罗马教廷的利益，对自己的亲戚、宠臣或朋友不闻不问，这真是令人惊奇；事实上，当人们看到他的家族是如何变得穷困潦倒、四分五裂的时候，一定会惊叹不已"。使徒司库空空如也，甚至无法支付为教皇中最杰出者举行葬礼的蜡烛费用；必须使用为红衣主教瑞阿里奥举行葬礼时所用的蜡烛。诗人、艺术家和学者、罗马的托斯卡纳人以及无数享受过利奥慷慨的人都热泪盈眶地哀悼他。人们称赞他是幸福的，因为他是在得到胜利的消息后去世的。他最热切希望看到法国人被赶出意大利，帕尔马和皮亚琴察重归教会的愿望终于实现了。下一次战争的变化可能会摧毁这种幸福，但这种幸福是否足以荣耀教皇的最后时刻，哲学家和基督徒们都会怀疑。

利奥十世不仅作为最光荣的教皇，也作为最幸福的教皇出现在世人和后人面前。然而，即使是同时代的人也敢在他身上看到一个凡人的形象，而这个凡人其实是极不幸福的。不治之症、流放、囚禁、敌意、红衣主教的阴谋、战争，最后几乎失去了所有的邻居和朋友，这一切让教皇的快乐时光变得暗淡无光。如果瓦勒里亚努斯能够预见到德国宗教改革的重要性，他就会为自己忧郁的观点找到更有力的证据：因为利奥十世不仅目睹了宗教改革的发生，而且还通过滥用教皇权力和宫廷的异教奢华实际上挑起了宗教改革。

在利奥的天性中，值得称道的品质与鲁莽、虚伪和无情的特质混杂在一起。他的聪明才智并非建立在高尚的男子气概之上。他的天性是广义的，但没有严肃的道德观，没有深度和独创性，闪烁着他所处时代文艺复兴教育的所有光辉，使其显得光彩夺目。他的马基雅维利主义源于世俗的教皇权力，而司铎的道德美德与之绝不相容。即使是最仁慈的判断者也会承认，为了评判教皇的行为而将王子与神职人员分开的诡辩企图是不可接受的。因为在使徒的法庭上，他们会找到一个法官，允许他们用世俗的王室外衣来掩盖自己的罪行，并将两种权力的结合视为两种本性的结合吗？作为世俗君主，尤利乌斯二世和利奥十世等人并不比当时的其他君主差，甚至更好；但作为教皇，他们在任何公正的审判中都显得令人无法容忍和憎恶。那个时代的教皇自称是基督的代理婚姻，实际上是上帝在人间的代理；这正是历史无情地对待他们的原因。因为他们出于权力的欲望而篡改了爱的神圣法则，他们用世俗权力的卑劣欲望取代了基督教的崇高理想。

利奥十世的讴歌者和宫廷谄媚者的长号声（从来没有哪位教皇有如此多如此雄辩的讴歌者和宫廷谄媚者）再也无法扰乱后人的舆论，后人必须拒绝加入这种对利奥十世的偶像崇拜，并将他列入历史伟人之列。他继承了博尔哈和罗韦雷对教皇制度的改造，并将其传给了他，还在教皇制度中加入了他精通的美第奇外交艺术。他将这种阴谋、虚伪和政治家般模棱两可的制度作为罗马教廷的世俗教条传给了他的继任者。耶稣会主义首次作为一种政教合一的政策出现。利奥仍然将欧洲关系的重心放在教皇身上，并毫无疑问地赋予了教皇在意大利的至高无上的地位。他加强了罗马教廷的精神力量，并再次征服了法国，但在德国的努力却失败了。人们倾向于称他为伟大的思想：将外国人驱逐出意大利，在教皇的统治下统一这个国家，在欧洲建立和平与平衡，以及东方战争：这些在他的行动中要么显得如此不连贯，要么显得如此不成功，以至于人们只能人为地将这些思想编成他的施政纲领。

教会本身让利奥十世站在了毁灭的悬崖边上。他沉浸在辉煌和统治的计划中，沉浸在美学的放纵中，对教会危机没有表现出丝毫的理解。他沉醉于自己的辉煌，在辉煌中享受着整个精神力量的伟大和充实，将其视为一种包罗万象的幸福：在他的享受中，教皇制度就像罗马的旧皇权一样烟消云散了。他将教皇制度淹没在新拉丁异教的华丽之中。他不理解自己的基督教使命，因为像文艺复兴时期的所有教皇一样，他把教皇的伟大与教会本身的伟大混为一谈，这种罗马人对基督教理想的篡改，是教皇们所有错误中最漫长、最可怕的错误，导致了德意志宗教改革。

利奥十世的名字在文明史上最为闪亮。至少从传统意义上讲，它代表着文艺复兴的顶峰。在那里，命运对他最为眷顾。他收获了更伟大的前辈们创造性地播种的成果。在意大利民族精神找到其经典成就的时候，他手中握着慷慨的聚宝盆。利奥十世具备了使他成为那个时代教皇的品质：对文化中一切伟大和美好的事物毫无偏见的欣赏，对天才创造的真正热情，对整个时代形成的理解，以及最后，他的王室血统中的王者气度。从根本上说，也正是科西莫和洛伦佐-美第奇的光环确保了他在文化史上的奥古斯都地位。
