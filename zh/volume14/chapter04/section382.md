_4\. 意大利诗歌 本博对意大利语的贡献。莫尔扎。特巴尔代奥 贝尔纳多-阿科尔蒂 贝扎诺 维托利亚-科隆纳 圣维罗妮卡-甘巴拉 贝尔尼和滑稽诗歌 皮耶罗-阿雷蒂诺 阿莱曼尼 鲁切莱 阿里奥斯托 特里西诺 戏剧 喜剧。卡兰德拉-比比埃纳》。悲剧的尝试_

古典主义、没有自然的艺术、没有灵魂的美丽感性形式是文艺复兴时期意大利诗歌的基本特征，因此他们也掌握了白话诗。十五世纪，对拉丁语的热情曾威胁到意大利语的生命，但洛伦佐-美第奇、波利齐亚诺和普尔西已经消除了这一危险。意大利语已经成为一出音色饱满的弦乐剧，其旋律的魔力由一种有意识的艺术来调节。就连西塞罗也不再忌讳用意大利语写诗了。就连他们的首领本博（Bembo）也为意大利语的语法发展做出了不小的贡献。他还在 1502 年为阿尔丁版本的但丁提供了文本，人们对这位诗歌英雄的自豪感重新唤起，以至于在利奥时代，佛罗伦萨想要从拉文纳收回但丁的骨灰，并将其安葬在一座纪念碑中。1530 年，罗莫洛-阿马塞奥（Romolo Amaseo）在博洛尼亚向查理五世发表演讲时，仍然认为意大利语是乌合之众的语言，这不过是迂腐的愚蠢之举。同时掌握两种语言被认为是一种丰富的创造力，每种语言都被视为民族语言。一种是教会、政治、法律和科学的伟大的世界语言，同时也是高贵和时尚之美的语言，古代的知识生活在这种语言中奇迹般地重新流淌起来；另一种则属于当代和祖国的脉搏。在拉丁文学家中找不到意大利最好的诗人，也找不到最好的历史学家。最后，尽管有普劳图斯（Plautus）和特伦斯（Terence），新戏剧还是宣布人民的语言是戏剧的自然语言。

几乎没有一个受过教育的意大利人没有写过十四行诗、狂歌和其他诗歌。XVI 世纪的文学作品中充斥着数不胜数的里马托里（Rimatori）诗人。他们不再是文艺复兴时期的诗人，而已经是但丁和彼特拉克之后衰落时期的诗人。我们会注意到，在意大利人的艺术达到顶峰的同时，他们的优美文学也在衰落。他们曾一度主宰了欧洲的文学趣味，但其影响力在成为民族性文学之后便立即减弱了。十六世纪的意大利诗歌缺乏想象力，思想贫乏。它既没有内心的激情，也没有探究人生问题的深刻精神。

对文体的主要需求造就了十四行诗的模板，在这种模板中，感情成了范本的奴隶。正如有人正确指出的那样，这种民族的、随时可用的表达形式有其优点，但作为一种方式，它也有其缺点。十六世纪的抒情诗人只是模仿者。彼特拉克是他们的偶像，而对于这个轻浮的时代来说，但丁太深沉、太伟大了，他只能是个背景。彼特拉克在无数著作中被阐释，他的柏拉图主义被模仿。本博被视为意大利诗歌的革新者，但在他的诗歌中，他只是平淡的十四行诗人的合唱队长。在奢华的前教权时代，我们遇到的大多是动物般的、淫荡的、虚荣的孟子韵文或被赞美的廷臣诗歌。如果说任何地方的诗歌都是时代的一面镜子，那么利奥十世时代的诗歌则显示出感情和思想的无边浅薄。没有哪位伟大诗人的灵魂为国家的衰落而悲愤不已。意大利的诗人中没有萨沃纳罗拉：他们歌颂他们的赞助人和菲利内斯，写田园诗和骑士历险记，而意大利的自由正在消亡。但但丁比他们更早，甚至彼特拉克也为祖国的苦难大声疾呼。在当时众多的诗歌中很难发现几首爱国诗篇。在强大的朱利叶斯时代有几首，而在利奥时代，缪斯也变得女性化了。

罗马可以支持学者和艺术家，但只能为诗歌精神提供奴隶的枷锁。那些被罗马的玩世不恭所束缚，成为红衣主教的寄生虫和沙龙诗人的人才是值得惋惜的。有些人在不同的环境下会取得更大的成就，比如弗朗切斯科-玛丽亚-莫尔扎（Francesco Maria Molza），他是摩德纳贵族，可能是当时最有才华的诗人。他曾在朱利叶斯统治下的罗马生活了很长时间，后来又在利奥宫廷生活，后来又与红衣主教希波吕托斯-美第奇和亚历山德罗-法尔内塞生活在一起。1548 年，在经历了凄凉的一生后，他死于高卢病。他博学多才，是一位精通两种语言的诗人；他的田园诗《台伯河仙女》歌颂了他的罗马情人福斯蒂娜-曼奇尼，获得了极大的赞誉。

费拉雷人安东尼奥-泰巴尔代奥原本是一名医生，是利奥十世宫廷中塞拉菲诺和奥菲斯的继任者，作为一名即兴创作者而大放异彩。拉斐尔将他列入《帕纳索斯》的诗人之列，并赋予阿波罗另一位著名即兴演奏家贾科莫-萨内塞多（Giacomo Sansecondo）的特征。没有哪个民族比意大利人更容易接受闪电般的即兴表演。贝尔纳多-阿科尔蒂（Bernardo Accolti）所激起的热情表明，意大利人对艺术语言的瞬时生成感是多么发达。年轻时，这位才华横溢的阿雷蒂纳人通过即兴演奏琵琶激励了乌尔比诺宫廷，随后又让利奥十世和整个罗马为之着迷。当他歌唱时，人们蜂拥而至梵蒂冈，教皇为他敞开了大门。阿科尔蒂以奥林匹亚的自信自称为 "Unico Aretino"，阿里奥斯托也以钦佩的口吻这样称呼他。利奥十世给了他如此丰厚的礼物，以至于他为自己买了一个尼皮公爵的头衔，这个头衔现在更适合一位诗人王子了，而在亚历山大六世时期，一个两岁的私生子博尔哈曾拥有过这个头衔。他于 1534 年左右去世，他的情色诗歌和抒情民歌（Strambotti）可与塞拉菲诺和泰巴尔代奥的作品相媲美，尤其是他的个人表演为这些作品增添了热情洋溢的音乐魔力。如今，阿科尔蒂只活在文学史上。他还根据薄伽丘的一部长篇小说创作了浪漫喜剧《八度音中的弗吉尼亚》，莎士比亚在创作《皆大欢喜》时也借鉴了这一寓言故事。弗吉尼亚》与莎士比亚戏剧的关系就像花芽与盛开的花朵的关系，但它仍然以其巧妙的诗意力量和常常令人着迷的情感给人带来惊喜。在文艺复兴时期的喜剧中，仅其欢乐的寓言就使它成为真正的瑰宝。

特巴尔代奥（Tebaldeo）、莫尔扎（Molza）、本博（Bembo）、阿科尔蒂（Accolti）以及本博的朋友、十四行诗家阿戈斯蒂诺-贝扎诺（Agostino Beazzano）是那个时代罗马圈子最著名的意大利诗人。后来，法布里齐奥的女儿维托利亚-科隆纳才加入了他的行列。这位米开朗基罗的朋友、佩斯卡拉的妻子，与其说是凭借真才实学，不如说是凭借富丽堂皇的住所和丈夫的名气，使她的许多同时代诗人黯然失色。因此，她的诗歌至今仍被传诵。这些诗歌献给了宗教、爱情、忠诚和友谊，尽管是对彼特拉克的模仿，却烙上了独立、道德高尚的印记。紧随其后的是圣维罗妮卡-甘巴拉，她是来自布雷西亚的詹弗朗西斯科-甘巴拉伯爵的女儿，也是科雷焦领主吉贝托的妻子，她很早就失去了吉贝托。她不属于罗马文坛，因为她的一生部分时间在博洛尼亚度过，部分时间在科雷焦度过，1550 年在科雷焦去世。

意大利人采用了各种古老的形式：讽刺诗、说教诗、史诗和戏剧。他们敏锐的智慧可能使他们发展了讽刺诗，但在这里，闹剧和滑稽戏的倾向占了上风；即使是阿里奥斯托的讽刺诗也只是平庸之作，没有生动的人物形象和真正的艺术风格。来自托斯卡纳的弗朗切斯科-贝尔尼（Francesco Berni）曾长期居住在罗马，为比比埃纳服务，后来成为吉贝尔蒂的宠臣，他创立了滑稽诗歌。与他一起成名的还有来自弗留利的乔瓦尼-毛罗，他也曾作为主教的侍从居住在罗马。当您阅读这些 "戏谑 "的诗歌时，要么会为其题材的无厘头而发笑，要么会为其公然揭露的不道德的深渊而不寒而栗。无耻的裸体是当时意大利文学的一大特色。这是一种 "hetaera "文学，是对民族精神机体的道德梅毒。这些通奸者往往是神职人员，在祭坛上做弥撒。肮脏的《Capitolo del Forno》的作者乔瓦尼-德拉-卡萨（Giovanni della Casa）死后担任贝内文托大主教，并在威尼斯担任宗教裁判官。马卡龙诗歌的创始人泰奥菲洛-福伦戈（Teofilo Folengo）是一名本笃会修士。粗俗的班德洛（Bandello）是一位多明我会修士，死后担任阿根主教。

在朱利叶斯、利奥和克莱门特七世时期，皮埃特罗-阿雷蒂诺曾多次作为文学冒险家出现在罗马，但都没有在那里站稳脚跟。这个人代表了意大利堕落的精神磷光沼泽，他就是 XVI 世纪文学中的切萨雷-博尔哈（Cesare Borgia）。他是一个不道德的现象，在任何时代的任何国家都是前所未有的。人们几乎不知道更应该惊叹的是什么，是这种玩世不恭的厚颜无耻，还是这位记者的力量以及他强加给这个世纪的偶像崇拜。在这个国家里，一个如此无知、如此无耻、如此卑鄙的乞丐作为暴君登上了文学的宝座，并受到世界上所有伟人的敬畏和尊崇，而他却鄙视和掠夺这些伟人，这证明这个国家的每一个道德源泉都受到了毒害，奴役是其必然的命运。残暴的《Raggionamenti》的作者用同一支笔写下了《圣母的生活》和其他宗教内容的著作，教皇尤利乌斯三世拥抱并亲吻了他，并使他成为圣彼得教堂的骑士。

有两位佛罗伦萨人因其堪称楷模的说教诗而闻名，洛多维科-阿莱曼尼（Lodovico Alemanni）的《农业》和精神领袖乔瓦尼-鲁切莱（Giovanni Ruccellai）的《蜜蜂》。只有后者进入了罗马的圈子。他是博学的贝尔纳多之子利奥十世的表弟，成为了一名司铎，但没有成为红衣主教。利奥在外交事务中重用了他。教皇去世后，鲁切莱离开罗马，在克莱门特七世时期回到罗马。他作为圣天使城堡的城堡官去世。

意大利人在浪漫史诗中获得了最大的声誉。继普尔西和博雅尔多之后，阿里奥斯托的史诗达到了顶峰，意大利语的完美杰作塔索的诗歌更是达到了顶峰。在但丁复兴之前，意大利诗歌对两位伟大史诗诗人的推崇是建立在他们的基础之上的。在阿里奥斯托那里，意大利绘画的创造精神变成了诗歌。他创作的色彩斑斓的魔幻剧《奥兰多》本身缺乏个性和思想，但却与利奥十世的时代非常吻合。它反映了意大利在感官和精神上的奢华熏陶，是一部迷人、诱人、充满音乐感的腐朽的诗歌作品，正如但丁的诗歌曾经是国家男性力量的一面镜子一样。阿里奥斯托用诗歌的 "吉兰多"（girandole）伴随着这场美第奇的文化狂欢；其火与光的混乱洪流是意大利精神最绚丽的现象之一，民族生活的现实世界已溶解在其中。但丁的诗歌已成为民族精神取之不尽、用之不竭的生命源泉，是民族的诗歌福音；阿里奥斯托和塔索的诗歌却没有产生这样的影响：它们只是意大利文学最美丽的装饰品。在朱利叶斯二世时代，我们看到阿里奥斯托在罗马执行一项危险的任务；后来他来祝贺利奥十世，但他从他的朋友美第奇那里除了获得印刷特权外一无所获，几天后他离开了罗马，再也没有回来。他在一些讽刺诗中宣称，对他来说，适度的自由比罗马宫廷服务的金色奴役更有价值，但如果教皇给他一个职位，也许他的观点会有所改变。奥兰多》于 1516 年在费拉拉出版了四十回，然后在诗人去世前一年，即 1532 年出版了全集。

虽然利奥十世没有试图记录这位才华横溢的诗人，但特里西诺在他的宫廷中过着非常尊贵的生活。这位多才多艺、博学多才的文森特人富有而独立，作为外交官为教皇提供了许多服务。他想以荷马为蓝本，在坚实的历史基础上创作一部民族史诗，以超越阿里奥斯托的声望，于是他花了 20 年时间研究古典文学，创作了《从哥特人手中解放出来的意大利》。这部平庸的学术著作和一味模仿的产物仅存于纸上。但是，高贵的特里西诺却能以不费吹灰之力就获得当之无愧的戏剧家声誉而自慰。

许多条件都有利于戏剧在意大利的兴起：对戏剧表现的热情，丰富的节日生活，所有艺术、教育和品位的参与，阶级和个性中人物的多样性，道德状态的公开性和流动性，模仿和表演的最高天赋，以及完美的不道德，喜剧诗人完全不受职位审查的限制。事实上，戏剧的复兴也是意大利人伟大的文化和历史成就。他们发展了表演艺术和戏剧形式，成为欧洲舞台的大师。神秘剧、喜剧、阴谋剧、浪漫悲喜剧、严肃戏剧和悲剧、假面舞会、即兴戏剧、歌剧：意大利人尝试了所有这些流派，他们才华横溢，就像文艺复兴时期戏剧的诙谐本性一样，令人钦佩。他们在 XVI 世纪创作的戏剧，尤其是喜剧，多达数千部，但意大利舞台上却没有莎士比亚。然而，这位伟大的英国人使用意大利人的小说和戏剧，就像一个懂得如何摸到纯金的矿工。他们的诗歌精神似乎缺乏一种基本力量，没有这种力量，戏剧激情就无法在道德上深化。这种力量就是诗人的哲学。独立性一直是缺失的。古代传统、拉丁学术和正规艺术仍然是意大利戏剧大众化发展的敌人。

塞内加（Seneca），尤其是普劳图斯（Plautus）和特伦斯（Terence），庞波尼乌斯-拉埃特斯（Pomponius Laetus）曾为之付出了巨大的努力，他们在晚期获得的影响力或许超过了他们自己。他们的作品被无数人翻译和模仿，成为上百所学院的守护神。他们的喜剧到处上演。现在，教皇为戏剧定下了基调，开创了一个时代。利奥十世希望看到新老剧目首先上演。梵蒂冈是当时欧洲最辉煌的剧院。其他宫殿也上演戏剧，几乎没有一个节日没有戏剧演出。1513 年 9 月，元老宫的剧院布置得无比奢华，为纪念被任命为罗马元老的朱利安-美第奇（Julian Medici），上演了神话场景和普劳图斯（Plautus）的《波努卢斯》（Poenulus）。1519 年 3 月，利奥让拉斐尔为阿里奥斯托的《Suppositi》绘制场景，并在 2000 名观众面前演出。中场休息时演奏了音乐，还表演了芭蕾舞剧《莫雷斯卡》。

演员大多是在庞波尼乌斯学校受训的学者。英吉拉米和诗人加卢斯等人也受到了赞扬。特伦西亚人弗朗切斯科-切雷亚（Francesco Cherea）的喜剧艺术在利奥的宫廷中大放异彩。在其他城市的学院里，角色艺术也得到了很好的发展，演员们都会客串演出。利奥让锡耶纳罗齐学院的喜剧演员每年来罗马演出。这些戏剧的放荡程度无以复加。如果除了喜剧之外，我们没有其他证据可以证明十六世纪意大利人的思想生活，那么我们就不得不判断，这个民族的道德败坏程度完全等同于古罗马和拜占庭戏剧时代。这些喜剧大多以引诱、通奸和最卑鄙的感官享乐为中心，但它们却是最高阶级的首选娱乐。教皇、王子、神职人员和贵族们都热衷于上演这些喜剧。利奥陶醉于其中，从未感到羞耻或满足。人们可以理解他喜欢宫廷小丑的卑鄙笑话，也可以理解他因为一个修道士写了一部糟糕的喜剧而在戏剧舞台上把他打得鼻青脸肿，人们甚至可以原谅约维乌斯这种愚蠢的异想天开，即使他没有因时代精神而产生的恶意的次要意图。但是，人们很难解释这位教会领袖对他所处时代的戏剧完全不加批判的行为。因为与他同时代的人，甚至是古代爱好者，有时也会对此感到后怕。不要说伊拉斯谟，就是吉拉尔迪也是这样说的："时代啊，礼仪啊！旧场景的一切污秽都回来了；到处都在上演寓言故事：所有基督徒的思想曾经因其不道德而被放逐和毁灭的东西，司铎们，甚至我们的教皇，更不用说王公贵族们，现在都公开把它召回剧院。事实上，充满野心的神职人员自己也在寻求演员的名声"。一部最庸俗的喜剧的作者，因为是红衣主教的作者，所以他的印记更加光彩夺目。

这位诗人是利奥和拉斐尔的朋友，托斯卡纳比比亚的贝尔纳多-多维齐奥。他出生于 1470 年，经兄弟皮耶罗引荐给洛伦佐-美第奇，成为美第奇家族最热心的仆人。他陪同红衣主教乔瓦尼流亡国外，为他当选教皇而努力，获得了紫袍，一夜暴富，成为罗马最受尊敬的政治家之一。他机智、开朗的性格和对生活的热情使他广受欢迎。在利奥的宫廷里，他是真正的享乐大师，是所有欢乐活动的导演，尤其是狂欢节和戏剧。多维齐是一个非常平庸的诗人，在朱利叶斯二世统治时期，他就已经写出了模仿普劳图斯的《梅纳奇门》的散文剧本《卡兰德拉》。该剧作为意大利第一部开创性的喜剧引起了轰动，很快就在所有宫廷上演。利奥天真地在梵蒂冈为曼图亚的伊莎贝拉侯爵夫人上演了这出轻浮的戏剧，由巴尔塔萨-佩鲁齐（Balthasar Peruzzi）绘制场景，这在今天看来是难以理解的。中场休息时充满了音乐。

卡兰德拉》拉开了马基雅维利、阿里奥斯托、阿雷蒂诺和其他诗人创作的一系列同样庸俗的喜剧的序幕，这些诗人热情地投入到喜剧中。在这里，他们可以把自己塑造成时代弊病的评判者或时代风尚的描绘者，从而确保达到无懈可击的效果。然而，尽管这些喜剧的内容和情节有时很诙谐，很像小说（这也是它们吸引莎士比亚的原因），但意大利文艺复兴时期戏剧中的激情心理却鲜有人涉足。即使是最负盛名的喜剧诗人也还处于社会喜剧的早期阶段。他们所做的只是撷取古罗马喜剧的主题，对现代人来说，古罗马喜剧中的人物只能具有伊特鲁里亚人和面具人的特征。

在悲剧方面，意大利人试图模仿塞内加，翻译索福克勒斯和欧里庇得斯的作品。但是，希腊的天才抵制了意大利的大众精神，因为意大利的大众精神既没有语言器官，也无法表达希腊本质的庄严和丰满。这些悲剧中最古老的是特里西诺的《索菲尼斯贝》，这是一部杰出的剧作，在素材处理上既快乐又富有创造性，欧里庇得斯模仿了它的处理方式，并将其作为高贵的悲剧萌芽植入意大利文学，而没有进一步发展。据说《索福尼斯贝》于 1514 年在维琴察上演；可能在利奥十世时期在罗马上演过。稍后，Ruccellai 的《Rosmunda》和他的《Orest》上演；1515 年 11 月，利奥十世访问佛罗伦萨期间，第一部悲剧在他面前上演。经过最初的努力，意大利人的悲剧缪斯很快就疲惫了。他们一味地模仿欧里庇得斯，甚至模仿特里西诺，塞内加悲剧的舞台上充斥着蒂亚特式的恐怖。题材的残酷性让人不寒而栗：从十六世纪末开始，意大利的殉道者绘画变得和悲剧一样野蛮。

戏剧的非凡之处在于它脱离了教会，放弃了精神奥秘的道路，变得完全异教化和世俗化。意大利人摒弃了基督教和《圣经》中的悲剧素材，转而追求古代或浪漫主义。他们的喜剧嘲笑修道院、神职人员和教会道德。文艺复兴时期的戏剧甚至可以说是彻底推翻旧信仰的一种手段，但事实证明，它在将人们从迷信和等级制度中解放出来方面是最薄弱的力量。它能够在喜剧的攻击下幸存下来，僧侣和司铎被允许继续做马基雅维利的《曼德拉戈拉》和类似戏剧中安静的同道中人，这也许是意大利戏剧内在非实体性的最有力证明，它只是从外部把握生活，作为一种人工产物，只停留在学术领域。
