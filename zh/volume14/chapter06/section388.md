### 第六章

_1\. 克莱门特七世作为意大利独立斗争的领袖。皇帝派蒙卡达去见教皇。克莱门特拒绝了他的请求。庞贝奥-科隆纳和吉贝尔派。联盟战争不幸开始。乌尔比诺公爵反对米兰的努力无果而终。科隆纳于 1526 年 9 月 20 日入侵罗马，劫掠了博尔戈。克莱门特被迫签订不光彩的条约。皇帝致教皇的宣言。施派尔议会。建立宗教改革_。

克莱门特做出了他一生中唯一一个大胆的决定，这违背了他的本性。那就是为把意大利，也许是欧洲从凯撒的统治下解放出来而战，自格列高列七世以来，教皇们一直在与凯撒的统治进行着不懈的斗争，并最终将其击败。1526 年，教皇最后一次成为意大利人民的代表；但这位教皇是可悲的克莱门特七世，他只能用教会和中世纪国内政治的小本能来歪曲向他提出的伟大构想。

他寄希望于联盟的优势、意大利人愤慨的民族情绪和一万名瑞士人的速度。他有必胜的把握。米兰准备起义；洛迪、克雷莫纳和帕维亚组织了阴谋活动。威尼斯人在他们的总指挥乌尔比诺公爵的率领下将越过阿达河前进，教皇在兰戈内（Rangone）、维泰利（Vitelli）、乔瓦尼-美第奇（Giovanni Medici）和教皇副将弗朗切斯科-吉恰尔迪尼（Francesco Guicciardini）的率领下于 6 月聚集在皮亚琴察。他们的目标是尽快洗劫米兰城堡。佩德罗-纳瓦罗率领的法国舰队和安德烈亚-奥贝托-多利亚率领的教皇舰队将首先征服热那亚，然后进攻那不勒斯。萨多莱托（Sadoleto）和驻西班牙大使卡斯蒂利昂（Castiglione）等谨慎的人呼吁和平，但克莱门特和吉贝尔蒂（Giberti）却热衷于仇恨皇帝，希望开战。意大利的第一批政治家马基雅维利、维托里和吉恰尔迪尼，甚至所有的爱国者，都宣布这场战争是神圣而必要的国家大事。圭恰尔迪尼尤其热切地建议教皇这样做。但是，一颗凶星笼罩着美第奇。它将他从一个错误拖向另一个错误。

查理被科涅克同盟吓了一跳，在伦巴第没有做好准备。在他驻罗马的大使努力将教皇从列强联盟中分离出来但毫无结果之后，皇帝派雨果-蒙卡达（Hugo Moncada）去找他。这位西班牙冒险家再次出现在他在博尔哈时代就非常熟悉的舞台上。他出身贵族家庭，年轻时随查理七世来到意大利，后来加入了切萨雷-博尔哈的军饷团，亚历山大六世死后，他投靠了康萨尔沃。他曾在摩尔人的海战中服役，并被授予罗德骑士称号，在卡拉布里亚拥有一个司令部。查理任命他为西西里副王，他在西西里因残忍而遭到憎恨。他在一次海战中被法国人俘虏，被换成了蒙莫朗西，出狱后先是回到了西班牙，然后又回到了意大利，并被任命为帝国舰队的海军上将。作为博尔哈学派的一员，他现在可以在罗马大展拳脚了。

蒙卡达于 6 月 17 日抵达，四天前教皇确认了同盟的缔结。他的任务是说服克莱门特同意签订条约，或者同意庞培的计划，庞培答应皇帝通过罗马起义来战胜教皇。庞培奥从会议开始就恨他，而当时的红衣主教，尤其是世袭罔替的红衣主教是如此独立，以至于他只把自己作为教会亲王的尊严看作是个人的东西，要牺牲的是更高的考虑，即普遍的利益和家族的福祉。蒙卡达在梵蒂冈宣称，查理准备将米兰拱手让给斯福尔扎公爵，但为了维护帝国的荣誉，应该先对他做出判决。克莱门特回答说，只有皇帝给予意大利自由并释放弗朗西斯一世的儿子们，他才会放下武器；没有他的盟友，他什么也做不了。蒙卡达提出，如果教皇和意大利其他各邦愿意支付帝国军队的军饷，他就释放米兰。克莱门特随后咨询了法国和英国的使节，拒绝了任何单独的条约。甚至为了和平，皇帝提出将所有争端交由教皇处理的提议也遭到了拒绝。del Vasto 和 Leyva 写给蒙卡达和帝国大使的信被截获，他们在信中描述了他们在米兰的绝望处境，并迫切要求与教皇达成协议。这可能对他毫无意义的决定产生了影响。他想要战争。

6 月 20 日，蒙卡达在塞萨的威胁下离开了梵蒂冈。皇帝的使节忘乎所以，竟然带着一个骑马的傻子跟在他身后，傻子当着众人的面龇牙咧嘴地表达了他的蔑视。使节们告诉皇帝，教皇是他公开的敌人，意大利各族人民也同样是他的敌人，没有军饷的军队的状况让他有理由担心一切。他应该派钱派兵，波旁去伦巴第，兰努瓦去那不勒斯。他们召集了吉贝尔派的亲信，而人民则开始变得焦躁不安。6 月 26 日，蒙卡达离开罗马加入科隆纳。塞萨于 29 日向教皇献上了钦尼耶，但没有进一步的贡品，然后前往马里诺。维斯帕西亚诺和庞培奥已经在教皇的眼皮底下集结部队，而后者则联合了斯特凡-科隆纳和奥尔西尼家族，准备对那不勒斯发动一场战役。6 月 23 日，他给皇帝写了一封拒绝信，在信中他将战争归咎于皇帝过度的权力欲望：他自己拿起武器只是出于对意大利和罗马教廷自由的考虑。信刚发出，他就后悔了；6 月 25 日，他写了一封更温和的信，并命令他的圣座大使卡斯蒂利昂不要发出第一封信。但为时已晚。

战争是在不利的情况下开始的。威尼斯人没有渡过阿达河，瑞士人也没有出现；萨卢佐侯爵本应带来的辅助部队也没有装备。早在 6 月，就有传言说一支雇佣军正在蒂罗尔集结。克莱门特担心被法国宫廷抛弃，副王正在与法国宫廷谈判；他敦促国王不要因为爱子心切而被诱惑与查理达成和解；他敦促亨利八世真正加入同盟。吉贝尔蒂热情洋溢。当人们读到他写给驻外圣座大使的信时，很难相信这些信是一个司铎写的，更让人惊讶的是，发动这样一场伟大战争的手段竟然如此悲惨。

米兰的起义失败了，斯福尔扎有可能在城堡里饿死；帝国军队于 6 月 20 日解除了米兰的武装，赶走了贵族。只有 6 月 24 日威尼斯将军马拉泰斯塔家族攻克洛迪才是联盟的成功。教皇们最终在那里与威尼斯人联合，这支由两万人组成的军队于 7 月 7 日向米兰挺进。但在这座城市里，躺着莱瓦（Leyva）和巴斯托（Vasto）麾下 7000 名饥肠辘辘的西班牙人和兰斯肯特人，身为帝国省督的波旁（Bourbon）刚刚带着新兵和一些军饷从热那亚高高兴兴地赶来，这就是为什么同盟军在 7 月 8 日撤退到马里尼亚诺（Marignano）的原因。随着这次撤退，乌尔比诺公爵开始了他的拖延战术，并从那时起一直坚持了下来。同盟中没有人相互信任。威尼斯怀疑教皇会与皇帝达成协议，教皇则怀疑法国也会这样做；乌尔比诺公爵的态度在吉恰尔迪尼看来是可疑的。在马里尼亚诺，他无所事事地看着米兰的苦难，西班牙人像对待一群奴隶一样对待米兰人民。只有当几千名瑞士人加入他的行列时，他才再次向前推进，但他不敢尝试救援。斯福尔扎于 7 月 24 日投降，撤回洛迪；乌尔比诺随后对征服米兰感到绝望，并让人围攻克雷莫纳。大约在同一时间，针对倾向于皇室的锡耶纳的行动也失败了，教皇在锡耶纳支持潘多尔夫的儿子法比奥-佩特鲁奇（Fabio Petrucci）。在那里，安圭拉家族和皮蒂利亚诺家族的奥尔西尼家族与佛罗伦萨人一起被可耻地赶跑了，教皇的声誉也因此大打折扣。法国宫廷的冷淡让他感到绝望；他派桑加前往那里敦促国王出兵伦巴第，在副王率领西班牙舰队返回之前对那不勒斯下手。

拉丁姆的吉贝尔党人蠢蠢欲动。时隔许久之后，它看到了强大复兴的帝国再次与罗马教皇开战。古老的独立思想觉醒了；然而，罗马资产阶级的精神已经消亡：只有男爵代表罗马的自由思想，而且是为了自私的目的。但反对教皇统治的呼声在罗马始终不绝于耳，每当皇帝召见时，吉贝尔党就会出现。该党的领袖是庞培奥（Pompeo）和他的兄弟马尔切洛（Marcello）、朱利奥（Giulio）、普洛斯彼罗的儿子、丰迪的韦斯帕西亚诺-科隆纳（Vespasiano Colonna），以及阿斯卡尼奥（Ascanio）和他的亲兄弟斯基亚拉（Sciarra）。菲莱蒂诺的切萨雷-加埃塔尼（Cesare Gaëtani）、马里奥-奥尔西尼（Mario Orsini）、詹巴蒂斯塔-孔蒂（Giambattista Conti）和萨尔尼伯爵吉罗拉莫-埃斯托特维尔（Girolamo Estouteville）也加入了进来。他们的计划是通过突袭使克莱门斯失去战斗力。就在蒙卡达与科隆纳安排此事时，塞萨公爵在马里诺病倒了。他被带到罗马，在弥留之际，他怂恿科隆纳用条约欺骗教皇。8 月 18 日，唐-路易斯-德-科尔多瓦死在奎里纳尔河上。

克莱门特已经对这些男爵发出了警告。在与他和解的借口下，他们随后派维斯帕西亚诺前往罗马，并于 8 月 22 日实际签订了以下条约：科隆纳交出阿纳尼和其他地方；他们撤回那不勒斯，在那里他们可以为皇帝服务；教皇赦免他们，并撤销对庞培发布的监察令；任何人不得用战争入侵他们的领地。吉贝尔蒂曾试图阻止他的主人签署这项条约，但徒劳无功，而且这项条约得到了德拉瓦莱红衣主教的担保。他在朋友和敌人面前都变得卑鄙无耻。他相信韦斯帕先的承诺，出于吝啬，他解雇了他的大部分军队，这些军队是他在安圭拉家族和保罗-巴格利奥内伯爵的带领下开赴罗马的。这一切刚刚发生，科隆纳人就占领了阿纳尼，并将部队调往拉蒂尼山脉。他们封锁了所有道路，没有任何关于他们行动的消息传到罗马，这里也不相信谣言。这些男爵在蒙卡达（Moncada）作为皇帝的演说家或塞萨的代理婚姻的陪同下，以快速行军的方式向城市推进。他们有 800 名骑兵和 3000 名步兵，有些枪支是用水牛拖来的。庞培-科隆纳现在可以召回博尼法斯八世的劲敌西亚拉（Sciarra）了。如果克莱门特如他所愿在战斗的骚乱中丧生，他就想强迫自己当选教皇。

9 月 20 日凌晨，科隆纳军从圣约翰门进入罗马。梵蒂冈传出消息说，敌人已经到了论坛，惊恐万分的教皇召开了宗教会议。在那里，瓦莱（Valle）和西博（Cibò）被派往科隆纳、坎佩吉（Camppeggi）和元老宫的切萨里尼（Cesarini），号召人民自卫。但 1526 年的元老宫是什么，罗马人民又是什么，教皇们摧毁了他们的权力和宪法吗？克莱门特故意只任命了一些无关紧要的人担任保卫者。元老是佛罗伦萨的西蒙-托尔纳布奥尼，他是利奥十世的亲戚。罗马人对此毫无反应：他们告诉红衣主教，这不是他们的事，而是教皇的事。尽管克莱门特有很多优点，但他在任何地方都不受欢迎。马可-福斯卡里（Marco Foscari）在 1526 年这样描述他："这位教皇四十八岁，是个聪明人，但下定决心的速度很慢，这也是他行动摇摆不定的原因。他能言善辩，洞察一切，但却非常胆小。在国家大事上，他不允许自己被人控制：他倾听每个人的意见，然后按照自己的意愿行事。他刚正不阿，敬畏上帝：在有三位红衣主教和三位书记员的议院中，他不会做任何有损于他人的事情。当他签署请愿书时，他不再像签署了许多请愿书的利奥教皇那样撤销请愿书。他不会取消福利，也不会吝啬地给予福利；他不会像利奥和其他人那样通过给予恩惠来获得职位。他希望所有事情都有法可依。他不宽恕别人，也不把别人的东西送给别人。但他被称为吝啬鬼。然而，教皇利奥却非常开明，给予和赐予很多东西。这位教皇却恰恰相反，所以罗马才会怨声载道。他施舍很多，但却不受欢迎。他非常节制，不以任何形式的奢华著称。- 他不想看小丑或音乐家，也不去打猎或参加其他娱乐活动。自从担任教皇以来，他只离开过罗马两次去马利亚纳，很少去两英里外的葡萄园。他所有的乐趣都在于与工程师交谈，谈论水利工程"。

教皇的贪婪和他的摄影师红衣主教阿尔梅利努斯的贪婪深深地激怒了人民。红衣主教蓬佩奥曾在主教会议上大声说道，当时正在讨论 camerlengo 提出的某些条件：应该剥掉 Armellinus 的皮，让他的皮在教皇国周围展示一个夸特林，这样就能收集到大量的钱财。公民、官员和神职人员都受到关税和税收的压迫；高利贷和粮食垄断造成了粮食短缺：教皇军团深受憎恨，如果罗马将科隆纳视为解放者，克莱门特也不会抱怨。

庞培的传令官在街上宣称，任何人都不必害怕受到伤害，因为科隆纳只是来把罗马从吝啬的教皇的暴政下解放出来。没有一户人家，没有一个商人的金库关门；科隆纳的到来就像一场奇观一样受到关注。被派往庞培的红衣主教们没有入场，但战争中的人们高呼着自由的口号向特拉斯提韦尔进发！自由！到特拉斯提韦尔去！"的呐喊声，在圣灵门压倒了教皇的小队，然后强行进入博尔戈。和博尼法斯八世一样，教皇起初也想在王座上等待敌人的到来，但吉贝尔蒂和菲利波-斯特罗齐把他一起拉进了圣天使城堡。法国大使纪尧姆-杜贝莱（Guillaume du Bellay）也在那里保住了性命。司铎们带着金银财宝冲进城堡。其他人则躲进了城里。瑞士卫队也被调入圣天使城堡，使梵蒂冈毫无防备。教皇、红衣主教和主教的寝宫很快被暴风雨洗劫一空，圣彼得大教堂也遭到了无情的洗劫。甚至教皇的士兵也混入敌军，高喊 "西班牙"，分享战利品。虽然炮兵从堡垒中开火，但也只能保证新博尔戈的安全。俘虏被抓，赎金被勒索。据估计，几个小时内就抢走了 30 万杜卡特，因为到了傍晚，满载战利品的蜂群在一片混乱中退到了科隆纳附近，几百人就能将其摧毁。

克莱门特看到自己陷入了极不光彩的境地：一群由叛乱的红衣主教率领的附庸暴徒对他进行了无名的侮辱，整个罗马人民几乎都在嘲笑地看着这场袭击！于是，教皇对罗马的统治并没有得到人民的爱戴，在罗马人看来，教皇的统治始终是一种令人憎恶的篡夺。圣天使城堡没有粮草，难以为继，于是葡萄牙国王的侄子兼演说家唐-马蒂诺（Don Martino）在叛军和教皇之间斡旋，达成了和解。当天晚上，克莱门斯让蒙卡达受到了朔姆贝格的召见。与他同在圣阿波斯托利宫殿的庞培想把他挡回去，但狡猾的西班牙人接受了邀请，因为邀请答应了他的要求。他先收留了里多尔菲枢机主教和西博枢机主教作为人质，然后前往圣天使城堡。查理五世的大臣带着掩饰不住的喜悦匍匐在教皇福森的脚下，哀叹完全是无心的掠夺，并劝克莱门特与这位伟大的皇帝和解，因为他并不渴望意大利的统治权，尽管根据古代帝国法律，统治权应属于他。蒙卡达归还了从强盗手中抢来的银质教皇杖和头冠，从他手中这些被亵渎的标志就像是帝国叙任权的象征。教皇痛斥维斯帕西亚诺背信弃义；他没有提到庞培奥的名字，或者只是讽刺性地提到，说他想签订条约，希望与皇帝和解。

9月21日，外国使节被召到圣天使城堡，蒙卡达在此起草了以下协议：克莱门特和查理之间将达成为期四个月的停战协议；教皇将从伦巴第召回军队，从热那亚召回舰队；获得完全赦免的科隆纳将撤回那不勒斯。克莱门特的一位心腹公开宣称，他缔结这一条约的目的是不履行条约。然而，蒙卡达使克莱门特脱离联盟的意图已经达到。他兴高采烈地向皇帝报告了他的政变，皇帝毫不犹豫地建议他被这些事件激怒，以便让教皇感到满意。而科隆纳人则对蒙卡达大发雷霆；他们本想冲进城堡，俘虏教皇，推翻罗马；他们称西班牙人是被收买的叛徒。9月22日上午，在戴克里先浴场扎营的科隆战士满载战利品启程前往格罗塔费拉塔，而蒙卡达则把克拉丽斯-美第奇的丈夫菲利波-斯特罗齐带到了那不勒斯。克莱门特对罗马人的态度深感愤怒。他说，我想让他们明白，罗马不再有教皇意味着什么。他打算暂时离开罗马城。

在查理得知这一条约缔结之前，他于9月17日从格拉纳达向教皇发表了一份宣言：这是他对教皇6月23日信件的回复，也是他对众多指责的称义，尤其是关于他与斯福尔扎的诉讼。他在信中正确地指出克莱门特是战争的始作俑者，并威胁他要召开议会。10 月 6 日，他还写信给红衣主教们：教皇忽视了他拒绝和平建议的职责，只想着对他这个皇帝发动战争、阴谋和叛乱，而他对皇帝亏欠太多了；他们应该召开会议，因为如果不召开会议，路德教运动就无法平息，德国就会脱离天主教会。

这表明，德国宗教改革与克莱门特肆无忌惮地挑起的战争有着多么密切的关系；他自己也促使皇帝将路德教派视为自己的盟友。查理五世会愿意支持《沃尔姆斯敕令》，而支持一个正用叛国罪和暴力的所有武器与他对抗、想要从他手中夺走那不勒斯，并在胜利的情况下夺走帝国王冠的教皇吗？各庄园要求在纽伦堡召开会议；当施佩尔议会于 1526 年 6 月召开时，反教皇党已经占了上风，在斐迪南大公的同意下，帝国做出了一项重要决定，即在召开教会大会之前，各庄园应按照其认为有悖于上帝和帝国威严的方式处理与《沃尔姆斯敕令》有关的问题。因此，虽然皇帝并未真正废除诏书，但宗教改革却由各庄园自行决定，从而在法律上确立了教会的领土分离。一场不屈不挠的革命推翻了德意志帝国的等级制度，使欧洲中心脱离了天主教信仰。与此同时，土耳其人成为匈牙利的统治者，他们于 1526 年 8 月 29 日在莫哈奇杀死了匈牙利年轻的国王路易二世，他是斐迪南大公的妹夫。当教皇看到这场世界大战并考虑到自己的处境时，他一定会感到绝望。在促使欧洲列强与皇帝开战之后，他自己在几个小时内就被一次可耻的攻击解除了武装，他所有的政治家才能就像蜘蛛网一样被撕碎了。皇帝对他进行了轻蔑的打击，提醒他要依靠皇帝的仁慈。如果他这个整个事业的首领退出，意大利的解放也将失败。
