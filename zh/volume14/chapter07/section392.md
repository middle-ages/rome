### 第七章

_1\. 教皇投降 1527年6月5日 罗马和军队的惨状 军队迁往翁布里亚的夏令营。纳尔尼被洗劫一空。克莱门特七世被囚禁在圣天使城堡。罗马的灾难给列强留下的印象。渥西成为反对查理联盟的灵魂人物。1527年7月，劳特累克进入意大利北部。查理五世的行为和政策。教皇的世俗权力是否应继续存在的问题。

瘟疫在罗马肆虐，军队变得肆无忌惮，而来自英国和法国的战争威胁也在上升。因此，菲利贝尔王子希望与教皇和平相处。只有军队不想与条约扯上任何关系。这些掠夺者中的大多数人再次陷入贫困，有些人甚至因疲惫而自杀。他们身边的锡耶纳先知向他们预言了真相："亲爱的伙伴们，时机已到，抢劫并拿走你们找到的一切，但你们必须再把它们吐出来；祭司的物品和战争物品来来回回"。战仆们抢到了三万多弗罗林，现在他们吵着要回自己的报酬。他们要求将教皇作为人质，并劫掠圣天使城堡，他们认为那里堆满了世界的珍宝。奥兰治答应了叛军的要求，作为他们的保证。情况发生了奇妙的变化，掠夺者被他们的掠夺窒息，发现自己的处境和他们在圣天使城堡的敌人一样糟糕。

甚至在联邦军队从伊索拉撤出之前，克莱门特就已经把锡耶纳的兰诺召到罗马，用他的威望支持他，因为这位教皇始终忠于自己；即使是现在，他仍在与朋友和敌人、与同盟军和帝国军队同时进行谈判。当副王于 5 月 28 日抵达时，仆人们威胁要杀了他；他逃出罗马，在蒙卡达城外遇到了阿马尔菲公爵、德尔-瓦斯托、阿拉贡和唐-恩里科-曼里克斯，他们带着几千人从泰拉奇纳开来。他与他们在同一天返回罗马。

他不再受到尊重；菲利贝尔通过全权代表进行谈判。5月31日，王子在圣天使城堡附近中弹受伤，当时城堡仍在向战壕开火，帝国军队威胁要攻入城堡，屠杀教皇和红衣主教。马里奥城堡上已经安装了大炮，可以向这座城堡开火。他们还从城堡大门处埋设了地雷，地雷已经埋到了地基；最糟糕的情况是，他们威胁要炸死教皇和所有红衣主教及牧师。

6 月 1 日，教皇派肖姆贝格前往帝国部队；他还让红衣主教科隆纳应邀加入他的行列--这是他最痛苦的决定。现在，他把最凶猛的敌人比作阿喀琉斯的长矛，既能伤人又能治病，并向他的同情和慷慨求助。他们一起哀悼罗马的苦难，哀悼自己的无知造成了这些苦难。庞培努力减轻这些苦难；此后，他一直支持教皇筹集他必须支付的款项。克莱门特最终不得不决定签订一项条约，因为他再也无法在饥荒和瘟疫肆虐的圣天使城堡坚持八天了。他还看到，除了翁布里亚被联邦军队占领外，他几乎失去了所有的州。威尼斯利用盟友的不幸，重新占领了拉文纳和切尔维亚。西吉斯蒙多-马拉泰斯塔家族进驻里米尼；阿方索向摩德纳推进。就连让教皇操心的佛罗伦萨也在5月16日迫使红衣主教帕塞里尼、希波吕托斯和亚历山德罗撤退。菲利波-斯特罗齐和他的妻子克拉丽斯一直对这些麦地那私生子的起义耿耿于怀，他们在这场动乱中扮演了重要角色。共和国在此成立，著名的皮耶罗之子尼科洛-卡波尼（Niccolò Capponi）于 6 月 1 日被任命为共和国元老。不幸的是，佛罗伦萨人重新与法国国王结盟。

6 月 5 日，克莱门特与帝国特使詹巴托洛梅奥-加蒂纳拉签署了条约：他向查理投降。他要分三次向军队支付 40 万杜卡特，为此他让西潘托和比萨的大主教、皮斯托亚和维罗纳的主教以及他的亲戚雅各布-萨尔维亚蒂（Jacopo Salviati）、洛伦佐-里多尔菲（Lorenzo Ridolfi）和西蒙尼-里卡索利（Simone Ricasoli）作为担保人。他承诺交出奥斯提亚、奇维塔韦基亚、摩德纳、帕尔马和皮亚琴察作为保证，并恢复科隆纳人的权利。他本人将与红衣主教们一起留在圣天使城堡，直到支付完款项，然后自由前往那不勒斯或更远的地方与皇帝缔结和平协定。伦佐-奥尔西尼家族、奥拉齐奥-巴廖内和外国使节则被允许自由离开城堡。

6 月 7 日，教皇卫戍部队离开圣天使城堡，阿拉贡率领由德国人、意大利人和西班牙人组成的三个营进入城堡。这位督军可以吹嘘自己曾在两年内囚禁过法国国王和教皇。其余的瑞士近卫军被允许离开，取而代之的是舍特林（Schertlin）麾下的两百名兰斯昆奈人。"这位上尉写道："在那里，我们发现教皇克莱门特和十二位红衣主教在一个狭窄的大厅里，我们抓住了他。他们非常悲惨，哭得很厉害，我们都发财了"。伦佐率领的四百名意大利人在阿尔贝托-皮奥的陪同下，也带着战功离开了，尽管兰斯昆奈人怀疑他们携带了教皇的珍宝。两人在奇维塔韦基亚登船前往法国。教皇本人的解放取决于条约的履行，而条约在罗马的确立则取决于皇帝的意愿。查理五世在马德里大笔一挥，就结束了教皇国的统治。通过贷款煞费苦心地筹集到了第一笔款项，又用金银铸造了新的货币：甚至十字架、圣杯和其他教会珠宝也被用来支付吵吵嚷嚷的兰斯昆内。教皇的头冠被切利尼熔化了；他似乎把所有的华丽都扔进了坩埚。但按照条约交出要塞却遇到了困难，因为克莱门特本人秘密命令 Behfelshaber 不要交出要塞。帝国军队只占领了奥斯提亚。奥贝托-多利亚拒绝在收到欠款之前离开奇维塔韦基亚港；弗朗切斯科-达-比比埃纳（Francesco da Bibiena）以联盟的名义要求占领奇维塔-卡斯特利亚纳；帕尔马和皮亚琴察都不愿接待帝国使节加蒂纳拉（Gattinara）和洛德隆（Lodron）。阿方索已于 6 月 6 日进入摩德纳，圭多的弟弟洛多维科-兰戈内（Lodovico Rangone）没有阻止他。

罗马的局势非常糟糕：城市里似乎住满了幼虫；由于拉莫特（La Motte）已经启程前往西班牙，所以由唐-佩德罗-拉米雷斯（Don Pedro Ramires）指挥。城里还有24000人，其中一半是德国人。他们总是在反抗，愤怒地索要军饷；他们称自己的队长为背叛他们的叛徒。副王和德尔-瓦斯托不得不逃亡以寻求救赎。菲利贝尔本人太年轻，缺乏经验，无法胜任这个艰难的职位。查理的议员们坚持向罗马派遣一位新的总司令。皇帝将这一职位指派给了费拉拉公爵，但他拒绝担任兵变者的Befehlshaber。

6月17日，费兰特-贡萨加率领骑兵出发前往韦莱特里。由于罗马饥荒严重，瘟疫肆虐，法国军队的人在坎帕尼亚寻找住处。三千多名兰斯昆奈人丧生，其中包括克劳斯-塞登施蒂克（Klaus Seidenstücker）和克里斯托夫-冯-埃伯施泰因（Christoph von Eberstein）伯爵等德高望重的上尉。当情况变得无法忍受时，督军们劝说军队转移到夏令营。他们筹集了一点钱来平息叫嚣者的怒火；三名上尉奉命将人质留在罗卡-迪-帕帕，并于 7 月 10 日启程前往翁布里亚。贝梅尔贝格（Bemelberg）和舍特林（Schertlin）率领德军，菲利贝尔（Philibert）率领 150 名骑兵前往锡耶纳，为皇帝保住这座城市。坎帕尼亚（Campagna）的城镇，教皇曾通过教士信条下令为战士们提供食物和住所，当他们看到这些成群结队的人正在逼近时，都惊恐万分。加泰梅拉塔的故乡，小纳尼城，以绝望的勇气让懦弱的罗马蒙羞。男男女女保卫着城墙，但舍特林和安东尼-冯-费尔德基兴（Antoni von Feldkirchen）率领的德军于 7 月 17 日猛攻城墙，不幸的堡垒在刀光剑影中被夷为平地。托迪镇只是因为乌尔比诺公爵的占领才逃脱了同样的命运。特尔尼支持皇帝，甚至出于旧恨参与了对纳尔尼的破坏。斯波莱托坚壁清野，向阿夸斯帕尔塔的营地送去面包，但同样要支付报酬。德国人回到了纳尔尼，西班牙人回到了特尔尼和阿梅利亚。夏日炎炎、物资短缺和兵变使他们的营地变成了真正的地狱。发烧让他们成百上千地离开。9 月 1 日，当卡斯帕-施韦格勒（Kaspar Schwegler）在纳尔尼召集兰斯肯特人时，他只数到了七千人。

信使们从米兰和罗马赶来。在那里，莱瓦在联盟的压力下，要求立即给予帮助，而从这里传来的消息是，教皇并没有履行条约。走投无路的队长们向那不勒斯的兰努瓦派出了使节，请求他亲自前来，组织一次会议，指挥不屈不挠的人民。他拒绝了，并派德尔-瓦斯托作为谈判代表。翁布里亚各地的情况都很糟糕，包括在乌尔比诺和萨卢佐麾下的联邦军营地，他们正在庞特努沃附近掩护佩鲁贾。他们忍饥挨饿，奋起反抗，在穷乡僻壤大肆掠夺。公爵本人与其他指挥官发生了争吵；不仅弗朗西斯一世不信任他，就连吉奇亚尔迪尼（Guicciardini）策反的威尼斯人也不信任他。威权统治者威胁要把他在威尼斯的妻子和儿子当作棋子，直到皮萨尼检察官恢复关系。佩鲁贾陷入了无政府状态。乌尔比诺的门徒奥拉齐奥-巴格利奥内（Orazio Baglione）在那里杀害了他的堂兄詹蒂莱（Gentile）和他的其他家人。帝国军队在特尔尼地区攻占了卡梅里诺，雇佣军在那里推进，而西班牙人和意大利人则在阿尔维亚诺（Alviano）和卡斯蒂里奥内德拉特维里纳（Castiglione della Teverina）附近扎营。

克莱门特发现自己就像一个被活埋的人，在可怕的圣天使城堡里，在野蛮的士兵中间，在夏日猛烈的寂静中，过着无助和被遗弃的可怕日日夜夜。他和红衣主教们一起住在城堡所谓的马西奥（Maschio）里；西班牙人住在下面。他们对教皇戒备森严，几乎不允许任何人见他。他身上连十块银币都没有。当时有两位红衣主教死在了城堡里；高利贷者阿尔梅里尼死于失去财富的痛苦，才华横溢的埃尔科莱-兰戈内死于劳役或瘟疫。

在地牢里，教皇致信查理五世和欧洲各国，要求释放他。皇帝们提出带他去加埃塔，但他拒绝了。随后，他们允许他将亚历山德罗-法尔内塞派往马德里。这位后来成为保罗三世的红衣主教离开了圣天使城堡，但他拒绝了他的使命，这促使教皇将其委托给法国宫廷的萨尔维亚蒂红衣主教。但即使是后者也认为不宜将自己置于皇帝的权力之下；他将谈判委托给了驻西班牙大使，不幸的卡斯蒂利昂，他对罗马的灾难深感悲痛。教皇国，实际上是教会本身的政府已经停止了；罗马以外的红衣主教们被分散了；在西博的领导下，威尼斯希望将他们联合到博洛尼亚，但这并没有实现。

与此同时，罗马被攻陷对世界产生了各种影响。皇帝的支持者们欢欣鼓舞地迎接教皇的倒台；路德教派欢欣鼓舞，因为正如预言中早已预言的那样，巴别塔已经陷落；不仅是宗教改革的秘密朋友，如西班牙人胡安-瓦尔德斯（Juan Valdez），就连虔诚的天主教徒也从罗马的不幸中看到了上天的审判。英国和法国对教皇的哀叹不及对皇帝的恐惧。圣座大使甘巴拉和萨尔维亚蒂与这些大国积极合作，他们已于4月30日在威斯敏斯特缔结了条约，5月29日他们又为释放教皇续签了条约，之后沃尔西在夏天前往法国。这位英国政治家对缔结同盟充满热情。他向国王表明，推翻教皇会危及他自己的事务，尤其是他与阿拉贡的凯瑟琳的婚事，这是他所希望的；他想把不在罗马的红衣主教们联合到阿维尼翁，将拯救教会的大权掌握在自己手中，他希望成为其中的主角。教皇可能被转移到西班牙，教皇职位也可能在那里成为西班牙人，这种想法让他感到恐惧。

联盟，或者说是法国，已经做好了战争的准备，1527 年 7 月底，劳特累克勋爵奥代-德-福瓦（Odet de Foix）已经越过阿尔卑斯山向意大利挺进。这位英勇的内廷大臣并不情愿在他的战败之地担任最高指挥官。在那里，他从未快乐过：在拉文纳受重伤，在比科卡战败，被赶出伦巴第。他的兄弟在帕维亚阵亡。他自己也再也见不到法国了。威尼斯人也向米兰发起了进攻，于是战争又在这个国家爆发了，对于一贫如洗的皇帝的省督莱瓦来说，保卫这个国家是一项艰巨的任务。

查理本人直到 6 月底才得知罗马发生的灾难。他对罗马城遭到的可怕劫掠感到震惊和羞愧；他禁止为儿子菲利普举行庆生仪式，并公开表示哀悼，但暗地里却称赞他的好运气，这也让教皇落入了他的手中。他没有急于释放他。他只是在 7 月 26 日写信给罗马人，哀叹他们的不幸，并承诺恢复罗马名字的荣誉和荣耀。他派基诺内斯和他的侍从唐-佩德罗-德-韦尔去见教皇的时间明显晚了。8 月 2 日，他写信给英格兰国王，称教皇是一切不幸的根源，因为他怂恿弗朗西斯一世破坏和平，挑起了新的联盟和战争。由于教皇竭力反对那不勒斯，帝国军队出发去拯救这个王国，并违背他和督军们的意愿，取道罗马。他对所犯下的暴行感到悲哀，虽然他对此毫无责任，但他认识到这是上帝正义的审判，上帝要惩罚有罪者的不义。

皇帝和教皇面临着历史上罕见的危机。当时，世俗权力和教会权力之间的关系可能会发生根本性的变化。教皇的豁免权起源于查理曼大帝，对意大利、帝国乃至教会本身都产生了严重的腐蚀作用，现在难道不是彻底废除这种豁免权的时候了吗？皇帝似乎只要颁布一项法令，就能再次使罗马成为帝国的首都，按照宗教改革的要求让教皇回到拉特兰担任主教，并最终通过大公会议改革教会。欧洲教会财产的世俗化带来了巨大的动荡，教皇统治或教皇国的垮台可能也导致教会解体为宗主国和地区教会，它们只能在联邦宪法中寻求统一。宪法。

皇帝和他的仆人们不得不面对这种性质的问题。6 月 8 日，一位不愿透露姓名的人从罗马写信给他："我们希望陛下能给我们下达明确的命令，这样我们就能知道您今后打算如何管理罗马城，以及是否应在罗马城保留使徒教廷的形式。我不会隐瞒陛下臣下的意见，他们认为罗马教廷不应该被完全废除；因为法国国王可能会立即在他的领土上设立宗主教，拒绝服从使徒教廷，英格兰和其他君主也会这样做。因此，国王陛下的臣子们认为，教廷的地位应该保持在国王陛下可以随时处置和指挥的低位。

5月31日，现任波希米亚和匈牙利国王的斐迪南大公从布拉格向他的弟弟报告了罗马被攻陷的消息，并劝诫他在一切和平解决之前不要释放教皇，否则他就会像弗朗西斯一世那样背叛教皇；最后，皇帝也应该记住枢密院。这正是同盟国所担心的，查理会试图强迫被囚禁的教皇召开大公会议，以帝国的武力改革教会，从而使自己成为教会的真正领袖。因此，英国和法国同意，只要教皇 "像农奴一样受制于不信神的敌人"，就不承认任何议会。吉贝尔派的思想由于得到了德国宗教改革的支持而更加强烈地觉醒了。如果但丁关于世界君主制的梦想能够成为现实，那么它可能就在那时实现了。虽然谨慎的兰诺建议皇帝求和并释放教皇，但他在信中说，现在是时候考虑通过议会改革教会纪律了。根据首相加蒂纳拉的建议，甚至佛罗伦萨和博洛尼亚也应该与帝国合并。西班牙的政治家们赞成在意大利建立全权，但要释放教皇。西班牙的教士们热情地表示支持教皇。

查理五世谨慎而冷酷，暂不做出判断；他先是想等待韦尔的报告，8 月他将报告发给了副王，然后又发给了教皇，命令中明确表达了他的想法。他宣称罗马的陷落是上天的安排，很可能会带来世界的和平和教会的改革；他希望教皇返回西班牙；如果不能在不使用暴力的情况下做到这一点，他打算通过副王让教皇重返罗马教廷；但这一自由必须只涉及精神职务，甚至在他获得这一自由之前，他必须给予皇帝一切安全保障，以防反复欺骗。最后，他想迫使教皇召开大公会议。
