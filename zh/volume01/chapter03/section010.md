_2\. 408年，阿拉里克向罗马发起进攻，他的恶魔。罗马陷落的预兆。第一次围攻。罗马公使馆。罗马的托斯卡纳异教。围城被收买。霍诺留拒绝议和。409 年，阿拉里克第二次来到罗马。 反皇帝阿塔卢斯。阿拉里克向拉文纳进军。他第三次在罗马扎营_。

哥特国王没有什么理由哀悼他昔日的敌人可耻的死亡，即使他曾希望作为朋友与他分享东方和西方。这个曾把他从意大利赶回罗马并迫使他沉寂了五年的强大对手已经不复存在，这让阿拉里克立刻成为了罗马命运的主宰。现在，他决定再次碰碰运气，于是从伊利里亚出发前往意大利北部，斯蒂利科朋友的复仇之心和阿里安人的名声邀请了他，而拒绝进贡则给了他毁约的借口。传说中，一个恶魔不断煽动这位已经摧毁了美丽的希腊的可怕征服者也要进攻罗马。一位虔诚的僧侣急忙跑到这位手持武器的野蛮国王面前，恳求他放过这座城市，不要再做他正在计划的滔天大罪；但恶魔回答说："我并不是自愿的，是一个生命在折磨我，驱使我躁动不安，并向我呼喊：起来，摧毁罗马！" 杰罗姆和奥古斯丁将阿拉里克的恶魔解释为神灵的一种冲动，神灵要惩罚这个堕落的世界之都的罪孽；谁又能不从驱使哥特国王敢于做出前所未闻的行为的历史力量中认出恶魔呢？罗马从未被敌人征服过，征服罗马的念头在人类的想象力中一定是匪夷所思的，同时它又对野心勃勃的野蛮人施展了不可抗拒的魔力。阿拉里克的军事行动标志着日耳曼民族征服衰弱的意大利的开始；也正是从那时起，日耳曼部落第一次摆脱了漫无目的的自然力量的不稳定生活，进入了文明合法发展的圈子；然而，这一事实却导致了罗马帝国的灭亡。起初，阿拉里克可以希望利用他对罗马的占有来更深层次地混淆意大利的政治局势，但肯定不能使自己成为这里的永久统治者，因为他自己在一个国家或民族中都没有靠山，也没有皮鲁斯和汉尼拔曾给予他如此强大力量的那种助手和关系。

罗马城仍然是所有文明的化身和人类的象征。即使罗马不再是皇帝和国家最高权力机构的所在地，它仍然是帝国的理想中心。它的名字为所有人所尊崇，其本身就是一种力量。罗马 "和 "罗马人 "表达了世界秩序。尽管这座城市通过可怕的战争逐渐征服了许多国家，但它并没有被憎恨，因为他们所有人，甚至是野蛮人，都自豪地称自己为罗马公民。只有狂热的基督徒才会憎恨这座显赫的城市，因为它是偶像崇拜的发源地；《启示录》预言了那座曾让万国沉浸在欲望之酒中的大巴别城的覆灭。安东尼时代在亚历山大写成的《西庇阿书》宣称，在反基督者即将出现之后，这座城市将会毁灭，反基督者被想象成从世界末日归来的怪物，基督徒的屠夫和母亲的谋杀者尼禄。届时，罗马的金字招牌将失去它的力量；但终有一天，通过基督，罗马和光荣的拉丁人的力量将上升到新的高度。与维吉尔相反，教父良（Tertullian）和赛普里安努斯（Cyprianus）断言，罗马帝国与之前的波斯人、玛德人、埃及人和马其顿人的统治一样，时间有限，即将终结。传说君士坦丁在神谕的指引下，在博斯普鲁斯海峡上建造了新罗马，因为旧罗马注定要遭到无可挽回的毁灭。

公元四世纪，萨尔马特人和日耳曼人入侵帝国边境，使这些预言更有可能成真，而罗马城将落入野蛮人之手的笃定预期又散布着恶魔般的恐怖，尤其是基督徒，他们长期以来一直相信野蛮人会像尼尼微或耶路撒冷一样，用火摧毁罗马。难怪在君士坦丁时代，已经可以听到一种声音，宣称罗马的灭亡就是世界的末日。"演说家拉克坦提乌斯说："当这个世界之首像西庇阿预言的那样倒下并化为火焰时，谁还会怀疑人类和世界的末日已经到来？因为这是一座仍在支撑着世界的城市，我们必须热切地向天神祈祷，如果他的旨意能够延缓，那个被诅咒的暴君可能不会比我们所相信的更早出现，他将犯下这一暴行，并放出那束光，而世界本身也将在他出现时灭亡。

随着哥特人首次出现在意大利，这种恐惧已经有了一定的形式。克劳迪安关于哥特战争的诗歌中就有一些深沉、幽怨的忧郁色彩，这种忧郁唤起了人们对不可避免的厄运的预感。"起来吧，"诗人喊道，"可敬的母亲，让你自己从对年龄的卑微恐惧中解脱出来吧，城市啊，在年龄上与极地相等。只有到那时，当顿河冲刷埃及，尼罗河淹没马约提亚沼泽时，钢铁般的拉克西斯才会把她的权利交给你！" 但这些豪言壮语只是惊恐的叹息。阿拉里克一骚动，罗马就陷入了恐慌，克劳迪安本人对此进行了生动的描述。402 年，哥特人的国王刚刚到达波河，罗马人就听到了野蛮人战马的嘶鸣。然后，他们收拾行李，准备逃往科西嘉岛、撒丁岛和希腊群岛；他们怀着迷信的恐惧望着漆黑的月亮，互相诉说着可怕的彗星、梦中的影像和可怕的神迹，而古老的解释，即罗穆卢斯的十二只秃鹫预言了这座城市十二个世纪的存在，现在似乎要成真了。当时，斯蒂利科拯救了罗马，但他已不在人世，而霍诺留的将军图尔皮里奥、瓦拉内斯和维吉兰提乌斯都无法取代他的天才。拉文纳的宫廷拒绝了阿拉里克的和平建议和他对金钱的微薄要求，他们骄傲地意识到了阿拉里克的威严，却没有意识到帝国的力量。他在亚得里亚海沼泽地感到安全，于是任由罗马自生自灭。这座城市不再是帝国权力的所在地，它甚至不会受到帝国征服的影响；"因为罗马是皇帝所在的地方"。

哥特人的国王在克雷莫纳渡过了波河；他肆意蹂躏这片土地，经博洛尼亚到里米尼，沿着弗拉米尼大道一路前行，没有遇到任何抵抗。然后，他带着密密麻麻的斯基泰骑兵和大批哥特步兵包围了罗马城墙，他们渴望鲜血和战利品。

阿拉里克没有试图攻城，他将罗马城团团围住，在每个大门前都布置了军队，切断了陆地和海上的所有补给，等待着他的措施产生不可避免的效果。罗马人把自己关在奥勒良新筑起的城墙内，试图用一位杰出女性的血淋淋头颅来吓退敌人。塞雷娜是斯蒂利科的不幸遗孀，狄奥多西皇帝的侄女，因为她是狄奥多西皇帝弟弟霍诺留的女儿，她住在罗马的宫殿里，宦官们把她被皇帝拒绝的女儿赛曼提亚带回了宫殿，因为皇帝在她的姐姐玛丽亚去世后，二婚娶了这个还没长大的女孩。元老院怀疑塞雷娜出于报复召来了哥特人，并与他们串通一气。元老院下令将她处死。普拉西迪娅公主是霍诺留的妹妹，也是塞雷娜通过狄奥多西娶的情妇，当时年仅 21 岁，她同意了这场悲惨的谋杀。她当时住在帕拉提姆宫的寝宫里，与此同时，罗马还有其他王室女子住在她的遗孀位子上，她们是曾经是格拉蒂安皇帝妻子的莱塔和她年迈的母亲皮萨梅纳。然而，元老院误以为哥特人会在塞雷娜死后放弃入城的希望而离开。饥荒和瘟疫肆虐了这座城市。那些高贵的公主们变卖了自己的珠宝，以缓解人民的困境。

走投无路的元老院终于派西班牙人巴西尔和帝国公证人约翰的护民官前往哥特人的营地谈判议和。代表们告诉阿拉里克，他们受到了不明智的指示：习惯于战争的伟大罗马人民已经做好了准备，如果他用不合理的条件把他们逼到极端，他们就会进行一场绝望的战斗。"草，"蛮族国王回答这些穿着破旧的罗马英雄紫袍的悲惨演员，"越厚的草越容易被割掉"。作为离开的回报，他要求交出所有珍贵的黄金和装备，以及所有蛮族出身的奴隶。当一位使者问他打算把什么留在罗马时，他简短地回答道："赤裸裸的生命！"

在这种绝望的情况下，古罗马人求助于被推翻的神的奥秘。托斯卡纳的老人对古代的占卜术、故乡的技艺仍然很有经验，他们也许是被罗马城的异教长官庞培亚努斯（Pompeianus）召唤来的，他们提出只要元老院按照古代习俗在元老宫和其他神庙举行隆重的祭祀活动，他们就会召唤雷电，使罗马摆脱敌人的困境。异教历史学家佐西穆斯（Zosimus）讲述了这一提议，他甚至声称，连主教英诺森（Innocent）都授权（尽管没有批准）这些占卜师的计划，但他还是诚恳地承认，事实证明异教已经彻底死亡，因为没有人敢参加祭祀；魔术师们被遣送回家，转而使用更有效的方法。

在第二次公使来访后，阿拉里克宣布自己对 5000 磅黄金和 30000 磅白银的赎金感到满意；他还索要 3000 张浸泡在紫色液体中的兽皮、4000 件丝绸盔甲和 3000 磅胡椒粉，这简直是野蛮人的奢侈要求。为了筹集这笔巨额现金，强制征税是不够的；因此，封存的神庙珍宝被没收；金银柱子被熔化，这证明罗马还有足够的珍贵雕像。在这些被送进熔炉的祭品中，佐西穆斯最惋惜的是维尔特斯（Virtus）民族雕像，罗马人最后的英勇和美德也随之消逝了。

阿拉里克一收到这笔钱，就给饥肠辘辘的罗马人开了几扇门，允许他们外出，还允许他们在港口集市三天，并提供补给。他自己则动身前往托斯卡纳安营扎寨，并带走了不少于 4 万名蛮族奴隶，这些奴隶是陆续从城市和主人的宫殿逃到他这里来的。他在等待拉文纳宫廷的答复，元老院的使节代表他去那里向皇帝提出和平与结盟的请求。尽管阿拉里克的要求并不过分，但霍诺留乌斯或他的大臣奥林匹乌斯拒绝了这些建议。他希望得到每年一笔的黄金和谷物、诺里库姆（Noricum）、达尔马提亚（Dalmatia）和威尼西亚（Venetias），以及帝国军队将军的尊严。

在罗马派往皇帝的使者中，有伊诺森提乌斯主教；但他的劝告和其他使者的恳求和陈述都没有给人留下任何印象，他们把罗马的困境描绘得黯淡无光，阿拉里克很快就在里米尼（新任大臣约维乌斯邀请他的地方）得知，霍诺留轻蔑地拒绝给予他帝国将军的头衔。现在，他第二次向罗马进军，但他首先派意大利主教们去找霍诺留，告诉他如果他坚持战争，他将不可避免地放弃这座令人尊敬的城市，这座城市一千多年来一直是世界之首，拥有宏伟的纪念碑，但现在却被野蛮人的火焰和掠夺所吞噬。他降低了自己的要求；他本人放弃了帝国的一切尊严；他希望得到诺里库姆（Noricum），得到一定数量的粮食和一个友好的联盟，使他能够将武器对准皇帝的敌人。然而，大臣们宣称，他们曾以皇帝的名义发誓，绝不与蛮族媾和，妒忌上帝比妒忌皇帝更好。

所有人，甚至是最大胆的野蛮人，都对帝国的权威心存敬畏，但哥特国王的温和态度却不足以解释这一点。征服者从来不会因敬畏而退缩，只会因恐惧而退缩。阿拉里克一定认为，依靠他分散的、补给不足的军队，他宁愿有限地拥有帝国的一个行省，通过公开条约来保证，也不愿拥有对城市的短暂权力。当他再次出现在罗马城前时，他意识到，对于他那些不擅长攻城的战士来说，要想突破或翻越奥勒良的城墙肯定是一件绝望的事情。因此，他决定包围并饿死这座城市。为此，他夺取了罗马位于台伯河右河口的重要港口波图斯（Portus），从而掌握了该城的所有物资。

他的下一个计划是在罗马发动一场政治动乱，这并不是因为他真的希望废黜霍诺留，而是因为他需要一个傀儡皇帝来让自己的要求看起来得到合法的承认。备受煎熬的罗马人同意了哥特代理人的建议，放弃了霍诺留，承认阿拉里克为罗马的保护者。一场叛乱迫使元老院与哥特国王谈判。在他的建议下，皇帝被宣布废黜，城市长官阿塔卢斯被提升到凯撒宫殿的皇帝宝座上。哥特国王并不打算亲自登上这个宝座；相反，他满足于通过元老院和人民的决定，推翻合法王朝，让罗马人取而代之成为皇帝，然后他亲自向罗马人致敬。他立即从阿塔路斯那里得到了帝国总司令的尊荣，而他的姐妹丈夫哥特人阿瑟尔夫则被任命为骑兵长官。

罗马暴民向阿塔路斯表示祝贺，为任命特尔图卢斯为执政官而喝彩，并希望能有马戏表演和丰厚的礼物。在这场骚乱中，只有阿尼西亚家族保持了冷漠，而这也是人们所不齿的。这个强大的家族是罗马基督教贵族的首领，他们担心异教的反应是有道理的。因为阿塔路斯本人也是异教徒；虽然他为了信奉阿里乌派基督教的哥特人，允许自己接受他们的一位主教的洗礼，但他不仅允许重新开放旧的神庙，而且自己还在钱币上继续使用带有基督单字的拉巴鲁姆，并用长矛和罗马维多利亚的形象代替十字架标志。

哥特军队与阿塔路斯的军队联合起来，开始围攻拉文纳。当哥特人出现在拉文纳城墙前时，霍诺留皇帝立刻失去了勇气。他立即进行了谈判；他甚至提出接受阿塔路斯为共同统治者。但这一提议遭到了拒绝。如果这位反皇帝以他的洞察力和力量支持阿拉里克的意图，他很可能会取得巨大的成功；但这位罗马的前省长不听他的创造者的建议；他蔑视野蛮人；他也没有采取任何行动从帝国军队手中夺回非洲，而哥特人的国王想要给他军队去征服非洲。他是个无能之辈，既不是政治家，也不是将军。然而，他的大臣约维乌斯（Jovius）的叛变坚定了霍诺留逃往君士坦丁堡的决心，直到拉文纳港突然出现的六支队伍给了他新的勇气。这座城市的力量挫败了阿拉里克的所有努力，而他也顺便继续与皇帝谈判。他只是把他的造物阿塔路斯当作一个有效的妖怪；最后，他在里米尼又从他那里夺走了紫色和头饰，把这些徽章送到了拉文纳，并把这位前皇帝和他的儿子安佩利乌斯当作俘虏关在自己的营地里。然而，拉文纳的和平谈判旷日持久。

大胆的哥特酋长、阿拉里克的死敌萨鲁斯出现了，他突然袭击了伏击他的部队的阿瑟尔夫，最后他进入了拉文纳的城墙，这一切让哥特国王相信他被故意欺骗了。因此，他拔营再次向罗马进军。如果说之前他是出于某种考虑而放过了帝国的首都，那么现在他决定用武力夺取它，并将其视为战利品。狼狈不堪的霍诺留投降了，因为他确信敌人已经撤出了拉文纳。

哥特人和匈奴人现在在罗马前方的高地上安营扎寨，国王曾许诺要掠夺这些地方。在梵蒂冈境内，这些野蛮人战士看到了使徒彼得的大教堂，以及大教堂外台伯河畔的圣保罗大教堂；首领们告诉他们，他们必须把注意力从这些装满金银财宝的圣殿上转移开；但是，当他们翻越奥勒良城墙时，城墙周围其他一切华丽的东西都应该是他们的。他们贪婪的目光凝视着这些建筑奇迹，这是一个由宫殿和街道组成的巨大世界，其中耸立着方尖碑和以镀金雕像加冕的独立圆柱； 他们看到神庙巍峨地排成长队，剧院和马戏团在壮观的曲线上拔地而起，温泉浴场的大厅或朦胧或昏暗，宽阔的穹顶在阳光下闪闪发光，最后是巨大的贵族宫殿，这些宫殿就像城中的许多富丽堂皇的城市，他们知道那里精致的寝室里镶满了珠宝，住着罗马繁花似锦的女人。他们野蛮的想象力中充斥着关于这座城市宝藏的传说，这些传说都是从伊斯特和马奥特沼泽的旅行者口中听到的，而对于他们兽性的贪婪来说，这是西庇阿、卡托、凯撒和特拉扬的城市，是他们赋予了人类文明的法则，这种不可触及的想法不会给他们带来更多的魅力。他们只知道罗马用武力征服了世界，她积聚了大量财富，这些尚未被任何敌人掠夺的宝藏将作为战利品落入他们手中。他们人多势众，希望把珍珠和宝石像谷物一样丈量，把金瓶和华丽的绣花长袍装满马车。阿拉里克军队中蓬头垢面、身披兽皮、手持弓箭的萨尔马特人，以及身披矿石铠甲、生性好战的强壮哥特人，甚至无法理解罗马艺术的奢华；他们只是暗暗觉得，在罗马，他们将像陷入一场所有感官的饕餮盛宴，他们知道，罗马人要么成了无能为力的美食家，要么成了僧侣般的苦行僧。
