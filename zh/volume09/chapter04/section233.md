### 第四章

_1\. 胡戈里努斯-孔蒂成为教皇格里高利九世，他向皇帝提出十字军东征的要求。1227年皇帝的出发、再出发和开除教籍。 皇帝和教皇的宣言。帝国派将格里高利九世逐出罗马。皇帝的十字军东征。1229年教皇入侵阿普利亚。 皇帝回归，教皇出逃_。

一位最爱好和平的教皇由一位具有强烈热情和坚定意志的人继位。胡戈里努斯，红衣主教，奥斯蒂亚主教，1227年3月19日在圣格雷戈里的七神大殿当选，并被宣布为格雷戈里九世，他出身于阿纳尼的孔蒂家族，与无辜有三代血缘关系。他经历了多位教皇的统治，年轻时吸收了亚历山大三世时期重大事件的印象。他或许更年轻的亲戚英诺森任命他为奥斯提亚的主教，并在那里修建了新的城墙。多年来，他一直在意大利和德意志指导教会事务，在那里，他作为使节主持了王位之争的艰难谈判。我们看到他是小修道士会的第一位保护人。圣方济各和圣多米尼克的火焰在他的精神中燃烧，增强了他与生俱来的性格力量，使他在面对一切反对时都能不屈不挠，勇往直前。他是一位能言善辩的老人，举止纯正，精通两种权利，对信仰充满热忱，他的外表和身材都呈现出宗主教的风范，而他坚韧不拔的记忆力使他的年龄不那么引人注目。

胡戈里努斯曾对霍诺留的纵容软弱心怀不满，当他登上罗马教廷时，可以肯定的是，他不会效仿前任的忍耐，而这正是红衣主教们推举他的原因。3月21日，他在圣彼得大教堂举行了祝圣仪式。罗马人民欢呼雀跃地陪同他前往圣彼得大教堂，在庄严的队伍中，元老院议员和市政长官都被认出。祝圣后的第三天，格里高利九世将自己的升迁通知了腓特烈，同时邀请他参加十字军东征。皇帝在加冕日从格里高利手中接过十字架的正是他本人。腓特烈随即宣布准备出发，许多十字军战士，大部分是日耳曼人，已经聚集在布林迪西，他们在热火朝天的季节里等待着出发的信号。一场类似瘟疫的流行病袭击了这些人，成千上万的人被带走了。最后，墨西拿的皇帝终于到了，可能从来没有一个十字军战士比死在叙利亚的那个巴巴罗萨的孙子更不情愿地登船了。

9 月 8 日，当他真正从布林迪西启航时，所有教堂里都响起了泰德音调，教皇的祈祷声伴随着他漂洋过海。但几天后，一个奇怪的传言传开了，说皇帝掉头返回陆地，推迟了十字军东征。事实的确如此。腓特烈在海上病倒了，不知是真病还是假病，他让桨帆船掉头，在奥特朗托上岸，图林根领地伯爵，也就是圣伊丽莎白的丈夫，很快就病倒了。当教皇收到确认和道歉信时，他怒不可遏。他不想听任何理由或承诺。9 月 29 日，他身着盛装登上阿纳尼大教堂的讲坛，根据《卡西诺条约》宣布了对皇帝的禁令，而高高祭坛两侧一字排开的司铎们则将点燃的蜡烛扔到了地上。在霍诺留无力的威胁之后，真正的霹雳落了下来。

在一些人看来，格里高利九世的迅速果敢是伟大的，而在另一些人看来，这不过是激情的迸发，可以原谅的是耐心的耗尽，而不是审慎。这位老人是那种容不得半点瑕疵的人，他挑战的人在他眼中只是教会的恶意敌人，他利用了霍诺留的弱点。他冲破了不明确的、因此也是难以忍受的环境，以公开的战争战胜了腐朽的和平。面具掉了下来。基督教的两位首脑通过他们的宣言向世人宣告，世袭的宿敌之间不可能和睦相处。在教会眼中，腓特烈真正的罪行是一再拖延十字军东征吗？毫无疑问不是，而是他的权力变得过于强大，西西里岛与帝国的联合，他对意大利北部和中部吉贝林城市的统治，这些都威胁到了伦巴第联盟。从来没有哪位皇帝像腓特烈二世这位不受限制的西西里国王一样，在意大利拥有如此之多、如此牢固的实际统治基础。从那时起，铲除霍亨斯陶芬势力就一直是教皇政治家的任务，他以令人钦佩的坚定性完成了这一任务。

在给所有主教的通函中，格里高利将腓特烈的忘恩负义描绘得淋漓尽致，在世人面前给他打上了无情的烙印。他先是很好地称义了自己从十字军东征中皈依的行为，然后又向诸王发表了一份宣言。在这封著名的信中，世俗权力第一次对无辜的教皇提出了抗议。皇帝清楚地意识到，作为世俗权力的代表，他本人必须捍卫世俗权力，抵御罗马专制主义的威胁。他以不幸的图卢兹伯爵和英格兰国王为例，向王公贵族和人民展示了等待他们的是什么，并无情地勾勒出一幅教庭世俗化和教皇权力欲望的图景。国家的最高权力使教会的缺陷成为全世界讨论的话题，基督世界的皇帝似乎证实了异教徒关于教皇制度非教会性质的观点。著名法学家贝内文托的罗弗雷德（Roffred of Benevento）也将帝国宣言带到了罗马，在元老宫上公开宣读，引起了一片叫好声。罗马人认为教会与帝国之间的斗争对他们自己的地位非常有利，因此立即成立了一个帝国党。格里高利九世在罗马城采取了严厉的行动；他拆毁了拉特兰城的一些贵族塔楼，而他保护的维泰博之争也使罗马城的社会变得更加混乱。异教徒也加入了政治派别的行列，他们越来越大胆地在葬礼的火堆间四处抬头，甚至在罗马也是如此。有一个例子可以说明这座城市仍然处于无政府状态。当教皇夏天在拉丁姆西时，贵族和市民，甚至僧侣和神职人员，都敢在梵蒂冈设立一个冒牌教皇助理司祭，让他为路过布林迪西的十字军开脱他们的誓言，以换取金钱。这种厚颜无耻的游戏在圣彼得大教堂的门廊里公开进行了六个星期，直到元老制止了它。

高贵的罗马人从腓特烈那里拿走了黄金；甚至理查德-孔蒂的儿子波利的约翰也出现在他的营地里。皇帝邀请这些大人物来到坎帕尼亚，引诱弗兰吉帕尼人将他们的货物卖给他，包括城中的堡垒，这些堡垒都是教皇的封地，然后再从他手中拿回去，从而承认自己是帝国的附庸。对腓特烈来说，重要的是在罗马为自己建立一个党派，在这里与教皇为敌，并将斗兽场置于他的控制之下。他的措施产生了起义的效果。1228年的濯足节周四，格里高利再次宣布了对皇帝的禁令；复活节周一，当他在圣彼得教堂做弥撒，向人们激烈抨击腓特烈时，吉贝尔人愤怒地喊叫着打断了他的话；他们在祭坛前对他大加辱骂，并将他赶出了圣殿。全城举起了武器，而逃亡中的教皇则在忠诚的圭尔夫军队的护送下匆忙赶往友好的维泰博城。罗马人以军队的力量追赶他；他们把他从那里赶到了里耶蒂和佩鲁贾，通过野蛮地毁坏田地来冷却他们对维泰博的仇恨，并征服了有争议的里斯潘帕诺城堡。格里高利九世在流放途中对迫害他的人施了咒语，然后在痛苦中等待着他的归来。

在此期间，皇帝准备真正实施他的十字军东征。他这样做不仅使教皇关于他从未认真考虑过十字军东征的说法失效，还让教皇相当尴尬。在当时的情况下，腓特烈的十字军东征更像是一次外交大手笔，因为教皇在他的道路上设置了最大的障碍，这让许多虔诚的教徒感到困惑。西方皇帝是为了当时教会最神圣的目的而踏上征途的，但却受到了教会的迷惑。当他于1228年6月28日从布林迪西启程时，她在他身后痛苦地喊道，他不是作为十字军去耶路撒冷，而是作为一个 "海盗"。他没有得到她的祝福，反而受到了她的诅咒；他自己在救世主的坟墓前也受到了诅咒。同一位教皇把腓特烈描绘成不进行十字军东征和进行十字军东征的罪犯。如果格里高利九世在敌人真的前往耶路撒冷时解除了对他的禁令，他就会战胜自己和敌人，光荣地站在世人面前。这种明显的矛盾削弱了人们对教皇们解放耶路撒冷的真诚热情的信心，也摧毁了两个世纪以来的妄想；至少从那时起，德国再也无法被说服进行这些旅行了。

前康拉德公爵的儿子雷纳德在皇帝不在期间被任命为意大利的助理司祭，他立即通过进攻斯波莱托来挑衅教皇，而格里高利九世也同样急于利用腓特烈的撤职来使阿普利亚臣服于教会。甚至在皇帝离开之前，他就已经招募了一支军队；现在，他呼吁伦巴第、西班牙、法国和英国，甚至整个欧洲，向他提供教会什一税或辅助部队，人们听到了十字架对皇帝的宣扬，而皇帝本人却打着十字架的旗帜去与异教徒作战；他们看到以教皇名义组建的军队入侵了不在的腓特烈的土地，而根据国际法和教会法，这些土地作为十字军的财产本应被视为不可侵犯的。教皇十字军的旗帜上印有圣彼得的钥匙，由皇帝的岳父布里安的约翰、红衣主教科隆纳和教皇的神甫助手阿纳尼的潘杜夫指挥。其中一些部队进入了雷纳德率领撒拉逊人和阿普利亚人入侵的马尔凯地区，潘杜夫则于 1229 年 1 月 18 日通过利里斯进入了坎帕尼亚。在这里，波利的约翰（John of Poli）幸运地守住了芬迪（Fundi），但许多城镇都向教皇投降了。罗马人在这场战争中幸免于难；心中只有阿普利亚的教皇甚至没有试图用他的十字军迫使城市服从。他匆忙征服了这个王国，用特许状诱使其城市背信弃义，这些城市的赋税负担沉重。加埃塔（Gaëta）也向他投降了，格里高利九世现在希望能守住这座长期被教会占领的城市。

这时，被这些事件的消息召唤回来的皇帝突然从东方回到了家中。1229年3月18日，他在耶路撒冷亲手将王冠戴在了自己的头上，通过条约将圣城归还给了基督徒，并在狂热主义的阻挠下取得了辉煌的成就。罗马教廷嫉妒他亵渎基督教，既不考虑他在东方做出的实际贡献，也不考虑西西里与黎凡特的贸易往来使他有责任与东方苏丹保持友好关系的现实动机。这是很自然的，因为皇帝第一次将十字军东征变成了世俗政治问题，将教皇赶出了东方，并将东方和平地纳入了帝国的经济关系中。

1229 年 6 月 10 日，当他出人意料地在布林迪西登陆时，他寻求与教皇和解，并向他派出了和平使者。由于未能成功，他几乎是不战而屈人之兵，将教皇的军队赶出了他的国家。十字架的旗帜与钥匙的旗帜对峙着；腓特烈的萨拉森人在基督的标志下向教皇们进军，教皇们疯狂地逃过了利里斯河，这令人吃惊。格里高利再次向皇帝及其在罗马的支持者发出禁令。他已经在一场毫无意义的战争中耗费了大量的金钱，甚至现在他还在呼吁全世界为他提供新的资源。罗马元老院的使节在阿基诺向皇帝表示祝贺；10 月，他向教皇国边境进军，在他用火和剑摧毁了那里的索拉之后，教皇听取了他的求和建议。
