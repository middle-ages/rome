###第258次推翻伊曼纽尔-德-马迪奥 1257年

*蛊惑人心的 Mattheus de Bealvere。Brancaleone 第二元老院。惩罚贵族。罗马贵族的毁灭。1258年布兰卡龙去世。他的钱币 Castellano degli Andalò 元老。他的陨落和被俘。拿破仑-奥尔西尼家族和理查德-安尼巴尔迪家族参议员。罗马诺迪隆巴尔迪亚家族的衰落。旗手现象*。

伊曼纽尔-德-马迪奥的统治充满了风暴和不幸。他是圭尔夫贵族的产物，只为党派的目的服务，由于软弱或虐待，使布兰卡龙培养起来的人民感到愤怒。安尼巴尔迪家族、科隆纳、波利、马拉布兰卡和其他伟人夺取了政权；旧日的混乱再次爆发，贵族的唾弃反动产生了内战。渴望得到布兰卡龙稳固统治的人民奋起反抗；他们在元老宫周围和城市的街道上展开了战斗。1257 年春，起义开始普遍化。行会联合起来，推举一位英国后裔面包师马修斯-德-比尔维尔（Mattheus de Bealvere）为首领。伊曼纽尔在城战中被杀，一些贵族被赶出城外，教皇本人也被迫前往维泰博，并于五月底抵达维泰博。

罗马人民立即召回了布兰卡龙；他的到来并非没有危险，因为教会正在追捕他。这位贵族曾在三年时间里积极管理市民，抵御贵族的傲慢，他的到来让人们欢欣鼓舞。毫无疑问，他又获得了三年的元老院权力。

布兰卡龙开始了他的第二个统治，他的严厉也许被复仇的情绪夸大了，但城市的状况使他必须这样做。他赶走了所有折磨人民的人，或用铁链锁住他们，或将他们处死。他把理查德枢机主教的两个家族安尼巴尔迪吊死在绞刑架上。他与曼弗雷德结盟，以消灭圭尔夫党，曼弗雷德现在完全控制了本土和西西里岛，并且已经在考虑将王冠戴在他的头上。布兰卡莱内的性格和倾向都是共和主义者，但他却与意大利城市自由的民族敌人结盟，这一矛盾源于罗马城与教皇的关系。如果说教皇在其他方面是圭尔夫派的天然首领和城市独立的保护者，那么在罗马，他则是吉贝尔派，是封建贵族的保护者，只有在他的帮助下才能遏制民主。亚历山大四世放逐了布兰卡龙和他的议员们。他的无能遭到了嘲笑。元老宣称教皇无权将罗马行政官逐出教会。他随即宣布通过公众舆论对阿纳尼进行报复；据说，教皇的这座父亲之城即使不从地球上抹去，也要臣服于元老院。亚历山大的亲戚们被阿纳尼沮丧的民众送到了维泰博，他们恳求地跪在教皇的福森面前，以至于教皇不得不屈尊乞求这位可怕的元老放过他们。教皇可能解除了对他的禁令。他在罗马的民事权力不再得到承认。

布兰卡龙现在想用一次重大打击来瓦解大人物们的反抗：他下令拆毁贵族的塔楼、人民的城堡、债务人的地牢和可耻的暴力窝点。据说，在 1257 年，有 140 多座坚固的塔楼在这份公告清单面前倒下了，人民以毁灭性的狂怒扑向这些塔楼。从被拆毁的城堡数量就可以看出它们的数量；因为即使公正的法律适用于大多数塔楼，布兰卡永也很难让它们全部被拆毁，许多吉贝利纳人或友好贵族的塔楼都幸免于难。如果我们算上城中的 300 座贵族塔楼、300 座城墙塔楼和同样多的教堂塔楼，那么当时的罗马就呈现出了一个拥有 900 座塔楼的战争城市形象。由于这些塔楼中有许多是在古代建筑的基础上建造的，而且在贵族宫殿中也占有很大比例，因此这种系统性的破坏必然导致许多古迹被毁。因此，Brancaleone 被认为是罗马古迹的最大敌人之一，古城毁灭的新纪元就是从他开始的。在十四世纪，甚至有传言说他摧毁了奎里努斯神庙。被毁的宫殿同时也遭到了抢劫，一些家族档案和文件也被毁于一旦。

在这一正义之举之后，城市的景象一定很可怕，但罗马和所有其他城市一样，已经习惯了这种破坏。那个时代的市民从未享受过父亲之城安全有序的感觉。他们行走在废墟之中，几乎每天都能看到新的废墟出现。野蛮拆毁房屋就像今天的警察条例一样司空见惯。中世纪的城市不断发生着革命，街道、城墙和民居在快速的变化中反映出政党的愤怒和政府的动荡不安。如果人民在任何地方起义，他们就会推倒敌人的房屋；如果一方惧怕另一方，战败方的宫殿就会被摧毁；如果当局流放罪犯，他们的住所就会被拆毁；如果宗教裁判所发现任何房屋里有异教徒，国家就会将其夷为平地。如果战争军队征服了敌方城市，除非城市本身被摧毁，否则其城墙也会被夷为平地。在著名的 Monteaperti 战役之后，凶猛的吉贝尔派只是因为一位伟大市民的高尚义愤才没有摧毁佛罗伦萨；甚至在十三世纪末，教皇的愤怒也曾将整座城市夷为平地。博尼法斯八世曾在帕莱斯特里纳的废墟上撒盐，就像巴巴罗萨曾在米兰撒盐一样。

王朝也被卷入了塔楼的废墟之中，许多伟人通过流放、没收财产和绞刑来赎罪。但现在，米兰城和坎帕尼亚地区一片安宁，贪婪的乌合之众被一网打尽。

布兰卡莱昂令人敬畏和爱戴，但他在位时间不长。他在围攻科尔内托（因其谷物市场而成为重镇）时突发高烧，科尔内托拒绝向他宣誓致敬；他被抬到罗马，1258 年在元老宫寿终正寝。 同时代的人一致称赞布兰卡龙-达安达洛是所有不公正行为的无情复仇者、法律的挚友和人民的保护者--这是任何时代统治者的最佳荣耀。在这位博洛尼亚的伟大公民身上，这位当地法学院的实用主义学生身上，古老的精神再次出现，他所处时代的共和活力得到了令人钦佩的证明。他在位数年，组织了这座支离破碎的城市，并赋予其法律自由，这足以让他名垂青史。如果他在位的时间更长一些，他就会在罗马人与教皇的关系上带来巨大的变化，即使是他这种人的长期暴政也只会对罗马人有利。

人民以一种奇怪的方式纪念他们最好的元老：他的头颅像遗物一样被放置在一个珍贵的花瓶中，并作为永久纪念品树立在一根大理石柱子的上方--这是一种怪异的神化，但这一战利品对元老宫的装饰胜过米兰的卡洛基姆（Carrocium）。在罗马，人们对布兰卡龙的记忆已经消失，没有任何纪念碑或碑文提及他。只有他的硬币留存了下来。钱币的一面是一只昂首阔步的狮子和布兰卡龙的名字，另一面是手持地球仪和手掌的罗马人，并刻有 "罗马，世界之首 "的字样。这是元老的名字第一次出现在罗马钱币上，而且钱币上只刻有世俗的符号，而没有按照惯例刻上使徒彼得的形象或他的名字。

当教皇摆脱了自己家族中最强大的敌人后，他希望恢复罗马教廷的统治。他派遣使者前往罗马，禁止在未经他同意的情况下重新选举元老；但罗马人嘲笑他的命令。布兰卡龙临死前建议他们让自己的叔叔继任，于是之前的费尔莫民选官卡斯特拉诺-德利-安达洛被任命为元老。教皇徒劳地要求他享有投票权；他徒劳地说，作为一名普通的罗马公民，他本人必须在元老选举中拥有一票。亚历山大四世当时正在阿纳尼；他再也没有回到罗马。卡斯特拉诺也以他的侄子为榜样，用人质来保证自己的安全，但他的处境更加艰难，他的倒台也是不可避免的。流亡贵族和教皇削弱了他的权力，因此他只能通过不断的斗争来维持自己的地位，直到 1259 年春天。被收买的暴民起来反对布兰卡龙的叔叔；卡斯特拉诺被赶出了元老宫，他投奔了罗马的一座堡垒，并英勇地抵抗了围攻者。现在，在教皇的影响下，两名本地元老再次就职： 拿破仑是著名的奥尔西尼家族马特乌斯-鲁贝乌斯的儿子，理查德是彼得-安尼巴尔迪的儿子。虽然这种旧制度的恢复让元老党重新掌权，但这些元老也继续坚持元老宫的独立。他们明确延长了布兰卡龙和伊曼纽尔-德-马迪奥已经与蒂沃利缔结的和约，使这座城市不得不作为附庸永远臣服于罗马人民。从那时起，它不仅每年缴纳一千英镑的贡品，还接受罗马市政委员会任命的一名伯爵。不过，它保留了按照自己的法规生活的权利，有权任命一名 sedialis 作为市政法官、一名 capitaneus militiae 或人民护民官以及其他地方官员。

卡斯特拉诺拿起武器，在狱中受尽折磨，就像他之前的侄子一样，只有他在博洛尼亚和朋友们一起扣押的罗马人质才使他免于一死。由于罗马人担心这些男孩的命运，他们请求教皇保护他们。亚历山大因此要求博洛尼亚市政府将人质交由自己看管，但遭到拒绝。教皇于是让维泰博主教对博洛尼亚实施了禁止教务。

卡斯特拉诺终于挽救了意大利城市中的一场奇怪的运动，这场运动是在埃泽利诺-达-罗马诺三世及其家族倒台之后发生的。这位中世纪众所周知的城市暴君逐渐统治了伦巴第最重要的公社。教皇们的任何诱惑都无法说服腓特烈的女婿违背自己的原则，为教会效力，因为教会会原谅他付出的任何代价。经过英勇的抵抗，他最终于 1259 年 9 月 27 日在卡萨诺落入联合起来的敌人之手。历史学家描述了这位非凡人物的最后一战，他所处的时代将崇高美德的冲动转化为恶魔般的罪行，因此他作为那个时代的尼禄而永垂不朽。他们描述了人们蜂拥而至欣赏这位被囚禁的暴君时的欢欣鼓舞，并将这位可怕的人物比作一只被普通鸟群包围、静静坐着的鹰鸮。1259 年 10 月 7 日，埃泽利诺-达-罗马诺三世背负着三重禁令，满怀着对世人、教皇和占星家向他宣布的命运的无声蔑视，在松奇诺城堡去世，他被光荣地安葬在那里。他的弟弟阿尔贝里希的命运也很悲惨，他又一次脱离了教会，带着七个儿子、两个女儿和妻子在拼死抵抗后投降了圣芝诺塔。他的全家被当面勒死，他本人也被马匹拖死。

除了罗马诺迪隆巴尔迪亚家族的可怕覆灭之外，还有其他恐怖事件充斥着人们本已充斥的心灵。无休止的战争和瘟疫肆虐着城市。当时的一位编年史家写道："我的灵魂在颤抖，""诉说着我所处时代的苦难及其毁灭；因为自从教会与帝国之间的不和导致意大利血流成河以来，已经过去了二十多年"。人类突然受到了电击，促使他们忏悔；无数的人群在城市里高呼哀号；他们成百上千，甚至数万人结队游行，鞭打自己，血流成河。一个又一个城镇被卷入这股绝望的洪流，很快，高山和塔勒响起了震耳欲聋的呼喊："和平！和平！"！和平！主啊，请赐予我们仁慈！" 当时的许多历史学家在谈到这一奇怪的事件时都感到惊讶；他们都说，这场道德风暴首先在佩鲁贾兴起，然后蔓延到罗马城。它影响到每个年龄段和每个阶层。就连五岁的孩子也在鞭打自己。僧侣和司铎背起十字架，宣讲忏悔；古老的隐士从荒野的坟墓中走出，第一次出现在街头，宣讲忏悔。人们把衣服脱到腰带以下，用头巾裹住头，拿起刑具。他们结伴游行；他们光着脚，成双成对，在夜里点着蜡烛，走过冬天的霜雪；他们唱着可怕的歌，绕着教堂转圈；他们跪在祭坛前哭泣；他们在唱着基督受难赞美诗时，疯狂地鞭打自己。他们时而倒在地上，时而将赤裸的手臂举向天堂。任何看到他们的人，如果没有像他们那样做，都会变成一块石头。不和谐停止了；高利贷者和强盗来到当局面前；罪人忏悔；地牢打开了；杀人犯走到他们的敌人面前，把赤裸的剑放在他们手中，求他们杀死他们；但他们厌恶地扔掉武器，倒在侮辱他们的人的福森脚下哭泣。当这些可怕的游荡人群来到另一座城市时，他们就像雷雨一样降临到这座城市，于是这场灾祸就从一座城市传染到另一座城市。1260 年深秋，他们从佩鲁贾来到罗马。就连强悍的罗马人也欣喜若狂。他们的地牢被打开，安达洛的城主逃回了家乡博洛尼亚。

旗手的出现是中世纪最令人震惊的现象之一。在十字军东征的虔诚怒火中，人类对救赎的渴望曾在帝国与圣职者之间的斗争造成的类似长期混乱中得到表达；在 1260 年的灾祸中，同样的渴望再次出现。苦难的人类在情感深处汇聚了震撼它的各种事件的印象：异端邪说、宗教裁判所和火刑柱、修道士的狂热、鞑靼人、两个世界大国的激烈斗争、各派的愤怒、所有城市毁灭性的内战、埃泽利诺-达-罗马诺三世的暴政、饥荒、瘟疫、麻风病或麻风病：这些就是当时祸害世界的瘟疫。鞭笞者的恶魔般的迁徙是当时社会普遍苦难、绝望的抗议和自我忏悔的流行表达，当时的社会仍然被流行性的群众情绪所笼罩，就像十字军东征的一代人一样强烈。人类以这种黑暗的忏悔形式告别了教会与帝国之间的世界斗争时代。在这一时代的末期，一位天才作为其成果出现了。这就是但丁，他为那个中世纪世界树起了一座独特的丰碑。他的不朽诗篇是一座哥特式风格的奇妙大教堂，在教堂的垛口上，可以看到当时的杰出人物：皇帝和教皇、异端和圣徒、暴君、共和主义者、新旧人士、知识分子和创造者、奴隶和自由人，所有这些都围绕着寻求自由的忏悔的人类精神而排列。
