_3\. 德国出现了有利于菲利普的转机。他与教皇的谈判 腓力国王遇刺。奥托在德意志被承认为国王。奥托四世前往罗马并加冕为皇帝。莱昂尼纳河战役

与此同时，德意志的武器和公众舆论的命运都向菲利普倾斜。正义、洞察力和优势战胜了狭隘的非民族政策。迄今为止最顽固地反对霍亨斯陶芬王朝的几位帝国亲王臣服于圭尔夫-英国党，或倒戈相向。

1205年1月6日，新当选并得到下莱茵王公承认的腓力在亚琛由科隆大主教阿道夫加冕，加冕地点正是这位主教曾经为奥托戴上王冠的地方。教皇的反对现在成了霍亨斯陶芬王位得到普遍承认的唯一障碍。英诺森不再拒绝与菲利普就王国的和平进行谈判，国王也给他回了一封详细的信。这封意义非凡的信是菲利普所有行动的称义，带有真诚和解和不掺假的印记。信中宣称，他愿意接受红衣主教和王公们对教会不利的一切裁决，但出于对宗教的敬畏，帝国对教皇不利的事情他将听之任之，这给人留下了最美好的印象。就连阿奎莱亚宗主教和其他在罗马的使者也给教皇带来了新的建议，证明了霍亨斯陶芬的天主教情感。在王位之争中，英诺森已经达到了他的目的，即把他的司法干预转变为各方都承认的教皇权利，因为腓力也是出于无奈才向他屈服的，就像奥托所做的那样。德意志局势的逆转迫使英诺森屈服，并使自己作为政治家的行为适应了当时的形势。他与腓力的交往已经让他受到了模棱两可的指责，就像格里高利七世曾经在类似情况下所经历的那样。1206 年初，他还在斥责英格兰的约翰和英国的大臣们没有给予奥托足够的支持；他自己也在劝说奥托坚持下去，劝说德意志的王公们支持他。但自 1206 年年中和 8 月科隆陷落以来，与菲利普的谈判变得更加活跃。胜利者霍亨斯陶芬宣布愿意休战，这也是英诺森最希望看到的。1207 年夏，红衣主教使节乌戈里诺和利奥来到德意志，在两个王位请求者之间斡旋求和，当然没有成功。然而，仁慈多于实权的腓力服从了在教会事务上强加给他的条件，并被这些使节解除了禁令，这让奥托大失所望。对意大利的局势来说，重要的是，该国的王公们在菲利普赦免之前就已经收到了他的封书。早在 1208 年春天，他就以罗马国王的身份出现，向托斯卡纳各城市（他曾派阿奎莱亚的沃尔夫热尔作为他的使节前往这些城市）索要他们在闭关期间篡夺的帝国权利，并得到了完全的承认。

他对奥托的胜利也得到了教皇的果断承认；然而，关于帝国权利的争论，如确认教会在意大利中部的领地，仍然是双方使节最棘手的任务。腓力曾以公爵的身份拥有马蒂尔迪克（Matildic）的领地和托斯卡纳（Tuscany），他不愿意像奥托（Otto）那样无耻地放弃帝国的权利。他是否再次提议将自己的皇室女儿嫁给教皇的侄子，即新贵理查德的儿子，并将有争议的托斯卡纳、斯波莱托和安科纳的土地作为婚姻财产送给她，这一点令人怀疑。这样的承诺在 1205 年就已经做出了，但第一任教皇想让他的裙带关系拥有一个公国的野心让他提出这样的要求比国王满足这些要求更加明显。教皇当时提出的要求的真实内容令人怀疑；这些要求并不高，因为教皇的要求不可能低于诺伊斯投降书。四分五裂的德国接受了自己的内政被拖上罗马法庭的事实，但受伤的民族感情的声音仍在爱国诗人的诗篇中向我们诉说。已经可以预见，如果奥托不能达成友好的解决方案，英诺森本人将同意依法解除奥托的职务；然后，残酷的一剑击碎了德意志的伟大努力和希望。1208 年 6 月 21 日，腓力国王在班贝格死于维特尔斯巴赫的奥托之手。 这位年轻的王子在经历了如此艰辛的事业之后，在胜利的前夜倒下了，这是德国历史上最悲惨的事件之一。霍亨斯陶芬王朝随之在德国灭亡。辉煌的巴巴罗萨家族只剩下一个继承人，那就是英诺森三世的门徒腓特烈，他从小就与国家疏远，在不幸的风暴中被留在了遥远的西西里。一瞬间，世界局势发生了变化，意大利和德意志的命运重新联系在一起，两个国家、帝国和教皇都陷入了一个世纪也未能平息的斗争迷宫。

这件事突然改变了他的计划，这让无邪深感不安。但他当时并没有意识到这一时刻所带来的不可估量的厄运。对政治家来说，这似乎是一个巧合，使他重新掌握了局势，摆脱了矛盾；而对司铎来说，这是上帝在帝国审判中作出的判决。

别无选择：奥托-盖尔夫已经被他们拒之门外，必须尽快得到承认。英诺森立即给他写了信，向他保证了自己的爱，向他展示了自己即将登上帝国宝座，但同时也向他展示了远方的敌人，被谋杀的菲利普的侄子。在这位伟大的西西里国王，霍亨斯陶芬王朝的合法继承人身上，奥托有了一个可怕的对手，只要教会认为有利，就可以武装起来对付他。看到腓特烈年轻的身影气势汹汹地站在背景中，教皇在很短的时间后就亲自把他从背景中召唤出来，他对教会和帝国同样具有破坏性，这真是魅力无穷。

英诺森真诚地希望解决长期以来的王位之争，并在法律上承认他的教会地位。他毫不怀疑自己能从奥托那里得到这一点，因为他仍然受制于《诺伊斯条约》。渴望和平的德国向圭尔夫致敬。1208年11月11日，奥托在法兰克福被所有皇室封为国王，并很快与世仇腓力的孤女订婚，痛苦、爱国和艰辛促成了一次庄严的和解，两家之间的宿怨似乎在这次和解中得到了解决。

前往罗马的行程已经宣布。但在此之前，奥托于 1209 年 3 月 22 日在施派尔重新签订了诺伊斯投降书。教皇国的全部领土都得到了承认；在教会不受国家权力约束方面也做出了重大让步，因此卡利克斯特二世的协约失去了效力。在割让给教会的土地上的帝国权利中，奥托只保留了前往罗马途中微不足道的福德鲁姆（Foderum），而这一权利却被写进了条约中，仿佛是一种嘲弄。这是帝国存在以来，罗马国王第一次自称 "承蒙上帝和教皇的恩典"。奥托不得不承认，他的地位提升完全归功于此。国王发下了皇帝无法兑现的誓言。

1 月，意大利使节带着他们城市的钥匙出现在奥格斯堡，其中包括伟大的米兰，米兰欢天喜地地迎接了一位贵族的登基。奥托任命宗主教沃尔弗格为他在意大利的使节，在伦巴第、托斯卡纳、斯波莱托、罗马涅和马尔凯行使帝国权利。因为即使在《康斯坦茨和约》以及与教皇签订的条约之后，皇帝们在意大利的城市中仍保留着最高权力的假象，甚至在罗马涅和马尔凯也保留着许多财政权。教皇们并不否认这一点。英诺森亲自告诫伦巴第和托斯卡纳的城市要服从皇室的权力使者，但提醒他说，他只能根据合同为教会占有马蒂尔迪克的地产。

当奥托随后于 1209 年 8 月率领大军从奥格斯堡经蒂罗尔下到波河流域时，没有人阻止这位盖尔夫的罗马之旅。意大利的不幸在于，它的城市未能建立长期的邦联。如果这样的话，在亨利六世死后，任何德国国王都不可能攻破伦巴第人民的堡垒。伦巴第人光荣的独立斗争既没有泯灭罗马帝国的传统（这一传统在后世仍然让意大利人如此热衷），也没有给整个民族带来持久的利益。因为在莱格纳诺战役胜利之后，意大利各共和国并没有像马拉松和普拉提亚战役之后的希腊各共和国一样，能够建立起一个政治国家。当各公社还在进行宪政斗争和内战时，那些赋予意大利历史如此奇特特征的城市暴君的身影已经出现了。奥纳拉的埃佐里诺和埃斯泰的阿佐七世侯爵是生死仇敌，在奥托面前互相指责，他们当时是两个政党的首领，这两个政党曾使国家四分五裂达两个世纪之久。紧随其后的是费拉拉的吉贝尔派萨林圭拉二世，他对权力和英勇的渴望也不遑多让。

当维尔夫家族的皇帝第一次经过伦巴第时，霍亨斯陶芬家族的所有敌人可能都期望得到他的专宠。但他们错了，因为皇权的朋友不再是身为皇帝的格尔夫的敌人。阿佐看到他在奥托阵营中的对手受到了极大的尊重；圭尔夫-佛罗伦萨受到了一千马克罚金的威胁，吉贝利派的比萨很快得到了特许状，并设法缔结了条约。

9月，英诺森在维泰博接见了奥托。第一次会面时，罗马国王不得不告诉自己，如果没有谋杀的巧合，这位教皇一定会把罗马人的王冠戴在敌人的头上。对于那些出于私心、用高价买来的仁慈的人，人们是不会有好感的。教皇的政策一定在奥托的灵魂深处留下了痛苦的报复情绪，也许即使在维泰博，他的目光也能穿透国王隐藏着怨恨的感激崇敬的面具。经过艰难的谈判，英诺森被迫放弃了要求奥托在加冕为皇帝之前宣誓偿还教会在1197年之前与帝国之间的一切纠纷的要求。教皇赶在他前面前往罗马；在施派尔的康拉德首相和御用参赞贡泽林率领的军队占领莱昂尼纳后，奥托于 10 月 2 日在马里奥扎营，并按照古老的习俗向罗马教廷和罗马人民保证了安全。

加冕礼于 1209 年 10 月 4 日在圣彼得大教堂举行，而军队仍留在帐篷里，但一些军队（他们是米兰人）占领了台伯河上的大桥，以防止隆隆作响的罗马人发动进攻。当读者了解到罗马人的敌对行动是如何经常地在帝国加冕礼上重演时，他们一定会露出讽刺的微笑。当日耳曼人靠近他们的城市时，罗马人封锁了城门；他们的皇帝及其随从只是从梵蒂冈向伟大的罗马投去好奇的目光，而罗马的奇妙世界对他们来说仍然是封闭的。一个奇怪的事实是，只有极少数皇帝进入过这座城市，就连奥托也从未见过它。罗马人在 1201 年宣布他为皇帝，如果他愿意用金钱来换取他们的投票，他们现在也会心甘情愿地承认他。十八年前亨利六世加冕时，他不得不通过签订条约的方式赢得当时自由而强大的城市的选票，但奥托四世却不需要这样做。这激怒了人民。元老院，甚至一些红衣主教都反对加冕；市民们在元老宫举行了武装集会。

加冕仪式结束后，游行队伍艰难地穿过密密麻麻的武士队伍，一直走到天使之桥；教皇在这里向皇帝告辞，让他返回拉特兰，并命令他第二天离开罗马领土，这是对帝国威严的公然侮辱。然而，一些争执点燃了罗马人的仇恨。传统的加冕战在莱昂尼纳打响，双方损失惨重后，奥托转移到了马里奥山上的营地。他在此盘踞数日，要求教皇和罗马人给予赔偿或补偿。
