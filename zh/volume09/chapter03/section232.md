_4\. 腓特烈二世返回西西里。罗马涅由帝国伯爵统治。斯波莱托的混乱。罗马和维泰博。佩鲁贾的民主运动。罗马和佩鲁贾 教皇逃离罗马。帕伦提斯参议员。多次推迟的十字军东征谈判。安杰洛-德-贝宁卡萨（Angelo de Benincasa）参议员。伦巴第人对皇帝的敌对立场。皇帝与教皇之间关系紧张。腓特烈与布里埃纳的约翰之间的裂痕。1227年霍诺留三世去世。

皇帝在马里奥的营地又停留了三天，然后于11月25日经苏特里和纳尔尼前往蒂沃利，12月5日他已经在那里了。教皇命令罗马托斯卡纳的城镇将福德鲁姆移交给帝国军队，但他否认了从滨海马萨和坎帕尼亚升起福德鲁姆的权利，因为加冕游行并未触及这些地区。他说，如果说以前的皇帝曾非法要求在这些地方维持秩序，那也只是在他们急于入侵西西里王国的时候。不过，他还是指示坎帕尼亚总督授予福德鲁姆（Foderum），这是帝国权利的最后一点可怜的残余。

腓特烈穿过拉丁姆，以皇帝的身份进入西西里继承地，正是这一举动让库里亚的喜悦蒙上了阴影，因为库里亚希望看到他在东方任职。在卡普亚，他召集了阿普利亚的男爵们，并立即着手用新法律组织王国。他再次向教皇确认了教皇国和马蒂尔迪克属地。他没有重复奥托四世的榜样，而是认真履行了自己的义务。1221年2月初，霍诺留得以承认，在斯波莱托皇帝的帮助下，他和平地统治了马蒂尔丹郡的大部分地区和从里里斯桥到拉迪科法尼的全部领地，而桀骜不驯的马尔凯七世则被赐予埃斯泰的阿佐，实际上是被这位封建主以教会的名义征服了。

霍诺留三世的野心远非其前任可比，他追求的只是教会与帝国之间的和平，以及实现他解放耶路撒冷的虔诚愿望。与其他教皇相比，他可能更喜欢平静地拥有教皇国。但是，大帝国的统治从来没有像罗马主教们想要称王的狭小领土给他们造成的那样，让王朝付出如此尴尬的斗争代价。一百多位教皇的天才、教会的权力和财富、无数的战争和放逐、誓言和协约，都被耗费在建立和维护教皇国上；几乎每一位教皇都不得不重新开始工作，费尽心力地将教会在尘世的躯体被王公贵族的刀剑一再击碎的碎片重新拼凑起来。在整个中世纪，教皇们都在滚动西西弗斯的石头。

当腓特烈以庄严的条约确认因诺琴蒂亚教皇国时，他最初的意图是允许它们存在。卡普亚的文件仍然证明了这一点。然而，亨利六世之子对教庭的极度不信任伴随着他的一举一动。 他自己在他们的意图中只看到了自私和阴谋。这种不信任比公开的敌对行为更具破坏性。罗马帝国的普世权力观念与教会的普世权力观念不断发生冲突，意大利自然成为永恒冲突的对象。腓特烈二世和奥托四世都想重新征服这片帝国根基所在的土地，派系纷争使城市在自相残杀的战争中四分五裂，这促使皇帝介入各方之间并加以利用。教会国家中不断出现的瓦解冲动诱使他再次伸出手来争夺他已经放弃的帝国权利，而教会则再次主张旧有的权利，这些权利随着时间的推移和财产的变化，如马蒂尔迪克庄园，已经几乎面目全非了。

霍诺留的满意很快就结束了。早在1221年6月，皇帝就册封布兰德拉特的戈特弗里德为罗马涅伯爵，该省自奥托尼王朝以来一直被视为帝国领土；帝国总督在这一地区的管辖一直持续到1250年，甚至更晚。在斯波莱托，与佩鲁贾和阿西西一样，直到那时才完全向教会投降，并由红衣主教雷纳-卡波奇家族统治，前康拉德公爵的儿子贝托尔德试图夺回其父已消亡的公国。他与宫廷总管贡泽林联合起来，在那里和在马尔格拉维特都对红衣主教采取敌对行动，挑动城市叛教，赶走教皇的官员，任命自己的官员。在这里，帝国法律也与新的教皇法律发生了冲突。虽然腓特烈制止了这些领主的行为，但罗马仍然怀疑他行事不诚实。

罗马人再次与维泰博开战，因为城堡占有权的争夺不断引发无尽的仇恨。维泰博城甚至在 1220 年 9 月通过购买的方式获得了奇维塔韦基亚；当时它的规模很大，贸易也很丰富；在托斯卡纳滨海马萨，只有科尔内托能与之抗衡。它能够召集 18000 名士兵。与所有公社一样，贵族和市民为权力而战，家族为夺权而起。加蒂家族（Gatti）和蒂格诺西家族（Tignosi）的敌对势力将罗马人卷入了冲突，罗马人在 1201 年的和约中失去了他们征服的权利。因此，战争从 1221 年开始，持续了很长时间。甚至连霍诺留也被卷入其中，他的调停立场或代表维泰贝人的参与导致了一场起义，他试图保护维泰贝人免受罗马人的愤怒。

佩鲁贾发生的事件也让罗马人充满了怀疑。这座繁荣的城市首先向英诺森三世致敬，并获得了他对其市政地位的承认。教皇作为佩鲁贾的保护者，曾试图解决贵族和人民（_Raspanti_）之间的激烈斗争，但没有成功；人民党甚至试图再次脱离教会，直到 1220 年，教皇才艰难地保住了佩鲁贾。在罗马，没有任何迹象表明行会或_artes_已经是强大的团体，而在佩鲁贾，他们在校长和执政官的领导下形成了武装联盟，这些校长和执政官努力推行民主政府。人民党颁布了反对神职人员自由的法令，并向他们征税，他们还与贵族和骑士进行斗争，对不公正的税收分配感到不满。教皇派往佩鲁贾的普拉塞德红衣主教约翰-科隆纳（John Colonna）拥有特殊的权力，他在各方之间进行了干预，并最终以自己的权力废除了政治形式的行会组织，1223 年，霍诺留（Honorius）确认了这一权力。从这个案例中，我们不能得出教皇完全镇压了公会的结论。他们的力量太弱，无法这样做；相反，他们与民主派联合起来，寻求对腓特烈的支持。对于腓特烈，他们可以说教皇的统治是轻柔的，因为这位皇帝奉行严格的君主政体原则，希望将所有的政治个性都置于他的法律之下，他坚决反对一切特殊的民主制度；在他的西西里王国，他禁止在城市中选举议员和执政官，否则将处以死刑。

毫无疑问，除了与维泰博的战争外，这些事件也对罗马产生了不愉快的影响，因为佩鲁贾正式承认了罗马元老院的权威。几乎在整个十三世纪，罗马贵族都在佩鲁贾担任市长一职。佩鲁贾的古罗马殖民地甚至仍然虔诚地尊崇教皇罗马为其杰出的母亲和女主人，因为改变一切的世纪并没有能够泯灭神圣的传统。在宪法法案中，甚至在 1279 年佩鲁贾市最古老的法规中，都可以看到对罗马人民主权的恭敬承认，以及对教皇主权的承认，在 "向 "圣人和教皇致敬之后，还可以看到对 "罗马圣母 "的致敬。罗马城的权威远在翁布里亚和斯波莱托公国的辖区之外就得到了承认，那里的波德斯塔（Podestà）一职经常由罗马人担任，尤其是在奥尔维耶托。后来，在 1286 年，佩鲁贾、托迪、纳尔尼和斯波莱托缔结了一个为期四十年的联盟，其中明确写道："为了纪念我们的母亲，崇高的城市"。1313 年，奥尔维耶托和佩鲁贾之间的盟约申请中也出现了 "为了纪念杰出的罗马城 "的字样。

在不久后爆发的罗马动乱中，已经在城市世仇中扮演重要角色的理查德-孔蒂也开始显露头角。腓特烈又从这位强大的伯爵手中夺取了索拉；他去了罗马，没有得到教皇的支持，现在开始与他的追随者一起对抗萨维利家族和霍诺留的其他朋友。1225 年 5 月，教皇逃到了蒂沃利，接着又逃到了里耶蒂。当时，帕伦提乌斯再次成为元老。虽然这位罗马人的亲戚中有一位殉道者，但他却是教士们的坚定敌人。即使是在卢卡担任教长期间，他也曾向教士征税或驱逐教士，因此招致了他曾被赦免的禁令。霍诺留可能拒绝了元老院对他的任命，而人民对他的暴力任命将是导致叛乱的主要原因之一。

教皇与皇帝之间的关系已经非常紧张，皇帝拒绝放弃在西西里岛的改革，以便踏上十字军东征之路，他一边狡猾地逃避着自己的义务，一边又不断受到十字军东征的折磨。达米埃特的陷落（1221 年 9 月 8 日）令西方国家感到恐惧。1222年4月，皇帝和教皇在维罗利（Veroli）举行了为期两星期的会议，并安排在维罗纳召开一次会议，但这次会议未能举行。在费伦蒂诺（1223 年春）举行的一次新会议上，耶路撒冷国王布里埃纳的约翰、宗主教和三位大法师也出席了会议。为了让腓特烈更加坚定地接受这一承诺，教皇说服他接受耶路撒冷名义国王的独女约兰达的求婚，因为他的第一任妻子康斯坦茨已于 1222 年 6 月 23 日去世。1225 年到来时，教皇的殷切希望并没有实现，因为西方的国王们拒绝支持他。腓特烈的使节们，包括布里安本人（他本人要求进一步推迟），在里耶蒂找到了被逐出罗马的教皇。7月25日，皇帝在卡西诺的教皇使节面前宣誓，他将于1227年8月开始十字军东征，否则将被开除教籍。

霍诺留在里耶蒂度过了一个冬天，同时为他的回国进行了谈判；因为皇帝已经实现了他的愿望，即使在这个时候，他也以调解人的身份进行了干预。秋天，教会和城市之间达成了和平： 帕伦提斯辞去了元老院的职务，安杰洛-德-贝宁卡萨接替了他的位置。随后，教皇于 1226 年 2 月返回罗马。他在这里又住了一年，生活在令人尴尬的躁动中，以至于他与皇帝的分歧几近破裂。在这些年里，腓特烈清除了阿普利亚和西西里的所有障碍，征服了叛乱的男爵，征服了岛上的萨拉森人并将他们转移到了大陆上的卢塞拉，在那不勒斯建立了大学，并通过更好的管理增强了这个神奇国家的实力。然而，现在许多情况加在一起，将他从与教会和意大利的和平中驱赶出来，陷入了伴随他一生的最可怕的战斗。

伦巴第城市拒绝接受《康斯坦茨和约》留给帝国的权利；旧帝国主权的残余，在其边界上的不确定性，使他们有理由不履行自己的职责，也使皇帝有理由提出比他应得的更多的要求，很快，他就宣布打算在波河上建立帝国政权，并收回整个意大利作为 "他的遗产"。变得强大并充满民族情绪的城市为自由和独立而战，就像巴巴罗萨时代一样。他们英勇的抵抗理应得到更好的回报，但他们的不团结是他们未能取得持久成功的罪魁祸首。当伦巴第人听说腓特烈从阿普利亚逼近时，他们于 1226 年 3 月 2 日在曼图亚签订了《莫西奥条约》，将旧的邦联延长了 25 年，教皇欣然批准了该条约。他们的威胁态度阻止了亨利穿越阿尔卑斯山口前往克雷莫纳的帝国议会，导致了帝国八国联军的出现。双方都要求教皇妥协，但教皇的妥协最不可能让腓特烈满意，因为事实证明霍诺留偏袒伦巴第人，这是很自然的。

关于西西里岛主教叙任权的争论加剧了紧张局势，教会要求获得主教叙任权，而腓特烈则提出了异议，他几乎不认为自己是其世袭土地的主人，因为他想让这片土地完全独立于教皇。教廷对皇帝将西西里王国转变为独立君主制的英明改革越来越怀疑：因为正是在这里，腓特烈建立了他的权力基础，他似乎要从这里开始，通过摧毁意大利的邦联、城市自由和教皇国，努力实现建立一个统一的君主制意大利的目标。当时的教皇宫廷已经开始担心这一点了。

布里埃纳的约翰也曾作为原告出庭。因为他刚刚与耶路撒冷的女继承人约兰达（Jolanthe）通过她的母亲玛丽结婚，皇帝就自封为耶路撒冷的国王，而他的岳父则被欺骗了所有的希望，他将自己的控诉带到了教皇的宝座前。霍诺留利用了这位骑士般的前国王的才能，他是瓦尔特的弟弟，英诺森三世曾经利用过他，授予他教皇国大部分地区的世俗总督职位。教皇热衷于组织十字军东征的悲惨结果就是：布永的戈特弗里德的继任者为教会服务，以勉强维持教区长的生计。

1227年3月18日，霍诺留三世在拉特兰去世。
