### 第三章

_1\. 弗雷德里克决定去德国 他来到罗马 1215年在亚琛加冕，发誓十字军东征。拉特兰大公会议。英诺森三世之死。举世闻名的伟大教皇_。

他以为已经被消灭的年轻的世袭仇敌突然起来反对奥托，就像教皇召唤出的大卫反对扫罗一样。奇怪的命运召唤着腓特烈，他是三位天命之子中的第一位，也是最有资格的一位，却在王位之争中最后一位出现，以恢复霍亨斯陶芬家族的统治，并赋予其新的伟大。在英诺森三世的手中，这些被选中的人就像棋局中的棋子，被他一个接一个地玩弄于股掌之间。他们都觉得自己不配成为别人意志的奴仆。亨利六世的儿子对主宰他一生的自私自利的司铎政治深恶痛绝。他从未忘记，他不得不以封建关系和失去宝贵的王权来换取教会的保护，也从未忘记，当教皇奥托四世召唤他时，他被排除在帝国王位之外。

腓特烈是在宫廷党派的阴谋诡计中长大的，就像当年的亨利四世一样，他也像那位国王一样掌握了全面智取的艺术。他自幼与罗马教廷及其在帝国和西西里的事业关系不睦，从中学到了后来用来对付教会的狡猾手段。他们的政治家风范就是他自己的学校。

奥托的反对者把他召到了德国。他发现教皇和罗马人准备承认腓特烈对罗马王位的要求，因为他拥有王位的事实突然被英诺森发现了。政治是一切理想的伟大、宗教和哲学美德的敌人，它甚至迫使像他这样的人走向平凡，改变自己，否定自己的观点。因为按照他们的说法，最后一位霍亨斯陶芬应该永远流放在西西里，成为教会的封臣，远离王国事务。教皇认为有可能阻止可怕的西西里与德意志的统一吗？看来他是被蒙蔽了。他邀请西西里国王夺取罗马王冠的那一刻，是教皇历史上最具灾难性的时刻之一：它引发了一场摧毁教会和帝国的战争，然后是安茹家族的统治、西西里晚祷和阿维尼翁人的流亡。英诺森铸造了第二把更锋利的剑，它将伤害教会。国王们在这位教皇的脚下放下了他们作为附庸的王冠，而这位教皇的一再欺骗却羞辱性地证明了即使是最杰出的头脑也对世界的规律和进程盲目无知。

1211 年秋天，当斯瓦比亚使者出现在巴勒莫，向腓特烈献上德意志王冠时，王后和议会奋起反对这一危险的邀请。西西里人在亨利六世的手中吃了太多的苦头，他们不痛恨与德国的任何关系。腓特烈本人也曾动摇过，后来他决定勇敢地投身于不可估量的未来浪潮中。当时他还不到18岁，自1209年8月起就与阿拉贡国王阿方索的女儿、匈牙利埃默里的遗孀、无子女的康斯坦莎结婚。他让不久前出生的儿子亨利加冕为西西里岛国王，把总督职位让给了妻子，然后从墨西拿启程，匆匆赶往罗马，1212年4月，他作为罗马人的当选国王受到了教皇的欢迎。英诺森第一次见到了他身无分文的门徒，之后就再也没有见过。英雄巴巴罗萨的孙子以候任皇帝的身份站在他面前；从比奥托四世更崇高的意义上讲，他是他的造物：他的职责所在，他的养子，他为保护他的养子付出了多年的心血。如果有报道把这个年轻人描述成一个沉醉于宫廷吟游诗人群中的傻瓜，那么他敏锐的目光很快就会发现亨利六世的儿子身上所具有的天才力量和早期锻炼出来的智慧。教会对腓特烈的升迁附加了一些条件，其中最重要的是确定了西西里与帝国的分离。新的帝国王位候选人是在与奥托四世相似的条件下登上王位的，这给帝国带来了不幸；因为奥托四世只是通过作伪证才撕开的枷锁也为腓特烈编织好了。但毋庸置疑的是，他当时的态度是真诚的，对未来充满了热切的希望。

教皇完全满意地解除了腓特烈的职务，甚至为他提供了资金支持。这位年轻的西西里人，"阿普利亚的孩子"，在财富的指引下到达了德意志。祖先的名声为他打开了祖国的大门，尽管他对祖国完全陌生，甚至几乎不懂德语。他慷慨地挥霍着家族和帝国封地的遗产，赢得了那些贪婪的大人物的青睐，而这位粗犷的圭尔夫皇帝的形象则成为了一个年轻人的陪衬，在这个古典的岛屿上，外国的恩惠用最美丽的礼物装饰着这个年轻人。

1212年12月5日，腓特烈在法兰克福被选为国王，1213年7月12日，在几乎整个德意志的承认下，他在埃格尔宣誓就职，在王国王公的同意下，他必须重申奥托四世对教皇的让步。教会在神职人员方面的自由得到了承认；因诺琴蒂亚教皇国得到了确认；在这些土地上，只有福德鲁姆（Foderum）在加冕礼上被保留给了帝国；教皇对阿普利亚和西西里的统治权再次被庄严宣布。

1214年7月27日，腓特烈二世在布维涅战场上击败了不幸的对手，光荣陨落。1215年7月25日，教皇的使节、总主教齐格弗里德（Siegfried）在亚琛为腓特烈二世加冕。奥托四世称他的对手为 "牧师国王"，出于对教会的服从，或许也出于骑士精神，他在加冕礼后带上十字架，前往应许之地。这个誓言将成为他巨大不幸的根源，当时他是认真的，但一旦他加冕为皇帝，也许他就不再保证将西西里岛作为教会领地从自己的王位中分离出来并让给他的儿子亨利了。

1215年11月11日，英诺森在拉特兰召开了盛大的会议，最终解决了德意志王位之争。奥托的拥护者和腓特烈的特使都得到了裁决，后者被否决，前者得到承认。来自基督教世界所有国家的 1500 多名牧师，以及来自国王和共和国的王子和使节跪在最有权势的教皇脚下，教皇作为欧洲的统治者坐在世界的宝座上。这次辉煌的会议是英诺森三世最后的庄严行动，它体现了英诺森赋予教会的新活力，以及他所维护的教会统一。这位不寻常之人生命的终结也是其人生的高潮。他即将前往托斯卡纳调和比萨和热那亚，并为十字军东征赢得这些海上强国的支持，而十字军东征是该次会议最重要的目标，1216年6月16日，他在佩鲁贾去世，没有为他的荣耀活得太久。

英诺森三世是真正的教皇奥古斯都，他并不像格里高利一世和格里高利七世那样是一位富有创造力的天才、 然而，他是中世纪最重要的人物之一，他严肃、庄重、忧郁，是一位成功的统治者，一位具有洞察力的政治家，一位对信仰充满热忱的大祭司，同时又具有巨大的野心和令人恐惧的意志力；他是教皇宝座上一位大胆的理想主义者，同时又是一位非常务实的君主，一位冷酷的法学家。一个人在平静的威严中真正按照自己的意愿塑造世界，哪怕只是一瞬间，这种景象也是崇高而奇妙的。他巧妙地利用历史环境，娴熟地运用教规和诗歌，引导大众的宗教情绪，赋予了教皇如此巨大的权力，以至于它不可抗拒地席卷了国家、教会和民间社会。仅凭司铎思想的力量，他就像格列高列七世一样，在位时间短得惊人：罗马、教皇国、西西里岛；意大利臣服于他，或投靠他，成为他的保护者；帝国被推倒在阿尔卑斯山上，在教皇的审判下屈服。德国、法国和英国、挪威、阿拉贡、莱昂、匈牙利、遥远的亚美尼亚、东方和西方的王国都承认了教皇的司法审判。对被遗弃的丹麦人英格博格的审判让英诺森有机会让强大的菲利浦-奥古斯都国王接受教会法的审判，而一场关于继承权的争端让他成为了英格兰的封建主。他对付英国国王的高超手段（他侵犯了英国国王的王权），他将自由英格兰转让给外国王子菲利浦-奥古斯都的妄想，他胆敢与这位君主本人玩的不受惩罚的游戏，他的成功和胜利，实际上都近乎不可思议。可恶的约翰在奴颜婢膝的恐惧中公开放下了王冠，作为罗马教廷的附庸从潘杜夫（Pandulf）手中接过了王冠，潘杜夫是一个颇具古罗马骄傲和英勇的普通使节。多佛尔的这一著名场景让人联想起古罗马时代，当时远方的国王根据总督的命令放下或拿起他们的王冠。它就像卡诺萨-迪普利亚的一幕一样，在教皇的历史上熠熠生辉，而它只是其中的一个小插曲。它深深地羞辱了英国；但没有一个国家能像这个男子汉国家一样，如此迅速、如此光荣地从屈辱中站立起来，从其懦弱的暴君手中强行夺取了《大宪章》--欧洲所有政治和公民自由的基础。

无辜三世的幸福无边无际。世界上所有的情况都对这位教皇出现的那一刻产生了影响，并通过这些情况变得强大。他甚至看到格列高列七世让希腊教会服从罗马法律的大胆梦想实现了，因为在拉丁十字军征服君士坦丁堡之后，罗马仪式被引入拜占庭教会。从来没有一位教皇能像英诺森三世--皇帝和国王的创造者和毁灭者--那样，对自己的权力有如此崇高而又真实的认识。格列高利七世大胆地提出了让欧洲成为罗马的领地、让教会成为世界的宪法的目标，没有一位教皇能如此接近这一目标。他的臣子们从国王开始，接着是王子、伯爵、主教、城市和领主，所有这些人都从这一位教皇那里获得了封地。他用恐怖主义包围着教会：在尼禄和特拉扬时代，罗马对权力的绝对控制在人类中散布的恐怖，并不比世人在罗马人英诺森三世温和的训诫或威胁性的雷霆一击面前的奴性敬畏大多少、 这位威严的司铎可以用《旧约》中的语言对战战兢兢的国王们说："就像在上帝之约的方舟中，杖与律法书并列，同样，在教皇的胸中，也安放着可怕的毁灭之力和甜蜜的温和之恩。通过他，罗马教廷成为了教义和教规权力的宝座，欧洲各国的政治法庭。在他的时代，西方和东方都承认，所有道德和政治秩序的重心都在教会、道德世界及其教皇身上。这是历史上最有利的星座。教皇制度在英诺森三世达到了一个令人眩晕、难以为继的顶峰。
