### 第三章

_1\. 屋大维努斯接替阿尔贝里奇掌权。他在 955 年以约翰十二世的身份成为教皇。他放弃了他父亲的政策。伦巴第人和约翰十二世称奥托一世为奥托一世。他于 962 年 2 月 2 日在罗马加冕为皇帝。 德意志民族新罗马帝国的特征_。

阿尔贝里希死后，年轻的屋大维（他与阿尔达所生的儿子）毫无异议地被承认为亲王和全体罗马人的元老。因此，他以传统的形式延续了父亲的世俗政府。我们没有他那个时代的罗马钱币，但他肯定也铸造了钱币，并在钱币上标注了他的名字和亲王头衔。他统治罗马时还不到 16 岁。出于骄傲和野心，他的父亲给他起了屋大维这个名字，也许是表达了希望皇帝之位传给他的部落的大胆愿望。他错了，因为在阿加皮图斯（Agapitus）担任教皇期间，教皇的主张又得到了更多的支持者，德意志的势力受到了来自远方的威胁。阿尔贝里奇亲自任命他的儿子为教皇，并将教皇的王位与世俗权力重新结合在一起；他就这样将罗马的历史引向了它的老路。

由于阿加皮图斯二世（Agapitus II）于 955 年秋季去世，这位年轻的罗马王子仅用了一年时间就成为了教皇。除了索拉克特的编年史家之外，没有任何历史学家提到过他曾接受过神职教育，我们也不知道他在升任罗马教廷主教之前是否拥有过任何教权。据说从那时起，改姓就成了教皇的惯例。现在，阿尔贝里希的继承人重新统一了两方势力，932 年的革命除了将贵族统治家族推上彼得教廷的宝座之外，别无其他结果。然而，约翰的王权倾向比他的精神职责更加强烈；他身上的两种天性，屋大维的天性和约翰十二世的天性，在他身上进行着不平等的斗争。在这样一个尚未成熟的青年时期，他拥有了一个能让他受到世人尊敬的地位，但他却失去了理智，陷入了最狂热的感官享受之中。他的拉特兰宫殿成了享乐之家和后宫；罗马的贵族青年是他最喜欢的伴侣。卡里古拉曾让他的马担任元老，教皇约翰十二世在马厩里为一位迪亚科努斯举行了祝圣仪式，也许他是在一次宴会上喝醉了酒，在那里他用异教的幽默向古老的神灵献酒。

然而，我们只能模糊地看到约翰十二世最初几年罗马的状况。这个鲁莽的年轻人放弃了他父亲的温和制度；作为一个兼任教皇的王子，他想干一番大事，将他的统治深入到南方。他联合罗马人、托斯卡纳人和斯波莱丁人，对贝内文托和卡普亚的潘杜尔夫和兰杜尔夫二世发动了军事行动，但萨莱诺的吉苏尔夫支持威胁者的运动迫使他回头，于是他在泰拉奇纳与这位王子媾和。教皇的威望激励着他；他从父亲那里继承了一些胆识，但没有智慧。作为教皇，他希望--事实上，他必须努力--确定教皇国的范围。此外，他在罗马的军团本身也处于危险之中，因为罗马人不再感受到阿尔贝里希的强大力量。身为教皇的儿子无法继续执行父亲通过限制来维护自己的政策；因此阿尔贝里希的工作失败了，约翰十二世最终不得不为了他的世俗省份而召见奥托国王。作为屋大维，他在罗马可能是强大的，但作为约翰十二世，他是被憎恨的、软弱的。由此可见，教皇身上国王和司铎两种天性的混合是多么奇怪地影响着他们的地位。

当时，贝伦加尔和阿达尔贝特利用奥托国王因其子女和匈牙利人的叛乱而被迁往德意志的机会，收服了不情愿的伦巴第伯爵和主教。他们来自德意志党派的敌人，即恶毒的柳特普朗（他受到了贝伦加尔的侮辱，我们不知道为什么），把这些王公的肖像涂成了最黑的颜色；贝伦加的妻子维拉因为贪婪而遭到憎恨，但这些国王为确保他们的统治所做的并不比他们的前任或后来的德意志国王自己允许的更多。贝伦加尔的父亲奥托（Otto）曾派柳道夫（Liudolf）前往意大利牵制贝伦加尔，柳道夫突然去世后，似乎没有什么能够抵挡他了。他威胁到了艾米利亚和罗马涅，而约翰十二世太过软弱，无法捍卫这些领地。曾经将奥托从罗马拒之门外的阿尔贝里希的儿子于 960 年邀请这位德国国王前往罗马。意大利许多伯爵和主教的使者也加入了他的使者行列，其中包括米兰大主教沃尔伯特，他亲自来到奥托面前。埃斯特家族的祖先奥伯特也来了。

德国国王接受了来自意大利的邀请，意大利向他提供了帝国王冠。他重新开始了大胆的阿努尔夫的工作。在沃尔姆斯，他首先为年幼的儿子确保了德意志的继承权，然后率领一支强大的军队经由特伦托下了阿尔卑斯山。当被伦巴第人遗弃的国王们还留在城堡里时，他在帕维亚庆祝了961年的圣诞节，并派富尔达的哈托（Hatto of Fulda）先行出发，自己则前往永恒之城。他于 962 年 1 月 31 日抵达罗马，在尼罗河草地上安营扎寨。他是根据与教皇签订的条约来到罗马的；通过承担保护和恢复教会的职责，他获得了卡洛林帝国的权利，但有一些限制。"当我奉上帝的旨意来到罗马时（这是他的誓言），我将尽我所能提升教会和教会领袖您的地位；您的生命、肢体或尊严绝不能因我的意愿或所知而受到冒犯：在罗马城，未经您的授权，我不会对您或罗马人应得的东西做出任何安抚或决定。我将把使徒彼得的财产归还给您。无论我把意大利王国交给谁，都要让他发誓，他将尽其所能帮助您保卫教皇国。

因此，奥托一开始就非常谨慎；不要忘记，他面前的是长期统治全国的阿尔贝里奇的罗马人。虽然他宣过誓，作为皇帝，他可以不受限制地掌握普拉西塔的主动权，但这一条约并不等同于帝国宪法，因为帝国宪法尚未制定。

2 月 2 日，奥托在加冕仪式上以帝王之尊进入莱昂尼纳。只有阿尔贝里奇的蔑视者们还笼罩在阴郁的沉默中；在这些罗马人的脸上，他读出了杀气腾腾的怨恨，他是来从他们手中夺取自由和权力的。在加冕骑马出发之前，他对他的佩剑手安斯弗里德-冯-洛文说："当我今天跪在使徒墓前时，请永远把你的剑举过我的头顶，因为我很清楚，我的祖先经常经历罗马人的不忠。智者通过谨慎来避免灾难；当我们回到蒙斯高迪时，您可以随心所欲地祈祷"。奥托和阿德尔海德在使徒彼得教堂举行了前所未有的隆重加冕仪式。就这样，在空缺了37年之后，皇帝的职位重新获得了延续，从意大利民族中撤出，在萨克森国王的异族部落中建立起来。查理曼大帝最伟大的继承人之一是由一位罗马人加冕的，奇怪的是他的名字叫屋大维努斯；但这一重大事件缺乏真正的尊严和神圣性。查理曼从一位可敬的老人手中接过了帝国的王冠，而奥托大帝则是由一位放荡不羁的年轻人膏封的。然而，德意志和意大利的历史却因为这次加冕而走向了新的方向。

当查理曼大帝的帝国建立时，它在人们的想象中具有很高的合理性；在伟大的法兰克君主制中，各民族仍然软弱地并肩站立，而查理曼大帝则被视为新的基督教共和国。从拜占庭统治下解放城市、以强大的基督教力量对抗可怕的伊斯兰教的需要，以及教皇的需要，这些都促成了加洛林帝国的建立。但是，这个神权帝国在内部发展的催促下分崩离析。新旧、罗马和日耳曼因素交织在一起，社会的发酵使第二帝国支离破碎；封建制度从官员中产生了地方世袭的王公，世俗权力与精神权力结合在一起，财产和法律的持续革命在君主制的躯体中产生，继承权的划分加速了它的解体。各民族开始激烈分裂；欧洲中心分裂成两个敌对的集团，帝国在存在了 150 年后解体，陷入了与建国前相似的状况： 新的野蛮人、诺曼人、匈牙利人、斯拉夫人、萨拉森人的入侵；各省的荒芜，科学和艺术的衰落；习俗的野蛮；教会倒退到查理时代的水平，教皇的力量减弱，教皇失去了精神力量，也失去了皮蓬和查理建立的国家；在罗马，贵族派别的野性比利奥三世时代更加危险。意大利人曾试图使罗马帝国民族化，但这一努力以失败告终，教皇本身再次寻求通过一个远离意大利和罗马的外国王朝恢复皇权来拯救自己。

罗马帝国现在由德意志民族复兴，但各族人民再也无法完全回到查理曼时代的理想中去了。帝国的传统仍在蓬勃发展；德意志发出了许多声音，哀叹帝国的衰落，希望帝国的复兴能给世界带来福音；但一个半世纪的不幸历史已经削弱了人民对这一制度的敬畏之情。查理曼君主制的统一已不复存在；法国、德意志和意大利已经成为了各自独立的国家，每个国家都试图以独立的政治形式展现自己。现在，奥托一世正在恢复帝国，很显然，这一任务可以由一位伟人来完成，但软弱的人格永远无法与封建制度、教皇和民族的斗争相提并论。总的来说，罗马帝国只是作为一种人为的、理想化的政治形式重新建立起来的，尽管它总是伟大的。这位匈牙利人、斯拉夫人和丹麦人的征服者，法国和勃艮第的守护神，意大利的领主，基督教的英勇传教士（他为此征服了更多的领土），理应被称为新的查理。甚至他的国家仍被称为法兰克帝国，他的德语仍被称为法兰克语。现在，他把罗马帝国的权力永久性地带到了德意志民族，这个充满活力的民族承担起了光荣而又吃力不讨好的任务--成为世界历史的地图集。德意志的影响很快导致了教会的改革和科学的复兴，而在意大利，正是日耳曼人自己创建了城市共和国。诚然，德意志和意大利是古代日耳曼最纯粹的代表，也是人类思想帝国中最美丽的省份，是历史的必然使它们结成了这种长久的关系；因此，我们的子孙后代决不能惋惜罗马帝国像命运一样降临到我们的祖国，迫使它在阿尔卑斯山以外的地方流血流汗数百年，以便为欧洲的总体文化奠定基础，而现代人类在本质上要归功于德意的结合。
