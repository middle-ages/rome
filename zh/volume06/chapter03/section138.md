_3\. 约翰十二世回国 利奥八世逃脱。他在一次会议上被废黜。约翰向他的敌人复仇。罗马人选出了本笃五世，奥托带领利奥八世返回罗马。本笃五世被废黜并流放。教皇向德意志皇帝屈服。利奥八世的枢密院__。

匆忙被召回罗马的约翰十二世率领一支由朋友和附庸组成的军队抵达罗马，利奥八世发现自己瞬间被抛弃了。他带着几个同伴逃到了卡梅里诺，向皇帝求助。后者已经把在圣利奥向他投降的贝伦加尔和威拉送到了班贝格，阿达尔贝特最后的努力对他来说不可谓不可怕，但他并没有立即前往罗马，也许是因为他已经遣散了许多部队，首先要集结新的部队。与此同时，约翰十二世对他的敌人进行了猛烈的报复。2 月 26 日，他在圣彼得大教堂召开了一次会议。在出席会议的十六位主教中，有十一位是签署了废黜他的命令的主教；他们可以正确或错误地把参加奥托会议说成是被迫的，红衣主教们也可以这样做，参加约翰会议的神职人员人数很少，而且他们还参加了两次取消会议，这表明罗马教会陷入了无可救药的混乱之中。约翰宣称，他曾因皇帝的暴行而被流放两个月，现在已经回到了他的教区；他谴责了废黜他的宗教会议。阿尔巴诺和波尔图斯的主教承认为利奥进行了未经圣约的祝福；他们被停职。曾授予利奥所有教会圣职的奥斯提亚的西科被解除了职务。

约翰十二世之后 约翰十二世谴责利奥后，他对许多著名的反对者进行了报复；他砍掉了红衣主教约翰的鼻子、舌头和两根手指，砍掉了原主教阿佐的手。当他邀请奥托到罗马时，这两人都曾是他的使节。他鞭打了施派尔的奥特格尔主教，但他抑制住了复仇的欲望，然后把他送到了皇帝那里，因为他不想激怒皇帝。与此同时，奥托正在卡梅里诺与教皇一起庆祝复活节；他准备向罗马进军，但在到达罗马之前，他被告知约翰十二世已经去世。如果某些报道属实，那么这位教皇的结局与他的生命相称：一天夜里，他在罗马城外因通奸的欲望被魔鬼带走，魔鬼的代理婚姻是一位受侮辱的丈夫。恶魔击中了他的头部，约翰在八天后于 964 年 5 月 14 日去世。 还有人说他中风了，这很有可能是因为他的精神受到了极大的刺激。因此，这位光荣的阿尔贝里希之子最终成为了自己放荡行为的牺牲品，同时也是他作为王子和教皇所处矛盾境地的牺牲品。他的年轻、他与那位伟大罗马人的血缘关系、他的悲剧性冲突都使他稍稍获得了减轻处罚的机会。

约翰死后，罗马人违背了逼迫他立下的誓言；他们不再承认2月26日被废黜的利奥八世为教皇，再次试图反抗皇帝。经过激烈的派系斗争，红衣主教本尼迪克特当选，并得到了民兵们的赞誉；他是一个值得尊敬的人，在罗马的野蛮统治下，他赢得了罕见的 "Grammaticus "称号。他作为约翰十二世的控告者签署了废黜书，但他也出席了谴责帝国教皇的二月宗教会议。罗马人认为他是一个能够勇敢地捍卫教会自由、对抗帝国权力的人。在皇帝的禁止下，当选的教皇被祝圣，并以本笃五世的身份登上宗座。

罗马人民的使者匆忙赶到里耶蒂，向奥托通报了新教皇的当选，并请求他予以确认。他告诉他们，他将带领合法的利奥教皇返回罗马，如果罗马拒绝服从他，他将惩罚罗马城。现在，他启程前往罗马。罗马境内的地方被他的士兵洗劫一空，城市本身也被包围了。当奥托站在罗马城前要求投降并交出本尼迪克特时，他被允许以一个要求臣服于他的城市服从他的皇帝的面目出现；但罗马人只能从他身上看到一个专制者，他是来剥夺他们最后的独立余地，即他们历来行使的教皇自由选举权的。约翰十二世的恶名已经湮灭，一位虔诚的人被选为他的继任者，并请求了帝国的确认。但奥托是否可以让利奥八世下台？罗马人是否可以在不宣布自己值得奴役的情况下，不试图对新皇帝行使旧的选举权？他们的教皇爬上城墙，劝说守城者进行抵抗。仅饥荒就开始在城中肆虐，几场暴风雨更是将被围困者的勇气震慑到了极点。6月23日，他们打开城门，向本尼迪克特五世投降，并在圣彼得墓前再次宣誓服从；他们本以为会受到残酷的惩罚，但皇帝却大赦天下。

利奥八世登基后，在奥托的授意下在拉特兰召开了一次会议。不幸的罗马教皇身着教皇袍被带进了会议厅；阿基迪亚科努斯问他，既然他的主人、他自己在约翰被废黜后共同推选的教皇利奥还活着，他凭什么要戴上最神圣的尊严的徽章；他被责备违背了对现任皇帝和主人的誓言，即未经他的同意不得推选教皇。"如果我缺席了，"本笃喊道，"请怜悯我，"他恳切地伸出双手。奥托泪流满面：罗马教会曾经是尼古拉一世统治下国王们如此可怕的法庭，如今却躺在帝国的福森脚下。他在宗教会议上为本尼迪克特求情，本尼迪克特拥抱了他的膝盖。随后，利奥八世将反教皇的披带一刀两断，从他手中夺过铁杖并将其折断，命令他坐在地上，剥去他的教皇法衣，剥夺了他所有的精神尊严；为了取悦皇帝，他给他留下了执事的官阶，并判处他永远流放。

长期以来，教皇之位一直被城市中的各派所占据；甚至有妇女被任命为教皇，对神圣职位的亵渎在马罗齐亚的孙子身上达到了最深刻的程度。因此，皇帝从粗鄙的贵族手中夺回了教皇选举权，为教会做出了真正的贡献。罗马的混乱使他成为了一个独裁者，因此他将教皇选举视为一种皇权，在德意志，他习惯于任意任命主教。从来没有一个皇帝取得过类似的胜利。通过他和他的一些继任者（他是他们的楷模）的个人权力，教皇成为了帝国的附庸，罗马教会成为了德国的附庸。帝国的权力上升到了一个可怕的高度，但被伟大统治者的威严所压制的罗马教皇却进行了报复，不仅重新获得了失去的自由（这就是事物按照自然规律变化的过程），而且还付出巨大的努力克服了自身的局限。教会与德意志帝国的斗争是中世纪的主要行动，也是中世纪历史上震撼世界的大戏。

罗马人维护选举权的尝试值得称赞，但却因更高的需要而沦为牺牲品，因为日耳曼王国必须暂时夺取罗马和教会的权力，以便对其进行改革。受辱的罗马城已经接受了皇帝作为它的主人，帝国教皇也已复位；因此，现在奥托很可能不再满足于宣誓，而是下令罗马人完全放弃选举权，而他的创造者利奥八世同意执行这一命令。这样一份文件以十一世纪的不完善版本保存了下来；只是它的真实性值得怀疑，而且明显的有利于帝国权利的伪造使其真实内容变得面目全非。
