_5\. 西尔维斯特二世开始担任教皇。 奥托三世捐赠。 十字军东征初露端倪。匈牙利成为罗马教会行省。奥托三世在阿汶丁山。他的神秘主义 他返回德意志。1000 年返回意大利。 西尔维斯特二世的艰难处境。 台伯河岛上的圣阿达尔贝特大教堂_。

西尔维斯特二世展现了他想成为教皇的精神。法国国王罗伯特被迫放弃了一桩不符合教规的婚姻，叛逆的伦巴第人阿尔杜因被放逐；主教们被告知，新教皇决心毫不留情地惩治淫乱和不道德行为，这样主教职位就能再次一尘不染地凌驾于国王的权力之上，而国王的权力就像普通的铅比黄金的光泽更耀眼一样。当需要实施格里高利五世寻求的教会改革时，西尔维斯特从奥托那里得到了最有力的支持；他需要奥托来实现这一崇高目标，同时也需要奥托在罗马维护自己的地位。当他决定为教皇建立一个新的世界统治权时，他发现身边有一位年轻、渴望荣耀的皇帝，他沉醉于古代辉煌的理想，希望为自己开创一个帝国的新纪元。因此，这位世故的大师和他浪漫的学生之间的关系非常奇特，因为他们的思想本质上是相互冲突的。奥托三世认为，他是皇帝，他立过两位教皇，他必须继承祖父的事业。当他慷慨地把教会宣称拥有的罗马涅的八个郡送给教皇时，他表达了这些原则。他宣称罗马是世界之首，罗马教会是基督教之母，但教皇们为了金钱而挥霍教会财产，使罗马教会的辉煌大打折扣。他还说，在法制混乱时期，教皇们根据君士坦丁的虚假捐赠篡夺了帝国的部分领土，而秃头查理的捐赠同样也是虚假的。他鄙视这些捏造，但他将佩萨罗、法诺、西尼加利亚、安科纳、福松布罗内、卡格利、杰西和奥西莫等郡交给了他的老师，并让他担任教皇。这一宣言可能是受到了他的大臣们这些严肃的人的启发，它显示出了一种帝国意识，能让西尔维斯特感到恐惧。

他小心翼翼地避免破坏这位贵族青年最喜欢的梦想，因为当奥托把他的老师提升为教皇时，他希望在他身上找到自己思想的推动者，只有死亡才使他免于最痛苦的失望。西尔维斯特打算教育这位年轻的狂热者，并通过他彻底恢复教皇国。他同意皇帝长期居住在罗马，因为这可以让他免受叛军的侵扰。他处处奉承奥托；他是世界君主，意大利、德意志、法国和斯拉夫国家都对他言听计从，他比希腊人甚至是希腊后裔都要聪明；因此，他激发了年轻人的想象力，而他们同时又被古代和修道主义所迷惑。

然而，西尔维斯特二世所受的高等教育使他超越了他所处的时代，他也分享了这个时代的某些倾向，因为他是这个时代的儿子。值得注意的是，他第一次向基督教发出了从异教徒手中解放耶路撒冷的号召。当时，教会和帝国都在庆祝新的胜利：保加利亚的损失被改宗的萨尔马特人所取代；波兰成为了罗马帝国；野蛮的匈牙利人，直到不久前还是意大利最可怕的蹂躏者，后来被德国人的武器所征服，臣服于罗马崇拜和德国的教会与国家机构。贤明的斯蒂芬王子的使者阿纳斯塔修斯（Anastasius）或阿斯塔里克（Astarik）出现在西尔维斯特面前，让他以王室的尊严奖赏皈依罗马的匈牙利人。教皇很高兴地将一顶王冠交到了特使的手中；这是按照奥托的意愿进行的，他将王权赐予了一个希望成为帝国附庸的人，但通过在罗马获得教皇的祝圣，他的王室尊严似乎来自教会的力量；教皇已经拥有了为皇帝加冕的权利，这也是他第一次将王冠作为彼得的礼物赐予一位外国王子。从那时起，这座城市也成为了和平的马扎尔人的家园，斯蒂芬在圣彼得大教堂为他们建立了朝圣者之家，同时他还建立了一所匈牙利神学院，如今它与日耳曼学院（Collegium Germanicum）合二为一。人们仍在圣彼得教堂的 S. Stefano degli Ungari 教堂中缅怀这位匈牙利第一位国王，朝圣者之家曾坐落于此；而匈牙利的教堂则是帕里奥内地区皮西努拉的 S. Stefano 教堂，据说那里曾坐落着供奉斯蒂芬主教的古老学院。

匈牙利的皈依是阿达尔贝特传教的结果，奥托开始将他奉为自己的守护神。他喜欢圣人曾居住过的阿汶丁山修道院，增加了修道院的物品，甚至把自己的加冕袍捐献给了圣坛。他在修道院旁的一座建筑中建立了自己的宫廷城堡，并在 "修道院旁的宫殿 "中确定了一些文件的日期。罗马没有哪座山比现在已经完全荒废的阿汶丁山更热闹了；除了圣玛丽亚修道院、圣博尼法齐奥修道院和霍夫堡（这里的圣人和贵宾从不空闲），那里还有许多漂亮的宫殿，空气被认为特别健康。

奥托给自己取了古罗马凯旋者的名字，如 Italicus、Saxonicus 和 Romanus，他还称自己是耶稣基督和使徒们的仆人；他声称自己最崇高的任务是让上帝的教会与罗马帝国和罗马人民的共和国一起繁荣昌盛。在这种思想的激励下，他不时陷入神秘的狂喜之中。希腊和罗马将他的灵魂提升到了理想的境界，但僧侣们又将其俘虏，使其陷入僧侣信仰的魔咒之中：因此，帝国青年的梦幻精神在凯撒狂热和忏悔者的弃世之间来回摇摆。他与沃尔姆斯的年轻主教弗兰科一起，在罗马圣克莱门特附近的一个隐士囚室里闭关了两个星期；然后，他在夏天去了贝内文托，并在苏比亚科的圣本尼迪克特修道院里再次自责。随后，他在教皇、罗马大人物和他的心腹、图西亚的休的陪同下前往法尔法；他愿意返回德意志，似乎已在那里安排好了他不在期间对意大利的管理，并任命休为他的副王。奥托的姨母玛蒂尔德在他不在的日子里有力而睿智地管理着德意志，她的去世让他感到悲伤，佛朗哥在罗马去世让他感到难过，他还在为失去阿达尔贝特和格里高利五世而哀悼，奥托于 1999 年 12 月离开了永恒之城，不久就得知了他的祖母阿德尔海德皇后去世的消息。德意志的事务在召唤着他；可怕的1000年即将来临，他发誓要去阿达尔贝特的陵墓朝圣。他带着几个罗马人，包括帕特里西乌斯-齐亚佐和几位红衣主教，而西尔维斯特则满怀恐惧地留在了罗马。教皇又给他写了一封信，劝他回去。"奥托回信说："我出于对您的崇敬之情，但迫不得已，意大利的空气对我的体质不利。我只是在肉体上与您分离，在精神上我永远与您在一起，为了保护您，我把意大利的王子们留给您。

克雷森提乌斯的征服者、教皇制度的缔造者、罗马帝国的复兴者受到了阿尔卑斯山另一边人民的热烈欢迎。他从雷根斯堡匆匆赶往格涅兹诺，在那里建立了波兰大主教区，然后前往亚琛。日耳曼民族罗马帝国的缔造者查理曼大帝被埋葬在大教堂的墓穴中，这位年轻的幻想家努力想成为查理曼大帝那样的人。在看到这位伟人的遗体时，他并没有意识到自己已经离开了他为德意志国王开辟的道路。

奥托早在六月就返回了意大利。基督教时代的一千年已经到来，但世界并没有像迷信的人类所预料的那样走向终结。相反，十一世纪对各国来说是利大于弊的。当奥托在伦巴第度过夏天时，罗马的叛乱情绪再次高涨；萨比纳蔑视教皇；在他去行使教会权利的霍尔塔，一场起义威胁着他，迫使他逃回罗马。他敦促年轻的皇帝返回。塔斯库勒姆的格里高利向奥托通报了该城的危险状况，奥托于 10 月率领一支军队抵达这里；德意志主教、巴伐利亚公爵亨利、下洛林公爵奥托和塔斯库勒姆的休随行。他搬进了自己在阿汶丁山的城堡，并决定将这里作为自己的永久居所。现在，他还让台伯河上的波尔图斯岛所属的波尔图斯主教为他在那里为纪念圣阿达尔贝特而修建的大教堂举行了祝圣仪式。他本想在世界各地为这位他所崇拜的殉道者建立庙宇，就像哈德良皇帝为他最喜爱的安提努斯所做的那样。他在拉文纳、亚琛和罗马都为他捐建了教堂。也许正是因为靠近阿汶丁山，奥托才选择了提贝里兰作为他崇拜圣人的地方。他曾住在阿汶丁山修道院，年轻的皇帝可以从阿汶丁山城堡俯瞰大教堂。岛上可能还有古代供奉阿斯克勒庇俄斯神的神庙遗迹，教堂就是在这些遗迹的基础上修建的。阿斯克勒庇俄斯神的儿子因此有了一个继承人，那就是神圣的野蛮人沃伊泰克或阿达尔贝特。如果您穿过修道院的小花园下到河边，河岸上长满了芦苇，您仍然可以看到曾经使这座小岛呈现出船形的石灰岩墙壁遗迹，以及蛇杖的石像，这让我们想起台伯河上的这座小岛被埃皮达鲁斯的圣蛇称为_Insula serpentis Epidaurii_。

奥托试图为他的圣人教堂寻找遗物珍宝。他向贝内文托市索要使徒巴塞洛缪的遗体，但市民们用诺拉的保利努斯的遗骨欺骗了他，奥托把这些遗骨带到了罗马，作为巴塞洛缪的遗体埋葬在那里。后来，当他得知这个虔诚的骗局时，他想为贝内文托报仇，但未能如愿。他创建的教堂被命名为圣阿达尔贝特和保利努斯教堂，并沿用了一段时间，但他的野蛮出身意味着这位波希米亚人在罗马并不感到自在，他只是通过帝国独裁者的行为才被引入罗马的崇拜中。罗马人很快就不再想知道关于他的任何事情；相反，他们声称使徒巴塞洛缪真的被埋葬在这座大殿里，因此他们以他的名字命名了这座大殿。1113 年，帕斯卡尔二世重建教堂时，在入口处仍然可以看到的碑文中不再提及圣阿达尔贝特。

这座大殿是罗马唯一一座奥托三世纪念碑。这座教堂经历了多次变迁，但其钟楼和中殿的 14 根花岗岩石柱可以追溯到奥托时期。
