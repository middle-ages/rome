_4\. 斯蒂芬八世教皇 939 年 阿尔贝里克镇压起义。马里努斯二世（Marinus II）教皇 942 年。被伊夫雷亚的贝伦加尔推翻。洛泰尔三世成为意大利国王。休与阿尔贝里奇和好。阿加皮图斯二世（Agapitus II）教皇 946 年。意大利国王贝伦加里乌斯 950 年，意大利人召见奥托大帝。阿尔贝里希将奥托赶出罗马。贝伦加成为奥托的臣子。954 年阿尔贝里希去世。

与此同时，利奥七世于 939 年 7 月去世，罗马人斯蒂芬八世继任教皇职位，由于阿尔贝里奇统治时期的教皇只在诏书上署名，因此历史上几乎没有提到这位教皇在位期间的事迹。有零星的消息称，斯蒂芬在一次起义中被肢解，因此在卑微的孤独中埋葬了自己的耻辱。如果这只是一个童话故事，那么它却揭示了当时人们对教皇的概念。

斯蒂芬八世的尊严归功于阿尔贝里奇；如果像后来的人们所认为的那样，他受到了王子追随者甚至是王子命令的如此严重的虐待，那么人们就不得不认为他参与了反对王子的阴谋。但即使提到了这样的阴谋，教皇的名字也没有被提及，而且他也不在阿尔贝里奇的惩罚之列。显然，罗马并不缺乏推翻统治者的企图。雨果从神职人员那里夺取了权力，许多嫉妒的贵族成员也对雨果的代理人言听计从，并收受贿赂。索拉克特》的编年史作者突然揭开了这些事件的面纱，但他只是模糊地让我们认识到了以主教本尼迪克特和马里乌斯为首的阴谋。甚至阿尔贝里奇的姐妹们也被认为参与其中，因为他告诉我们，其中一个姐妹出卖了计划，于是罪魁祸首受到了死刑、监禁和鞭刑的惩罚。阿尔贝里奇的强力武器胜利地镇压了神职人员和贵族；只要他还活着，就没有教皇敢伸出手来争夺世俗权力；相反，基督的代理婚姻顺从地登上了彼得的宝座，又悄无声息地从宝座上走下来。

斯蒂芬八世于 942 年去世后，阿尔贝里奇扶植马里努斯二世继位。这位影子形象持续了三年多，对亲王的命令唯命是从，"没有亲王的命令，这位温文尔雅、爱好和平的人什么也不敢做"。阿尔贝里奇还胜利地抵御了雨果的持续攻击，雨果从未厌倦过争取他在使徒彼得堡无法得到的帝国王冠。他早在 931 年就任命幼子洛塔尔（Lothar）为共同国王，并在 938 年与勃艮第鲁道夫二世的遗孀贝尔塔（Berta）结婚，以巩固自己的地位，但却将自己的儿子许配给了鲁道夫二世的女儿，也就是后来大名鼎鼎的阿德尔海德（Adelheid）。他寻求与拜占庭人结成更紧密的联盟；然而，尽管他让勃艮第人担任了最高级别的主教和伯爵职位，他在意大利的王位却摇摇欲坠。他的奸诈、暴虐的行为遭到了人们的憎恨；伦巴第的贵族们对他感到厌倦，而他对罗马的失败也使他的声望大打折扣。

941 年，他再次出现在城外，在圣阿涅塞（S. Agnese）设立了总部。也许整个冬天他都躲在城墙外，而克吕尼的奥多则再次试图促成和平。不是威胁，不是暴力，也不是欺骗性的承诺为他打开了城门。罗马人紧紧抓住阿尔贝里奇不放，他们眼看着自己领土上的城市和风景遭到破坏，但他们依然忠贞不二；历史学家柳特普兰德对破坏和国王贿赂的徒劳无功感到惊叹不已，他不得不将卑鄙的罗马的抵抗归功于上帝的暗中斡旋。

然而，休最终还是永久性地解放了这座城市，因为伦巴第爆发了一场风暴，他再也无法平息这场风暴了。尽管他竭尽全力，但仍无法赶走所有敌视他的大国。阿达尔贝特之子伊夫雷亚的贝伦加尔（Berengar of Ivrea）被休嫁给了他的侄女博索（Boso）的女儿维拉（Willa）；这位强大的侯爵本应受困于此，但他先是逃到了斯瓦比亚公爵那里，然后又逃到了德意志国王奥托（Otto）那里，从而避免了背叛。当他知道雨果脚下的意大利土壤已经被踩得足够深时，他立即于 945 年返回了意大利。许多主教立即宣布支持他，米兰向他敞开了大门，伦巴第人放弃了雨果的旗帜，以便从新的统治者那里获得主教辖区和郡县；但雨果派他和蔼可亲的儿子前往米兰，恳求大人物们至少把王位留给他，意大利人的政策就是这样，他们同意这样做是为了给贝伦加尔准备一个对手。由于休打算将王国的财宝逃往普罗旺斯，贝伦加尔亲自让他以聚集在米兰的伦巴第人的名义宣布，他们仍然希望承认他为意大利国王。然而，他很快就返回了普罗旺斯，留下他的儿子洛泰尔（Lothair）掌管虚构的意大利王权，过了几年不愉快的日子。

对罗马来说，这场动乱带来了和平。946 年，休放弃了所有要求，让阿尔贝里奇统治罗马城及其领土。自此以后，罗马王子的统治完全安全，而教皇则继续服从他的命令。马里努斯二世于 946 年 3 月去世，阿加皮图斯二世继位，他是罗马人出身，为人谨慎，担任教皇近十年。在他的领导下，教皇制度也开始得到加强，因为教皇重新与外国建立了许多关系，而这些关系在他的前任统治下都没有受到重视。德意志国王的势力进入了意大利，这使得罗马的一切都发生了变化，他们的力量已经衰竭到了无以复加的地步，在未来的许多世纪里，意大利的命运都将与德意志帝国紧紧联系在一起。

年轻的洛泰尔三世国王于 950 年 11 月 22 日在图尔姆突然去世，死于热病或贝伦加人的毒药。勃艮第党派随之垮台，意大利民族党派再次崛起，继续着吉多、兰伯特和贝伦加尔一世失败的尝试。12 月 15 日，伊夫雷亚的贝伦加尔继承了伦巴第王位，他还让自己的儿子阿达尔贝特作为他的共同国王加冕；就这样，意大利再次出现了两位本土国王，并有望获得帝国王位。贝伦加尔很可能希望把自己的儿子嫁给洛塔尔的妻子，以争取勃艮第人的支持，但他是否向洛塔尔的妻子提出过这样的建议还不确定。由于意大利王位前任的美丽遗孀是他怀疑的对象，他于 951 年 4 月 20 日将她囚禁在科莫，然后又囚禁在加尔达湖的一座塔楼上。但这个大胆的女人在阿达尔哈德主教的保护下逃到了雷焦，也许他把她送到卡诺萨城堡由阿佐或阿达尔伯特看管只是一个传说。突然，事情急转直下。阿德莱德、她在洛泰尔党内的支持者、贝伦加尔的敌人，尤其是米兰人、教皇阿加皮图斯（Agapitus），他们在罗马受到阿尔贝里奇的压迫，看到贝伦加尔的势力控制了主教区和五大波利斯，于是都把注意力转向了德国。他们没有为自己的国家制定国家法令，而是再次把一个外国人召到了意大利。

奥托身披战功，以其王室统治和智慧成为第二个查理曼大帝，他从德意志以武力进军。随着他的逼近，贝伦加尔的伦巴第军队被打散了：他向阿德莱德求婚，并于 951 年年底在帕维亚举行了婚礼。在他有力的臂膀下，年轻的伦巴第王后成为了意大利的象征。

奥托的父亲亨利一世是一位萨克森公爵，他建立了东法兰克帝国，并在与斯拉夫人、匈牙利人、丹麦人以及德意志部落王公的激烈战斗中建立了一个强大的民族国家。然而，帝国的理念在查理曼的国家体系消亡后依然存在，并在 936 年登上德意志王位的奥托一世身上找到了能够实现这一理念的英雄人物。意大利当时四分五裂，毫无力量；如果这个在道德和教育方面远远优于当时仍处于半野蛮状态的日耳曼人的国家在十世纪中叶有一位土生土长的伟大王子，他就能够实现这一目标。如果这个在道德和教育方面远远优于当时仍处于半野蛮状态的日耳曼人的国家能够像阿尔贝里希那样培养出一位伟大的本土王子作为国王，奥托就不会离开德意志了。

我们不知道阿加皮图斯是否在阿尔贝里希知情的情况下对他发出了传讯；我们猜测是的，因为削弱贝伦加尔对罗马王子来说肯定是好事，因为他预见到意大利国王雨果会再次对罗马发动进攻。然而，他和其他人都没有预见到奥托此举的后果。德意志国王已经下了阿尔卑斯山，看起来像是要去罗马朝圣。他打算根据罗马的情况来衡量自己的计划，并希望尽早在 952 年亲自访问罗马。他派美因茨和丘尔的主教前往罗马，在那里与教皇商谈他的入境事宜，可能还有更重要的事情；因为这些使者是给他而不是给罗马城的暴君的，但阿尔贝里希坚决拒绝接见他，这对这位罗马人的精力是一个不小的褒奖。这位伟大的国王遭到了所有罗马人元老的拒绝：他耐心地和妻子阿德莱德一起回到了自己的邦国。

贝伦加尔突然失去了所有的希望，很快就向奥托的意大利省督洛林公爵康拉德投降了。他和儿子一起出席了在奥格斯堡召开的帝国议会，并作为德意志的附庸接受了伦巴第王冠，而维罗纳和阿奎莱亚侯爵领地则被从意大利邦联中剥离出来，按照王室的意愿转交给了奥托的弟弟巴伐利亚公爵亨利。贝伦加尔屈辱地回到了自己的王国；奥托的剑从此在他的头顶盘旋，即使德意志的内乱让他多了几年的独立。他似乎主要居住在拉文纳。这座名城长期以来被帕维亚和米兰所掩盖，甚至几乎被遗忘，但后来却变得重要起来，并引起了皇帝们的注意。教皇和阿尔贝里奇的势力都没有达到意大利国王们逐渐从教会手中夺取的旧宗主国的遥远省份。

当这位杰出的罗马王子和元老离开历史舞台时，上意大利的情况就是这样。954 年，阿尔贝里希在权力鼎盛时期死于罗马，死因不详。命运眷顾他，没有让他看到自己的祖国在新的帝国枷锁下沦陷。当他感到自己的末日即将来临时，他匆忙赶往圣彼得大教堂（正如索拉克特编年史所记载的那样）；他让罗马的贵族们在使徒忏悔前宣誓，在阿加皮图斯二世死后，他将把自己的儿子和继承人屋大维推上教皇宝座。我们对此深信不疑：他清醒地认识到，从长远来看，罗马的世俗权力与教皇权是不可能分离的。但在阿加皮图斯的统治下，教皇因希望德意志的干预而获得了新的权力，奥托一世迟早会干预罗马的事务。阿尔贝里希意识到了这一点。因此，他说服罗马人授予他教皇王位，以确保他儿子的统治。这样，他至少有希望将罗马的权力留给自己的家族。

如果考虑到阿尔贝里希的统治在四个教皇任期的交替中持续了 22 年，考虑到他成功地抵御了教会的实际要求、习惯于无政府状态的贵族和人民的内部动乱以及来自外部的强大敌人的持续攻击，考虑到他的王权最终在他死后传给了他的小儿子，那么我们就必须承认这位 "元老 "在中世纪罗马所有公民中享有最高的荣誉。阿尔贝里奇是当时意大利的荣耀，因为他不愧为罗马人。他赢得了 "那个时代的伟人 "的称号，他的孙子们似乎也为自己是他的后裔而感到骄傲，并将这一称号赋予了他。他的血统并没有随着他或他的儿子屋大维而消亡，而是由许多成员继承，并在十一世纪作为塔斯库勒姆伯爵家族第二次统治了罗马。
