_4\. 十二世纪的古迹及其主人 世纪。罗马元老院开始注意保护它们。特拉扬的圆柱。马库斯-奥勒留的圆柱。十二世纪的私人建筑。世纪。尼古拉塔。罗马的塔楼_。

在十一世纪发生的不幸事件之后，我们完成了罗马城废墟的历史；在十二世纪，罗马也是战火纷飞，不难想象有多少古建筑被毁，尤其是在亨利四世和罗伯特-吉斯卡尔时期。当罗马再次恢复平静时，古建筑的遗迹为城市的重建提供了素材。没有权威机构负责保护古物，珍贵的大理石甚至雕像继续被扔进石灰窑。罗马继续被外国人当作珍贵材料的宝库加以利用。就像德西德里乌斯曾经把罗马柱运到卡西诺山一样，外国王子和主教现在肯定也在这样做。当他们来到这座城市时，他们热切地注视着古代最奇妙的雕塑，这些雕塑的遗弃邀请他们加以利用。与圣伯纳德同时代的圣德尼修道院院长苏格利乌斯承认，他曾在戴克里先巴登浴场和其他温泉浴场中看到最令人赞叹的圆柱，当时他就想用船把它们运到法国，因为他正在那里忙着重建他的修道院。如果说交通不便和其他情况阻碍了他这样做，那么可以想象，其他主教或城市也不会遇到这样的障碍。

不过，公共建筑理应属于国家所有，这一时期也有教皇将纪念碑借给私人或教堂的文件。大多数古代遗迹都转归私人所有；这使它们免于作为无主财产被彻底毁坏，即使所有者对它们的使用也只是损坏而非毁坏。塞普蒂米乌斯-塞维鲁的凯旋门就是这样处理的一个例子。1199 年，英诺森三世确认了圣谢尔盖和巴克斯教堂的部分所有权。"他的诏书中说："我们确认凯旋门的一半，凯旋门由三个拱门组成，其中一个更靠近你们的小教堂（其中一个塔楼建在它的上方），还有整个拱门中间的一半，小拱门旁边是司库"。然后又说，另一半属于某个西米努斯的继承人。因此，凯旋门有两个主人，它被完全封闭并加固，在其平台上还矗立着一座塔楼。

因此，教皇们继续将古建筑视为国家财产，人们还记得，教会也将圣天使城堡和万神殿一样视为自己的财产。罗马人获得自由后，城市本身也要求成为公共古迹的所有者，只要这些古迹还没有被罗马王朝改造成他们的塔楼宫殿。元老院承担了维护城墙的任务，教皇则必须每年为此缴纳一笔费用。因此，在古老的奥勒良城墙上，除了巴巴罗萨时代的中世纪元老的名字外，还有老皇帝和执政官的名字。1157 年，元老院在梅特罗比亚门（Porta Metrobia）重建了部分城墙，现在还能看到马拉纳塔上的纪念牌，上面写着这句话，并提到了当时执政的元老们的名字，但没有提到教皇。马拉纳河是一条溪流，从这座塔下方流入城市。

没有任何碑文记载元老或教皇修建了水渠；相反，古罗马的这些伟大工程被深深的沉默所掩盖。但在岛上的一座桥上，仍然刻着一位元老的名字。在 Pons Cestius 桥上，你可以读到这样的铭文："辉煌之城的最高元老贝内迪克特斯修复了这座几乎被摧毁的桥梁"。毫无疑问，是本尼迪克特-卡鲁索莫完成了这项工作。在亨利五世时期被罗马人摧毁的米尔维安桥也是由公社重建的，这一点从元老院写给康拉德的信中就能记起。

在这方面的另一个活动证明更加值得称赞。1162 年 3 月 27 日，也就是巴巴罗萨入侵不幸的米兰城的第二天，可能就在野蛮人开始摧毁这座城市的同一天，罗马元老院意外地决定保留特拉扬圆柱，"使它永远不会被摧毁或肢解，而是为了整个罗马人民的荣誉，只要世界还在，它就可以保持完好无损的屹立姿态。任何胆敢侵犯它的人都将被处以死刑，但其财产将归国库所有"。这座纪念特拉扬伟大战功的宏伟纪念碑当时属于圣西里亚库斯修道院的修女们，罗马元老院确认了这座修道院对圆柱及其脚下圣尼古拉小教堂的所有权，却没有想到这样的命运是不值得的。马库斯-奥勒留的圆柱也仍然理所当然地属于卡皮特的圣西尔维斯特修道院。这座修道院中庭的碑文上写道："因为属于圣西尔维斯特修道院的安东尼尼圆柱和旁边的圣安德烈教堂以及供品都被毁了。为了使这种情况不再发生，我们以圣彼得使徒和圣斯蒂芬、圣狄奥尼修斯和圣西尔维斯特的名义诅咒修道院院长和僧侣，如果他们承诺将柱子和教堂出租并从中获益，我们将以诅咒的契约约束他们。如果有人强行夺取我们修道院的柱子，就让他永远被诅咒为神庙强盗，并被永远诅咒。就这样吧！这是主教、红衣主教以及在场的许多司铎和平信徒的授权。彼得，蒙主恩典，本修道院谦卑的院长，与他的兄弟们，于主后1119年在第十二次诏书中执行并确认了这一诏书"。征税诏告"。

伴随着自由而来的是对古代的热爱、对古迹的崇敬以及对罗马从其祖先的作品中获得的辉煌的感知。伟人们也认为有必要通过建筑获得声誉，并增加城市的辉煌。正是本着这种精神，在元老桥（Ponte Rotto）上建造了一座塔楼，中世纪后期称之为 Monzone，而神话般的人们至今仍称其为彼拉多之家或 Cola di Rienzo。这座奇妙的建筑是一座桥塔（罗马所有的桥上都有桥塔），桥上的教学楼在这里升起，它自称是一座华丽的宫殿。如今，这座砖砌结构的建筑遗迹已成为罗马中世纪奇异私人建筑最杰出的纪念碑。墙角和小厢房将建筑分割开来，拱形入口朝向街道。房间内部有令人印象深刻的十字拱顶，从拱顶下部有石梯通往楼上。外墙装饰着古董碎片；用砖块砌成的粗糙半圆柱支撑着斑驳的楣板，楣板上时而可见大理石玫瑰花饰，时而可见阿拉伯花饰和神话人物的小浮雕。建造者的半身像（罗马又开始制作半身肖像了）最初被放置在入口处的一个外壁龛中，现在已经不见了，但与之相伴的夸张的碑文却被保留了下来。另一块长长的碑文以列奥尼克诗体书写了建造者及其家族的名字。碑文中的豪言壮语让人想起康拉德和腓特烈之前的罗马人的宣言，但碑文中以墓志铭的形式表达的对世间一切伟大都是徒劳无益的悲叹也不乏诗意的魅力。"这座房子的主人尼古拉斯深知世间的荣耀都是虚妄的。他建造这座房子，与其说是出于野心，不如说是为了重现古罗马的辉煌。在美丽的房子里，要记住坟墓，记住你不必在里面住很久。死亡乘着翅膀而来。没有人的生命是永恒的。我们的停留短暂，我们的飞行轻如鸿毛。纵然你躲避狂风，纵然你百次锁门，千次守卫，但死神仍在你的睡梦中徘徊。如果你住在几乎接近星空的城堡里，死亡这个猎物会更快地把你从城堡里夺走。巍峨的房屋耸入星空。它的山峰是由第一人中的第一人，伟大的尼古拉斯从地下拔起的，以延续他父辈的辉煌。他的父亲名叫克雷斯肯斯，母亲名叫狄奥多拉。他为他亲爱的孩子建造了这座著名的房子，并把它送给了大卫，大卫就是他的父亲"。

毫无道理，这位建筑者被视为克雷森斯家族的一员，实际上就是奥托三世时期著名的克雷森修斯本人。据我们所知，这个家族中没有尼古拉斯。创造出如此奇特建筑的罗马艺术与佛罗伦萨乔托的塔楼相去甚远，就像索拉克特的本尼迪克特编年史与维拉尼斯的编年史相去甚远一样。这座建筑的建造年代并不确定，但除了历史背景之外，碑文的精神也证明了它是在十一或十二世纪建造的。该宫殿建于公元 11 世纪或 12 世纪。这座男爵宫殿的风格似乎更加野蛮，因为在它的附近有两座保存完好的古朴美丽的罗马小神庙。与这两座神庙相比，建筑师应该感到羞愧，但他的建筑在完工后一定比当时的罗马更胜一筹，而且绝不缺乏宏伟壮丽的外观，当然也不乏如诗如画的效果。罗马执政官曾为这座建筑题词，认为它与拉美西斯的作品相得益彰，但如今只有塔楼的废墟这一最小的遗迹屹立不倒，而建筑者的虚荣心则被一座牛棚和干草棚所嘲弄，它们被布置在第一人的高大房屋中。

如果皮耶罗尼家族和弗兰吉帕尼家族的宫殿保存至今，我们眼前就会出现这样梦幻般的建筑。在那个时期，罗马到处都建起了塔楼，有的是重建的，有的是在旧砖砌的石碑上建造的。再也没有一座凯旋门的顶部没有塔楼了。仅弗兰吉帕尼人的堡垒就用过提图斯和君士坦丁的拱门以及几座雅努斯拱门。在离提图斯拱门不远处的帕拉丁山脚下，萨克拉大道的右侧，矗立着他们帕拉丁城堡的主塔--Turris Cartularia，《奇迹之书》说它建在阿斯克勒庇俄斯神庙上。十一世纪时，这座塔里存放着教皇档案的一部分，被称为 "Cartularium iuxta Palladium"，这座塔也因此被命名为 "Cartularia"。据说，马克西姆斯广场也曾从弗兰吉帕尼的塔楼上向外眺望；那里的一座拱门使他们家族的一个分支得名 de Arco。

当时，意大利所有城市都热衷于建造塔楼。比萨有如此多的塔楼，以至于图德拉的本杰明被允许将其数量夸大到 10,000 座。威尼斯的圣马可高塔、博洛尼亚高耸入云的阿西内拉塔和加里森达吊塔，以及比萨大教堂宏伟的吊塔，至今仍是那个城市自由和城市纷争时代的纪念碑。在罗马建造的塔楼很少像圣尼古拉塔那样珍贵或经过精心装饰；通常，它们都是昙花一现的建筑，很容易被摧毁，又很快被重建。这座城市还部分保留着中世纪的塔楼，它们都是用烧砖砌成，方方正正，没有塔尖，没有结构；它们大多是从城堡宫殿中拔地而起的。如果根据奇迹的统计，城墙上有 360 多座塔楼，如果想象一下无数的教堂钟楼、家族塔楼和众多高耸入云的古代遗迹，就能想象出这座今天如此宏伟壮观的城市在中世纪时的面貌。这片由黑暗和威胁性的塔楼组成的森林赋予了这座城市一种蔑视和好战的特征，甚至给最强大的皇帝也留下了深刻的印象。

但这座城市本身在十二世纪时却呈现出一种壮观的景象。但这座城市本身所呈现出的混乱废墟和荒野景象，即使是最生动的想象力也不足以将其想象出来。诺曼大火之后，山丘变得越来越荒凉；南方肆虐的力量用植被覆盖了山丘；昔日的街区变成了田野，疯狂的沼泽蔓延到低地。人们挤向台伯河和福森宫脚下的马尔斯广场，那里又恢复了自由；在被瓦砾堆、破碎的大理石庙宇和纪念碑打断的迷宫般的小巷里，坐着野蛮的罗马人，他们人数不多，却强大到足以把教皇赶走，把皇帝从奥勒良的古城墙上扔回去。
