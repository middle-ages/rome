### 第六章

_1\. 伦巴第诸城与腓特烈的斗争。帕斯卡尔三世在罗马 卡利斯图斯三世 塔斯库勒姆向教会投降。罗马人不让亚历山大三世进城。伦巴第人在莱格纳诺的胜利。腓特烈与教皇谈判。威尼斯会议与和平。亚历山大三世与罗马媾和。他凯旋进入拉特兰。

如果说腓特烈一世在罗马城前的灾难之后立即继续与城市作战所表现出的不可动摇的勇气令人钦佩，那么他的盲目性则令人惋惜。很快，这位英雄就会痛苦地希望自己能像亚历山大大帝一样，从未见过意大利，而是为遥远的亚洲而战。1168 年春，他被迫逃亡离开伦巴第。当他在与当时的强者的斗争中耗尽了帝国的力量时，教皇与之结盟。奇怪的巧合使共和国的自由受到了教会的保护，而后者的自由又受到了前者的保护。如果促进公民自由是教会的自愿行为，那将是教会的无上光荣。但教皇们在罗马反对它，因为它在罗马寻求皇帝的保护以对抗教皇；同时，他们又在伦巴第支持它，因为它在伦巴第寻求教皇的支持以对抗皇帝。然而，始终是民主的胜利将教皇从分裂和帝国独裁中拯救出来。

伦巴第同盟反对腓特烈的斗争，以高贵的希腊精神的纯粹辉煌点缀了意大利几个世纪。在如此黑暗的时代之后，公民自由的强势绽放是中世纪最美丽的现象。只有罗马城仍然注定要滚动西西弗斯的石头，痛苦地与比自己更强大的命运抗争。与伦巴第人的英勇斗争形成鲜明对比的是，罗马人不断地与邻近的小城镇开战，他们要向这些小城镇报仇雪恨。1168 年 4 月，在美因茨的克里斯蒂安和帝国长官的帮助下，他们摧毁了阿尔巴诺。尽管发生了八月的大灾难，但两人仍在罗马领导着德意志党派，而反教皇也从维泰博回到了罗马。帕夏尔三世曾在梵蒂冈住过一段时间，元老们为了释放人质而收留了他，但他们禁止他进城。他不得不到斯特凡-特巴尔迪（Stefan Tebaldi）的特拉斯特韦林塔楼避难，因为他害怕元老院换届，新的选举将于 1168 年 11 月 1 日举行。然而，他于9月20日在梵蒂冈去世，斯特鲁米修道院院长约翰接替了他的位置，成为卡利斯图斯三世。

罗马人嘲笑这两位教皇；虽然他们喜欢看到亚历山大三世流亡，但他们还是容忍他在梵蒂冈担任枢机主教。在这里，他努力争取他们的支持，而维特尔斯巴赫的康拉德则作为亚历山大的将军，从贝内文托威胁着拉丁姆。他的目标是塔斯库勒姆；罗马人听到这个名字气得发抖，他们和阿尔巴诺一样，想要摧毁这座要塞。康拉德被塞卡诺伯爵们赶了回去，未能如愿；塔斯库勒姆的最后一任领主雷诺不尊重教皇的权利，将被围困的地方换给了省长约翰。约翰占领了要塞，但罗马人猛攻要塞。省长逃走了，雷诺回来了，但不再被塔斯库勒姆的市民接受；相反，他们向教皇投降，希望从教皇那里得到保护，雷诺也将自己的所有权利让给了教会。于是，1170 年 8 月 8 日，著名的塔斯库勒姆归教皇所有。

亚历山大三世当时居住在维罗利，他与英国国王就坎特伯雷大主教托马斯的问题发生了激烈的争执，托马斯用金钱贿赂罗马的大人物，劝说他们投票支持他，但都徒劳无功。亚历山大收到了来自皇帝的使节，他希望和平，也收到了来自他召集的伦巴第城市的使节。希腊使节也带着新的请求赶来了；曼努埃尔-康奈诺斯（Manuel Komnenos）同意将自己的侄女嫁给教皇最大的臣子奥多-弗兰吉帕内（Oddo Frangipane）。这场婚宴在维罗利举行，但亚历山大三世没有同意希腊的提议。他与腓特烈的谈判也没有结果，但他现在希望能被罗马接受。1170 年 10 月 17 日，他率领部队进入了塔斯库勒姆。由于罗马人不让他进城，这位伟大的教皇不得不在这座与罗马遥遥相望的岩石城堡里生活了两年多。在那里，他得知了托马斯-贝克特在坎特伯雷被谋杀的消息，这一亵渎神明的事件很快就成为了他教皇权力最有力的杠杆；但是，当亚历山大在塔斯库勒姆接待英国神职人员和亨利国王的使者时，他正忙于处理教会最重要的问题，而他自己在拉丁要塞中的处境却与此形成了最明显的矛盾。美因茨的克里斯蒂安骚扰他，塔斯库勒姆人用很多钱才买到他的离开；罗马人骚扰他，痛恨他在保护塔斯库勒姆。最后，他们满怀诡计地向他提出了一个解决方案：他必须同意至少摧毁该地的部分城墙，然后他们就会接纳他回到罗马。八百名罗马公民宣誓遵守条约，但罗马人民却违背条约的规定，摧毁了这座令人憎恨的城市的所有防御工事。被出卖的教皇不愿返回罗马，他留在了空旷的塔斯库勒姆，然后在 1173 年初，无望地流亡到了塞格尼。

又过了几年，伦巴第人的一场大胜改变了一切。1174年9月，腓特烈重返与各城市的决战中：安科纳和新亚历山大城的英勇保卫鼓舞了勇敢市民的勇气，直到最后一场不朽的战役确保了他们的自由。1176年5月29日，在莱格纳诺，联合起来的市民民兵一举击溃了强大的皇帝，这一天是伦巴第共和国的马拉松；年轻的城市庆祝了历史上最纯粹的胜利之一：他们解放了自己，解放了祖国。当然，这场胜利的后果是皇帝与教皇达成了秘密协议，他向教皇派遣了和平使者前往阿纳尼，希望将教皇从各城市的优势中分离出来。为了达到这个目的，他放弃了曾经拒绝让与哈德良四世的基本帝国权利。就这样，自洛泰尔统治以来就已经失效的罗马帝国的皇权，被这位妄图恢复旧罗马帝国疆域的伟大皇帝放弃了。亚历山大急于从伦巴第人的胜利中为教会攫取一切好处，各城市怀疑他叛国。他乘坐西西里的船只从西潘托前往威尼斯，在费拉拉的一次会议上安抚了他们，并向他们郑重承诺不会在没有他们的情况下缔结最终和平。伦巴第的执政官们可以向他解释说，他是用言语或公文，而他们是用行动来对抗大敌的；但现在他们不得不满足于承担他们英勇努力的一半代价。

在所有大会中最引人注目的第一次大会上，外交官们还没有在绿色的会议桌上决定各国的命运，但自由城市的代表们第一次与皇帝和教皇一起独立行事，在著名的威尼斯大会上，亚历山大三世、腓特烈一世、各城市、希腊皇帝和西西里的威廉于 1177 年 8 月 1 日缔结了和平条约。卡利斯图斯三世被废黜，亚历山大三世得到承认，教皇国的所有权也确保归他所有。皇帝放弃了行政长官职位，承认教皇从此成为罗马和宗主国的独立主人。教皇将当时从阿卡彭丹特（Aquapendente）到塞普拉诺（Ceprano）的教皇国归还给了他，但斯波莱托（Spoleto）、安科纳马尔凯维特（Margraviate of Ancona）和罗马涅（Romagna）被教皇承认为毫无疑问属于帝国。伦巴第联邦城市获得了六年的休战期，在此之前它们已获得宪法的承认。

威尼斯和约》标志着意大利历史上一个伟大的时代，资产阶级在意大利蓬勃发展；它也初步决定了罗马的命运。然而，与皇帝和教皇的关系使这座城市的地位不如伦巴第城市。腓特烈不假思索地交出了他所承认的共和国，而他的美因茨将军克里斯蒂安（Christian of Mainz）现在甚至把武器借给了教会，以便根据条约将城市和财产归属于教会。当整个意大利都沉浸在和平的喜悦中时，罗马人却自顾不暇，失去了继续与罗马皇帝承认为罗马统治者的教皇作战的勇气。12 月中旬左右，亚历山大回到了阿纳尼；他知道自己的流亡生涯即将结束。七位高贵的罗马人给他带来了教士、元老院和人民的来信，邀请他回去。他不信任他们，又惦记着自己经历的苦难，犹豫不决；他派红衣主教和中间人到城里与人民达成协议。经过长时间的谈判，双方达成了协议：每年 9 月 1 日选出的元老将宣誓效忠教皇；圣彼得大教堂和教会的所有收入将归还教皇；所有前往罗马的人都将获得安全保障。罗马使者随后在阿纳尼跪拜在教皇福森脚下，并援引了条约。

亚历山大三世在坎帕尼亚流浪了十年之久，最终在克里斯蒂安大主教率领的德国士兵的带领下，经由塔斯库勒姆前往罗马。他于 1178 年 3 月 12 日抵达罗马，当天是圣格雷戈里节，元老院和地方行政长官、骑士和民兵在号角声中列队隆重迎接，全体人民手持橄榄枝，高唱颂歌。他的白色轿子只能缓慢地穿过挤在一起亲吻基督代理的脚的人群；直到傍晚，他才到达拉特兰门。然后，在人们的欢呼声中，他进入了教皇的古老所在地，并在那里向罗马人祝祷。复活节庆祝活动结束了教皇所经历过的最辉煌的凯旋盛宴之一。

世界上没有任何地方出现过这样的场面，与人性、人性的无能、人性的永恒和人性的持久有着如此悲惨的关联。教皇们在疯狂派别的枪炮声中出逃，又在欢呼声中被接见；教皇们的出出进进不断重演，使这座城市的历史具有了伟大史诗的严肃性，还有谁会比它更伟大呢？罗马似乎总是把自己变成耶路撒冷，而教皇则像救世主一样进入耶路撒冷，他自称是耶路撒冷的代理司祭，但精神上的谦卑和世俗的傲慢交织在一起，却无法消除这样一种想法，即基督的代理司祭正在重现旧皇帝的异教凯旋游行。特拉扬或塞维鲁在 1178 年 3 月 12 日一定会惊叹于罗马元老院和人民的变化，他们正在为一个骑着白色骡子的凯旋者欢呼，而这个人只是一个身着丝绸长袍的女司铎，没有佩剑。然而，这位司铎却像一位将军一样，从长期的战争中归来；在他的膝下，世界上最强大的人都谦卑了下来，胜过王子们在古代皇帝面前的谦卑。一位遥远的国王曾在一位被谋杀的主教的坟墓前，听命于僧侣对自己进行鞭笞；甚至罗马皇帝，一位凯撒式的英雄，也曾跪倒在地，亲吻亚历山大的福森，承认自己是被一位司铎所征服。
