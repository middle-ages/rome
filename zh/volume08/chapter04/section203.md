_2\. 黑暗世纪中的元老宫。它在政治上逐渐重生 看看它的废墟 朱庇特神庙在哪里？阿拉科利的圣玛丽。屋大维的异象传说。屋大维宫 元老宫_。

望着元老宫悲惨的废墟，人们很容易忽略这座古罗马帝国令人尊敬的所在地在黑暗世纪中的历史。然而，半个多世纪以来，夜幕一直笼罩着这个地球上最崇高的地方。自卡西奥多鲁斯之后，再无历史学家提及元老宫。只有《爱因西德恩的无名氏》（Unknown of Einsiedeln）中昙花一现地提到过它，只有传奇和传说迷迷糊糊地说起过这个世界奇迹。十世纪，卡皮托里奥的圣母修道院从无名神庙的废墟中诞生。那里的众多建筑废墟甚至没有被用作城市要塞；拥有塔尔皮岩城墙的古老阿克斯从未被提及作为与七神大殿和圣天使城堡并列的主要要塞。元老宫不再主宰任何一条主干道，因为该地区，尤其是古老的论坛，变得冷冷清清，居民们越来越深入火星地带，向具有重要战略意义的台伯河迁移。只有对这座元老宫曾经意义的不灭传说，才使它摆脱了历史的缺失，并在公民自由精神觉醒后，再次成为城市的政治首脑。早在十一世纪，元老宫就已成为所有纯粹城市事务的中心。到了奥托三世和贵族贵族时代，人们对罗马帝国神圣的马尔斯塔特（Malstatt）的记忆再次出现；元老宫的废墟在贵族和人民的集会中重新焕发生机，现在取代了特里亚法塔（Tria Fata）的位置。在本佐、格里高利七世和盖拉修斯二世时期，在总督选举的骚乱期间，在卡利克斯特二世选举的批准期间，召集罗马人参加议会或战车的地方总是元老宫。城市长官可能也住在元老宫，因为将维克多三世逐出罗马的亨利四世的长官就坐在那里，那里的一座宫殿曾是法庭的场所，法庭的行为用公式进行总结： 在罗马城的罗马公民行为_actum civitate Romana apud Capitolium_。

再大胆的想象力也无法描绘出这些废墟的阴郁壮观。在朱庇特神庙倒塌的柱子上，或在国家档案馆的地下室里，元老宫的僧侣、贪婪的执政官或无知的参议员可以坐在破碎的雕像和碑文之间，惊叹于废墟，思考命运的多变。看到这些废墟，他就会想起维吉尔在谈到元老宫时写下的诗句：

> 如今金碧辉煌，曾经荆棘丛生，林地诗意盎然；

而现在，元老宫又回到了它的原点，他应该反过来赞叹这首诗：

> 曾经金碧辉煌，如今却满目疮痍。

但当时大多数罗马人只知道维吉尔是从罗马逃到那不勒斯的魔术师，他为两座城市带来了神奇的艺术作品。元老们头戴高冠，身披金边斗篷，在废墟上走来走去，他们只是朦胧地知道，政治家的法律、演说家的宣言、国家的胜利和世界的命运都曾在这里得到表达。没有比这更可怕的对崇高的嘲弄了，罗马曾一度把元老宫作为财产送给了在废墟上种植卷心菜、祈祷、唱诗篇和用棍子抽打后背的僧侣。阿纳克莱图斯二世将元老宫的所有权授予了阿拉科埃利的圣玛丽修道院院长；他的诏书为这座由石窟、囚室、庭院和花园、房屋或小屋、废墙、石头和柱子组成的迷宫带来了一丝曙光。

当时，古老的克里沃斯（Clivus）仍然通向它，但也有小路从火星场一侧通向阿拉科埃利（Aracoeli）和元老宫广场。元老宫的废墟在亨利四世、吉斯卡德和帕斯卡利斯二世的暴风雨中不断扩大，一片荒凉。就像帕拉丁宫一样，那里也长出了花园，成群的山羊已经在大理石瓦砾上攀爬，元老宫的一部分因此得名 "山羊山"（_Monte Caprin_），就像广场变成了 "奶牛场 "一样。然而，卡皮托利尼广场上的摊位依然存在，罗马人长期以来一直在这里举行集市。除了圣玛丽教堂的修道士和圣谢尔盖与巴克斯的司铎之外。另一方面，古老的街道仍然环绕着山丘，如 Clivus Argentarius（马福里奥山），可能还有 Vicus Jugarius、Cannapara 和 Forum Olitorium（即今天的蒙塔纳拉广场）。

覆盖在元老宫顶部的神庙和门廊废墟如今已不复存在；在克利夫斯山上，只剩下土星神庙和维斯帕先神庙的最后遗迹、康考迪亚神庙的地基、档案馆未被摧毁的拱顶、桑塔学园（Schola Xantha）的寝室、演说厅和里程牌的遗迹，最后就是以平静的活力对抗岁月流逝的塞普蒂米乌斯-塞维鲁拱门。在十二世纪，所有这些古迹和其他古迹都被毁坏了。在十二世纪，所有这些古迹和其他古迹仍然呈现出一个杂草丛生的卫城的景象，从瓦砾堆中巍然耸立着一片柱林。对奇迹的描述转瞬即逝，但对这些废墟的描述就像一束玫瑰色的晚霞，我们没有关于当时的其他报道。读一读他们说的话是很有价值的：

"来自罗马的元老宫"

"Capitolium之所以被称为Capitolium，是因为它是整个世界的首脑（_caput_），因为执政官和元老们居住在那里，管理着城市和世界。它的正面覆盖着高大坚固的城墙，城墙上镶满了玻璃和黄金，还有奇妙的镶板。城堡内有一座宫殿，大部分是用黄金和宝石装饰的，据说价值相当于世界的三分之一；宫殿里的雕像和世界上的省份一样多，每个雕像的脖子上都有一个小铃铛。这些雕像是用魔法艺术布置的，如果罗马帝国的任何一个地区发生叛乱，它的雕像就会立刻转到那里；然后脖子上的小铃铛就会响起，然后元老宫的先知们（他们是那里的守护者）就会告诉元老院...... 那里还有几座神庙，因为在克里诺伦门城堡的高处，有朱庇特和莫内塔的神庙；在广场的一侧，有灶神和凯撒的神庙；那里有异教徒的神椅，元老们在三月六日在那里展示凯撒大帝。在元老宫的另一侧，坎纳帕拉之上，海格力斯公共论坛旁的朱诺神庙。在 Tarpejum，是庇护神庙，凯撒大帝就是在这里被元老们杀害的。在现在圣玛丽亚的位置，有两座神庙同时与宫殿相连，即菲比斯神庙和卡门蒂斯神庙，屋大维皇帝就是在这里看到了天堂的景象。Camelaria 旁边是守护元老宫的雅努斯神庙。所以它被称为黄金元老宫，因为它在世界所有王国面前闪烁着智慧和美丽的光芒"。

作为一份孤立的文献，阿纳克莱特的谕令与其说是满足了我们对知识的渴求，不如说是吸引了我们的想象力。因为古物学家的研究至今仍被罗马最黑暗的地形问题所折磨：元老宫朱庇特神庙的位置。自从汪达尔人掠夺了这座圣殿，抢走了它的屋顶后，它就被人们遗忘了。只有《奇迹》（Mirabilia）中再次纪念了这座神庙，在此之前，传说已经用一首最深刻的诗歌将元老宫神圣化了。罗马的主神殿是异教诸神崇拜的所在地，但却没有在万神殿之前很早就被改建成基督教神的大殿，这一事实将永远令人震惊，即使人们想用基督徒的憎恶和拜占庭皇帝的财产权来解释这一事实。

然而，尽管只是从昨天开始，我们才能够确定这座失落神庙的位置。当 Graphia 书中提到 "朱庇特和莫内塔神庙，金色的朱庇特雕像坐在金色的宝座上，矗立在城堡高地的 Crinorum 门廊之上 "时，我们今天仍然可以认出这个名称的门廊属于古老的奥利托里姆广场（Forum Olitorium）。中世纪的其他名称也支持朱庇特神庙位于西山（阿德奥托达二世）的观点；塔尔皮亚岩石的西部位置和神庙本身的位置在十五世纪已经被一些教堂证明是可能的。正如 S. Caterina sub Tarpeio 教堂的名字保留了人们对 Tarpeium 的记忆一样，人们也通过 S. Salvatore in Maximis 寻找 Jupiter Maximus 神庙。最后，自 1865 年以来在阿德奥达托二世花园进行的发掘工作确定了朱庇特神庙确实矗立在那里。

因此，阿拉科埃利的圣玛丽亚教堂取代其位置的观点不攻自破。然而，这是罗马人在元老宫山上修建的唯一一座教堂，它耸立在古阿克斯河上的一个显要位置。在利奥四世时期（约 850 年）的教堂和修道院的精确目录中没有提到它。由此可见，在这位教皇统治时期，它要么还不存在，要么只是一座不起眼的小教堂。

天坛 "这个姓氏在十四世纪之前还没有听说过，但它与一个古老的希腊传说有关，这个传说被收录在《罗马奇迹》中。当元老们看到屋大维难以形容的美貌和他称霸世界的喜悦时，他们对他说：我们想崇拜你，因为你身上有神灵。屋大维很沮丧，他要求缓一缓，并召来提布尔的西庇阿，把元老院的决定告诉了她。她要求休息三天，然后在如此漫长的斋戒之后向皇帝预言： > 审判的征兆；大地将被毁灭：

>"审判的征兆；汗水即将滴落大地、  
世纪之王从天而降"。

屋大维认真地聆听着西比勒的预言，突然天开了，难以忍受的光辉倾泻而下，他看到光芒四射的圣母站在天堂的祭坛之上，怀里抱着基督的孩子。一个天籁般的声音在呼唤："这就是圣母，她将迎接世界的救世主！" 另一个声音说："这是上帝之子的祭坛！" 屋大维匍匐在地，顶礼膜拜。他向元老们展示了他的异象，但当第二天人们决定称他为 "主 "时，他立即用手和嘴禁止了。他说，"我是凡人，所以我的孩子们也不能这样称呼我：

> "我是凡人，所以主这个称呼对我来说并不合适"。

这个美丽的传说还说，屋大维在元老宫为 "上帝的长子 "立了一座祭坛。在第十二章中，"神的长子 "的名字被改成了 "上帝之子"。根据这个传说，圣玛利亚教堂已经被标为 "ubi est ara filii Dei"，后来似乎又被改为 Aracoeli。然而，非常令人震惊的是，这个古老的传说丝毫没有将祭坛与朱庇特神庙联系起来，而只是告诉我们屋大维将它竖立在元老宫上或其上的一个高处。如果阿拉科埃利的教堂真的取代了旧神庙的位置，那么传说或传统中一定会纪念它。

因此，在中世纪，元老宫深沉的墓穴寂静只被修道院的钟声和传说打破。在西庇阿人、格拉奇人、马略人和苏拉人、庞培人和凯撒人的事迹和胜利的空荡荡的舞台上，盘旋着圣母玛利亚和耶稣圣婴、崇拜的屋大维和年迈的西比拉等梦幻般的人物，他们的神秘书籍曾经保存在这座元老宫中！

早在十一世纪，这个传说就已经与这里联系在一起，这使得 "屋大维宫 "作为本索故居的提法无可争议，因为只有在元老宫才能找到他。这座宫殿被认为位于阿拉科埃利修道院附近，如果能够准确确定其位置和用途，将会非常有价值。在对宫殿的简要概述中，《奇迹之书》没有提到元老宫上的任何宫殿；但他们含糊地提到了元老宫的一座宫殿，这座宫殿 "位于 "城堡内部，用黄金和宝石装饰得富丽堂皇，里面矗立着各省的响亮雕像。他们明确地将 "屋大维看到天堂异象的地方 "的宫殿与圣玛丽教堂联系在一起，因此它肯定是修道院建筑本身的一部分。最后，《罗马山摘要》（Summarium of the Mountains of Rome）特别提到 "元老宫或塔尔佩斯宫（Tarpeus）上的元老宫 "属于文士的存在。这三座宫殿几乎不可能仅指同一座建筑，因为元老宫上矗立着许多废墟，在中世纪它们被称为 "palatium"。如果在卡皮托利尼山的 XII. 如果朱庇特神庙的遗迹在十二世纪还存在，那么即使这些遗迹在当时也可以被称为 "宫殿"；但我们现在已经无法判断当时的情况是否属实。因此，在 Mirabilia 的三座宫殿中，元老宫是过去和神话中的宫殿，屋大维的宫殿、本佐的住所、阿拉科利修道院建筑的一部分建在古代废墟上，最后是元老宫，即中世纪真正的元老院宫殿，只有它能为我们确定。在元老宫映入眼帘的古迹遗址中，最令人印象深刻的莫过于共和国时期的旧州档案馆或所谓的 Tabularium 遗址，这里有佩佩林的巨大城墙、奇妙的大厅和拱形的司库。十二世纪的市政书记员在这里工作。十二世纪的城市文士，在粗略列举元老宫上的山峰时，只提到了元老宫，因此，他指的只能是那座雄伟的建筑。人们在看到这座奇妙的建筑时，会想象以前的执政官或元老曾在这里居住过，十二世纪的贵族们在阿拉科教堂之外还发现了元老们的宫殿。除了阿拉科利教堂本身，十二世纪的贵族们再也找不到比这里更合适的集会场所了，而人民在恢复元老院时也没有找到更合适的地方。因此，我们可以想象，即使在当时，Tabularium（后来成为真正的元老院）也是为此目的临时布置的。1143 年，罗马共和国的朦胧形象再次出现在这里，它梦幻般地漂浮在废墟之上，本身就是一个古老的传说或幻象，让无力的孙子着迷。
