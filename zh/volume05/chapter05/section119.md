_3\. 约翰八世于 872 年成为教皇，路易二世皇帝去世。德意志的路易之子和秃头查理为争夺意大利而争吵。875 年，秃头查理称帝。 罗马皇权衰落。秃头查理意大利国王。罗马的德意志派。贵族的过激行为。波图斯的福莫索斯

然而，在那个时候，教会仍然有幸由一些教皇继位，他们的活力丝毫不亚于那些将罗马从拜占庭枷锁下解救出来的教皇。当卡洛林王朝的宝座被越来越弱小的统治者占据时，在外交技巧、坚定性和力量方面都远胜于他们的人登上了彼得的宝座。

哈德良二世去世后，更强大的约翰八世于 872 年 12 月 14 日即位，他可能是罗马伦巴第人后裔贡多的儿子。路易二世皇帝是卡洛林王朝的最后一位皇帝，他思想活跃、计划周密，但几年后也去世了。他在下意大利进行了长期斗争，从萨拉森人手中拯救了王国并实现了统一，但却无法阻止封建原则和主教辖区豁免权必然带来的内部分裂，之后于 875 年 8 月 12 日在布雷西亚附近去世，葬于米兰的圣安博。他是中世纪第一位陷入意大利灾难性迷宫的皇帝，几乎成为意大利人，却在那里灭亡。他的死标志着帝国历史上的一个转折期，帝国随着他的死而失去了权力和尊严；因为帝国现在沦落为教皇和意大利大人物手中的傀儡戏，而意大利本身也陷入了矛盾之中，这种矛盾一直持续到今天，由于其地理位置，它成为了法国和德国之间争夺的焦点。

除了女儿伊尔明加尔外，路易没有留下任何后嗣。他的继承人法兰西的秃头查理和德意志的路易各自争夺意大利和帝国王位。支持德意志一方的皇太后于9月在帕维亚召开了一次御前会议，但会议没有取得成功。武力决定了一切。路易的儿子查理大帝和查理曼大帝得到了弗留利的贝仁加侯爵的支持，贝仁加侯爵是虔诚的路易通过母亲吉塞拉得到的亲孙子。他们一个接一个地从阿尔卑斯山下来与他们的舅舅作战，但舅舅用黄金和谎言让他们无计可施。教皇已经把王冠许配给了这位可怜的王子。哈德良曾秘密向秃头查理许诺，在皇帝死后，他将把王冠赐给除他以外的任何王子。将王位传给德意志民族国王的想法还很渺茫，或者说由于意大利与德意志的关系过于密切而显得很危险；因此，约翰八世毫不犹豫地决定支持法国一方，因为法国更强大，让他有希望得到强有力的支持，以对抗罗马的强者和可怕的萨拉森人。他通过波图斯的福莫索斯（Formosus of Portus）主教、韦莱特里的加德里库斯（Gadericus of Velletri）主教和阿雷佐的约翰（John of Arezzo）主教邀请秃头查理来罗马参加加冕礼。875 年 12 月 17 日，他在圣彼得大教堂受到教皇的隆重欢迎，随后在圣诞节当天加冕为罗马皇帝。

查理用巨额金钱收买了教皇和罗马人的选票，以至于他的德国敌人把他比作贿赂罗马元老院的朱古塔。由于他不像他的前辈们一样，通过皇父的意愿和帝国议会的选举获得帝位，他不得不屈尊以候选人的身份争取罗马贵族的选票，教皇也被允许用前所未有的语言，敢于在公开场合称罗马皇帝为他的造物主。我们不知道秃头查理与教会缔结的条约的全部细节。既然他是从一位慷慨的捐赠者手中得到王冠的，那么他所做的让步肯定是巨大的。如果一个无能的王子的捐赠能像虔诚的路易这位统帅级皇帝的捐赠那样有价值，那么这些捐赠很可能会在教皇的历史上留下浓墨重彩的一笔。帝国的威严随着查理曼大帝而低落，教皇的威严却随着查理曼大帝而高涨。查理曼和洛泰尔的宪法在罗马失修，皇权不复存在，或者只是一个虚无缥缈的名称；皇帝之位很快就成了教皇们的游戏，很快就成了大封建主们的游戏，很快意大利的伯爵们就能以查理曼的王冠为荣，他们从查理曼的帝国中脱颖而出，成为王室的附庸。

新皇帝在罗马只停留到 876 年 1 月 5 日。他匆忙赶往帕维亚，教皇也紧随其后，在这里，他不仅在意大利王国主教和大人物的集会上被确认为皇帝，还被选为意大利国王，并由米兰大主教安斯佩特加冕，而他在这个王国的前任只是由皇帝和一个非意大利帝国议会决定任命的。因此，秃头查理的当选标志着意大利历史上的一个转折点；它既表明了教皇、主教和意大利优等公民的权力得到了极大的加强，也表明了意大利北部民族情绪的明确出现。国王委托博索公爵管理意大利事务，他娶了博索公爵的妹妹里希尔达（Richilda）为妻；他亲自前往法国，于 7 月在庞蒂翁（Ponthion）举行的帝国议会上承认自己为皇帝，他身着华丽的拜占庭长袍，像封臣一样从教皇使节手中接过金权杖。

约翰八世征服帝国政权后，从帕维亚返回罗马，萨拉森人的进攻和罗马贵族的敌对态度召见了他。对帝国的胜利之后是如此无政府的状况，以至于很快就成了罗马教皇的悲惨失败，因为它不再有帝国的臂膀来保护它；罗马教皇的野心计划很少像当时经历的那样被如此尖刻的讽刺所嘲弄。城中有一个强大的、具有德国思想的党派，他们与太后、弗留利的贝伦加尔、托斯卡纳的阿达尔贝特以及斯波莱托和卡梅里诺的侯爵们保持着默契。她抵制查理的选举，寻求全面独立，并处处让教皇感到恐惧。这些大人物的性格与他们所处的时代的粗鲁相吻合，但如果同时代的人都称赞福莫苏斯主教是圣洁的人，那么对他们的指控的真实性就值得怀疑了。

波尔图斯的福莫索斯因其在保加利亚人土地上的使命而声名显赫，因其才华和学识而在神职人员中出类拔萃，却招致了多疑的教皇和许多红衣主教的憎恨。如果说他之前被派去邀请查理加冕，那么他要么是勉强接受了这一公使身份，要么是出于谨慎而屈从于此，隐瞒了自己倾向于德意志党派的情绪。人们担心，他之所以觊觎教皇的王位，是因为作为一个重要人物，他得到了一个庞大派系的保证。他离开波图斯主教辖区的原因不明。因此，他被指控与罗马人密谋反对皇帝和教皇。

城里的大人物们形成了强大的裙带关系。他们包括民兵将军或宫廷大臣、命名者格里高利、他的女婿乔治、康斯坦丁娜、他的女儿斯蒂芬（secundicerius Stephen）和军事长官塞尔吉乌斯（Sergius）。乔治谋杀了自己的妻子，也就是本笃三世的侄女，目的是与康斯坦丁娜结合；岳父格里高利的影响力和法官的贿赂使他免于任何惩罚。伟大的教皇尼古拉一世的侄子谢尔盖也抛弃了自己的妻子，效仿王室通奸者的做法，与他的法兰克妾瓦尔瓦辛杜拉生活在一起。这些亵渎神明的人被迫离开罗马，因为新皇帝的当选和教皇的回归，当时萨拉森人正游荡到罗马城门前。乔治和格里高利先是抢劫了拉特兰教堂和其他教堂，然后在夜里打开了圣潘克拉修斯的大门，逃到斯波莱托寻找藏身之处。这让教皇有理由指责他们想让穆罕默德人进入罗马；876 年 4 月 19 日，教皇在万神殿召开了宗教会议。在宣读了指控之后，他宣布如果这些罗马人和波尔图斯主教在某个日期之前不自首，就开除他们的教籍。由于没有自首，判决得以执行，福莫索斯也被剥夺了主教职位和所有教会学位。毫无疑问，他和逃亡的罗马人与斯波莱托和卡梅里诺的侯爵以及托斯卡纳的阿达尔贝特都有联系，因为我们很快就会看到他们在他们的保护下出现，但他们与萨拉森人背信弃义的勾结是不可能的，至少福莫苏斯必须被免除责任。
