_3\. 罗马发生暴乱 伯纳德被派往该城进行调查。利奥三世于816年去世。 利奥在罗马的建筑。当时的建筑和艺术特色。罗马的名义教堂和著名修道院_。

听到皇帝驾崩的消息后，教皇看到自己的福森面前打开了一个深渊。因为罗马人几乎还没有意识到这位伟大的统治者已经驾崩，他们就再次将仇恨发泄到了主教的民事权力上。如果要把教皇国自建立以来一千多年间经历的所有革命加在一起，其数量将是令人困惑的，即使是最大国家中一半的动荡也足以将其摧毁得无影无踪； 然而，教会国家一直延续至今，尽管对主教世俗权力的反叛在教会国家建立之初就已开始--这证明在这种神职与王权的混合中存在着难以容忍的矛盾，同时教会国家的存在本身也蕴含着与革命同等重要的原则。

坎普卢斯和帕斯卡利斯的追随者（这些罗马人在流亡中迷失了十四年）密谋反对教皇，但他们的意图被识破了。利奥毫不犹豫地处决了这些 "大不敬罪犯"，从此神圣的主教被迫双手沾满自己罗马人的鲜血。这些处决的消息甚至引起了查理虔诚的继任者的不满。路易皇帝认为，教皇如此迅速而严厉地执行死刑应该受到谴责，最重要的是，在他看来，教皇对罗马大人物的判决侵犯了他的帝国权利，而他的使者却没有被召见。同时，如果罗马人受到任何冒犯，他有责任保护他们的一切权利。因此，他派意大利国王前往罗马进行调查。伯恩哈德在这里病倒了，但格罗尔德伯爵向皇帝报告了他所了解到的情况。现在，教皇也急忙向罗马的首脑称义。他的使节们竭力为他洗脱罪名，因为这些罪名可能使伯纳德本人，无疑还有罗马人，登上了路易的宝座。815年，利奥的敌人们在他病重期间因这些事件而骚动起来。他们烧毁了教皇的庄园，包括旧庄园和利奥新建立的庄园。暴动的地点一般都在罗马城外；大罗马人武装了他们庄园里的殖民者和奴隶，骚动了乡镇，并威胁要进城迫使教皇交出从他们或他们被斩首的朋友那里没收的财产，并将他们打入使徒室。这次起义预示着罗马贵族的势力日益壮大，后来变得如此可怕。为了平息叛乱，伯纳德派遣斯波莱托的威尼格公爵率领军队进驻罗马。然而，816 年 6 月 11 日，教皇在悲痛中去世。

利奥三世在彼得的宝座上坐了二十多年，期间发生了许多大事。他以司铎的身份开创了人类的新纪元。他被罗马人憎恨，因为他夺取了这座城市的世俗统治权，被辱骂至死，被驱赶逃亡，被重新起用，被一再的叛乱弄得人心惶惶，但他没有屈服于他的对手。他是一个坚强的人，精明能干，敢作敢为；在圣彼得大教堂为西方新皇帝加冕的那一刻，使他成为了世界历史的见证者，并为他赢得了不可磨灭的名声。

利奥三世通过建筑为这座城市做出的贡献几乎超过了哈德良。罗马教会在卡洛林王朝时期焕然一新，如果把君士坦丁王朝视为第一个不朽时期的话，这是它的第二个不朽时期。由于当时的教皇们大兴土木，他们当然也是古城最狂热的破坏者之一。建筑艺术仍在继续。教会在四世纪、五世纪和六世纪已经建造了最伟大的建筑，但由于恪守教会的传统，教会的建筑艺术已无法与之相提并论，只能在较小的规模上进行模仿。他们继续使用古罗马建筑的柱子和装饰；新建筑只是从旧建筑中拼凑出来的。这就是为什么卡洛林王朝时期留下了许多华丽的教堂翻修工程，却没有在罗马建造独立的大型建筑的原因。从古老的大教堂模型来看，建筑艺术仍然保持着一定的水平，但教堂和修道院的数量不计其数，使得大型设计成为不可能。仅仅因为这个原因，在罗马卡洛林王朝时期的建筑中就能发现某种小气。屋顶下的瓦边楣饰、大多为拱形窗的小塔楼结构（由柱子（camerae）分隔）、塔楼正面用色彩鲜艳的圆形大理石圆盘装饰、凹陷的前庭及其小柱子和音乐楣饰（有时还用马赛克镶嵌装饰），所有这些都证明了建筑规模的缩小。

当利奥三世在拉文纳建造圣阿波利纳里斯大教堂时，他派出了罗马建筑大师。他这样做可能是出于民族自豪感，也可能是为了给罗马人提供工作，因此这并不完全表明罗马大师享有特殊的声誉，就像科莫的大师过去享有的声誉一样。然而，在罗马，持续不断的努力一定比意大利其他任何城市培养出了更多的艺术人才。利奥三世生平的作者认真地列举了罗马因这位教皇而兴建的所有教堂建筑。我们已经知道他在拉特兰的主要纪念碑--三层大教堂；他还扩建和美化了教皇宫殿，并在那里为天使长建造了一座讲堂。在圣彼得大教堂，他修缮了著名的达马苏斯洗礼堂，保留或赋予其圆形。他重建了由西马库斯（Symmachus）设计的十字架礼拜堂，并用麝香加以装饰。他用华丽的珠宝装饰教堂。在银柱上放置了金银使徒雕像和小天使雕像，地板上铺满了更多的金板。值得注意的是，在圣彼得教堂和圣保罗教堂的使徒墓两侧都贴有两块银盾，上面可以用拉丁文和希腊文读出使徒的象征意义。因此，希腊语信条在当时还没有受到冒犯。利奥还在圣彼得大教堂旁边的主教公寓上进行了修建，并在那里建造了一座非常漂亮的三层大礼堂，地板上铺满了彩色大理石。在圣彼得大教堂修建了塔楼，并在方尖碑旁为朝圣者修建了一个宏伟的圆形澡堂，方尖碑从漫长的黑暗中突然出现，成为一个大圆柱（columna maior）。另一个古老的名字也再次出现在这里：利奥在这里建立了一家医院，名为 "Naumachia"。这家医院位于梵蒂冈附近，供奉的是二世纪在高卢殉难的罗马司铎圣佩尔格里努斯（St Peregrinus）。他的名字使他成为大批朝圣者（_peregrini_）的守护神，这些朝圣者特别是从古代高卢来到这里。今天天使之门附近的圣佩莱格里诺小教堂是为了纪念利奥在同一地点建立的，因为该地区被称为瑙马奇亚（Naumachia），所以多米提安的瑙马奇亚（Naumachia）也曾位于此地。

除了圣彼得大教堂，利奥还修缮了斯蒂芬主教修道院，并修复了附近的圣马丁修道院。

这座城市最古老的教堂之一，位于阿皮亚大道上的圣尼厄斯和阿奇里奥斯教堂（Fasciola）因洪水而成为废墟；利奥在一个较高的地方重建了这座教堂。教堂以其古老的形式保存了下来，经过一些改建，成为一座拥有三个中殿的小型大教堂，比例非常协调，但只留下了马赛克的残片。在利奥的建筑目录中，几乎没有一座教堂不是他建造的，而无数的器皿和帷幔礼物也见证了拉特兰教堂的财富。教皇们恢复了古罗马人对华丽的热爱。除了一些彩色玻璃窗和古抄本的缩影外，马赛克似乎是利奥时代的主要装饰品，我们可以大胆地用 "pictura "一词来理解这种艺术。青铜、白银和黄金的金属铸造工艺也得到了很好的应用，因为当时制作了无数这样的雕像。他们还懂得如何用银铸造和用尼罗纸铺设。我们没有这类雕像的记录，但毋庸置疑的是，教堂里已经开始使用木制的圣人雕像，这些雕像被涂上颜色，披上长袍。

从利奥的基金会目录中摘录罗马当时拥有的有头衔的教堂、执事会和修道院的名称并非不重要；因为几个世纪后，这些名称将不再以同样的完整性列出。共有 24 个长老头衔：埃米利亚纳（Aemiliana）、阿纳斯塔西娅（Anastasia）、拉奎拉（Aquila）和普里斯卡（Prisca）；巴尔比纳（Balbina）；卡利斯托（Calisto）或 S. 尤西比乌斯（Eusebius）；卢西纳的洛伦佐（Lorenzo in Lucina）、达马索的洛伦佐（Lorenzo in Damaso）；马塞卢斯（Marcellus）；马库斯（Marcus）；内雷乌斯（Nereus）和阿奇列乌斯（Achilleus）；帕马奇乌斯（Pammachius）、普拉塞迪斯（Praxedis）、普登斯（Pudens）；Quatuor Coronatorum；萨比娜（Sabina）、西尔维斯特（Silvester）和马蒂努斯（Martinus）、西斯图斯（Sixtus）、苏珊娜（Susanna）；维塔利斯（Vitalis）。

还提到了以下女执事

Adrianus, Agatha, Archangelus; Boniface on the Aventine; Cosma and Damianus; Eustachius; Georgius; Lucia _in septem viis_, that is _in septizonio_, or _ad septem solia_; Lucia iuxta Orphea; Santa Maria Antiqua（今天的 Francesca Romana），此外还有 Adrianio、Cosmedin、Cyro 或 Aquiro、Domnica、Via Lata、圣彼得门前的玛丽亚教堂；Sergius 和 Aquiro、Domnica、Via Lata、圣彼得门前的玛丽亚教堂。Peter's Gate；Sergius 和 Bacchus；在圣彼得的 Silvester 和 Martinus；Theodorus；在 Macello 的 Vitus。

这里已经列出了四十多所修道院，但罗马还有更多修道院。

在圣彼得大教堂旁边有五座修道院：Stephanus Maior 或 Protomartyr，又称 Catagalla Patritia；Stephanus Minor；John and Paul；Martin 和耶路撒冷修道院。

除了拉特兰修道院，还提到了以下修道院： 潘克拉修斯（Pancratius）、安德烈亚斯（Andreas）和姓霍诺里（Honori）的巴托洛迈乌（Bartholomaeus）；斯代法努斯（Stephanus）和塞尔吉乌斯与巴克斯女子修道院。

在圣玛丽亚-马焦雷旁边有以下修道院：安德烈亚斯修道院，又名卡塔巴拉-帕特里蒂亚修道院，也许与安德烈亚斯修道院（Andreas _in massa Juliana_）相同；科斯马修道院（Cosma）和达米亚努斯修道院（Damianus）；哈德良努斯修道院（Hadrianus），又名圣劳伦提修道院（Sancti Laurentii）。他们都姓_ad Praesepe。

大门外的圣保罗教堂是凯撒里乌斯和斯蒂法努斯的修道院，他们的姓氏是 _ad quatuor angulos_；圣洛伦佐教堂是斯蒂法努斯和卡西安的修道院。

其他罗马修道院有

Agata _super Suburam_, Porta Nomentana 前的 Agnes, title Eudoxia 的 Agapitus, Anastasius ad Aquas Salvias, Clivus Scauri 上的 Andreas, Santi Apostoli 的 Andreas; Bibiana; Trastevere 的 Chrysogonus; Caput Africae 上的修道院, Via Appia 上的修道院 _de Corsas_ 或 Caesarii; the monastery _de Sardas_, probably at S. Vito; Donatus near St. Vito；阿汶丁山圣普里斯卡附近的多纳图斯；科利乌斯山上的伊拉斯谟；拉丁门前的尤金尼娅；圣普登蒂安娜附近的尤菲玛和阿昌吉勒斯；可能在 S. Vito 的修道院_de Sardas_。Pudentiana；_duo Furna_修道院，可能在纳沃纳河畔的_Agone_；Isidore，可能在 Pincius 河畔；St John 在阿汶丁山；Lutara 修道院；Laurentius Pallacini 在圣马尔科；Lucia Renati _in Renatis_ 或 _de Serenatis_； Maria Ambrosii，可能与位于 Forum Piscarium 的 Ambrosii de Maxima 相同；位于台伯河岛的 Maria Iuliae；位于 Campo Marzo 的 Maria 修道院和位于 Capitolio 的 Maria 修道院在利奥三世的基金会目录中没有提及。圣米迦勒，未知；Tempuli 修道院；Silvester (de Capite)；St Saba 或 Cella Nova；Semitrii，未知；Victor 位于奥雷利亚大道上的圣潘克拉修斯。

当时，后来在众多修道院中脱颖而出的 20 座修道院尚未建立。这些修道院的数量不断增加，据说在十世纪末，罗马的修道院越来越多。十一世纪末，据说在罗马，修女拥有二十座修道院，僧侣四十座，教士六十座。
