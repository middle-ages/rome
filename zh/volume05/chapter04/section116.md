_5\. 罗马的无知 阿纳斯塔修斯的Liber Pontificalis。它的起源，它的特点。阿纳斯塔修斯的希腊文译本。约翰-迪亚科努斯撰写的格雷戈里大帝的生平。

如果萨莱诺的阿纳尼穆斯在尼古拉一世时来到罗马，他就永远无法在这里发现 32 名哲学家，就像他声称在 870 年在繁华的贝内文托市数到的那样。如果保罗-迪亚科努斯（Paul Diaconus）的伦巴第人历史的续写者埃尔切姆佩特（Erchempert）从他博学的卡西诺山修道院（当时优秀的修道院院长贝尔塔里乌斯（Bertarius）在此居住）来到罗马，他一定会被这里的僧侣和红衣主教的无知所震惊；如果被尼古拉一世逐出教会的希腊元老普提乌斯（Photius）来到这里，他一定会被这里的哲学家之多所震惊。在罗马，几乎没有一个学者知道如何为圣贤和诗人的雕像命名，这些雕像仍然黑乎乎地矗立在被毁坏的特拉阿努斯广场上，残缺不全。

仅凭拜占庭人的科学知识，西塞罗的城市并不感到羞耻；因为那些掠夺了使徒彼得和圣保罗宝藏的阿拉伯人也可以夸耀他们的大学和哲学家、神学家和语法学家、天文学家和数学家，他们装饰着凯拉万、塞维利亚、亚历山大、巴索拉和巴格达，东方的穆罕默德雅典。君士坦丁堡，这个神学家和诡辩家、语法学家和博学的迂腐者云集的伟大国际大都市，在她的王公贵族中找到了曾推翻宗主教伊格内修斯的凯撒-巴尔达斯、 在利奥-菲洛索福斯（Leo Philosophus）和他后来的儿子君士坦丁-波菲罗奈图斯（Constantin Porphyrogenetus）身上，他们都是热衷于科学的学生，而在普提乌斯（Photius）身上，他则是野蛮时代的新普林尼或亚里士多德，在他著名的 "图书馆 "中，只记录了他阅读的一小部分内容，即 280 位作家的作品摘录。

拜占庭人意识到希腊语相对纯正，几个世纪以来一直保持着其科学生命力，因此对罗马不屑一顾。在给教皇尼古拉一世的一封信中，迈克尔皇帝嘲笑罗马人的拉丁语，称其为 "野蛮人和斯基泰人 "的语言，而人们说拉丁语、公证人书写拉丁语甚至编年史的方式，都让博学的希腊人有足够的理由嘲笑拉丁语。教皇用非常好的拉丁语作答，他或他的总理府（在风格上仍很实用）用拉丁语振振有词；这是最好的一种辩护。他可以恰当地回答皇帝说，他声称自己是罗马人的皇帝是可笑的，因为他不懂罗马人的语言，因此称他们为野蛮人，但他为凯撒、西塞罗和维吉尔的语言辩护的理由只是来自基督教和十字架，而十字架的名称 I. N. R. I. 是拉丁语。

即使是被罗马人斥之为野蛮人的德意志和高卢人，也继续通过拉丁语和拉丁科学的教育使自己脱颖而出。在兰斯的红衣主教眼中，兰斯的辛克马就是一个奇迹。诗歌，无论是神圣的还是世俗的，在这里都已沉寂，但与此同时，当罗马人几乎没有足够的才华为他们的教堂音乐、城门或墓碑用野蛮的韵律和文字创作几首咏叹调时，法兰克编年史家，如埃尔莫尔德-尼格勒斯（Ermold Nigellus），用拉丁诗歌创作了他们的历史，而德意志诗人，他们的祖先仍然是异教徒，用我们民族强有力的原始语言创作了福音和声，其独创性至今仍让我们钦佩不已。罗马再也没有神学著作问世。虽然德国和法国，甚至是下意大利，在那里可敬的卡西诺山（Monte Cassino）培育了大量的历史著作，但罗马僧侣们的懒惰将这座城市的事件埋葬在了深深的黑暗之中。

然而，正是在这个时代，罗马教皇热衷于延续其古老的编年史。自从教会国家形成以来，不仅教皇的权力增加了，主教的权力也增加了，他们的辖区成为了丰富的豁免权，因此，将教会的历史按照统治者的顺序有条不紊地传承给后世并描述他们的生活变得越来越明显。这种需求并不是孤立的，因为同一时期还产生了几部此类文集，它们都是以主教目录、他们的书信或记录以及其他文件为基础的。在罗马之外，阿格内卢斯收集并撰写了野蛮但值得称道的拉文纳主教史，那不勒斯人迪科努斯-约翰内斯撰写了其家乡主教的传记。人们还认为，《教皇书》（Liber Pontificalis）是由阿纳斯塔修斯在同一时期收集和编辑的，因为他的名字一直被附在一般的《教皇书》上，尽管没有理由。

这位拥有 "图书管理员 "头衔的阿纳斯塔修斯曾在尼古拉一世和约翰八世时期生活过。自第二和第三世纪起，教皇传记就已经以历法记录和教皇统治及行动目录的形式被编纂出来。从格里高利大帝开始，他们的书信和档案也被用于这一目的。教皇们的官方传记就是在这些日益完整的资料基础上编纂而成的，其中以卡洛林王朝时期的传记最为全面。这些传记没有任何年鉴性质，因此难以使用；它们笨拙地混合了对建筑物和祭品的精确记录以及真正的历史事件。它们拙劣的文体与罗马教会语言大相径庭，而尼古拉斯一世和约翰八世的登记表中流畅、准确和充满活力的语言仍然让我们感到惊讶，幸运的是，这些登记表流传到了我们这个时代。然而，它们的价值是不可估量的，因为它们来自最可靠的资料来源，即使是一些故意歪曲事实、有利于教皇制度的资料也无法削弱它们的价值。如果没有它们，许多世纪以来有关教皇和罗马城的知识将完全处于黑暗之中。由于在尼古拉一世传记之后的很长一段时间里，《教皇传》不再以传统的方式继续出版，我们很快就会对这一罗马城历史资料的枯竭感到惋惜。

顺便提一下，图书管理员阿纳斯塔修斯精通希腊语；他翻译了尼斯弗鲁斯、乔治-辛塞勒斯和西奥法尼斯的编年史以及其他希腊教会文学作品。他的同胞迪亚科努斯-约翰内斯（Diaconus Johannes）也懂希腊语。他利用拉特兰档案馆的文件撰写了格里高利大帝的生平。值得注意的是，这样一部专著是在卡洛林王朝时期撰写的，而且是在作者经历了尼古拉一世的教皇生涯之后，尼古拉一世是一位回顾了格里高利的活动和伟大的教皇。他的著作是一部独立的作品，与所有关于教皇生平的描述都截然不同。该书展现了一位想象力丰富的修辞学作家，他以一种令人遗憾的方式追求优雅和丰满，并透露出对古代文学的一些了解。
