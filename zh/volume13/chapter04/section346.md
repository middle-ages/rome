_6\. 查理八世与教皇谈判。1495年1月15日条约。 国王退位。红衣主教切萨雷出逃。阿方索退位，斐迪南二世继位。 查理八世在那不勒斯。杰姆之死 1495年3月反对查理的联盟，查理从那不勒斯撤退。教皇逃往奥尔维耶托。查理八世在罗马。1495 年 7 月 6 日在塔罗的胜利，返回法国。亚历山大六世返回罗马。法军在那不勒斯陷落。1495 年 12 月台伯河泛滥_。

亚历山大六世进入罗马两天后，切萨雷-博尔哈和其他红衣主教，除了奥尔西尼和卡拉法，都在等着国王。国王毫不客气地接见了他们。他们就条约的基础进行了谈判，为此教皇授权卡瓦哈尔、奥贝托二世-帕拉维奇诺和里阿里奥。博尔吉亚现在的任务是：用艺术挽救可以挽救的一切，确保王位，为自己消除风暴，最后智取国王。他发现自己正处于一生中最危险的时刻：他是最强大的王子的俘虏，他的大炮可以在几小时内摧毁圣天使城堡：他是愤怒的敌人们憎恨的对象，他们包围着国王，而国王的意图还是个秘密。反对派的红衣主教朱利安、古尔克、桑塞韦里诺、圣德尼、萨维利家族、科隆纳和阿斯卡尼奥敦促查理宣布自己为教会改革者，通过审判废黜西蒙派教皇，将一位有资格的人推上罗马教廷。废黜教皇的法令已经起草完毕。阿斯卡尼奥是博尔哈当选的始作俑者，现在又是他的死敌，很可能希望成为他的继任者。如果查理八世听从了反对派的意见，他在教会中引起的动荡会比他在意大利的活动更大。这位最虔诚的基督徒国王似乎是被一只更高的手引领到罗马来改革腐败的教廷的，而被剥夺了这种改革的世界肯定会心甘情愿地将萨克森和法兰克的伟大帝王曾经为了基督教的利益而行使过的独裁统治让给他。他完全有能力将教会从亚历山大六世的统治下解放出来，如果查理八世能够在1495年做出伟大的决定，那么切萨雷-博尔吉亚这个历史人物就永远不会出现。但是，这样一个年轻而微不足道的人，只想着军事征服的虚荣，他能做到这一点吗？他的心腹布里索内（Briçonnet）以红衣主教帽子的承诺为亚历山大赢得了胜利：国王拒绝了反对派的要求；他满足于迫使教皇签订一份有利的条约，而这正是博尔哈的救星。

法国人在城中的暴力行为促使亚历山大于 1 月 6 日迁往圣天使城堡，卡拉法、阿纳斯塔西亚、蒙雷亚莱、奥尔西尼家族和切萨雷红衣主教紧随其后。这座城堡被西班牙雇佣兵占领，但城墙薄弱；在国王进城前不久，有一段城墙倒塌了。当不久后再次发生这样的倒塌时，教皇的敌人从中看到了上天的旨意。虽然罗马人并不想知道任何关于防御的事情，但当他们看到一个外国国王在自己的城市里统治时，他们的民族感情被激起了。他们仇恨地注视着这些斗志昂扬的 "野蛮人"。法国人强行占据了市民的房屋；早在 1 月 3 日，他们就抢劫了富有的牧师的住宅。犹太人在犹太区被勒死，罗马人又刺伤了法国人。法国军事指挥部在菲奥里广场（Campo de' Fiori）上架起了绞刑架，一些抢劫者像罗马人一样被处死。1 月 8 日，士兵们强行进入保罗-德-布兰卡（Paul de Branca）的家，杀死了他的两个儿子。加斯科因士兵和瑞士士兵冲进了马库斯-马泰被刺死的银行。令教皇深感羞耻的是，他们甚至洗劫了教皇孩子的母亲瓦诺扎的宫殿，这座宫殿就坐落在布兰卡广场上。

查理要求交出圣天使城堡，但教皇拒绝了。他告诉教皇，如果城堡受到攻击，我将带着最神圣的圣物站在城堡的城墙上。国王下令两次调来大炮，但一枪未发。如果亚历山大打开了圣天使城堡，他自己就会落入敌人之手，毫无还手之力；因此他坚持要求查尔斯放弃对城堡的占领。国王在圣马可富丽堂皇的宫殿里继续谈判条约，那里华丽的大厅里总是挤满了罗马的贵族和红衣主教。可悲的皮埃特罗-美第奇每天也会到这里朝拜。1月13日，查理八世首次在罗马公开露面。在卫兵的陪同下，他经常骑马穿过城市，参观教堂和纪念碑。出于对天主教的虔诚，他每天都要去七座教堂中的一座，听弥撒、看圣物。但亚历山大的固执让法国人感到不耐烦和愤怒：1 月 13 日，他们在罗马多处劫掠；犹太人的犹太教堂被毁。

最后，在 1 月 15 日，双方签订了如下条约：亚历山大承诺将泰拉奇纳、奇维塔韦基亚、维泰博和斯波莱托交给查尔斯，在教皇国只任命与他意见一致的校长，将杰姆王子交给他，并赦免有法国思想的红衣主教和大人物。红衣主教切萨雷将作为使节陪同国王四个月；奥斯提亚将归还给红衣主教朱利安，圣天使城堡将继续由教皇占领。

正如康米尼斯（Commines）所判断的那样，这个条约过于激烈，难以维持，它使查理八世成为了教皇国的主人，但却使亚历山大摆脱了最紧迫的危险，因为国王郑重承诺承认他为教皇，并保护他的一切权利。反对派的红衣主教们深感愤怒。阿斯卡尼奥和卢纳特心怀不满，离开罗马前往米兰。其他人为了不与国王分开，勉强留了下来。

1 月 16 日，国王和教皇按照事先安排好的日程进行了第一次会面。当教皇被抬出城堡时，他偶然出现在有顶棚的人行道起始处的花园里。教皇冲到国王面前，在他第三次下跪时拥抱了他。两人同时捂住了头，然后就去了梵蒂冈。狡猾的博尔哈可以轻蔑地看着这位年轻的君主，教皇、罗马和意大利都在他的掌控之中，而他却没有从自己真正的帝王地位中得到什么好处。查理想为布里索内戴上红帽子，教皇立即给这位宠臣戴上了。1月19日，他满意地看到意大利的征服者出现在主教会议上，接受了他之前拒绝的服从。国王亲吻了他的手和脚，并说出了规定的话："我是来向教皇陛下表示服从和崇敬的，就像我的前辈法国国王们习惯做的那样"；这时，巴黎总统解释说，这位最虔诚的基督徒国王是来承认并尊崇教皇为基督的代牧和使徒王子的继承人的。为了庆祝这一和解，亚历山大第二天在圣彼得教堂做弥撒时，国王把圣水递给了他，然后他在第一位红衣主教之后谦卑地就座。他在圣彼得罗尼拉小教堂为法兰西王室创造了荒唐的奇迹，罗马人惊奇地看着他：也许他们惊讶于这位伟大的君主只想治愈他们的甲状腺肿，而不是他们教堂的损坏。1 月 21 日，亚历山大还把红衣主教的帽子送给了国王的堂弟卢森堡的菲利普。1 月 25 日，他与国王一起骑马公开穿过罗马。双方都以此标榜自己的亲密联盟，但谁也不信任谁。然而，吉贝尔派却怨声载道。当古尔克红衣主教从教皇那里获得契约赦免时，他毫不犹豫地当着奥尔西尼家族和瑞阿里奥红衣主教的面斥责他的西蒙尼选择、他的恶行以及他与土耳其人的背信弃义的联盟。

只有一件事查理八世无法实现：那不勒斯的叙任权，教皇拒绝给予他。他迫不及待地启程前往那不勒斯王国，在那里，他已经派出了法布里齐奥-科隆纳和罗伯特-德-莱农库特率领的军队，目的是让阿布鲁佐起义。他还在梦想着对君士坦丁堡开战；他宣称，东方帝国的权利已经从最后一位古罗马人手中转到了法兰西王室手中。1494年9月6日，莫雷亚暴君安德鲁在红衣主教古尔克面前签署了一份文件，将他对拜占庭的权利让给了查理国王。查理曼的十字军东征确实在意料之中；他是在罗马的诗歌鼓励下这样做的。出发当天，杰姆被送到了圣马可。他在那里聆听了弥撒，与教皇共进晚餐并告辞。他于 1495 年 1 月 28 日离开罗马，踏上了 229 年前安茹的查理出兵攻打曼弗雷德的那条拉丁大道，和那时一样，春天提前到来了。和当时一样，现在看来，他的努力是愚蠢和冒险的。我们的目标是征服一个装备精良的帝国，而后方随时都有可能出现或明或暗的敌人。自从征服奥特朗托（Otranto）以来，阿方索一直被认为是意大利的第一位战争队长；人们相信他拥有巨额财富；事实上，意大利的要塞补给充足，大量军队服役。但即使在 1495 年，那不勒斯的力量也只是一只可怕的幼虫。暴政收获了血腥的种子。

查理八世一进入罗马，卡拉布里亚王子一渡过里里斯河返回，整个国家就陷入了骚乱之中。第一批法国人一出现在阿布鲁佐，拉奎拉就升起了法国国旗，安日文党人四处起义。阿方索在那不勒斯的城堡里陷入了阴郁的绝望之中。当夜晚海浪咆哮时，他相信海浪在向他呼唤： 法国！法兰西 树木、石头，所有的一切似乎都在他耳边呼喊着这个名字。1月23日，这位懦弱的暴君被自己的罪恶和臣民的仇恨击垮，放下了王冠。他让自己无过错的儿子费朗蒂诺（Ferrantino）骑马穿过城市，宣布他为国王，教皇也建议他这样做。然后，他带着财宝驶往西西里，把自己的耻辱藏在了修道院里。

查理在马里诺的第一夜宿舍里就得知了王位更迭的消息，他的继任者是切萨雷-博尔哈（Cesare Borgia），名义上是教皇的使节，实际上是他父亲效忠的人质。在韦莱特里，年轻的红衣主教首次展示了他未来的风采。夜里，他把自己裹在马夫的衣服里，骑上马追回了罗马。1 月 30 日上午，教皇得知红衣主教躲在审计员安东尼奥-弗洛雷斯（Antonio Flores）的房子里，父亲可以满意地证明他的爱子的效率。切萨雷先是从罗马逃到了里尼亚诺，然后又逃到了斯波莱托，而教皇却声称对他一无所知。现在，国王意识到教皇在背叛他；他是应该回头，还是直接派兵去罗马，从罗马救回一名逃亡的红衣主教？他派布雷斯的菲利普去向教皇讨要说法，亚历山大则派尼皮的主教去向他道歉。罗马市民的使节们也赶到国王的阵营，向他解释罗马城在这次违约事件中没有任何责任。

在韦莱特里，西班牙大使胡安-阿尔比恩（Juan Albion）和丰塞卡（Fonseca）对这一暴力行动提出了抗议，天主教徒斐迪南在巴塞罗那和约中并未同意这一行动。于是发生了激烈的一幕：丰塞卡当着国王的面撕毁了和约。但查尔斯继续前进。没有一个敌人能阻止他。只有孔蒂的要塞蒙特索蒂诺（Montesortino）被德国雇佣兵督军恩格尔贝特-克莱维斯（Engelbert of Cleves）攻破。这样做是为了打击科隆纳人，因为科隆纳人的敌人就是孔蒂人；雅各布斯-孔蒂也曾在那不勒斯服役。村庄的守军被屠杀殆尽。由于圣乔瓦尼山遭受了同样的命运，这种野蛮行径在边境地区的所有城镇传播开来。特里武齐奥和皮蒂利亚诺领导下的那不勒斯人从卡西诺逃往卡普亚，年轻的国王费兰特希望留在那里。然而，当那不勒斯的一场起义迫使他匆忙赶往那里时，特里武齐奥与查理谈判，向他开放了卡普亚。维吉尼乌斯和皮蒂利亚诺在诺拉向敌人投降，费兰特回到卡普亚的时间太晚了，他不得不折回那不勒斯，在那里他发现自己迷路了。2 月 21 日，他乘船前往伊斯基亚岛，22 日，查理八世在人民的欢呼声中进入王国首都。只有那不勒斯的城堡抵抗了一段时间，直到它们也投降为止。

亚历山大六世说，法国人用木刺征服了意大利，用手中的粉笔标记了他们的驻地，除此以外再无其他麻烦。法国国王被比作亚历山大和凯撒。当他在卡普阿诺城堡坐上安茹和阿拉贡的王位时，他必须以当时最伟大的君主的形象出现。他对亚洲的十字军东征现在可以如康米尼斯和最高贵的法国人所希望的那样进行了。巴加泽特苏丹已经惶惶不可终日，因为他知道自己的弟弟在查尔斯国王的掌控之中；但不幸的杰姆还没走进那不勒斯的城堡，就于2月25日去世了。国王下令将他的死讯保密。马上就有人说，他是奉亚历山大之命被人下了白粉毒药。

查尔斯受到了那不勒斯上层贵族和人民，甚至是流亡王朝近亲的朝拜。除了几个海上城市外，王国的所有土地都服从于他。他现在要求为亚历山大六世叙任权和加冕，由于教皇拒绝，他于 5 月 12 日在圣亚努阿留大教堂举行了庄严的游行。然而，就在他沉醉于那不勒斯的欢乐之中时，一场风暴却在他身后悄然袭来。所有的强国都为他的征服深感恐惧。教皇、威尼斯和洛多维科（因奥尔良对米兰的要求而感到震惊）联合起来，共同面临危险。西班牙国王担心西西里，他向那里派遣了康萨尔沃（Consalvo）军队，甚至连马克西米利安也不能承认法国拥有意大利后正在篡夺欧洲的霸权。所有这些强国都在威尼斯召开了一次会议，并于 1495 年 3 月 31 日在那里缔结了保卫国家的大同盟。土耳其战争是其借口，秘密条款中表达的真正目的是对抗法国征服者。新欧洲的历史就是从这个列强联盟开始的。

现在，查理不得不开始撤退。他任命蒙彭西埃（Montpensier）为那不勒斯副王，奥比尼（Aubigny）为卡拉布里亚总司令；5月20日，他带着剩下的军队出发了，同行的还有特里武齐奥（Trivulzio），他现在是他的部下，还有朱利安（Julian）红衣主教、圣德尼（St Denis）红衣主教、菲斯基（Fieschi）红衣主教和圣马洛（St Malo）红衣主教。2 万头骡子驮走了那不勒斯的战利品，包括国王掠夺的艺术珍品。正是在这个不幸的那不勒斯，就像在整个意大利一样，法国人学到了文艺复兴的精神；意大利人的优良教育从此影响了法国。甚至在出发之前，查理就已经派圣保罗伯爵前往罗马与教皇谈判；但是，尽管罗马人紧急恳求他留在城里，因为他们想在圣天使城堡保卫他，亚历山大还是在5月27日，也就是国王抵达塞普拉诺的一天后逃跑了。马克西米利安早在 3 月就敦促他撤出罗马。联盟和教会的军队近万人随他前往奥尔维耶托；所有的使节和红衣主教都跟随他而去，只有圣阿纳斯塔西亚的约翰-莫顿作为助理司祭留在城里。

6月1日星期一，查理八世返回罗马，按照亚历山大的命令，他在罗马受到了隆重的接待。守护者们派代表以教皇的名义迎接他，然后治安官和无数人追上了他。他骑马来到圣彼得大教堂，在那里进行了祈祷。他拒绝住在梵蒂冈，搬进了红衣主教 S. 克莱门特在博尔戈的宫殿，也就是今天使徒彼得学院的所在地。虽然他完全有理由敌视罗马，并对不忠实的教皇进行干预，但他没有这么做。这一次，他的军队甚至维持了更好的秩序，尤其是所有西班牙人都离开了这座城市。星期三，从米兰传来了重要的消息，查尔斯立即动身前往伊索拉。陪同他的是法布里齐奥和普罗斯佩罗-科隆纳。他在维吉尼乌斯-奥尔西尼家族的城堡坎帕尼亚诺过夜，然后前往维泰博。他邀请教皇会面；他甚至把根据条约条款占领的城堡还给了教皇，只把奥斯提亚留给了红衣主教朱利安。但亚历山大避免了与国王的任何会面，并于 6 月 5 日离开奥尔维耶托前往佩鲁贾。进军的法国人劫掠了图斯卡尼亚，并在那里屠杀了居民；随后，查理于 6 月 13 日来到锡耶纳，并从那里前往比萨。比萨人大声哀嚎，恳求他不要把他们交给佛罗伦萨换钱；他拒绝了。他避开了佛罗伦萨。佛罗伦萨因为他在自己的阵营中接纳了皮埃特罗-美第奇，却没有将比萨或其他要塞归还给她而恼羞成怒，于是在他靠近时固守，并自己接纳了威尼斯军队。他们的使节与查理进行了谈判。萨伏那洛拉在波吉邦西（Poggibonsi）与他当面对质，痛斥他背弃了佛罗伦萨人的信仰，欺骗世人他所期待的教会改革。

意大利人满怀激动地注视着国王的退却，他轻蔑而漫不经心地丈量着凯旋之路，而联盟的军队正在北方集结，准备切断他的退路。如果同一支军队迅速出动，全力对付他，那么毁灭或俘虏将是对这个无礼入侵者的公正惩罚，意大利将通过不朽的民族行动恢复她的荣誉，甚至独立，就像她曾经在莱格纳诺所做的那样。在这个国家的历史上，很少有像这样具有决定性意义的时刻，但不幸的是，由于恐惧、嫉妒和笨拙，这个伟大的时刻已经失去了。

查理试图到达阿斯蒂，奥尔良军队出其不意地攻占了米兰城市诺瓦拉，迫使洛多维科-斯福尔扎出兵围攻。国王获准通过蓬特雷莫利附近的隘口进军，只是在福尔诺沃附近的塔罗河畔，盟军在其战地指挥官曼图亚的吉安-弗朗西斯科-贡萨加（Gian-Francesco Gonzaga）的率领下筑起的营垒挡住了他的去路。这支军队远胜于查尔斯的军队，因为疲惫不堪的法军人数不过一万多人。这支军队的核心是瑞士和德国的步兵，从那时起，法国的国王们主要依靠我们这个四分五裂的祖国的力量进行战争。1495 年 7 月 6 日，著名的塔罗战役只持续了不到一个小时。双方都宣称取得了胜利，但在战场上，意大利人比法国人多，虽然他们损失了辎重，但还是冲破了敌人的包围网。他们冲入敌军阵地，将其击退。查理本人就像一个普通士兵一样作战；在意大利战役中，他只摘取了芋头上的桂冠，并将其带回了法国。经过漫长的岁月，意大利人再次打响了一场民族战役，其目标是将自己从外国统治下解放出来；他们英勇作战，但没有取得他们所期望的成功，这决定了他们未来的命运。查理八世高兴地奇迹般地逃过一劫，到达了皮亚琴察和阿斯蒂。

随着风暴云的北移，亚历山大也于 6 月 27 日返回了罗马。在威尼斯大使热罗尼莫-佐尔齐（Geronimo Zorzi）的怂恿下，他这才鼓起勇气向法国国王发出了一份监察令，呼吁他在受到教会惩罚的威胁下不要再对意大利发动进攻。与此同时，当查理在都灵时，联盟的军队在诺瓦拉围攻奥尔良公爵，在这里，他成功地将洛多维科-斯福尔扎从联盟中拉了出来，于10月9日与他签订了《韦切利和约》（Separate Peace of Vercelli）。洛多维科因此夺回了诺瓦拉，但允许国王在热那亚装备船只，并承诺在下一次对那不勒斯的战争中支持国王。斯福尔扎是在盟友不知情的情况下签订这份条约的；威尼斯拒绝了条约中的条款，国王则拒绝了共和国的条件。斯福尔扎本人带着荣耀回到了法国，但却没有得到什么好处；因为他的军队在那不勒斯遭到了最惨痛的破坏。

费兰特二世离开后，立即从墨西拿返回了他的王国，在那里，过于自信的法国人已经让他们受到了普遍的憎恨。他已经在西西里寻求西班牙的帮助，而天主教徒费迪南德很高兴地答应了，因为作为阿拉贡的胡安之子，阿方索一世的弟弟，他本人要求继承那不勒斯的王位。他派遣他的大将康萨尔沃率领军队前往卡拉布里亚；威尼斯应邀前来援助，也急切地占领了沿海的一些城镇。

费兰特二世早在 1495 年 7 月 7 日就进入了那不勒斯。普洛斯彼罗和法布里齐奥（现在是他的部下）以及教皇的部队在那里为他提供了坚固的防御，而蒙彭西埃和奥比尼则接连失守。蒙彭西埃最终在阿特拉投降，1496年10月5日在波佐利去世；奥比尼则在11月离开加埃塔，踏上了前往法国的旅程。几乎所有的法国人都在那不勒斯王国找到了自己的归宿。

1496年10月7日，年轻的费兰特二世因膝下无子而逝世，他那高贵、才华横溢的叔叔、阿尔塔穆拉伯爵唐-费德里戈（Don Federigo）登上了王位。在这种情况下，阿方索很可能会再次称王，但他已于 1495 年 11 月 10 日在马扎拉去世。

这样，查理八世邪恶的征服就化为乌有了。剩下的就是可怕的情欲瘟疫，它被命名为 "法国病"，像瘟疫一样在欧洲蔓延。当然，人们想知道它是从美洲赤身裸体的野蛮人的天堂传过来的；但事实上，性病出现在意大利和其他国家的时候，正是道德败坏最严重的时候，也是它在身体上的表现。

上天的愤怒还体现在罗马遭受的一次最可怕的台伯河洪水中。1495 年 12 月 4 日，台伯河决堤，洪水淹没了下城。刚从主教会议上出来的红衣主教们艰难地通过天使之桥才得以自救；帕尔马红衣主教再也无法到达他的住所。洪流冲垮了宫殿，侵袭了教堂，并在街道上肆虐。人们在这里乘坐驳船，就像在威尼斯的泻湖中一样。许多人溺水身亡；托尔迪诺纳的囚犯无法获救。据统计，损失达 30 万杜卡特，威尼斯目击者的信中说，罗马在二十五年内都无法恢复。时至今日，在 S. Eustachio 附近一栋房子的墙角处，您仍然可以看到大理石碑文，上面刻着洪水的高度。
