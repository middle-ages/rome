_6\. 诗歌的人文艺术 Cencio. 洛斯基 Maffeo Vegio. Correr. 达蒂 尼古拉斯-瓦拉 贾南托尼奥-坎帕诺 奥雷利奥-布兰德里尼 朱斯托-德-孔蒂 戏剧的开端。神秘剧和受难剧。罗马剧院和场景表演。红衣主教拉斐尔-瑞阿里奥的剧院。费迪南德斯-塞尔瓦图斯。庞波尼乌斯-拉埃特斯（Pomponius Laetus）和学者表演的意大利戏剧_。

与科学一样，人文主义者也将诗歌作为正式的研究课题。他们热衷于古代诗歌，摒弃了意大利语，将其视为缪斯女神不配穿的衣服，并创作了拉丁颂歌、挽歌、书信体、田园诗和史诗。如果说今天我们只能出于文化史的目的来阅读这些冷冰冰的模仿之作，甚至是当时最著名诗人的作品，那么这些作品却表达了他们所处时代的方向，往往只是以仿古的形式再现了其内容，并在社会中传播了大量的知识生活。

十五世纪涌现出了许多才华横溢的诗人，他们的作品如今已被尘封在现代图书馆中。在罗马，文艺复兴早期最伟大的诗人是鲁斯蒂奇（Rustici）、洛斯基（Loschi）和维吉奥（Vegio）。阿加皮托-迪-岑西（Agapito di Cenci）是罗马人，出身于鲁斯蒂奇故居，是波焦的朋友、克里索拉斯的学生、古代文学的热心研究者，同时也是一位医生，是当时著名的人文主义者。马丁五世任命他为使徒秘书，庇护人庇护二世任命他为卡梅里诺主教。他于 1464 年去世，但我们已无法判断他未发表的诗歌的价值。

波焦的另一位朋友，文森特人安东尼奥-洛斯基曾在罗马担任教皇秘书，1450 年去世。他用诗歌创作了寓言和书信；作为语法学家，他也备受推崇。

欧仁四世时期，马菲奥-维吉奥进入罗马总理府工作，先是担任缩写员，后又担任院长。他来自洛迪，1406 年出生在那里。这位多才多艺的贵族是少数回归教会的人文主义者之一。他本人也成为了奥古斯丁派教徒。他撰写了教会古籍、道德论文以及法律著作。他为欧仁四世撰写了奥古斯丁及其母亲莫妮卡的生平，还为锡耶纳的贝尔纳迪诺撰写了传记。不过，他此前也曾作为拉丁诗人而声名鹊起。他大胆地创作了《埃涅伊德》的第十三卷，这在他的时代引起了惊叹，并被作为维吉尔的续篇印刷。维吉奥于 1458 年去世，葬于圣阿戈斯蒂诺的圣莫妮卡教堂，他为圣莫妮卡修建了一座坟墓。与他同时代的还有来自尤金家族的格雷戈里奥-科雷尔（Gregorio Correr）和莱昂纳多-达蒂（Leonardo Dati），格雷戈里奥-科雷尔是著名的诗人和人文主义者，他在德国发现了萨尔维昂的著作《神赐》（De Divina Providentia），达蒂是佛罗伦萨人，他最初是红衣主教乔丹-奥尔西尼家族的秘书，后来又担任过卡利斯图斯三世（Calixtus III）以后几位教皇的秘书。

罗马人尼古拉斯-瓦莱是博学的主教律师莱里奥的儿子，他翻译的赫西俄德诗集于 1471 年付梓，为他赢得了更高的声誉。皮乌斯二世发动土耳其战争时，瓦莱写了一首挽歌诗，诗中君士坦丁诺波利斯祈求罗马拯救她，而罗马回答说 "虔诚的埃涅阿斯 "将为她复仇。然而，这首文笔流畅的诗歌并没有打上天才诗人的烙印。诗人也死得太早，年仅 21 岁，还没来得及完成《伊利亚特》的翻译。他的同时代人对这位青年怀有崇高的敬意。

与瓦莱家族一样，波尔卡里家族在教育方面也颇有建树；因此，他们最能缅怀不幸的骑士斯蒂法诺。他们在密涅瓦附近的宫殿是一座古物博物馆，也是学者和艺术家的聚会场所。保卢斯-波尔修斯（Paulus Porcius）在西斯笃四世（Sixtus IV）时期是一位杰出的修辞学家和诗人。这个家族的其他人也在地方行政机构或教会中担任要职。Gyraldi 还光荣地将 Camillus Porcius 与另一位罗马人 Evangelista Magdaleni Capo di Ferro 并列为诗人，后者一定是文艺复兴时期罗马最杰出的人才之一，后来成为利奥十世的宠儿。

这些拉丁文作家的诗歌在他们的时代曾经很有价值，但现在已经失传或被埋没在图书馆中；因为谁还知道来自萨宾莫诺波利的皮埃特罗-奥多的诗句，布隆达斯评价他拥有奥维德和贺拉斯的雄辩？还是 1490 年英年早逝的罗马著名诗人保卢斯-庞皮利乌斯的诗歌？还是皮埃特罗-瑞阿里奥最喜欢的埃米利乌斯-薄伽梵的诗歌，他用拉丁文六音节歌颂了为埃莱奥诺拉公主举办的盛大宴会？

在那个奇怪的时代，罗马青年可能为庞波尼乌斯学院提供了足够多的年轻诗人，我们今天很难想象他们在古典诗歌方面的醉心程度。当然，在当时的意大利，诗歌比现在更多的是一种文体活动，它的守护神不是阿波罗，而是语法学家多纳特；但一个人必须生活在文艺复兴的整个元素中，才能对得起它，对得起那个机智的生命。因为文艺复兴的生活不能从它在罗马和其他地方的后来学院中留下的反思中得到理解。

一般文学史家可能会更多地提及拉丁诗人，如费拉拉的两位斯特罗齐，佛罗伦萨的波利齐亚诺和马鲁洛，那不勒斯的蓬塔努斯和桑纳扎尔，但这些都与罗马无关，我们只想强调贾南托尼奥-坎帕诺（Gianantonio Campano）。

这位才华横溢的人是坎帕尼亚仆人的儿子，小时候放羊，上过牧师学校，在那不勒斯师从瓦拉，然后到佩鲁贾学习，1455 年成为那里的演说家。他有卡利班的外表，但却有即兴创作的天才，文采斐然，似乎比老一辈的拉丁文学家更胜一筹。充满幽默的滑稽天性使他成为最讨人喜欢的伙伴，并赢得了庇护二世的青睐，庇护二世授予他泰拉莫主教职位。1471 年，由于土耳其战争，保罗二世将他派往雷根斯堡议会。坎帕尼亚诗人发现自己在那里就像奥维德与吉特人在一起一样，他对德国人的气候、生活方式和缺乏文化的恶趣味是无法与意大利人相提并论的。人文主义时代激发了意大利人的民族情绪，古老的野蛮人概念也随之复苏。但是，坎帕纳斯从那里写给他的朋友安马纳蒂的书信中表达的却是一种愤世嫉俗的民族仇恨。今天，当我们的祖国不再那么糟糕的时候，我们读到这些谩骂时仍然会会心一笑。这位开朗的诗人因为大胆支持被教皇围困的卡斯泰洛城而失宠于西斯笃四世，而他正是卡斯泰洛城的校长。1477 年，坎帕诺在锡耶纳流亡期间去世，他的作品光辉地见证了他的才华。他写了《庇护二世传》、《布拉奇奥生平》、许多演讲和论文、许多书信，这些都是当时最诙谐的作品，最后还有挽歌、书信体诗歌和偶尔写的各种诗歌，其幽默和轻松的表达方式使这些作品更有价值。这位人文主义者还为修订古文献做出了杰出贡献。

当时的许多诗人都生活在王公贵族的宫廷中：贝卡德利（Beccadelli）为阿方索一世的宫廷增光添彩，庞塔诺（Pontano）为阿方索二世和斐迪南二世的宫廷增光添彩；著名的曼托瓦诺（Mantovano）为费德里戈-贡萨加（Federigo Gonzaga）的宫廷增光添彩，斯特罗兹（Strozzi）为博尔索（Borso）的宫廷增光添彩，菲列福（Filelfo）为弗朗切斯科-斯福尔扎（Francesco Sforza）的宫廷增光添彩；巴西尼奥（Basinio）和波切里奥（Porcellio）为西吉斯蒙多-马拉泰斯塔家族及其情妇伊索塔（Isotta）的宫廷增光添彩。正如人文主义者通过演讲和传记来颂扬其赞助人的事迹一样，宫廷诗人也通过史诗歌曲来颂扬其赞助人的事迹。坎帕诺可以说是庇护二世的宫廷诗人，而在其他教皇的宫廷中也能听到谄媚的即兴演奏者的琴声。佛罗伦萨的盲人奥雷利乌斯-布兰德利努斯-利普斯（Aurelius Brandolinus Lippus）以他的拉丁吟唱和节日赞美诗让教皇西斯笃四世和亚历山大六世也为之倾倒；1498 年，他在罗马去世，享誉盛名。 他的兄弟拉斐尔（Raphael）后来也以同样的即兴艺术让利奥十世的宫廷为之着迷。他曾是不幸的比斯切利王子阿方索和红衣主教德尔蒙特（即后来的教皇尤利乌斯三世）的家庭教师。

新拉丁诗歌显然阻碍了民族诗歌的发展，正因如此，少数仍敢于写出人民自己理解的诗歌的意大利人尤其值得称赞。奇怪的是，正是罗马产生了那个时代最优秀的意大利诗人之一：瓦尔蒙托内的朱斯托-德-孔蒂（Giusto dei Conti of Valmontone），出身于英诺森三世王朝的一个分支。他于十四世纪末出生于罗马，学习法律，后移居里米尼，于 1449 年 11 月 19 日在那里去世，在圣弗朗西斯科教堂（S. Francesco）至今仍能读到僭主马拉泰斯塔家族为他题写的碑文。孔蒂给自己的意大利诗集取名为《美丽的手掌》（La Bella Mano），因为他在诗集中更多地歌颂了爱人美丽的手掌。顺便提一句，他只是彼特拉克的一个苍白的模仿者，他是彼特拉克派的第一位合唱团长，他们的歌声至今仍像蟋蟀一样唧唧喳喳。

然而，意大利诗歌重新获得了自己的权利，或者说自然本身冲破了人为的障碍。这种变化早在十五世纪下半叶就已出现。洛伦佐-美第奇、普尔奇家族、波利齐亚诺、桑纳扎尔再次用意大利语写作，博雅多已经走在了阿里奥斯托的前面。但所有这些诗人都属于文学史上的人物，即使是拉奎拉的塞拉菲诺，这位曾被奉为偶像、被抬高到彼特拉克之上的诗人，在这里也只能被提及，因为他最后一次生活在切萨雷-博尔哈的宫廷，1500 年死于罗马，年仅 34 岁。和他的对手泰巴尔代奥一样，他也用琵琶即兴伴奏诗歌。

意大利独立戏剧的开端也可追溯到本世纪下半叶，罗马尤其是戏剧艺术的灵感之源。罗马最古老的古迹之一是贡法隆兄弟会在复活节星期五在罗马斗兽场表演的神秘剧。他们不仅使用了圆形剧场的部分排座位，还使用了建在圆形剧场内的古老的安尼巴尔迪家族宫殿，演员们会在那里集合和着装。他们本身也是市民，通常来自罗马最好的阶层。佛罗伦萨人朱利亚诺-达蒂（Giuliano Dati）、罗马人贝尔纳多-迪-马斯特罗-安东尼奥（Bernardo di Mastro Antonio）和马里亚诺-帕蒂卡帕（Mariano Particappa）被认为是这些奥塔夫林粗糙场景最古老的作者。顺便提一句，他们早有前辈，因为费奥-贝尔卡里（Feo Belcari）的奥塔夫林神秘剧《以撒和亚伯拉罕》早在 1449 年就在佛罗伦萨上演了。

罗马是一个节日剧场，凯旋游行、帝国加冕礼和教皇加冕礼、地方官和外国使节游行、游行、民间戏剧和化妆舞会以及各种华丽的骑兵队伍在此上演。尼古拉五世之后，游行队伍，尤其是圣体节的游行队伍更加壮观，保罗二世之后，罗马狂欢节的表演或_Ludi Romani_的美轮美奂举世闻名。文艺复兴使形式更加艺术化，并用古罗马主义取代了骑士精神。就像诗歌一样，神话也以哑剧的形式出现在节日生活中。1473 年，红衣主教瑞阿里奥（Riario）在同一个节日剧院里交替上演圣经和神话场景，没有人对此表示不满。狂欢节和古代的土星节一样，在 12 月底开始的假面游行中，众神和英雄、仙女、小仙女和丘比特都出现在红衣主教们装备的装饰精美的花车上。

值得注意的是，1483 年在曼图亚为纪念红衣主教弗朗切斯科-贡扎加（Francesco Gonzaga）而上演的第一部意大利戏剧《奥菲斯-波利齐亚诺斯》（Orpheus Polizianos）就取材于神话。随后，古罗马历史也被搬上了公共舞台。当时的人们并不只是想从古代作家那里吸收古代知识，他们还要求将其再现为活生生的形象。文艺复兴时期的这一需求一直延续到今天，在英国、法国、德国和瑞士的许多节日中，都有神话和历史的表现形式。保罗二世曾在狂欢节上描绘了一个盛大的凯旋游行队伍，在队伍中可以看到奥古斯都和克里奥帕特拉、战败的国王、罗马元老院、执政官、地方长官，以及所有相关的徽章，甚至是绣在丝绸上的元老院执政官。游行队伍周围挤满了神话人物。在四辆巨大的花车上，其他人高唱着祖国之父，即教皇的赞歌。红衣主教皮埃特罗-里阿里奥（Pietro Riario）用 70 辆装饰华丽的骡子表演了人民向罗马致敬的节目。1484 年，君士坦丁的故事在西斯笃四世面前的梵蒂冈宫廷中上演。1500 年，为纪念切萨雷-博尔哈（Cesare Borgia），在纳沃纳广场举行了凯撒大帝的凯旋游行。为了庆祝卢克蕾齐娅（Lucrezia）与费拉拉的阿方索（Alfonso）结婚，梵蒂冈上演了装饰华丽的田园喜剧，圣彼得广场则上演了罗马历史场景；而在弗利尼奥，则上演了巴黎审判，以纪念教皇的女儿。

即使是这样的庆典活动，也是通过古代人物来象征性地表达当代环境；但历史感也发展到了当代历史的戏剧化。西班牙大使在纳沃纳广场上庆祝格拉纳达的陷落，摩尔人的城堡被攻破。与此同时，拉斐尔-瑞阿里奥（Raphael Riario）也在他的宫殿里上演了一场与同一主题有关的戏，由秘书卡罗-维拉尔迪（Carlo Verardi）用散文写了拉丁文本。剧场是在院子里临时搭建的，正如作者所夸耀的那样，演出获得了最热烈的掌声。在序幕的诗句中，作者向观众宣布，他呈现给他们的不是普劳图斯或奈维乌斯虚构的喜剧，而是真实的历史和一部严格意义上的道德剧。该剧以国王波阿布迪尔和他绝望的议员们之间的对话开始；巴加泽特的使者出现并鼓励抵抗：接着是斐迪南和他的骑士们之间的对话。剧中没有任何动作，只有跑腿的人和使者报告幕后发生的事情。整部剧充满了童趣和原始气息。

维拉尔迪的侄子马凯利努斯（Marcellinus）写了一部拉丁戏剧《费迪南德的仆人》（Ferdinandus Servatus），讲述的是从刺客手中营救西班牙君主的故事，1492 年 4 月由同一位红衣主教里阿里奥（Riario）上演。这些毫无艺术性可言的对话至少包含了未来戏剧的种子，尽管它们本身似乎比意大利最古老的戏剧尝试--阿尔贝蒂诺-穆萨托的悲剧--还要落后。但无论是神圣的奥秘还是世俗的场景，都没有发展成为意大利的民族戏剧。意大利文艺复兴时期没有发展成民族戏剧的原因，究竟是教会和宗教裁判所没能阻碍西班牙戏剧的发展，还是希腊舞台没有扼杀的节日盛典的丰富发展，这一点非常值得怀疑。这种不足可以从意大利的大众精神本身寻找，它似乎并没有在个人激情中得到戏剧性的熏陶。文艺复兴时期还鄙视一切流行事物，取而代之的是普劳图斯（Plautus）和特伦斯（Terence）的古典喜剧。

古代喜剧的宝藏很快从人文主义者手中转移到了王公贵族的舞台上，尤其是在曼图亚和费拉拉。在罗马，又是西克斯图斯四世的两位红衣主教侄子上演拉丁戏剧，尤其是拉斐尔-瑞阿里奥在这方面声名鹊起。庞波尼乌斯鼓励并指导了这些演出。他的学者们自己也是演员。由于没有固定的剧场，这些戏剧在不同的地方演出，包括红衣主教府邸的庭院、庞波尼乌斯的宫廷、圣天使城堡、梵蒂冈，甚至有一次在元老宫演出，当时西斯笃四世的一个侄子被任命为城市长官。他们主要在瑞阿里奥宫殿的庭院中演出。红衣主教的舞台是可移动的，是一个五福森高的脚手架（_pulpitum_），甚至还有彩绘装饰。因此，它类似于今天的 pulcinello 舞台，可以搭建在这里或那里。观众坐在一排排木制座椅上，用绷布遮挡阳光。这就是一位目击者描述的爱好艺术的红衣主教宫殿庭院中的戏剧表演。从演出地点可以看出，受邀观看演出的观众人数可能很少。当时人们希望这位开明的红衣主教能在罗马建造一座常设剧院，但这只是一个虔诚的愿望。另一方面，赫拉克勒斯一世（Hercules I）在费拉拉（Ferrara）建造了一座剧院，并在落成典礼上演出了意大利语翻译的普劳图斯（Plautus）的《门徒》（Menächmen）。
