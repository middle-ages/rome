_5\. 历史的书写 弗拉维奥-比昂多 萨贝里库斯。皮乌斯二世的回忆录。Ammanati. 帕特里齐。教皇史续编。作为教皇传记作者的人文主义者。Vespasiano. 马内蒂 坎帕纳斯 卡内西乌斯 维罗纳的加斯帕罗 普拉蒂纳 他的教皇史 沃尔特拉的雅各布斯 斯特拉斯堡的布尔卡德 罗马日记 保罗-佩特罗尼 南蒂波尔图的公证人。

十五世纪，历史学也蓬勃发展。城市、王公和暴君，甚至平民都找到了自己的历史学家；罗马教皇有了自己的第一位历史学家，中世纪也有了自己的历史记载。李维、萨卢斯特和普鲁塔克是人们希望在语言和形式上达到的典范。为了打破修道院和城市编年史的过时形式，为了获得政治立场的考量，为了将历史学的诉求提升到艺术作品的高度，通过古典主义的途径是必要的。布鲁尼和波焦的佛罗伦萨史是人文史学的开端，是冷冰冰的模仿，但人们早已认识到，如果没有这个古典主义学派，就很难产生马基雅维利和吉恰尔迪尼的国家著作。

无需解释为什么罗马没有人着手编写城市史。科里奥可以撰写米兰史，科伦纽奇奥可以撰写那不勒斯通史，佛罗伦萨可以撰写从波焦（Poggio）到马基雅维利（Machiavelli）的历史学家系列丛书，威尼斯可以撰写萨贝里科（Sabellico）、朱斯蒂尼亚诺（Giustiniano）和本博（Bembo）的著作，但罗马不再是一个国家：因此，它的历史不得不回到意大利和教会的圈子里。

在罗马，我们可以把属于历史学领域的一切都归于五个名字：布隆达斯、皮乌斯二世、普拉蒂纳、布卡德和因费苏拉。它们分别代表通史、回忆录、教皇史、日记和年鉴。

比翁多的原著《罗马帝国衰亡三十年》是吉本作品的前身。与他同时代的马特奥-帕尔米耶里的名作是一部世界编年史，而比翁多则首先着手撰写从阿拉里克到他自己时代的帝国和意大利的中世纪历史。他按照李维的模式将历史划分为几十年。他的成就确实令人钦佩，因为他是第一个进入一个基本不为人知的领域的人。虽然他的作品中仍有不少错误和批评，但他对资料的了解令人惊叹，尤其是在人们只追求经典的时代，对中世纪编年史的研究还是新生事物。布隆达斯并不追求人文之美，他的目的是将埋藏在黑暗中的东西揭示出来。因此，他实际上首先从编年史中引出了中世纪的历史，并将其理解和确立为人类的一个时代。庇护二世摘录了卢修斯-福努斯翻译成意大利文的《比昂多十年》。

除了布隆多斯，罗马还有萨贝里库斯，如果这位多产作家没有为威尼斯共和国献身的话。她使萨宾人疏远罗马，就像她吸引伏尔斯人阿尔都斯一样。马尔康托尼奥-萨贝里科是乔瓦尼-科乔的儿子，1436 年左右出生于蒂沃利附近的维科瓦罗奥西尼亚城堡。他是庞波尼乌斯的学生。由于对学院的审判，他似乎也逃离了罗马。1475 年，他成为乌迪内的一名教授，并在那里撰写了一部关于阿奎莱亚古迹的著作。1484 年，他应召前往威尼斯；后来他去了维罗纳，在那里他代表威尼斯撰写了非常粗略的威尼斯共和国史，后来贝姆博继续撰写。他最伟大的作品是《历史狂想曲》（Enneads or Rhapsodies of History），这是一部截至 1504 年的世界通史，这部作品材料丰富，但研究深度不够，受到了比翁多的影响。萨贝里库斯于 1506 年去世。

皮乌斯二世的著作是当代史最丰富的资料来源之一，在他的著作中，广泛的生产史和与之相关的地理构成了核心内容。埃涅阿斯-西尔维乌斯不再属于片面的古典人文主义者，尽管他与他们有很多共同之处。他是文学新潮流的代表人物；他是一位修辞学家和世故之人，懂得诙谐地谈论一切，拥有丰富的知识。因此，他的著作体现了受过现代教育的个性。它们立足于当下，其源头也是偶发的、个人的。作者并不勉强自己形成一种学术风格，而是以其自由流动的风格吸引读者。如果一位教皇现在用自己的作品而不是注释和布道来为世人提供令人振奋的娱乐，那么欧洲的精神状态一定会发生巨大的变化。

在他担任教皇之前的时期，他出版了许多主要是历史方面的书籍，如关于巴塞尔会议、波希米亚史、腓特烈三世或奥地利史、约旦哥特人史摘要以及地理著作。他计划撰写一部巨著，将各民族的历史，尤其是他所处时代的历史与对各国的描述联系起来。这本宇宙论包括亚洲和欧洲两部分。但这部著作仍然支离破碎。当他还是教皇时，他在蒂沃利完成了 "亚洲"，即对小亚细亚的描述。他对这部作品最为重视；但后人可以没有这部作品，也可以没有教皇的其他著作，他们将永远把 "评论 "视为他的主要作品。

像凯撒这样的教皇写回忆录是史无前例的，这表明他的个性已经完全摆脱了种姓和传统的束缚。皮乌斯二世撰写这些回忆录也不是为了美化教会，而是出于将其丰富的一生传给子孙后代的需要，这一生在教皇宝座上结束。这些回忆录涵盖了从 1405 年到 1463 年这一时期，不仅对当代历史具有重要价值，而且也是一面镜子，在这面镜子中，皮乌斯二世作为一个人和作家的整个本性、他的倾向、才能和机智都展现得淋漓尽致。在这里，他展示了自己是一位诗人、古董商、现代自然爱好者，甚至是一位风俗画家。他对罗马坎帕尼亚、蒂沃利、维科瓦罗和阿尼奥山谷的描写，或对奥斯提亚的描写，或他在阿米亚塔山或阿尔班丘陵的夏日旅居的描写，完全是现代的，可以作为任何现代旅行者的指南和范本。评注是教皇晚年的旨意，只是允许他的宠臣坎帕诺对其进行润色和修改，甚至将其删去，这一点令人遗憾。安马纳蒂枢机主教一直将这部作品延续到 1469 年，这部续集的版本尤其珍贵，因为其中收录了枢机主教的许多信件。

锡耶纳人阿戈斯蒂诺-帕特里齐（Agostino Patrizi）也是保罗二世的司仪，他曾作为秘书为这位受过良好教育的人文主义者安马纳蒂服务。他撰写了一部锡耶纳历史，但一直未出版，并根据塞戈维亚的约翰的著作出版了巴塞尔会议的历史和记录。他于 1496 年在罗马去世。

皮乌斯二世的自传仍然是文学作品中独一无二的产品，因为没有其他教皇效仿他的做法。它自然使 15 世纪所有关于所谓 "教皇生活 "的作品黯然失色。然而，现在人文主义者，尤其是教皇的秘书们，也开始为他们的赞助人撰写传记，他们写的不是忠实的历史传记，而是修辞上的悼词，几乎是葬礼上的演说词，由于强调肖像的艺术性，往往很有吸引力。普鲁塔克创造了一种新的传记文学，在现代人格时代，这是文艺复兴时期最受欢迎的题材。佛罗伦萨人维斯帕西亚诺（Vespasiano）用意大利语撰写了 103 篇简短优美的 15 世纪名人传记，他还撰写了尤金四世和尼古拉五世的生平。坎帕纳斯（Campanus）和迈克尔-卡内修斯（Michael Canensius）分别为庇护二世和保罗二世撰写了类似的悼词。西克斯图斯四世的生平始于普拉蒂纳。

巴托洛梅奥-萨基（Bartolomeo Sacchi）或普拉蒂纳（Platina），来自克雷莫纳的皮亚迪纳，最初是一名军人，在曼图亚师从维托里诺并取得了巨大成功。他被红衣主教贡扎加吸引到罗马，安马纳蒂将他推荐给庇护二世，庇护二世任命他为缩写师。在与美第奇家族的接触中，普拉蒂纳还加入了贝萨里翁和庞波尼乌斯。在经历了保罗二世的考验后，他在西斯图斯四世时期开始了幸福的生活，西斯图斯四世让他担任自己图书馆的管理员。从那时起，普拉蒂纳就在奎里纳尔的宅邸中生活，受到了极高的尊敬。他端庄的外表、铿锵有力的嗓音、步态和姿态，无不显示出他受过良好教育。1481 年 9 月 21 日，他死于鼠疫；1482 年 4 月 18 日，罗马学院在他的家中举行了纪念活动。

西斯笃曾委托他两项任务：收集有关罗马教廷世俗权利的文件和撰写教皇史。普拉蒂纳因此编写了一本三卷本的文件集；虽然教会的年鉴编纂者使用过这本书，但它没有印刷，现在仍收藏在梵蒂冈博物馆。作为档案保管员，普拉蒂纳掌握了有关教皇历史的所有资料。他是第一个完成这项最艰巨的历史任务的人，而这项任务今天还没有任何一个力量能够完成，这是他的荣耀。西克斯图斯四世本人也是一位小教皇，却把教皇史交给了一位被怀疑是基督教否认者的诉讼学者，也许没有什么比这更清楚地表明人文主义战胜了修道院主义了。普拉蒂纳还以人文主义者的身份对待他的研究对象。他写得轻松优雅。但他的作品没有历史基础，没有精神洞察力，只是一本令人愉快的手册，其中的古典传记被视为范本。即使不从文化史或哲学的角度来看待他那个时代的历史，也可以说普拉提纳只是一个二流人才。对于同样的任务，布隆达斯会采取更宏大、更历史性的视角。普拉蒂纳热爱真理，判断坦率。他也认为有必要进行批评，但他并不尖锐，他不想扰乱他的表述。对于较早的时期，他使用了 "阿纳斯塔修斯 "等人的教皇生平；对于他自己的时期，他则是独创的。高尚地蔑视年鉴学家编年史的人文主义方式使他的作品难以使用。他对保罗二世进行了报复，以一个野蛮人的怨毒形象结束了他的一生；他在这里夸大了，但并非在所有事情上都夸大了。普拉蒂纳的作品始终标志着从马蒂纳斯-波洛努斯或里科波尔德的僧侣式谎言和寓言到历史的巨大进步：它取代了中世纪的这些手册，首次记述了教皇的生平，符合当时教育的需要。这本书很快传遍了全世界；潘维尼乌斯后来继续撰写这本书，直到今天，人们还能津津有味地阅读这些教皇传记。

普拉蒂纳还撰写了曼图亚史、多篇论文和对话录，以及内里-卡波尼等人的传记。他还开始撰写西克斯图斯四世的生平，这位教皇被允许感叹死亡夺走了他感激不尽的传记作者。然而，这位教皇的历史是由沃尔特拉的雅各布斯（Jacobus of Volterra）撰写的，他最初是安马纳蒂的秘书，后来成为西斯图斯四世的秘书。作为一位受过教育的人文主义者，雅各布斯的文风简洁明快；他避开政治；他从不批评或刻画人物。他对西克斯图斯四世很有好感，但抱怨西克斯图斯四世的学习热情在他的时代有所减退。他的著作素材丰富，开启了 "日记 "的先河，自西斯笃四世以来的罗马历史基本上都是从这些 "日记 "中写成的。这些日记由教皇的礼仪官或独立的市民撰写。后者是教皇礼拜堂的神职人员，他们记录教皇每天所做的一切，或宫廷中发生的与官方仪式有关的一切；因此他们的日记也应运而生，其中大部分是关于仪式的干巴巴的报告，但也包含历史数据。

在这些日记中，约翰-布卡德（Johann Burkard）的日记几乎获得了神话般的声誉，而作者本人对此却几乎一无所知。这位教士来自斯特拉斯堡附近的哈斯拉赫，1481 年年轻时从那里来到罗马，1483 年成为礼仪总管，并一直担任霍尔塔主教这一有影响力的职位。皮乌斯三世曾在 1503 年许诺给他这个主教职位，朱利叶斯二世也在布卡德没有居住在那里的情况下确认了这一许诺。他的日记始于 1483 年 12 月，止于 1506 年 4 月 27 日；他似乎是为了自己使用而写下这些日记，而非官方记录。从英诺森八世和亚历山大六世统治的整个时期到 1494 年，他几乎只记录了一些形式上的东西。从 1494 年起，他的作品开始具有历史性。他用粗糙的拉丁文写作，毫无科学和人文教育的意识，甚至毫无才干：一个没有头脑的官方迂腐者。博尔哈宫廷史中的事实尤其让布卡德的日记声名鹊起。他的报道简单干练，从不妄加评论，但这恰恰是他真实可信之处。他的日记不能像普罗科皮乌斯的《阿卡纳史》（Historia Arcana）那样沦为小册子，但它却是当时教皇历史的真实来源，无可争辩。有人声称，在他的日记副本中存在插文；如果这是真的，那么无论有多少段落被标注为插文，它们都能在最著名的副本中找到，这就非常引人注目了。因此，它们指向一个共同的来源，即伯卡德本人的日记。每个副本都不完整。日记的亲笔手稿收藏在梵蒂冈图书馆。

布卡德死后，他自己的手稿最初由他的继任者博洛尼亚人帕里斯-德-格拉西斯（Paris de Grassis）接手，此人宣称手稿上的字迹难以辨认。帕里斯认为，这是出于妒忌的保密心理。他是前任的死敌，不仅是作为意大利人，还是作为年轻的同事。1504年5月，布卡德曾试图阻挠他被任命为第二司仪，帕里斯是个更无趣的迂腐之人，他自己也是这么说的。最常见的莫过于这种同事之间的私人恩怨。帕里斯曾多次在日记中抱怨布卡德对他不闻不问，对他的艺术和工作不加指导，一切都按照他自己的意愿行事。在新圣彼得大教堂奠基仪式上，他就是这样自作主张的。他甚至愤怒地指责他偷了尤利乌斯二世在奠基仪式上沉入水底的一枚纪念币。他对他进行了最粗鲁的辱骂。然而，我们不知道他在巴黎的任何一段话中亲自攻击了布卡德日记的内容，或指责他捏造或歪曲事实。通过同时代的报告，即威尼斯、佛罗伦萨和费拉拉大使的报告，我们几乎可以无一例外地证明布卡德所提供的历史信息的准确性。

此人在教皇宫廷中备受尊崇，深受尤利乌斯二世的喜爱，尽管巴黎指责他狡猾地强行担任了教皇的助理和顾问。他于 1506 年 5 月 15 日在罗马去世，当时他刚刚下令举行了四月份最值得纪念的仪式之一：为世界上最大的圣殿圣彼得大教堂奠基。他的敌人和继任者不得不在圣玛丽亚-德尔-波波罗（S. Maria del Popolo）组织他的遗体告别仪式，而且他自己也承认，他是以一种严峻的责任感来完成这项工作的，以至于引起了人们的笑声。

在罗马，还有一位德国人文学者与亚历山大六世的私人生活有着千丝万缕的联系，他就是来自纽伦堡的劳伦提斯-贝海姆（Laurentius Behaim），可能来自著名骑士马丁（Martin）的家族。在博尔吉亚还是红衣主教的时候，他以看守人的身份为博尔吉亚服务了 22 年。遗憾的是，他没有利用闲暇时间撰写罗马回忆录，而是抄写碑文，这些碑文被收集到纽伦堡。

从那时起，人们就开始尝试撰写罗马的当代史，我们已经提到了安东尼-佩特里（Antonius Petri，1404-1417 年）的罗马日记。在尼古拉五世时期，罗马贵族 Laelius Petronius 之子保罗用意大利语撰写了所谓的《罗马年鉴》（Mesticanza，1433-1446 年）；这本著作缺乏灵气，没有历史感，但因一些注释而显得天真烂漫，非常有用；但它远远落后于《科拉-迪-里安佐的一生》。

在这些记者中，只有来自特雷维地区的罗马人斯特凡诺-因费苏拉（Stefano Infessura）具有真正的重要性。此人的生平无人知晓，只是通过他我们知道他曾于 1478 年在霍尔塔担任民选官，随后成为元老院书记官。他部分用意大利语、部分用拉丁语撰写了罗马城的日记，日记的开头很零散：因为日记从 1295 年开始，然后跳到了 1403 年，记述了 15 世纪上半叶的历史，好像是从其他编年史家那里摘录的，然后变得独立而丰富，尤其是从西斯笃四世开始。显然，Infessura 并没有实施更大的计划。他是一名法学博士，但与布卡德一样，没有受过人文教育。他对罗马的科学和艺术生活丝毫不感兴趣。布卡德是一名宫廷官员，他的人性从未大胆表达过：而在因费索拉身上，一个直言不讳的公民的心在跳动，他的思想在做出判断。他表明自己是一个天性简单粗暴的实干家，一个罗马爱国者，一个有倾向性和原则性的共和主义者，一个教皇权力的敌人，这就是为什么他公开承认自己是朋友波卡洛的崇拜者。这就是为什么他在批评教皇，尤其是他深恶痛绝的西克斯图斯四世时使用了最鲜明的色彩。但无法证明他篡改了历史。但他是片面的，他几乎不知道该如何评价西斯笃的功绩。他可以被称为罗马城最后的共和主义者；一个最有能力、充满公民荣誉感的人。他是教皇西斯笃斯和英诺森时期公共生活的最佳导师，他是这一时期公共生活的主要来源。他功勋卓著的著作被多次采用。就连曾任霍尔塔主教的布卡德（可能是因费苏拉的朋友）也在 1492 年的一些地方写下了他的名字。
