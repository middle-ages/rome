### 第三章

_1\. 皮乌斯二世于1459年1月前往曼图亚。海姆堡的格里高利 安茹的约翰在那不勒斯的王位请求者。罗马的骚乱。提布尔特人 教皇回归 1460 年 10 月 7 日 提布尔特人被消灭 对罗马男爵和皮奇尼诺的战争。与马拉泰斯塔家族的战争。安杰文在那不勒斯战败。皮科洛米尼家族对庇护二世的任人唯亲。1463 年推翻马拉泰斯塔家族。

1459 年 1 月 22 日，庇护与贝萨里翁、埃斯托特维尔、阿兰、卡兰德里尼、巴尔博、科隆纳和博尔吉亚红衣主教一起离开罗马。长长的教廷队伍骑马离去。乡下人蜂拥而至，争相一睹教皇的风采。教皇与以往任何一位教皇都不一样，他穿过空旷的乡村，只有几名武装人员掩护。即使身为教皇，流浪的皮科洛米尼也热爱旅行；但在纳尔尼，他已经对旅行感到厌恶，贪婪的人们扑向他，要撕下他头上的披带。剑光在教皇眼前闪过；他还记得腓特烈三世在前往罗马的途中曾在维泰博遭遇的残酷场面。从那时起，他就坐上了轿子，轿夫每走五百步就换一个。就这样，从罗马到曼图亚的短途旅行花费了四个月之久，其中还包括在城市里度过的时间。

皮乌斯二世在斯波莱托看望了妹妹卡特琳娜，并在阿西西稍事休息后，乘坐白色轿子进入佩鲁贾，该城的治安官们则在他的头顶上搭起了紫色的顶篷。十二匹白马由马夫牵着金缰绳走在他前面。他进入翁布里亚首府将是一场皇家盛典，因为已经有七十年没有教皇在这里露面了。他接受了蒙特费尔特罗家族的费德里戈的朝拜，并选他为自己的将军。他在佩鲁贾停留了三个星期，然后踏上特拉西梅诺湖前往锡耶纳。这个由人民党统治的共和国对自己的同胞充满了怀疑。虽然它恢复了皮科洛米尼家族的地位，但它担心皮乌斯会要求恢复所有贵族的地位。她的使者已经在罗马与皮乌斯进行了谈判，他们还出现在佩鲁贾：威权统治者要求皮乌斯不要作为自由的敌人前来，而应避免干涉城市的宪法。市民们满腹狐疑。经过长时间的谈判，锡耶纳使节在丘西（Chiusi）追上了教皇，并允许他进入自己的家乡。在去那里之前，皮乌斯参观了他的出生地科西尼亚诺和他父亲的坟墓，以证明他的教皇之名是正确的；他的母亲四年前被葬在锡耶纳。他决定将科西尼亚诺升格为主教区，并用宫殿加以装饰。他将这个地方命名为皮恩扎（Pienza），即使在今天，那里宏伟建筑的废墟仍在提醒人们皮乌斯二世的虔诚。

当他于 2 月 24 日抵达锡耶纳时，千头万绪的事情让他想起了过去，其中有许多他本想用面纱遮住，而他却更高兴地流连于他将唐娜-莱奥诺拉带到皇帝面前时的庆典。在宏伟的大教堂里，他向人们讲述了他的共和国的伟大，它为教会带来了一位举世闻名的教皇--亚历山大三世。他向威权统治者赠送了金玫瑰，但他提出的重新允许贵族担任国家公职的不明智要求激怒了民主派。人民议会稍作克制后做出了让步。皮乌斯将锡耶纳升格为大主教区，并将格罗塞托、马萨、基乌西和索阿纳划归其管辖；锡耶纳人很早以前就在一位名叫皮科洛米尼（Piccolomini）的人的领导下征服了拉迪科法尼（Radicofani），皮乌斯将其赐予他们作为永久的教会领地。

来自皇帝、西班牙、葡萄牙、勃艮第、波希米亚和匈牙利的使节抵达锡耶纳，教皇让他们跟随自己前往佛罗伦萨，4 月 25 日，教皇在里米尼、法恩扎、福尔里和伊莫拉威权统治者的带领下进入佛罗伦萨，受到市民们的热烈欢迎，而佛罗伦萨共和国的盟友米兰的斯福尔扎则派出了他的长子加莱阿佐和华丽的随从迎接他。教皇国的暴君们，其中包括亵渎神灵的西吉斯蒙多-马拉泰斯塔家族（Sigismondo Malatesta），不情愿地将他们的主子扛在肩上，直到教皇在庄严的队伍中被带到圣玛丽亚新维拉（Santa Maria Novella），这里也曾是马丁五世和尤金四世的居所。除了威尼斯，意大利没有其他城市比佛罗伦萨更加繁荣昌盛了。佛罗伦萨共和国的首领仍然是科西莫，他是佛罗伦萨的第一位公民，也是佛罗伦萨最富有的商人，他主宰着欧洲、亚洲和非洲的市场，他是克洛伊索斯，同时也是意大利最睿智的政治家。皮乌斯看到了这位赞助人建造的建筑，估计其造价为 60 万弗罗林。睿智的美第奇彬彬有礼地接待了他，他只是谨慎地谈论了意大利的事务。皮乌斯在佛罗伦萨逗留了八天，人们为他举行了盛大的庆祝活动，包括狮马比赛。他悼念了被尊为圣人的安东宁大主教的去世，安东宁大主教是在他逗留佛罗伦萨期间去世的。

如果说他在此之前的旅程就像凯旋游行，那么当他越过亚平宁山脉时，情况就会发生变化；因为在那里，教皇的统帅影响力即使不是地理帝国，也已经停止了。拉丁姆、萨比纳、斯波莱托和都西亚，尽管圣彼得的领地并不安全，但至少还在罗马的势力范围之内，但越过亚平宁山脉，一个不同的国家地区就开始了。罗马涅和马尔凯是教皇国最遥远和最不安定的省份，其政治中心在米兰和威尼斯。那里首先是强大的博洛尼亚，高耸入云的塔楼上写着 "Libertas"。在那里，教会的使节没有丝毫权力。相反，本蒂沃格里人在睿智而强大的桑蒂的指导下进行统治。在斯福尔扎的建议下，博洛尼亚人已经邀请教皇到罗马访问他们的城市，但同时也让十队米兰骑兵进入了他们的城墙。当市民们听说教皇即将到访时，他们变得躁动不安，仿佛一个暴君即将到来，给自由带来死亡。皮乌斯本人拒绝进入博洛尼亚，直到这些米兰军队宣誓就职，并由先于他到达博洛尼亚的年轻的加莱阿佐指挥。双方达成了谅解：5 月 9 日，皮乌斯光荣地被带入博洛尼亚；地方长官跪在地上，把城市的钥匙交给了他，他又把钥匙还给了安齐亚人；最高贵的市民们抬着他的轿子，但当他从轿子里向人们祝福时，却看到黑暗的宫殿被手持武器的反抗青年包围着。律师博尔尼奥是一位喋喋不休的演说家，他在公开欢迎仪式上对博洛尼亚的无政府状态表示哀叹，并鼓励教皇改革这座城市。

5 月 16 日，比约怀着比抵达时更愉快的心情离开了阴森的博洛尼亚。他在一艘船的护送下沿波河而下。然后，5 月 18 日，摩德纳公爵博尔索带领他来到了费拉拉，他从教会那里获得了费拉拉的封地。这座城市挤满了不远万里赶来参加庆典的人们，因为博尔索用各种敬意、游戏和庆典来庆祝他的客人。当他试图通过取消所欠贡品和授予费拉拉公爵头衔来收回这笔巨额开支时，他的意图没有得逞。他怀恨在心，在装饰精美的贡多拉犁过的波河上护送教皇前往曼图亚领地。他承诺会及时赶到那里，但他从未做到。

皮乌斯最终于 1459 年 5 月 27 日抵达曼图亚。统治这座维吉尔古城的是文质彬彬的洛多维科-贡萨加，他是战功赫赫的乔万-弗朗西斯科的儿子，西吉斯蒙德曾将他提升为侯爵。他跪在城门口，向教皇递交了这座城市的钥匙，并将其交由教皇处置，然后列队护送教皇前往他的住所。在洛多维科的继任者将其扩建为意大利最宏伟的庄园之前，这座被黑塔环绕的城堡甚至可以与当时的乌尔比诺城堡相媲美。

曼图亚议会在欧洲历史上留下了浓墨重彩的一笔。它是第一次真正意义上的大国大会。由于土耳其战争仍被视为十字军东征，教皇认为自己不仅有权召集会议，还有权担任会议主席。帝国首脑的声望如此之低，以至于没有人对这一授权提出异议，皇帝平静地将管理欧洲事务的重任交给了教皇；当然，他也预见到了徒劳无功的结果。皮乌斯的期望落空了，因为曼图亚的使节空空如也；欧洲各国，甚至意大利，要么根本没有派使节参加会议，要么派得很晚。此外，费兰特的受封阻碍了教皇的计划，因为他建立了一些党派，这些党派今后将决定意大利的组织。法国国王代表了安茹家族的要求，威尼斯和佛罗伦萨都倾向于法国；现在奥尔良家族也对米兰提出了要求，这迫使斯福尔扎支持费兰特的事业。他与庇护二世结盟。在将妻子和五个孩子（包括加列阿佐-玛丽亚和十六岁的希波吕忒）送往曼图亚后，他本人于 1459 年 9 月抵达那里。

被围困在伯罗奔尼撒半岛的古生物学家托马斯以及其他来自伊庇鲁斯、莱斯沃斯、塞浦路斯和罗德斯的使者将拜占庭帝国最后残余的求救信号带到了教皇宝座前，皮乌斯于 9 月 26 日宣布大会开幕。他对土耳其人的演讲赢得了西塞罗式雄辩家的掌声，但却没有引起听众对 "Deus lo vult "的呼喊，而乌尔班二世曾在克莱蒙对这种毫无艺术性的言辞做出过回应。在他之后，贝萨里翁以枢机团的名义发表了长篇演讲。会议充满了华丽的辞藻或令人尴尬的争论，直到最后教皇在 1460 年 1 月 15 日的诏书中总结了他的努力成果，诏书宣布从 4 月 1 日起在欧洲发动为期三年的土耳其战争，并宣布对基督徒和犹太人普遍征收十一税。这次十字军东征的旗手是皇帝腓特烈三世，历史上没有比第一和第二任腓特烈更伟大的漫画了。这位维也纳总理府的前办事员被允许冒昧地正式任命帝国元首为十字军总指挥。他派贝萨里翁（Bessarion）作为使节去见他；这位红衣主教在皇帝和帝国诸侯面前用尽了口才，却徒劳无功，直到他返回意大利也没有成功。

教皇于 1 月 18 日颁布了一份诏书，宣布从今往后，无论谁向议会提出上诉，都将被视为异端邪说和亵渎君主的罪行而受到惩罚，会议就此结束。诏书_Execrabilis_是这次会议上出现的最令人震惊的文件。在教皇派看来，这可能是它的真正目的。它取消了康斯坦茨和巴塞尔会议的成果；它旨在确保教皇君主制不受议会运动浪潮的冲击，自中世纪教皇制度结束以来，议会运动对教皇君主制的冲击越来越大。无论是敌对的王公还是需要改革的人民，向大公会议发出呼吁都是所有武器中最强大的武器，而现在，庇护二世自以为是地认为，他已经永远打破了这一武器，永远拯救了教皇君主制。当这份诏书公布时，每个人都会惊讶地发现，它的发布者正是曾经如此热心地维护巴塞尔大公会议权威的皮科洛米尼教皇。现在，他通过向教会大会发出呼吁，也禁止了教会的全面改革，因为只有教会大会才能强迫任何教皇进行改革。皮乌斯二世之所以在曼图亚发布命令，是因为他要求法国使节放弃布尔日的实用主义认可，而法国使节威胁要向议会提出申诉。蒂罗尔的西格蒙德公爵也采取了同样的行动，当时他正与库萨枢机主教就后者占据的比克森主教区发生激烈争执。皮乌斯很快得知他的诏书被忽视了；西格蒙德大胆的顾问、德国最有权势的人物之一、宗教改革的先驱海姆堡的格雷戈里向议会提出上诉，教皇于 1460 年 10 月 18 日禁止了他。西格蒙德与库萨之间的争论在较小的范围内重演了路德维希二世与教皇的斗争；在海姆堡，他树立了一位斗士，他以同样的胆识和更加锐利的精神捍卫马西利乌斯的原则。这场斗争属于德国历史，它与宗教改革已经显而易见的结构交织在一起，我们之所以记住它，只是因为那份大胆的诏书_Execrabilis_，德国的宗教改革精神通过海姆堡给出了答案。

皮乌斯于1460年1月底离开曼图亚前往锡耶纳，并于31日抵达。他病了，而且对阻碍他的世界局势深感失望。英格兰、西班牙和德意志的王朝混乱不堪；那不勒斯也爆发了战争。在那不勒斯，许多男爵出于对费兰特的憎恨和旧日的效忠，站在了安奇温党一边。塔兰托的贾南托尼奥-奥尔西尼家族（Gianantonio Orsini）、塞萨王子马里诺-马尔扎诺（Marino Marzano）、克罗托内侯爵从热那亚召见了勒内的幼子，勒内在热那亚担任法国查理七世的摄政王，因为这个受阿方索压迫的共和国在 1458 年接受了法国国王的保护。洛林-安茹的约翰并没有被他的家族在那不勒斯的一系列不成功的伪装者运动所吓倒，而是早在 1459 年 10 月就带着一支最初在马赛为十字军东征准备的舰队出现在了那不勒斯海岸，当时大多数男爵都宣布支持他。费兰特很快发现自己被限制在那不勒斯和坎帕尼亚。1460 年春，安茹日益强大的势力壮大了皮奇尼诺的实力，而教皇刚刚在曼图亚向费兰特要求和平的西吉斯蒙多-马拉泰斯塔家族也脱离队伍，拿起武器。国王现在向斯福尔扎和教皇寻求庇护；两人都向他派遣了军队，但他在 1460 年 7 月 7 日的萨尔诺战役中失利，不久之后，皮奇尼诺在阿布鲁佐的圣法比亚诺战胜了教皇的督军亚历山德罗-斯福尔扎和乌尔比诺的费德里戈。

皮乌斯二世当时仍在锡耶纳，一方面忙于建设皮恩扎，另一方面在马切雷托和佩特里奥洛的巴登浴场恢复健康。就在那时，罗德里戈-博尔哈奢侈的生活方式引起了教皇的不满：这位教会副主教在锡耶纳的花园里与美女们举行宴会，而男人们却不能参加。皮乌斯给他写了一封严肃的信，这是未来的亚历山大六世私人历史上的第一份文件。

罗马传来了越来越坏的消息。罗马教廷的缺席造成了无法无天的局面，让人想起了这座城市最黑暗的时期。从皮乌斯二世的宫廷诗人康帕纳斯的描述中可以看出，当时的人口甚至给受过教育的意大利人留下了深刻的印象。他满怀憧憬地来到这座城市，在失望之余，他给朋友马特奥-乌巴尔多（Matteo Ubaldo）写下了这样一段话："这里的人更像野蛮人，而不是罗马人，令人作呕，方言不同，纪律涣散，文化程度像农民。这也难怪，他们从世界各地涌向这座城市，就像涌向一个奴役的兽栏。只有少数市民还保留着旧贵族的特征。因为他们蔑视武器的荣耀、帝国的伟大、道德的严谨和正义，认为这些都是过时的、陌生的东西，他们把自己扔进了奢侈和软弱、贫穷、傲慢和无节制的欲望之中。- 罗马的陌生人不过是一群仆人，有厨师、香肠师、怂恿者和小丑。这样的人现在占据了元老宫。他们用各种罪恶的污秽玷污了他们所居住的卡图利、西庇阿和凯撒的杰出雕像。缅怀罗马人民、元老院和军队的光辉事迹，看到那些辉煌的雕像沦落到如此肮脏的境地，而著名的罗马人、军事领袖和皇帝的宅邸如今却被刺客、厨师和皮条客所占有，他们的碑文被炊烟和难以言喻的污秽所覆盖，除非是蔑视、粗心大意或年老体衰将其彻底毁坏，否则谁不应该感叹他们的悲惨生活和命运的无常呢？"

这幅画恶毒而夸张，但也有一定的真实性；它揭示了博尔吉亚统治自然发展的要素。坎帕纳斯本人就是一名教士，所以当他在同一封信中说："所有的尊严都属于司铎，是他们的出身或天才让他们升到了这个地位 "时，也就不足为奇了。然后，他又皱着眉头厚颜无耻地补充道："正是这些人让罗马变得像努玛的圣洁而不是罗穆卢斯的力量一样。但并非所有人都能成为司铎"。

罗马当时的状况证实了这样一个真理：只有自由才能使一个民族变得高贵，而缺乏自由才是堕落的真正根源。我们看到罗马人的政治生活日益堕落，直至在复辟教皇统治下消亡。在波尔卡罗，民主运动已经堕落到卡蒂林的地步；而在 1460 年的英雄提布尔提乌斯和瓦勒里亚努斯身上，民主运动则沦落到强盗的地步。这对不幸兄弟的命运与波卡罗的命运有着可怕的联系，因为他们的父亲安杰洛-德-马索（Angelo de Maso）死于刽子手之手，而他们的哥哥则是刽子手的帮凶。血债血偿和对自由的渴望折磨着这些年轻人。由于无法组建政党，他们团结同龄人，让罗马充满了恐怖。他们约有三百名反抗青年，其中包括斯贝基、伦齐和罗西等名门望族的子弟，他们日夜全副武装，在罗马城游荡。总督不得不离开鲜花广场的住所，逃往梵蒂冈。这使得叛军更加胆大妄为。他们抢劫妇女，淹死不情愿的女孩，洗劫反对者的房屋。3 月 30 日，教皇写信给治安官：这是对他的侮辱：震惊的世人会说他不再是罗马人民的主人了；他不明白治安官是如何容忍罗马子民的这种暴行的；有人告诉他，这样做是为了迫使他回国；但他自己却想召回他留在那里的法庭。罗马当局仍然无能为力；红衣主教库萨早已离开了这座城市；参议员是锡耶纳的弗朗切斯科-德利-阿林希耶里。

提布尔提乌斯教派在坎帕尼亚有靠山，因为科隆纳、萨维利家族和埃韦尔苏斯可以把他们当作工具。那不勒斯战争爆发后，这些男爵又开始活跃起来；他们站在安茹一边，还与皮奇尼诺和马拉泰斯塔家族联络。蒂沃利附近的帕隆巴拉是雅各布-萨维利（Jacopo Savelli）担任男爵的地方，每当欧洲人在罗马不安全时，这里就会成为他们的避难所。5 月 16 日，一个年轻的罗马人抢劫了一个即将出嫁的女孩，抓他的人把他带到了元老宫；提伯提乌斯的强盗团伙很快从帕隆巴拉进入了罗马城；他们拖着元老院的一名成员作为人质，先是拖到了波波洛圣玛丽亚附近的一座塔楼上，然后又拖到了先贤祠。强盗们在这座教堂里盘踞了九天，对周围的居民大肆抢劫。他们不敢进攻；当局进行了谈判：囚犯被交了出来，他大笑着把偷来的女孩变成了自己的妻子。尽管如此，提布尔特人还是与俘虏大打出手，犯下了无数暴行。之所以会发生这样的恶行，是因为大部分人憎恨教皇的权力，对庇护二世的罢免和他参与那不勒斯战争感到不满。他让自己的裙带关系安东尼奥担任部队的队长，带领部队前往费兰特国王那里，同时指示他恢复罗马的和平。他率领一队骑兵抵达罗马，却一无所获。叛军先是盘踞在卢西纳圣洛伦佐附近的一座塔楼上，然后又进驻卡普拉尼卡宫。他们白天在这里狂欢，晚上则外出袭击。提布尔提乌斯是他们的国王。最后，罗马大人物劝说他离开。这位年轻的血腥复仇者在人们的陪伴下，从保护人和 Protonotar Georg Cesarini 之间骄傲地穿过城市，一直走到城门口，然后他和他的战友们撤退到了巴伦巴拉。

庇护二世在不情愿的情况下才决定回家。他之所以这样做，是因为发现了一个阴谋：塔兰托王子、埃弗苏斯伯爵、罗马男爵和提布尔提乌斯想要把皮奇尼诺伯爵召回罗马。皮奇尼诺从阿布鲁佐出发，打算对这座城市采取行动；与此同时，马拉泰斯塔家族正在攻占马尔凯地区的城镇和安圭拉家族的城镇。皮乌斯于 1460 年 9 月 10 日离开锡耶纳。罗马特使安东尼奥-阿德奥达托二世（Antonio Caffarelli）和安德烈亚-圣-克罗齐（Andrea Santa Croce）--大学里著名的法律学者--出现在维泰博，告诉他这座城市正迫不及待地等待着他：他应该原谅年轻时犯下的罪行。"教皇回答说："有哪个城市比罗马更自由？你们不用纳税，不用承担任何负担，你们担任着最尊贵的职位，你们的酒和谷物可以随意出售，你们的房子可以给你们带来丰厚的利息。此外，你们的主人是谁？是伯爵、侯爵、公爵、国王还是皇帝？不，是比这些更伟大的罗马教皇，彼得的继承人，基督的代治者--是他给你们带来了荣耀和繁荣，是他给你们带来了来自世界各地的财富。"

据说皮奇尼诺正在逼近罗马，红衣主教们建议教皇留在维泰博，直到乌尔比诺的费德里戈和亚历山德罗-斯福尔扎逼近，因为罗马人可以轻而易举地向皮奇尼诺开放这座城市。皮乌斯宣称，他必须抢在兀术之前，否则罗马和那不勒斯就会失守。斯福尔扎公爵特别坚持要教皇回来，他向维泰博派出了五百名骑兵。皮乌斯经由内皮、坎帕尼亚诺和福梅洛缓慢前往罗马。途中，他没有发现任何接待他的准备：他们临时买了一些酒和面包。总督和参议员在一处阴凉的泉水边与教皇碰面，他是个热爱大自然的人。在第六个里程碑上，守卫者们迎接了他；他们带来了一群罗马年轻人，这些人将抬着教皇的轿子，这些挑衅的男孩大多是提布尔提乌斯的同志，皮乌斯不得不忽视这一点。皮乌斯在离开罗马近两年后抵达罗马，虽然受到了罗马人民的隆重欢迎，但他的心情还是十分悲痛。他在S. Maria del Popolo过了一夜，然后于10月7日（1460年）移居梵蒂冈。

他发现罗马人对他深感不满。皮奇尼诺已经推进到了里耶蒂，并将军队推进到了帕隆巴拉。这些士兵与男爵们联合起来，破坏了萨比纳河，掠夺了奥西尼亚的庄园，并以新的破坏威胁着罗马人的经济。皮乌斯在人民议会上发表了长达两个小时的演讲，为他的那不勒斯政策辩护；由于他曾被指责热爱锡耶纳人，他肯定了自己的爱国主义，甚至从他的名字埃涅阿斯-西尔维乌斯（Aeneas Sylvius）推断出他的血统是罗马血统。如果他的雄辩不能让罗马人相信他建国的理由是正确的，那么他的出现却让这座城市感到安心。这座城市之所以对他忠心耿耿，与其说是他的敌人无所作为，不如说是运气使然。他让教皇国几乎不设防；他不重视要塞；他只招募了支持费兰特所需的部队。他带着一小队骑兵返回罗马，把守卫城市的任务留给了市民。幸运的是，皮奇尼诺还不够强大；他害怕费德里戈、亚历山德罗和红衣主教福尔泰格拉在他背后的行动。他在蒂沃利的尝试失败了。

提布尔提乌斯徒劳地呼吁皮奇尼诺下山，结束罗马令人憎恨的牧师团。很快，他自己的愚蠢就给他带来了厄运。10 月 29 日，博纳诺-斯佩基奥冒险进城，在斗兽场落入俘虏之手。随后，提布尔提乌斯带着 14 名战友勇敢地进入罗马，解救了他的朋友。他们抓住了一个锡耶纳人，把他拖走，并呼吁人们释放他。市民们回答说："太晚了！"。参议员洛多维科-佩特罗尼（Lodovico Petroni）和教皇的男仆亚历山德罗-米拉贝利（Alessandro Mirabelli）带着军队追赶这些大胆的年轻人。他们在一片甘蔗丛中抓住了提布尔提乌斯和他的五个同伴，并将他们捆绑着带到了元老宫。途中，教皇派嘲笑他是护民官和共和国的恢复者。他在严刑拷打下承认，占卜者曾预言要推翻祭司的统治，而他的计划就是将祖国从神职人员的束缚中解放出来，因为罗马人可耻地承受着神职人员的枷锁；因此，他与皮奇尼诺联合起来；他们的目的是掠夺城市，尤其是红衣主教和斯卡兰波斯人的宫殿。与不幸的一生相比，提布尔提乌斯在死后表现得更为高尚。他只要求尽快处决。教皇禁止对他施以酷刑，10 月 31 日，这个死刑犯像他的父亲一样被绞死在元老宫。与他同命运的还有博纳诺-斯贝基奥、科拉-罗西和另外两个年轻人。1461 年 3 月，又有 11 名罗马人被绳之以法，他们继续从巴伦巴拉发动袭击。这是波尔卡罗将罗马从祭司统治下解放出来的阴谋的悲惨结局。

现在，皮乌斯希望劝说雅各布-萨维利家族屈服：但这位挑衅的男爵拒绝了条件，因此被宣布为逃犯。教皇紧急向佛罗伦萨和米兰求援。冬天，亚历山德罗-斯福尔扎（Alessandro Sforza）和费德里戈（Federigo）终于将他从皮奇尼诺（Piccinino）手中解救出来，后者返回了阿布鲁佐。皮乌斯事先已经占领了泰拉奇纳。费兰特国王在萨尔诺战败后，教皇派系在这座赐予他十年之久的城市中崛起；教皇派系要求教会提供保护，庇护二世在富迪伯爵击败他之前就让他的裙带关系安东尼奥占领了泰拉奇纳。虽然这激起了费兰特和斯福尔扎的愤怒，但教皇还是保住了坎帕尼亚的这把钥匙：1460 年 10 月 21 日，他确认了泰拉希纳人的自治权，并给予他们一系列特权。

次年，教会的统帅乌尔比诺的费德里戈将整个萨比纳上交教皇。1461 年 7 月，帕隆巴拉的萨维利家族也臣服了。出于对与他有姻亲关系的罗马大人物的考虑，皮乌斯放过了这个人；他只从他那里夺走了七座城堡，其他的都留给了他。自此以后，萨维利家族日益衰败，萨宾人的所有庄园很快只剩下了陡峭的阿斯普拉和帕隆巴拉。

现在，皮乌斯在费德里戈的陪同下，得以前往蒂沃利，在那里度过夏天，建造了一座城堡，并在闲暇时勾勒他对亚洲的描述。即使身为教皇，他对乡村度假也情有独钟。当他带着一个诗人和古董商的愉悦心情游历拉丁姆、奥斯提亚、蒂沃利和阿尔班丘陵时，他从未显得如此和蔼可亲。夏天，他去伊特鲁里亚和坎帕尼亚旅行，在萨图恩古城流连忘返，他描述了这些城市的历史和状况。前几任教皇只是带着军队或在逃亡途中游历过这些地区，而庇护二世却手捧维吉尔，悠闲地游览了这些地方。

只有与西吉斯蒙德（Sigismondo）的战争和那不勒斯的战争扰乱了他的平静。潘道尔夫-马拉泰斯塔家族的私生子是个不折不扣的暴君，他亵渎神灵、英俊、勇敢、能言善辩、精通人文主义研究，还是个无神论者。冬天，皮乌斯对他和法恩扎的阿斯托里-曼弗雷迪下了咒语，这个咒语让人想起中世纪最黑暗的时代，而在一位最有教养的教皇口中则显得更加残暴。1461 年 7 月 2 日，这位强大的暴君在莱昂内城堡击打了教皇督军的头部，并将战争又持续了两年。

皮乌斯在那不勒斯的处境要有利得多，他的目标是赶走与米兰结盟的法国人。早在 1461 年 3 月，热那亚就摆脱了他们的枷锁，任命普洛斯彼罗-阿多诺为总督。法国国王和勒内的中队围攻这座解放的城市，但没有取得成功。在输掉一场战役后，他心灰意冷地回到了普罗旺斯。他年轻的儿子约翰很快在那不勒斯遭遇了同样的命运。1461 年春，皮乌斯二世任人唯亲，向费兰特派遣了军队，而斯坎德贝格-卡斯特里奥塔（Skanderbeg Kastriota）也带着掠夺性的战士从阿尔巴尼亚赶来，费兰特逐渐成为了国家的主人。他用给劳多米亚和锡耶纳人南尼-托德奇尼的儿子安东尼奥丰厚的封地来换取教皇的帮助。裙带关系诱使皮乌斯让这个无足轻重的侄子变得伟大，而那不勒斯一直是裙带关系的埃尔多拉多，它为皮乌斯提供了这样做的手段。

1461 年，费兰特先封那个安东尼奥为塞萨公爵、王国大法官，然后又封他为阿马尔菲公爵；还把他嫁给了自己的亲生女儿阿拉贡的玛丽亚。安茹的约翰战败后，尼波图斯家族更加幸运。勒内的儿子被盟友和男爵们抛弃，最后也被皮奇尼诺抛弃，他在 1463 年夏天逃到了伊斯基亚岛，并从那里逃到了普罗旺斯。皮乌斯（Pius）的武器并没有决定这些胜利，他以教会的名义要求将皮埃特罗-坎特米（Pietro Cantelmi）所在的美丽的索拉公国作为封地，因为他想把这个公国送给他的侄子。乌尔比诺的费德里戈和拿破仑-奥尔西尼家族首先攻克了伊索拉城堡，阿尔皮诺和索拉随即投降。皮耶罗向教皇求和，并向教皇交出了所有这些地方；阿方索曾从尤金四世手中征服的庞特科沃也向教皇投降。皮乌斯并不满足于此，他还要求得到富奇诺湖的切拉诺，并不诚实地利用了科贝拉伯爵夫人和她儿子鲁杰罗之间的家庭纠纷。费兰特激烈抵制了这些要求，但他认为谨慎起见还是让步了，安东尼奥-皮科洛米尼作为那不勒斯王室的附庸被授予了切拉诺的马西亚郡。

庇护二世还展示了裙带关系对教皇的诱惑是多么不可抗拒。在劳多米亚的四个儿子中，他封安东尼奥为公爵，弗朗切斯科为红衣主教，安德烈亚为 Castiglione della Pescaja 领主，贾科莫为 Montemarciano 的威权统治者。尼科洛-福尔泰格拉（Niccolò Forteguerra）与教皇有母系血缘关系，很快就因军功而闻名，成为了红衣主教；贾科莫-托洛梅（Giacomo Tolomei）在罗马备受憎恨，却成为了圣天使城堡的执达官；亚历山德罗-米拉贝利-皮科洛米尼（Alessandro Mirabelli Piccolomini）与安布罗西奥-斯潘诺奇（Ambrosio Spannochi）在罗马有一家银行，他担任看守一职，并担任弗拉斯卡蒂的校长；锡耶纳人雅各布-安马纳蒂（Jacopo Ammanati）与教皇家族的其他许多人一样，被授予帕维亚主教职位并戴上了红帽子。教皇最亲近的人是他的秘书格雷戈里奥-洛利（Gregorio Lolli），他是姨妈巴托洛梅亚（Bartolomea）的儿子。无数锡耶纳人获得了职位；可以说，锡耶纳在罗马蓬勃发展，似乎已经移居到了罗马。就连神圣的凯瑟琳也要归功于庇护二世。如果他从土耳其人手中夺取了希腊，皮科洛米尼就会被视为希腊的暴君。然而，至少皮乌斯没有以牺牲教皇国为代价使他的侄子们富裕起来，甚至在战胜了马拉泰斯塔家族之后，他也表现出了这种克制。

西吉斯蒙德遭到乌尔比诺和福尔泰盖拉的费德里戈的成功反对，并于 1462 年 8 月 13 日在曼多尔福被击败，他向威尼斯人求助，威尼斯人拥有拉文纳，保护了这位暴君，因为他们不希望教会在亚得里亚海变得强大。皮乌斯看穿了共和国的意图，共和国刚刚在1463年5月从多梅尼科-马拉泰斯塔-诺韦罗（Domenico Malatesta Novello）手中买下了切尔维亚（因其盐矿而闻名）；他激烈地拒绝了共和国的要求，直到在费德里戈征服法诺（Fano）和西尼加利亚（Sinigaglia）后听从了共和国的威胁，因为威尼斯人刚刚围攻了的里雅斯特，而皮乌斯曾是那里的主教。在他所有的城市中，教皇只给西吉斯蒙德留下了里米尼作为进贡的回报，以及他的兄弟切塞纳和贝尔蒂诺罗，但即使是马拉泰斯塔家族最后的这些城市，在他们死后也要归还给教会。1463 年 10 月的条约摧毁了著名的韦鲁基奥 Guelph 家族的势力，因此教皇君主制也在这些土地上铺平了道路。命运眷顾皮乌斯；这位厌恶战争的教皇击败了所有敌人，征服了他们的土地，扩大了教皇国的版图。两位将军帮助他实现了这一目标，他们是著名的费德里戈（Federigo）和男子气概十足的红衣主教福尔盖拉（Forteguerra）。他曾满意地从阿尔班丘陵之巅的卡沃山上俯瞰广袤的教皇国，从那令人愉悦的高处可以看到教皇国奇妙的疆域，一直延伸到泰拉奇纳和蒙塔罗真塔里奥；即使这片土地上除了罗马神庙别无其他，但它的统治者似乎与帝王们平起平坐。
