
_5\. 路易十二在意大利北部。博尔哈和切萨雷的敌人匆匆赶来。他的俘虏们叛变了。他智取了他们。教皇逮捕了红衣主教奥尔西尼家族。切萨雷在翁布里亚 奥尔西尼家族的督军被处死。切萨雷在锡耶纳城外 拉丁男爵起义 切萨雷在世袭领地 红衣主教奥尔西尼家族被毒死。切萨雷在罗马 凯撒投降 约翰-乔丹缔结条约。米歇尔红衣主教中毒。法国与教皇关系紧张。康萨尔沃在那不勒斯消灭了法国人。博尔哈与西班牙谈判。推翻特罗基奥。任命红衣主教。法军出兵那不勒斯。亚历山大六世结束 1503 年 8 月_。

与此同时，那不勒斯发生的事件把路易十二召回了意大利，法国和西班牙在那里爆发了争夺亵渎神明的战利品独占权的战争。当国王于 1502 年 7 月底抵达阿斯蒂时，意大利的许多领主、博尔哈的敌人或受害者都赶来向他哀悼。红衣主教奥尔西尼家族也从罗马逃来与他会合。君主听取了他们的意见，但切萨雷也匆匆赶来，他先是在罗马与父亲进行了商议。八月，他在米兰见到了父亲。在这里，他以不可抗拒的技巧赢得了红衣主教昂布瓦兹的青睐，后者已经开始希望得到头冠，最后国王本人也被他带到了热那亚。

公爵对博洛尼亚的意图，他对教皇计划将奥尔西尼家族从切萨雷的阵营中引诱到罗马的怀疑，吓坏了所有那些小暴君，他们迄今为止一直是切萨雷的盟友或帮凶，他们曾毫无意义地向他们提供武器，以推翻蒙特费尔特罗家族和瓦拉诺家族。他们告诉自己，如果不一起努力自救，就会一个接一个地屈服。奥尔西尼家族，维吉尼乌斯的私生子查尔斯，红衣主教拉蒂努斯的儿子保罗，红衣主教詹巴蒂斯塔本人，格拉维纳公爵弗朗切斯科，维特罗佐-维特利，奥利维托，可怕的暴君费尔莫、 佩鲁贾的詹波罗-巴格利奥内（Giampolo Baglione）、锡耶纳的潘多尔夫-佩特鲁奇（Pandolfo Petrucci）、博洛尼亚的本蒂沃格里奥（Bentivoglio）等人在佩鲁贾附近的拉马乔内（La Magione）亲自或通过信使进行了商议。他们联合了一支一万人的军队，突然拿起武器对抗切萨雷。他的队长乌戈-蒙卡达（Ugo Moncada）在福松布罗内（Fossombrone）被击败，米切莱托好不容易才逃过一劫。来自威尼斯的圭多巴尔多和来自拉奎拉的约翰-玛丽亚-瓦拉诺很快回到了自己的国家，他们受到了国家的热烈欢迎。然而，愤怒的队长们占领了许多要塞，向法诺挺进，并威胁要将公爵囚禁在伊莫拉。

博尔哈的部下的叛变使他处于最危险的境地，因为他们的果断行动会摧毁他的权力，并迫使博尔哈远在罗马的所有敌人起义。在这种困境下，教皇和他的儿子向法国国王求助，法国国王认为自己在那不勒斯战争中需要博尔哈，于是解救了他们。他命令肖蒙率领一些部队向伊莫拉进发，并撮合切萨雷与尚未做出决定的博尔基亚人达成和解。与此同时，曾要求这些督军加入的佛罗伦萨人出于对美第奇家族的亲戚维泰利家族和奥尔西尼家族的仇恨，以及对起义成功的不信任，拒绝了这一联盟。相反，他们派秘书马基雅维利前往伊莫拉，以确保和平，并向陷入困境的切萨雷保证友谊。正是在那里，这位伟大的思想家第一次看到了身边这个可怕的人，并以他的 "王子 "为原型。费拉拉公爵还提出，如果教皇因奥尔西尼家族起义而陷入困境，他将向教皇派遣军队。为了以防万一，亚历山大早在 1502 年 1 月就在奇维塔-卡斯泰拉纳（Civita Castellana）修建了防御工事，正如他自己所说的那样，这是自己和红衣主教们的避难所，或者在他死后，他儿子的避难所。他曾于 9 月 17 日视察过这座新城堡。

由于受到来自法国的威胁，各红衣主教之间意见不一，又被博尔哈的艺术所迷惑，他们在劝说下与切萨雷签订了个人条约。保罗-奥尔西尼家族于 10 月 25 日抵达伊莫拉，并在那里与他签订了条约。其他所有人都回到了他们刚刚将其推向毁灭边缘的人的麾下。当这一背信弃义的和解于 10 月 28 日发生后，红衣主教奥尔西尼家族在教皇的信函邀请下，也于 11 月返回罗马，他的警告是徒劳的。本蒂沃格里奥（Bentivoglio）也受到了他的引诱，幸运的是，他因受到怀疑而留了下来，否则就会被博洛尼亚人阻止离开。吉多巴尔多发现自己毫无还手之力，不得不与切萨雷达成协议，并于 12 月 8 日再次逃亡离开了父亲的宫殿。同样，10 月 18 日在佩尔戈拉被米切莱托勒死的瓦拉诺的儿子也从卡梅里诺逃了出来。

当切萨雷带着无声的冷笑撒下渔网，准备抓捕这些痴情的套间里人时，他几乎还没来得及自救。他们已经帮他把蒙特费尔特罗家族和瓦拉诺家族再次赶出了他们的州，在那里他们亲自召见了他们；然后，他们让他负责西尼加利亚的征服工作，而法国的辅助部队则很意外地被召走了。自西斯笃四世起，西尼加利亚就属于省长乔瓦尼-罗夫雷（Giovanni Rovere），他是蒙特费尔特罗家族琼（Joan of Montefeltre）的丈夫，琼是圭多巴尔多（Guidobaldo）的妹妹。当后者于 1501 年去世时，亚历山大让他 11 岁的儿子弗朗切斯科-玛丽亚（Francesco Maria）继承了市政官的职位。乌尔比诺年幼的继承人在第一次出逃时被舅舅带到了安全的地方，现在和母亲一起住在西尼加利亚城堡（Sinigaglia Castle），后来大名鼎鼎的安德烈亚-奥贝托-多利亚（Andrea Doria）正在这座城堡抵御兀术分子的进攻。1502年12月底，奥贝托-多利亚先是在威尼斯让公主和她的儿子上船，然后自己前往佛罗伦萨。他命令副官守住城堡。兀术们要求他交出城堡，但他宣称只想把钥匙交给公爵。因此，他们毫无理智地召见了他们的腐败分子，却忘记了一个深受其害的敌人永远不会是一个真诚的朋友。

切萨雷抓住受害者的狡猾手段并不那么令人吃惊，倒不如说他的盲目性更令人吃惊，因为许多暴君都深谙各种不义之举，却落入了主人的圈套。公爵从附近的法诺出发，命令他们把部队转移到西尼加利亚附近，因为他本人想和他的战士们一起在城里驻扎。他们愚蠢地照办了。当切萨雷于 12 月 31 日出现在西尼加利亚时，他用虚伪的友好态度迎接了这些绅士。一个善良的恶魔徒劳地警告了他们。他们像中了魔咒一样，踉踉跄跄地向巨龙走去。维特洛佐没有穿盔甲，满脸忧郁和不祥的预感，但他还是来了。公爵把这些督军请进了他居住的宫殿，他们刚一进门，就被士兵们团团围住。维特洛佐打倒了其中一人；奥利维托、保罗-奥尔西尼家族和格拉维纳公爵一起被捕。潘道尔夫-佩特鲁奇逃脱了。切萨雷很快就解除了俘虏部队的武装，西尼加利亚则被洗劫一空。晚上，维特洛佐和奥利维托被勒死了，据说是背靠背坐在两把椅子上。他们死得毫无尊严。奥利维托哭泣着把责任推给了维特洛佐，而后者在临死前最大的念头莫过于想从教皇那里，从亚历山大六世那里获得赦免。

本来是切萨雷的不幸，却成了他的幸运：他一举除掉了他的敌人，包括奥尔西尼家族，用尽了他们的服务。他们自己给了他这样做的机会，现在他不仅可以要求世人承认他的聪明才智，还可以让他的行动看起来是正确的。就在这一天，他向意大利的几个大国派出了信使，告诉他们他已经阻止了他的叛徒，并为他们的背叛行为画上了一个应有的句号。信使于 1503 年 1 月 3 日抵达罗马，当时正值最热闹的节日，因为狂欢节已于圣诞节开始。亚历山大听说政变成功了，他们都死了，还被锁上了锁链，于是他决定按照约定抓捕他们。切萨雷的信敦促他现在就抓住罗马的奥尔西尼家族；他的秘书哈德良在夜里把信读给他听，这位秘密抄写员没有离开梵蒂冈，以免引起教皇的怀疑，如果红衣主教奥尔西尼在其他人的警告下逃跑的话。教皇立即传话给这位红衣主教，说西尼加利亚已经投降。第二天一早，奥尔西尼家族便骑马前往梵蒂冈表示祝贺。在路上，他遇到了该城的总督，总督仿佛是他偶然遇到的同伴。当红衣主教走进鹦鹉厅时，武装人员将他团团围住。他脸色苍白，被带到了博尔哈塔。与此同时，他们还逮捕了佛罗伦萨大主教里纳尔多-奥尔西尼（Rinaldo Orsini）、Protonotar家族的奥尔西尼、维尔吉尼乌斯的亲戚雅各布-圣克罗齐（Jacopo Santa Croce）以及修道院院长贝尔纳迪诺-阿尔维亚诺（Bernardino d'Alviano），他是大名鼎鼎的巴托洛梅奥的兄弟。总督很快就骑马来到了被他搬空的乔尔达诺山上的宫殿。红衣主教八十多岁的母亲像疯了一样在街上踉踉跄跄地走着，因为没有人敢收留她。她的儿子被送往圣天使城堡，他的珍宝被送往梵蒂冈。

1 月 5 日，若弗雷阁下率领部队出发去攻占罗通多山、其他奥西尼亚城堡和法尔法，因为这是俘虏们必须付出的生命代价。圣-克罗齐（Santa Croce）为自己的生命付出了 2 万个杜卡特，他不得不陪同教皇的儿子去执行这次投降行动。这样，奥尔西尼家族的末日也来临了。

所有红衣主教都去找教皇为他们的同僚求情，但都徒劳无功；教皇回答说，奥尔西尼家族是叛徒，是反对公爵阴谋的同谋。整个罗马陷入了极度的恐慌之中。每天都有高级官员被押往圣天使城堡的报道。每个有地位有财富的人都担心自己会被列入征兵名单。就连流亡在罗马的美第奇家族也战战兢兢。基乌西（Chiusi）主教兼使徒秘书西诺尔弗（Sinolfo）吓死了。2 月 1 日，人们在西斯托桥上发现了一具身着猩红色衣服的尸体。当扼杀天使切萨雷带着他的军队来到罗马时，人们还能期待什么呢？

他的士兵们的高超技艺让各地都对公爵的实力肃然起敬。许多人称赞他，就连法国国王也称他的行为是罗马人的行为。事实上，他是一条吞食小蛇的巨龙。1503 年 1 月 1 日，在新的恐怖印象下，他从西尼加利亚出发，穿越意大利中部的土地。暴君们像被猎杀的猎物一样从他面前逃走：卡斯泰洛城的维泰利（Vitelli）、佩鲁贾的詹波罗-巴格利奥内（Giampolo Baglione）。他们惧怕的是他的狡猾，而不是他的剑，因为这个征服了半个意大利的人曾围攻过城市，却从未打过仗。他经由翁布里亚的瓜尔多（Gualdo）前进。卡斯泰洛城向他投降；1 月 6 日，佩鲁贾向他提供了威权统治者。他在那里任命卡洛-巴格利奥内（Carlo Baglione）为摄政王，但以教会的名义，没有进入城市。他打算前往锡耶纳，佩特鲁奇在那里救了自己。在行军途中，他听说红衣主教在皮耶堡（Castel della Pieve）被捕了，现在他让人勒死了格拉维纳和他带走的保罗-奥尔西尼家族（Paul Orsini），时间是 1 月 18 日。保罗-奥尔西尼家族因此受到了惩罚，因为他轻率地加入了博尔哈的麾下；1498 年 9 月，他将自己的儿子法比奥嫁给了约翰-博尔哈红衣主教的妹妹、年轻的希罗尼玛-博尔哈。马基雅弗利作为佛罗伦萨人的演说家陪伴着切萨雷，公爵敦促他努力工作，让他的共和国与他一起对抗锡耶纳，而亚历山大则给潘多尔夫写了一封虚伪的信。

教皇暗中希望，同时又害怕对锡耶纳发动战争，因为这座城市受到法国的保护。因此，他公开斥责他的儿子：他所做的一切都是出于固执，他想让他与整个意大利为敌。他非常生气，甚至骂他是私生子和叛徒，想把他放逐。不过，据说他之所以恼羞成怒，是因为公爵要求立即交付 3 万个杜卡特。

切萨雷洗劫了锡耶纳的几座城堡，然后给锡耶纳寄去信件，以最可怕的威胁要求立即放逐潘多尔夫。暴君于 1 月 28 日宣布，为了祖国的利益，他希望离开，并于同一天前往卢卡。随后，切萨雷按照条约离开了锡耶纳，并交出了他的战利品。只有他的秘书来到城里，坚持要求宣布潘多尔夫为流放者。

紧急信使将公爵叫到了他的领地。因为台伯河两岸的残余男爵们突然起兵，要为他们的同族报仇雪恨，并避免自己的同族倒台。当时奥尔西尼家族的首领是布拉奇亚诺领主约翰-乔丹和皮蒂利亚诺伯爵尼古劳斯，前者在那不勒斯为法国效力，后者为威尼斯人效力。在他们请求这些强国保护的同时，他们的亲戚结成了联盟，萨维利家族和一些科隆纳家族也加入了联盟。科隆纳的穆提乌斯（Mutius Colonna）和萨维利家族（Silvius Savelli）夺取了帕隆巴拉；被勒死的保罗的儿子法比奥-奥尔西尼（Fabio Orsini）和被囚禁的红衣主教的兄弟朱利叶斯（Julius）在切维特里（Cervetri）和布拉奇亚诺（Bracciano）拿起了武器。1 月 23 日，男爵们甚至攻入了诺门塔诺桥（Ponte Nomentano），罗马随即开始行动。教皇调兵进入梵蒂冈，但奥尔西尼家族被赶了回去。皮蒂利亚诺之子、尼科西亚大主教阿尔多布兰迪尼逃出城外。教皇要求从法国引渡他，但法国大使拒绝了。亚历山大愤怒地喊道，我要彻底消灭这个家族！他怀疑地关上了宫殿的大门。他给卡埃雷的朱利叶斯-奥尔西尼家族传话，说他将对红衣主教的死负责。

二月初，公爵匆匆赶往领地。他的战队所经过的阿卡彭丹特、蒙特菲亚斯科、维泰博等城市充斥着各种暴行。不堪一击的奥尔西尼家族四处退让：惊慌失措的萨维利家族脱离了他们，将帕隆巴拉交给了教皇。只有布拉恰诺还能进行顽强抵抗。2月16日，教皇派出炮兵围攻这个要塞，因为要不惜一切代价拿下它。然而，切萨雷避开了法国国王，而约翰-约旦正是在法国国王的保护下，这让他与父亲产生了矛盾。后者在主教会议上公开抱怨自己的儿子；他还建议红衣主教们自己用大炮武装宫殿，因为他们害怕奥尔西尼家族的进攻。

公爵的接近让罗马充满了恐惧。由于害怕，红衣主教希波吕于 2 月 15 日离开罗马前往费拉拉。与此同时，曾经是亚历山大六世擢升工具的红衣主教奥尔西尼家族坐在圣天使城堡里，成为他悔恨和痛苦回忆的牺牲品。他的母亲每天给他送饭，直到被禁止为止。红衣主教为他的自由提供了大笔资金，他的母亲却徒劳无功。她派儿子的一个情妇乔装打扮，带着一颗他梦寐以求的珍贵珍珠去见教皇。教皇收下了珍珠，并再次允许将食物送给儿子。"但人们普遍认为他已经喝下了 那杯按照教皇的命令为他调制的酒" 尽管如此，亚历山大还是嘱咐这个不幸的人要振作起来，保重身体。当毒药已经在囚犯体内生效时，教皇在主教会议上告诉红衣主教们，他已经命令医生们尽力帮助奥尔西尼家族。2月15日，据说红衣主教得了热病；22日，他去世了，当时切萨雷正在苏特里，并让凯雷被围困。根据教皇的命令，逝者、四十名火炬手、总督哈德良阁下和宫中的传教士们陪同他前往圣萨尔瓦托雷泰莱西诺。

切萨雷本人在 2 月底来到罗马，但他只是戴着面具出门；据说 2 月 27 日在皇宫上演喜剧时，人们就是这样看到他的。那时，除了布拉奇亚诺、卡埃雷和维科瓦罗之外，所有的奥尔西尼家族城堡都已经过去了。教皇迫不及待地想看到这些城堡也陷落，但法国国王的命令禁止再对约翰-乔丹造成任何损害。因此，公爵不想冒任何风险，这激怒了他的父亲，他命令公爵立即向布拉奇亚诺发动进攻，并以放逐和失去封地相威胁。他被迫于 3 月 12 日离开罗马前往卡埃雷，并将他的副手洛多维科-德拉-米兰多拉伯爵、乌戈-蒙卡达和米切莱托-科雷吉亚留在了城墙外。他 4 月 6 日才离开罗马，在路上得知城堡在朱利叶斯-奥尔西尼、约翰-奥尔西尼和他的儿子伦佐的带领下已经投降。这些先生们将自己和这个地方交给了切萨雷；他立即带着朱利叶斯-奥尔西尼家族去见教皇，请求他释放自己。现在，亚历山大希望彻底推翻奥尔西尼家族，只有法国的否决权暂时保护了这个王朝。

秘密抵达布拉奇亚诺的约翰-乔丹前往阿布鲁佐的策勒。教皇向他提出了狡猾的建议：他向他提供了斯奎莱斯公国或马尔凯纳地区的补偿，以换取他在罗马的财产。在法国大使的调解下，奥尔西尼家族被迫于 1503 年 4 月 8 日签署了一份条约，他在条约中同意了这些建议，并获得了前往法国的护照，以便与他的保护者国王做进一步的安排。

现在，切萨雷回到了罗马，成为了意大利最强大的人物。他的成功、教会的资源、他的胆识和力量使他看起来是一个真正的强者。士兵和治安官们奔走相告。在教皇国的几乎所有城堡里，西班牙人都是他的执达官。他所取得的一切，不是因为英勇或军事天才，而只是因为犯罪和背叛。在这一点上，他是他那个时代的主宰，他毒害了那个时代的全部政策。

他从犯罪走向犯罪。4 月 10 日，保罗二世的裙带关系、红衣主教乔瓦尼-米歇尔（Cardinal Giovanni Michiel）也在圣天使城堡被毒死。他刚刚离开，他的财产就被人从家中抢走，价值 15 万杜卡特。教皇闪耀着幸福和健康的光芒。他似乎坚不可摧。当他在 4 月 17 日做弥撒时，人们对他那有力的嗓音感到惊讶。4 月 24 日，他与切萨雷一起前往安圭拉家族，参观被征服的奥尔西尼家族城堡；5 月 11 日，他参观了一些昔日的科隆风景区。
纵观他们的所作所为，博尔吉亚人发现自己取得了令人难以置信的成就：罗马的两大贵族派别从未被征服过，现在却被击溃了；其他所有的男爵、教皇国的所有暴君都被消灭或赶走了；罗马处于耐心的奴役之中；枢机团成了一个颤抖、顺从的元老院；教廷成了一个精致、顺从的工具；获得或巧妙地赢得了强大的盟友。当时，亚历山大正考虑给他的儿子罗马涅和马尔凯国王的头衔，但他仍然害怕法国的反对，因为法国不能容忍一个博尔基亚君主制国家。它可能会变得很可怕，因为它将精神和世俗权力结合在一起。教皇仍然是它的中心，是基督教的财源。在父子俩这两位精湛的外交艺术大师中，一个能够用宗教的盾牌来掩盖另一个的罪行。

但当博尔基亚家族审视他们的现实版图时，他们意识到这一版图并没有延伸到教皇国之外；甚至在这里也被博洛尼亚和费拉拉打断了。他们制定了进军托斯卡纳的计划，绝望的比萨向切萨雷提供了威权统治者。得知这一消息后，路易十二缔结了佛罗伦萨、锡耶纳、卢卡和博洛尼亚之间的联盟，该联盟也将在那不勒斯支持他。因此，早在 1503 年 3 月 29 日，潘多尔夫-佩特鲁奇就在法国人的护送下返回了锡耶纳。但该联盟的不团结让切萨雷仍抱有希望，与西班牙的秘密谈判也让他倍受鼓舞。那不勒斯事件的转机为他开辟了新的前景。因为在那不勒斯与法国交战的西班牙在他身上看到了盟友的影子，而他也在与西班牙的联盟中看到了迫使路易十二让步的有效手段。这为政治家的艺术开辟了新的领域。

1503 年 4 月，康萨尔沃从巴莱塔开始了他在阿普利亚的辉煌战役，2 月 13 日著名的决斗是一个好兆头。13 名意大利人战胜了同样多的法国人；但他们的胜利（至今仍在文字和歌谣中流传）与为外国主人、他们国家的征服者而战的耻辱是分不开的。奥比尼（Aubigny）和内穆尔（Nemours）屡战屡败：康萨尔沃（Consalvo）于5月14日进入那不勒斯，法军的残部躲进了加埃塔（Gaëta）要塞。因此，路易十二在那不勒斯和查理八世一样不幸，就像安茹王位的所有请求者一样。在这场不幸中，法国的一位历史学家想要承认是上天的手在惩罚国王与渎神的博尔哈结盟。当然，不可否认的是，这些人的罪行和伟大只是在法国的保护下才达到如此程度。现在，这位国王可以从他的门徒那里收获他应得的感谢了。

他们对法国的失败感到满意，对西班牙的胜利欢欣鼓舞。现在，他们可以在这里和那里索取高价的支持了。路易十二准备了一支新的军队，带领特里莫耶（La Trémouille）穿过托斯卡纳和罗马前往那不勒斯。他的使节要求自由通过罗马帝国，并将切萨雷的军队与法国的军队统一起来。作为回报，博尔哈要求在托斯卡纳自由行动，并交出布拉奇亚诺。双方没有达成任何协议，因为谨慎（如果不是荣誉的话）禁止国王背叛佛罗伦萨和锡耶纳。博尔哈本人既不能丢掉友谊的面具，也不敢在法国军队在城市联盟的增援下向托斯卡纳进军的时候对那里采取行动。因此，他们宣布允许通行，但将保持教皇国的中立。在这种中立的幌子下，一旦法国军队卷入他们新的、可能是不幸的远征，他们就可以进攻托斯卡纳。同时，他们倾向于西班牙；教皇甚至允许康萨尔沃在罗马招募雇佣兵；他让皇帝的大使明白，如果他加入西班牙，他也会这样做。

亚历山大的秘书和宠臣特罗切可能向法国出卖了西班牙的谈判；他于5月18日逃出了梵蒂冈，但被派往科西嘉岛的船只追上，带回罗马后于6月8日被米切莱托勒死在特拉斯特维尔的一座塔楼上，切萨雷在一旁暗中监视。这个不幸的人近年来在教皇心目中的地位不断上升，从他写给曼图亚侯爵夫人的信中可以看出，他是一个受过良好教育并具有人文主义倾向的人。有传言说，他之所以中了圈套，是因为他抱怨自己没有被列入新任红衣主教的名单中。当教皇告诉他名单是切萨雷（Cesare）制定的，公爵会因为他的言论而杀了他时，秘书匆忙逃走了。雅各布-圣克罗齐（Jacopo Santa Croce）也在此时被处死；他的尸体在天使桥上一直保留到傍晚，而他的物品则被没收。恐怖如此之大，以至于许多罗马人移居国外。

人们通过正常途径为切萨雷筹到了钱。他的刽子手米切莱托-科雷利亚（西班牙人）和城市长官带着武装人员进入民宅，以马兰人为借口将许多人关进了监狱。出于同样的目的，还发布了针对犹太人的诏书。5 月 31 日，亚历山大又任命了 11 名红衣主教，其中包括他的两位亲戚巴伦西亚的卡斯特拉尔和伊洛利斯、沃尔特拉的弗朗切斯科-索德里尼和阿德里亚诺-卡斯特利。这位受过古典教育的拉丁文学家来自科尔内托。他曾担任过英诺森八世在英格兰的圣座大使，并通过亨利七世的恩惠获得了赫福德主教辖区和其他大片领地。弗洛里杜斯倒台后，他成为了教皇的秘密抄写员、宠臣和心腹。他是罗马最富有的牧师之一，布拉曼特在博尔戈为他建造了一座美丽的宫殿。

切萨雷是这些新红衣主教的创造者，当他们被任命时，他出席了主教会议；他为他们举行了宴会，这是他回国后第一次公开露面。新的计划正在制定中：教皇希望将奥尔西尼家族、萨维利家族和科隆纳家族的所有土地归还给教会，为此神圣院要同意切萨雷将马尔凯与罗马涅联合起来。公爵于 6 月底前往罗马涅，教皇希望在 8 月对他进行访问。他的政府在那个国家扎下了根；管理有方，司法无情。切萨雷利用拉米罗作为他的总督后，他牺牲了这个令人憎恨的舆论工具；他把他捆绑起来，就这样暴露在切塞纳的广场上，刽子手的斧头就在他的身边，早晨的人们对此感到非常惊讶。

法王于是向教皇提出了一个奇怪的建议，如果教皇割让博洛尼亚和罗马涅给他，他就把整个那不勒斯送给教皇。另一方面，教皇向皇帝提出交涉，以便为他的儿子获得比萨、锡耶纳和卢卡的叙任权。与此同时，8 月初，拉特穆耶率领军队途经托斯卡纳前往那不勒斯，在接近罗马领土时发生了一件事，一下子切断了博尔哈的所有联系。

8 月 12 日星期六，刚刚从罗马涅返回的教皇和他的儿子都病倒了。两人都严重发烧并伴有呕吐。13 日，教皇休克。他感觉好多了；他让一些红衣主教在床边玩牌。14日高烧复发，15日退烧，16日烧得更厉害了。宫殿被关闭了；最初几天，任何医生或药剂师都不得离开宫殿。梵蒂冈走廊上一个被活活堵死的女人被要求为教皇祈祷：圣人回答说他没有希望了。8 月 18 日星期五，亚历山大向库尔姆主教忏悔（这个人一定听到了怎样的忏悔！），然后坐下来领受圣餐。阿尔博雷亚、科森扎、蒙雷亚莱、卡萨诺瓦和伊洛里斯五位红衣主教围着他。他的死期已到。同一时刻，切萨雷也匍匐在地，但已经脱离了危险，准备连夜从有顶的通道逃往圣天使城堡，他的两个小孩和许多财产已经被劫走。他匆忙召集的战士们已经挤满了博尔戈；鼓手们穿过罗马，召集所有守卫人员前往梵蒂冈，否则将处以绞刑。同年 8 月 18 日晚，库尔姆主教为教皇举行了最后的仪式，亚历山大六世在达塔尔和一些随从的见证下离开了人世。

人们立即传言他是死于毒药，而看到面目全非的尸体似乎也证实了这一点。人们的想象力充满了可怕的杜撰。据说亚历山大在病倒之前，在他的房间里看到了猴子形状的魔鬼，这个魔鬼带走了他。很快，所有人都相信这是中毒。八月是罗马最危险的月份，当时当然特别炎热和发烧。费拉拉的特使写信给他的主人说，很多人生病和死亡，梵蒂冈的几乎所有教士都病了。佛罗伦萨特使索德里尼也病倒了，因此，正如他自己指出的那样，他不再给威权统治者写报告了。因此，炎热的夏季空气可能是导致老教皇致命高烧的原因。8 月 18 日，在亚历山大去世前不久，从皇宫赶来的医生西庇阿告诉威尼斯大使朱斯提尼安，他的病是中风，但没有提到可能是中毒。但世人的厌恶始终不愿相信这位最令人憎恨的教皇会自然地结束自己的生命。所有与他同时代的人，其中包括著名的历史学家、吉恰尔迪尼（Guicciardini）、本博（Bembo）、约维乌斯（Jovius）、红衣主教埃吉迪乌斯（Egidius）、拉斐尔-沃拉特拉努斯（Raphael Volaterranus），都声称他是与切萨雷同时中毒的。根据其中最有名的报道，教皇曾与他商定，在梵蒂冈附近的维尼亚用餐时毒死了富有的红衣主教哈德良，喝下了混在酒瓶中的死亡之酒，而切萨雷也犯了同样的错误。教皇因此而死，公爵则凭借年轻时的活力恢复了健康。这种情况本身就非常不可能，因为如此经验丰富的人会犯下如此严重的过失吗？如果真的发生了下毒事件，那么威尼斯的一份报告，即红衣主教哈德良用 1 万个杜卡特买来的教皇的侍杯手混入了有毒的点心，似乎更可信一些。毫无疑问，这顿饭是在红衣主教的花园里吃的。亚历山大死后，佛罗伦萨立即收到了一份关于此事的报告，这份报告更加可信，因为它虽然将教皇的病因归咎于那顿晚餐，但还没有提到中毒。

切萨雷同时生病并伴有同样的症状，这是让人相信中毒的最重要的原因。当然，公爵在康复后对马基雅弗利说，他自己的病与教皇的病的致命巧合是他唯一没有计算到的不幸时，并没有提到中毒。但同样病倒的红衣主教哈德良却告诉历史学家约维乌斯，他当时也被下了毒，并承受了后果。

我们再也无法读懂濒临死亡的博尔哈的灵魂，也无法知道他的灵魂中是否还残存着良知，是否还能被那些围绕在有罪之人临终前的灵魂所触及。值得注意的是，在他病重期间，他同样病重的儿子没有来探望过他，他也没有说过他和卢克蕾齐娅的名字。如果只看外部情况，这位教皇甚至是在他最幸福的时候去世的。因为他的一切都成功了，每一个计划、每一次犯罪都成为了一种权力。一想到切萨雷的命运，他当然会担心，因为他太了解教皇侄子们的历史了。但他可以对自己说，他把儿子留在了枢机团，给他留下了财宝、军队、土地和许多仆人，他是个男子汉，足以找到自己的路。或者，他相信他的儿子即将死去，而他一直无法隐瞒他儿子的病情？抑或正因如此，他才默默地注视着即将吞噬他那亵渎神灵的房子的深渊？

事实本身就说明了对亚历山大六世的评价。诚然，人的性格在很大程度上是时代环境的产物。但是，如果说当时意大利人所处的公共和道德环境的无边堕落可以减轻许多人因时代特征而犯下的罪行，那么手捧福音书的教皇可能是同时代人中最后一个有权得到这种减轻的人了。因为亚历山大六世是教皇，所以他看起来比他的儿子更可恨。后者向世人挑战的可怕罪行的勇气甚至有一种壮烈的意味，而他的父亲则是迫于他的地位，不得不去做并让同意的事情发生。人们通常只能看到他像躲在幕后一样行动。

亚历山大六世的真实身材被错误地测量了，即过于高大：事实上，这表明他是多么渺小。从原则上说，把他想象成一个恶魔是完全错误的：如果真有这样的本性的话。这个精力充沛而又轻浮的人的犯罪起源是一步步追溯他的历史的。这些罪行更多地是源于他的感官，而不是他的思想，因为他的思想只是普通的。如果他像其他同类一样，将自己的放荡行为隐藏起来，也不会引起如此大的轰动。只有他的无耻才是无与伦比的。如果宗教不仅仅是教会的仪式和对创造奇迹的圣人的信仰，那么就必须承认亚历山大六世是一个没有宗教信仰的教皇。他在其他方面所具有的优秀品质--因为在自然界中既没有绝对的恶，也没有绝对的善--或者在他身后因矛盾的魅力而受到赞美的品质，在他的整体性格面前一文不值，而天上的亡灵法官即使不会轻蔑地将它们抛出他的躯壳，至少也会过于轻易地对它们做出评判。

历史学家还反驳了那些发现这位教皇具有政治天才的人的判断。他的智慧在欲望和欺骗方面都是大师级的，但却从未达到如此高的境界。在他的整个教皇生涯中，无论是教会还是国家，无论是司铎还是王子，都没有一个伟大的想法。在他身上找不到一丝创造性活动的痕迹。在教皇史上，他也是独一无二的，因为他完全放弃了教会的优势。他与世俗的教皇国的关系也非常引人注目：他对所有教皇都慎重守护的财产如此不屑一顾，以至于他的侄子们使教皇国濒临世俗化；因为他想把整个教皇国纳入自己的家族，而这将导致教皇国的解体。"在我之后，大洪水"：这似乎是这位教皇的格言。博尔吉亚撒旦般的激情升级、法律的腐败以及当时所有的政治条件都使得最畸形的计划成为可能。当然，如果亚历山大真的实现了让切萨雷当教皇并让头冠和王冠在博尔哈家族世袭的想法，他就不得不放弃这个想法，但他会毫不犹豫地把教皇国献给他的私生子，让他作为意大利王权的核心和基础，而这显然是切萨雷所向往的。亚历山大六世本人，在他那可怕的儿子的权力下，似乎并不像一个想要陶醉于自己王权感觉的人。他们的负担只会让他感到不舒服。在这位享乐之人的被动性格中，丝毫看不出伟大的动力、王室的野心，也没有西斯笃四世或朱利叶斯二世那种对行动和统治的渴求。他只是被环境所驱使；他从未控制过环境；他从未大胆而有力地面对环境。他只有一种激情：对子女的爱。他的所有行为都以此为背景，别无其他。在他最后的日子里，他对儿子的恨，他的恶魔，与他对儿子的爱在斗争。在黑暗的岁月里，他可能会杀死这个儿子，但他不能除掉切萨雷，因为他自己的安全和王位最终取决于他的伟大和权力。

事实上，在亚历山大六世的历史中，除了不惜一切代价让自己的孩子掌权这一可悲的想法之外，没有人能发现任何其他的指导思想。这位教皇的政治行为包括消灭了许多暴君，建立了昙花一现的塞萨雷斯公国（Cesares），这个公国有无数不义之处，支持并掩盖了他自己篡夺的教皇职位，为了这个任人唯亲和自我保护的悲惨目的，他牺牲了自己的良知、人民的幸福、意大利的生存和教会的利益。

一场长达半个多世纪的战争，比中世纪以往任何一场战争都要可怕，它摧毁了意大利，摧毁了城市的繁荣，摧毁了民族和自由意识，使这个在不光彩的外国统治下的伟大国家陷入了数百年的沉睡，就像哥特战争后的衰竭一样。即使亚历山大六世不是这一深重灾难的唯一始作俑者，还有其他上百个原因造成了这一灾难，但他将意大利拱手让给西班牙人和法国人，只是为了让他的私生子们变得强大。他是这个国家衰落的重要原因，他也以同样的身份屹立于教会的历史中。

至于罗马城本身，最后的公民自觉也在博尔吉亚的统治下消亡了，罗马人民的士气完全丧失了。当时的历史学家对罗马从未起来反抗亚历山大六世表示惊讶，尽管有那么多伟人被扼杀，尽管还有其他种种暴行。如果认为罗马没有起义是因为对教皇的政府感到满意，那就太荒谬了。导致罗马人保持冷静态度的原因是博尔吉亚军团的恐怖主义，他们有间谍、刽子手和西班牙士兵，最后是罗马人自身的堕落和已经被奴役的思想。当时一位著名的历史学家，他本人也是一位主教，他说："罗马人，无论是因为对昔日辉煌和古老自由的记忆，还是因为他们狂野不安的性格，都无法心平气和地忍受司铎们的统治，因为他们往往统治过度，贪得无厌"。然而，他们只是对亚历山大进行了无力的讽刺，而他们的城市却陷入了令人想起古代最混乱皇帝时代的状态。当一位与博尔哈同时代的人写道："在这座城市里，角斗士的无礼从未如此之大，人民的自由从未如此之少。城里到处都是吹牛皮的人。最轻微的仇恨表达都会被处死。此外，整个罗马到处都是强盗，夜晚没有一条街道是安全的。罗马在任何时候都是民族的庇护所和人民的城堡，如今却成了屠宰场，而亚历山大六世却出于对子女的爱纵容了这一切"。

亚历山大六世统治时期的另一位目击者，后来非常著名的维泰博红衣主教埃吉迪乌斯描绘了当时的景象："黑暗和暴风雨之夜笼罩着一切；我不想谈论家族中发生的事件，也不想谈论提亚特人的悲剧；在教皇国的城市里，从来没有发生过比这更可怕的暴行、更多的掠夺和更血腥的谋杀。街上的抢劫从未如此肆无忌惮；罗马从未有如此多的恶人；吹牛者和强盗的活动从未如此放肆。人们既不能离开城门，也不能在城内居住。拥有黄金或任何贵重物品都会被视为大不敬。没有任何东西可以被保护，房屋、寝宫、塔楼都不例外。法律荡然无存。黄金、暴力和感官享乐统治着一切。在此之前，意大利一直不受外国统治，因为它摆脱了外国的暴政；因为虽然国王阿方索是阿拉贡人，但他在教育、自由和宽宏大量方面并不比任何意大利人逊色。但现在，自由之后是奴役，现在意大利人从独立沉沦为外国人的黑暗奴役"。

