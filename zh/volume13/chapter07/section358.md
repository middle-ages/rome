### 第七章

_1\. 文艺复兴时期的艺术 马丁五世、尤金四世、斯卡兰波的活动。鲜花广场。宫殿建筑。圣奥诺弗里奥。S. Antonio de' Portoghesi. 英国人和德国人的医院。尼古拉五世。他的新梵蒂冈和圣彼得大教堂计划。他的修复工程。S. Giacomo dei Spagnuoli. 圣萨尔瓦托雷泰莱西诺。元老宫。处女泉。皮乌斯二世的拉里亚诺教堂被毁。维科瓦罗小教堂。纳沃纳河畔的奥尔西尼家族。托尔克马达扩建密涅瓦宫。保罗二世教堂和圣马可宫_

在科学改革的同时，美术改革也在缓慢进行。意大利人转向了生动的现实主义：超自然的本质从他们的艺术中消失了，而形式的世界变得更加自然和易懂。在南方的充实生活中，一个宁静之美的境界最终展现出来，其不朽的遗迹与古代的遗迹一起，至今仍是人类重要的艺术财富。

与新古典主义文学相比，新古典主义艺术一般更具独创性。除了一些装饰图案外，绘画不知道古代的模式；它仍然是意大利最独特的艺术，并始终意识到它与基督教的联系。另一方面，作为教会的异教徒继子，雕塑远远落后于教会，尽管古代为其提供了丰富的图案。建筑学面前只有一片废墟，因为西西里和希腊的神庙仍然不为人知。可以理解的是，意大利人从未重复过神庙，也没有按照古代作家的设计图建造温泉浴场或别墅；但他们从被人文主义鄙视为野蛮和非民族的哥特式风格回归到古典建筑形式、空间关系和表面、罗马线条和柱子位置。他们从古代借鉴了丰富的装饰：他们先是在中世纪城堡的基础上建造了带有美丽柱廊庭院的高贵简洁的宫殿，然后又建造了宏伟的中央教堂，在教堂的穹顶上，他们大胆地将万神殿搬到了空中。

罗马对这里的影响是巨大的。因为它的废墟为维特鲁威的理论提供了不朽的例证。艺术家们将对废墟世界朝圣般的惊叹转化为对古代建筑的真正研究。佛罗伦萨是新拉丁建筑的发源地，十五世纪初，新拉丁建筑的伟大创始人布鲁内莱斯基和雕塑家多纳太罗从佛罗伦萨来到罗马，在这里他们进行了测量并绘制了草图。Francesco di Giorgio Martini、Filarete、Ciriaco、San Gallo、Rosellino、Cronaca、Bramantino 和许多其他人也做过同样的工作。伟大的莱昂-巴蒂斯塔-阿尔贝蒂（Leon Battista Alberti）从对罗马废墟的研究中汲取灵感，创作了他的作品《建筑学》（De re aedificatoria），这是文艺复兴早期新古典主义艺术理论的奠基之作，令人钦佩。罗马遗迹主导了艺术家的想象力；他们现在主要用罗马门廊、凯旋门和神庙的形象来装饰绘画和壁画的背景。罗马因此成为托斯卡纳艺术的实践学校，托斯卡纳艺术就像科学一样，在教皇的追随或召唤下从佛罗伦萨走向罗马。

罗马本身并不具有创造性。古代的天才启发罗马人梦想政治上的文艺复兴，而不是创造艺术作品。当他们打完党派之战后，就像中世纪一样，无所事事地坐在古代的瓦砾堆旁，任由教皇们打理。外国人来了，给他们带来了书籍和印刷品，为他们建造、绘画和凿刻，而他们未曾枯竭的土地则敞开怀抱，让古老的神灵和英雄、圣贤和公民以大理石和矿石的形式回归世界：这是一个漫长的古代进程，至今仍未到达终点。罗马人的贫瘠可以用阿维尼翁流亡和分裂导致的艺术活动衰退来解释，但其更深层次的原因在于国际大都市的非民族性和非政治性。城市的性质也使得任何统一的建筑设计变得不可能；空间太大而人口太少，废墟太多而太庞大，最后，人们独立发展的精神也不复存在。即使是罗马最美丽的新意大利建筑遗迹，也只是在空间上显得随意、孤立和无机。它们是瞬息万变的教会王公们的个人杰作。教皇只是偶尔支持、改进或美化不断衰落的罗马。罗马没有特定的时代特征，这正是它的魅力所在。

让我们简要概述一下罗马的文艺复兴，因为它属于一个历史进程。从总体上看，十五世纪的罗马如今只剩下一些教堂建筑、宫殿、堡垒和城墙。复辟时期的教皇们进行了修复，教皇国王们进行了建设，而资产阶级大多无动于衷。

马丁五世（Martin V）发现街道沼泽化，住宅成了废墟，教堂成了废墟。虽然这位教皇的旧传记说罗马人开始重新建造房屋，但这只是零星的活动。马丁计划修复所有的教区教堂，他也鼓励红衣主教们这样做。一些事情发生了。他修缮了_Santi Apostoli_教堂，并在教堂旁边的一座宫殿中为自己建造了一座居所，这也是他曾经居住过的地方。他用铅板重新铺设了万神殿的屋顶。他翻修了摇摇欲坠的圣彼得大教堂的四角柱廊，并修复了梵蒂冈许多破旧的建筑。他对圣约翰拉特兰大教堂的贡献最大。在那里，人们仍能从音乐厅地板的残迹中缅怀他。但教堂的衰败是如此普遍，以至于马丁对较小的教堂听之任之：他甚至下令从这些教堂取走珍贵的大理石，用来铺设拉特兰教堂的地板。红衣主教也时不时地修复他名下的教堂，比如阿方索-卡里略（Alfonso Carillo）古老的四冠圣徒殿（Basilica of the Four Crownned），这次他缩小了教堂的规模。让-德-罗歇泰尔修复了卢西纳的圣洛伦佐教堂，该教堂在尼古拉五世时期由红衣主教卡兰德里尼重建。马丁修建了元老桥。

他的继任者尤金四世热爱艺术，只是在流亡归来后才得以继续这项值得称赞的活动。他修复了许多教堂，包括使徒彼得教堂和梵蒂冈教堂，并在教堂旁修建了造币厂。他修复了拉特兰宫，并在旁边建立了圣器室和修道院，在修建过程中发现了许多古老的房间、地板和精美的雕像，这些都是_Laterani_宫殿的遗迹。在拉特兰大教堂，他先是把柱子和柱子砌成了墙，这让人感到遗憾。尤金还考虑扩建罗马的街道，当时的街道形成了一个几乎无法解开的迷宫。早在1442年，万神殿前庭的残垣断壁就被清除了。奇妙的圆柱最先重获自由。万神殿前的广场铺上了石灰华，通往马尔斯广场的道路也铺上了石灰华。这次还发现了两只玄武岩狮子（现存于梵蒂冈埃及博物馆），以及装饰拉特兰克莱门特十二世墓的华丽斑岩盆。发掘出来后，它被放置在万神殿大厅的前面。当时，人们认为里面装的是奥古斯都的骨灰，而在万神殿发现的铜像碎片则被认为是阿格里帕的铜像碎片。欧仁每年拨出 325 个杜卡特修复城墙，并修复了几座城门。他修缮了奥斯提亚城堡；伟大的教父奥古斯丁的母亲莫妮卡的遗骸在这座海港城市旧大教堂的高祭坛下被发现，并从那里被带到罗马的奥古斯丁教堂。

尤金最喜欢的建筑师是威尼斯人布雷尼奥（Bregno）或安东尼奥-里奇奥（Antonio Riccio）（他的真名）；他的得力助手，也可以说是他的阿格里帕（Agrippa），是红衣主教卡梅伦戈-斯卡兰波（Camerlengo Scarampo）在所有这些功勋活动中的得力助手。他的前任维特莱斯基摧毁了拉丁姆的城市，在科尔内托为自己修建了一座宫殿，但却没有为罗马做任何值得一提的事情。他只是试图重新安置因拉迪斯劳斯国王的军事行动而沦为废墟的梵蒂冈博尔戈。斯卡兰波后来为罗马做了更多的事情；据说他努力使陷入惰性的罗马人变得更有人情味。

1456 年，他为罗马修建了鲜花广场。庞培剧场曾经所在的这个广场，当时所占的面积比现在还要大。广场被称为 "花田"，因为广场上有一片草地。直到尤金时代，牛一直在这里吃草。斯卡兰波（Scarampo）在这里铺设了路面；他本人则住在附近的达马索圣洛伦佐宫殿里。自尤金之后，红衣主教们开始热衷于建筑。弗朗切斯科-孔杜尔马洛（Francesco Condulmaro）在庞培剧院的废墟上修建了一座宫殿，后来红衣主教彼得罗-伊斯瓦利斯（Pietro Isvalies）用绘画和圆柱装饰了这座宫殿。不久之后，它又传给了奥尔西尼家族，后来又传给了卡尔皮的皮奥王子。让-勒热纳（Jean le Jeune）在拉塔大街的马库斯-奥勒留拱门上扩建了一栋建筑，其宏伟程度令比翁多（Biondo）称其为继梵蒂冈之后最美丽的建筑。如今，菲亚诺宫（Fiano Palace）矗立在它的位置上。尼科洛-阿奇亚帕奇（Niccolò Acciapacci）在拉塔大街的圣玛丽亚宫原址上修建了一座宫殿，后来在此基础上修建了奥贝托-多利亚宫。在尤金时代，多米尼克斯-卡普拉尼卡也在阿奎罗的圣玛丽亚附近开始建造自己的宫殿；由于他打算将其建成一所文法学校，他的弟弟安杰洛（庇护二世时期的红衣主教）建造了学院大楼，该大楼现在仍然屹立不倒。这座卡普拉尼卡宫殿如今是罗马早期文艺复兴时期最古老的纪念碑，是哥特式风格向新拉丁风格过渡的最明显例证。

1439 年，罗马家族 de Cupis 和虔诚的苏尔蒙人 Forca Palena 的 Nicolaus 建立了一座新的修道院教堂，即 Janiculus 河畔的 Sant' Onofrio 教堂。尤金将其交给了希罗尼米特骑士团。红衣主教安东-马丁内斯-德-沙维斯（Anton Martinez de Chaves）创建了葡萄牙人的教堂 S. Antonio im Marsfelde。这些国家基金会的主要目的是为朝圣者和病人提供医院。英国人从 1398 年起也在 S. Maria del Monserrato 街建立了自己的医院。Maria del Monserrato 街。德意志医院（后来成为 S. Maria dell'Anima）也建于 1399 年左右。

欧仁四世之后是这座城市第一位伟大的修复者尼古拉五世。他有两个爱好：藏书和建筑。如果说在那里他被比作托勒密，那么在这里他可以比作哈德良。事实上，这位教皇复兴了古罗马人宏伟的建筑意识。他以帝王般的气魄向罗马发起了进攻；自古以来，整座城市至少在他看来第一次成为了一个建筑整体。尼古拉五世在这方面可谓才华横溢。主导他的思想是文艺复兴的现代意识：罗马要成为教会（即教皇）不朽的丰碑，从而在万民面前荣耀崛起。当然，尼古拉五世的大胆想法几乎没有一个能够实现；它们仍然是草图，但却产生了强大的影响。

罗马的一些地方人口稀少，例如，从加里安努斯拱门和圣维托（S. Vito）到圣玛丽亚-马焦雷（S. Maria Maggiore）和圣普拉塞德（S. Prassede）附近一片荒凉；尼古拉号召罗马人到那里定居，并承诺免除一切税收作为回报。起初，他只是想对城市进行全面修复，同时对莱昂尼纳进行凯撒式改造，将帕拉丁宫移到梵蒂冈。他想以罗马教皇的身份进行建设。他逐步开始，直到禧年的财政收入让他能够大展宏图。罗马就像一个建筑工地，一个巨大的车间；成群结队的工匠和工人涌向这座城市，在这里形成了整个殖民地。尤其是伦巴第艺术家和技术人员大量移民。几个世纪以来，罗马从未出现过这种投机取巧的建筑承包商，他们与富裕、奢华的客户签订合同。石灰华在蒂沃利附近开采，用马车运到城里，也许还用阿尼奥号运来，为此要对石灰华进行净化处理。与此同时，城墙在修建，桥梁在修复，防御塔在建造，教堂在翻新，新梵蒂冈在奠基。这些活动热火朝天。对荣耀的渴求和对即将到来的死亡的思念让教皇陶醉，同时也让他备受煎熬。

1451 年，他下令修建罗马城墙。城墙上仍不时出现教皇的盾徽。他让人在米尔维安桥上加固了塔楼；诺门坦桥堡垒仍然保持着他赋予它的形状。就连元老宫也是新加固的。几乎没有任何一位教皇能像这位饱读诗书的人一样建造如此多的城堡。他让人修建了纳尔尼和奥尔维耶托的城堡，并扩建了斯波莱托的阿尔博诺兹城堡。类似尤金四世被驱逐的事情再也没有发生过。尼古拉回顾了逃离罗马的教皇们，得出结论：如果他们有足够的防御工事保护，就不会遭受这样的命运。从那时起，梵蒂冈要塞就成了保护教皇不受内部革命影响的屏障。尼古拉在天使之桥（他拆除了桥上的马厩）上修建了塔楼，并亲自加固了城堡两侧的城墙。大名鼎鼎的阿尔贝蒂绘制了一个伞形屋顶，桥本应接受但却没有。现在，整个堡垒都要加固，新的梵蒂冈将作为阿维尼翁教皇城堡在堡垒中崛起。事实上，尼古拉已经开始在宫殿周围筑起围墙，并在维里达利亚（Viridaria）上修建了一座厚实的圆塔。

宗教大祭司躲在城墙、塔楼和喷火砲后面的景象，可以用罗马和人类或教皇的历史来解释和说明；但可以肯定的是，尼古拉五世非常清楚王子的这种可疑需求与他的精神尊严之间的矛盾，因为他想巧妙地将他的梵蒂冈防御工事系统与莱昂尼纳的改建结合起来。这座腐朽的博尔戈将成为一座巨大的教皇之城。从圣天使城堡前的广场出发，有三条街道组成了vicus curialis，通往圣彼得广场，广场上有六个大门廊、艺术画廊、艺术家工作室和兑换银行。他想象教皇和整个教廷都住在最华丽的宫殿里，那是一个带公园的宏伟建筑群。他在世界上的地位无人能及。他想建造一座剧院，供皇室加冕用；一座会议厅和一座剧院。人们可以通过一座宏伟的凯旋门进入这座教皇城堡。

新的圣彼得大教堂将矗立在旧大教堂的原址上，圆顶很高，呈拉丁十字形，前庭有两座塔楼，两侧是供神职人员使用的宏伟建筑，方尖碑将矗立在大教堂前的广场上，支撑着基督像，安放在一个青铜基座上，基座上有四个青铜使徒巨像。博洛尼亚人里多尔夫-费奥拉万特（Ridolfo Fioravante），又名亚里士多德，设计了竖立方尖碑的方案。

尼古拉希望用大教堂和宫殿、教堂、修道院、喷泉、花园、门廊、图书馆和高耸的城墙环绕整座新城，这样教皇城堡就像马内蒂所说的那样，只有天空中的鸟儿才能爬上去；他还希望自己能在这座教皇修道院中高高在上，就像亚洲的伟大国王在他的天堂中一样。事实上，他想超越所有七大奇迹，实现所罗门的荣耀，他同时建造了王宫和神庙。这个大胆的设计是以帝国建筑、帕拉丁宫、论坛和浴场的规划为基础的。遗憾的是，由于未能实现，它只能作为罗马文艺复兴时期最巨大的幻想之一而具有重要意义。顺便说一句，教皇们不会因为这个计划仍然是一个想法而感到遗憾；如果他们退缩到这样一个大理石堡垒中，他们就会获得欧洲达赖喇嘛的声望，但却注定要放弃罗马。因此，意大利人可能会感到遗憾的是，圣比尔吉塔梦寐以求的在莱昂尼纳封禁教皇的计划没有实现。

正如马内蒂所描述的那样，计划的实施是以许多教皇的时间和兰普西尼特的宝藏为前提的，从中我们可以看出当时的教皇可以信任什么。尼古拉在设计中使用了佛罗伦萨人贝尔纳多-甘巴雷利（Bernardo Gambarelli，人称罗塞利诺），尤其是当他来到罗马并通过比翁多与才华横溢的莱昂-巴蒂斯塔-阿尔贝蒂（Leon Battista Alberti）成为朋友时。1452 年，阿尔贝蒂向尼古拉五世展示了他的建筑学著作，这是继维特鲁维乌斯之后的第一部同类著作；他的艺术观点与中世纪和哥特式建筑充满敌意，开创了建筑学的新时代，尼古拉五世对此做出了回应。

重建圣彼得大教堂的这一最初决定非常奇怪，因为它是以摧毁旧大教堂为前提的，也就是大胆地打破了神圣的传统。当时，位于卡里古拉广场地基上的旧大教堂北侧面临坍塌的危险，并出现了令人担忧的裂缝；这为尼古拉的大胆计划提供了借口，但他并没有在受到威胁的地基上开始重建，而是从唱诗班开始，因为唱诗班在很长一段时间内都屹立不倒。为了建造新的护民官，他无情地毁掉了旧的普罗比圣殿，于是安尼基人的礼拜堂消失了。如果不是马菲奥-维吉奥（Maffeo Vegio）当时看到并描述了它，我们就不会再知道它的存在了。尼古拉三世去世时，护国寺只有几英尺高；在新的梵蒂冈中，现在的圣洛伦佐小教堂已经完工，似乎最初是这位教皇的书房，粗略地看，尼古拉三世的宫殿由一系列的房间进行了华丽的改建，亚历山大六世后来对其下层进行了扩建，而上层则是著名的画廊。尼古拉五世去世时，他周围的城墙和护城河已经成为废墟，这些都是巨大设计的基础。

在罗马，他完成了几乎所有 40 座车站教堂的建设，修复了圣斯特凡诺-罗通多、圣玛丽亚-马焦雷以及毗邻的圣普拉塞德宫、城墙外的洛伦佐宫和圣保罗宫。圣特奥多罗教堂也得到了重建。1450 年，罗德里戈主教阿方索-帕拉迪纳斯在纳沃纳河畔创建了西班牙的圣雅各布教堂。大约在同一时期，富有的红衣主教拉蒂诺-奥尔西尼家族在劳罗创建了圣萨尔瓦托雷泰莱西诺教堂和修道院。他将其捐赠给了阿尔加的圣乔治公理会，并将自己藏书丰富的图书馆遗赠给了这里。图书馆在 1527 年罗马被洗劫时被烧毁。

在元老宫，尼古拉可能翻修了完全破旧不堪的元老院宫殿。他重建了监管者宫，从而使中世纪的元老宫呈现出更加现代的面貌。他修建的维尔戈水渠非常值得称赞，这条水渠在尤金四世时期就已经使用过，是当时所有古代水渠中唯一在使用的一条。尼古拉让人在这条主要在地下的水渠的出水口装饰了一个简单的喷泉，并根据喷泉所在的三条路将其命名为特雷维（Trevi）。西克斯图斯四世完成了这项工程，伟大的艺术家阿尔贝蒂和罗塞里诺为其绘制了图纸。但直到克莱门特十二世，尼科洛-萨尔维才建造了现在的喷泉正面，1744 年，本笃十四世为这一伟大工程举行了落成典礼。

尼古拉五世不仅希望用纪念碑装饰罗马，还希望用纪念碑装饰教皇国。在维泰博和奇维塔维奇亚，在奇维塔卡斯特拉纳、阿西西、瓜尔多和法布里亚诺，他都建造了楼房，布置了广场，修建了教堂。事实上，自卡洛林王朝以来，还没有教皇建造过如此多的建筑。他满心欢喜地铸造了一枚印有城墙形象和古老铭文_罗马-菲利克斯_的奖章。

然而，这种建设热情却遭到了激烈的指责，如小狂热者卡皮斯特拉诺（Capistrano）。教皇被批评为允许拜占庭变成土耳其人，却在书籍和灰木上浪费了数百万。虔诚的基督徒可能会怀疑，这种对建筑的完全恺撒式的热爱更能说明教皇的伟大还是渺小，但从另一个角度来看，帝国的大规模奢侈也是一种值得称赞的品质。它继续对文化产生影响，防止人类陷入功利主义的劣根性。罗马一直都有这种建造宏伟纪念碑的冲动，先是在皇帝的统治下，然后是在效仿他们的独具匠心的教皇的统治下。尼古拉五世的继任者已经开始反对这种大胆的建筑计划，这主要是拜占庭灭亡的结果。卡利斯图斯三世（Calixtus III）将其前任的珍贵餐具货币化，也鄙视其前任的建筑；他将这些建筑的材料送给了罗马人。他本人只是继续修建城墙，并完成了米尔维奥桥的塔楼。在教堂中，只有圣普里斯卡教堂是他修复的。

庇护二世也只是赞叹尼古拉五世真正的罗马精神，自己却没有分享这种精神。与重建圣彼得大教堂相比，重新征服圣索菲亚大教堂也是一项更崇高的职责。皮科洛米尼的纪念碑没有保存在罗马，而是保存在锡耶纳和皮恩扎，他让罗塞利诺用宫殿和大教堂装饰了这两个地方。他净化了圣彼得大教堂，将中殿的帐幕和坟墓移到了侧廊；他自己在那里修建了圣安德鲁小教堂，翻修了前院的大台阶，并开始修建用于祝祷的门廊，为此他从屋大维娅门廊上拆下了七根柱子。如果说他修建了甘多尔夫城堡和萨维利家族城堡，那也许是出于对古迹的热情；出于战略考虑，他让人修建了蒂沃利城堡，无情地摧毁了那里的旧圆形剧场，将其用作采石场。另一方面，他下令拆除拉丁姆最古老的城堡之一。这座城堡是阿尔吉德斯河畔的拉里亚诺城堡，长期以来一直是安尼巴尔迪家族的领地，后来成为科隆纳家族的领地，1462 年由红衣主教普罗斯佩罗重建，红衣主教死后，他的妹妹维多利亚将城堡移交给了红衣主教皮科洛米尼。皮乌斯二世曾下令摧毁这座城堡，后来亚历山大六世将城堡的土地捐赠给了韦莱特里镇。皮乌斯希望让阿尼奥河通航，并清理特拉扬港（Portus），但这一愿望并未实现。在漫步阿尼奥河谷时，他欣赏到了维科瓦罗的圣雅各布教堂，该教堂是弗朗切斯科-奥尔西尼家族于 1450 年左右开始建造的，由他的侄子特拉尼主教约翰完成。这座教堂至今仍完好无损地矗立在那里，是罗马男爵热爱艺术的一座孤零零的纪念碑：它是一座八角形圆顶小教堂，有一个漂亮的入口以及许多装饰和雕像。它是由布鲁内莱斯基的学生建造的。塔利亚科佐伯爵弗朗切斯科-奥尔西尼家族（Francesco Orsini）是格拉维纳和孔韦萨诺的第一任伯爵，同时也是市政长官，他扩建了位于罗马纳沃纳尽头的旧莫斯卡宫殿。经过多次改建，这座奥西尼亚宫殿变成了今天的布拉希家族宫殿。

托克马达红衣主教在米涅瓦的建筑也属于庇护二世时期，他在那里修建了修道院庭院并用绘画进行了装饰；他还扩建了教堂的拱顶和圣母教堂。他在这方面得到了萨维利家族和盖塔尼家族以及弗朗切斯科-奥尔西尼的支持，奥尔西尼还自费完成了密涅瓦教堂的扩建工程。

1455 年，身为红衣主教的保罗二世开始建造他的圣马尔科宫殿，其真正罗马式的建筑条件是以前从未听说过的。这座巨大的建筑只能由王公贵族的宫廷来填充；它从未完工，但即使未完工，它也是罗马最宏伟的纪念碑之一，位于中世纪和现代的交界处。垛口和未完工的塔楼仍让人联想到中世纪。哥特式风格已经消失。外墙底部是罗马式拱形窗，顶部是文艺复兴时期的直角窗。整个建筑显示出一种色彩斑斓的力量和阴沉严肃的宽敞特征；深思熟虑的力量却不失优雅。主要的装饰是内廊柱，这座罗马第一座内廊柱如果完工，可能会是最华丽的。建筑师以斗兽场为蓝本，用柱子和半圆柱建造了大庭院的拱廊。这样，半圆柱的布置方式在罗马再次得到应用。有几位艺术家曾参与过这座红衣主教城堡的设计，他们是贾科莫-达-彼得拉桑塔、贝尔纳多-迪-洛伦佐、帕多瓦的维拉诺，但有关他们的资料却很可疑。城堡的建造耗资巨大，其负责人曾受到审判，但被无罪释放。教皇为了建造自己的宫殿，不惜掠夺古迹，甚至连斗兽场也不放过。由于救世主医院已成为斗兽场的所有者和监护人，因此它的兄弟会有权使用倒塌的建筑，并将其出售给医院。

即使作为红衣主教，保罗二世也住在圣马可宫，作为教皇，他在这里收藏了大量的古董；他在这里观看狂欢节比赛，拉塔路也因此得名科尔索。他的侄子马尔科-巴尔博（Marco Barbo）、红衣主教洛伦佐-西博（Lorenzo Cibò）和多梅尼科-格里马尼（Domenico Grimani）（与保罗二世一样，多梅尼科-格里马尼也是著名的艺术赞助人和艺术收藏家）继续修建这座宫殿。保罗三世通过一条有盖通道将宫殿与阿拉科利的教皇夏宫连接起来，宫殿总体上一直是教皇们的财产，直到 1564 年庇护四世将其割让给威尼斯共和国，作为捐赠给教皇驻威尼斯大使的房屋。从那时起，威尼斯宫富丽堂皇的房间就一直被显赫的共和国大使和圣马可教堂的红衣主教所占据，最后是奥地利大使。即使在失去威尼斯的今天，它仍然是奥地利的财产。自查理曼大帝以来，日耳曼帝国对意大利和罗马的古老权利，仅剩下一座宫殿，再无其他。

保罗二世还扩建了圣马可大教堂，将其移入宫殿内。这里最引人注目的也是美丽的石灰华门廊。建筑师是朱利亚诺-达-马亚诺（Giuliano da Majano），这位艺术家也曾在梵蒂冈工作过，在那里建造了圣伯多䘵大教堂的护民官，完成了祝祷箱，并在宫殿内建造了一个由三根柱子组成的华丽庭院，一根柱子高于另一根柱子。这个柱廊庭院在后来的改建中消失了。
