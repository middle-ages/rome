### 第四章

_1\. 萨宾尼安和博尼法斯三世的教宗职位和去世 博尼法斯四世 万神殿被献给了圣母玛利亚。

格里高利去世后，彼得宗座空缺了六个月，直到继任者被确认。继任者是来自沃拉泰雷的萨比尼阿努斯，他曾是罗马教会驻君士坦丁堡的圣座大使。他上任时正值罗马和整个意大利遭受严重饥荒的不祥之兆。尽管他打开了教会的粮仓，但还是没有足够的粮食来养活人们。人们甚至诅咒格里高利的记忆，指责他挥霍教会的财宝。一个粗俗的传说称，格列高里愤怒的灵魂出现在他的继任者面前，对他大加责备，最后敲打他的头部，教皇就此死去。萨比尼安似乎被一些罗马人视为敌人，妒忌他的前任；他于 606 年 2 月去世，可能是在一次民众起义中去世的，即使是死人也仍要惧怕饥饿暴民的愤怒，因为他是从拉特兰绕道城墙被领到圣彼得大教堂的。没有关于他是暴死的报道。

宗座空缺了一年，直到法卡斯确认罗马人博尼法斯三世的当选，他是约翰-卡图迪乌斯（John Cataudius）的儿子，他的名字证明了他的希腊血统。这位教皇在位时间不长，也没有什么内容；编年史只记载了他向法奥卡斯皇帝颁布了一项法令，愉快地结束了罗马主教与君士坦丁堡宗主教关于主教地位的争论。皇帝宣布罗马应被视为基督教的使徒首领。博尼法斯三世于 607 年 11 月 10 日逝世，教会的作家们认为是在 607 年 11 月 10 日，次年 9 月 15 日，来自瓦莱里亚的马尔斯-博尼法斯四世成为教皇。

他在位六年多，饱受饥荒、瘟疫和敌人压迫之苦。可以想象，荒废的罗马是多么迅速地沦为废墟。但正是在这位教皇的统治下，这座城市最令人惊叹的建筑之一从埋葬了几个世纪的黑暗深渊中浮出水面。广阔的马尔斯广场上曾布满了各式各样的宏伟建筑，但它的大厅、浴场和神庙，它的体育场、剧院和游艺园只是为了供人们享乐，那里的人口不可能很多。在那里兴起的教堂聚集了新的生命；它们通常是罗马荒凉地区新人口群体的中心，就像坎帕尼亚荒凉地区的其他教堂一样。在这座城市的众多教堂中，我们迄今只看到了两座建在马尔斯区最外围的著名教堂：卢西纳的圣劳伦提斯教堂和达马索教堂。在其中心，可能只有较小的圆形大厅。万神殿就矗立在这片被 590 年的洪水冲毁的大理石建筑群中。阿格里帕（Agrippa）、尼禄（Nero）和亚历山大（Alexander）的温泉浴场、密涅瓦-查尔西迪卡神庙（Temple of Minerva Chalcidica）、伊塞姆（Iseum）、颂歌厅（Odeum）和多米蒂安体育场（Stadium of Domitian）围绕着万神殿围成一圈。所有这些宏伟的建筑都年久失修，饱经风霜，但几乎没有成为废墟。

万神殿是阿格里帕最美丽的纪念碑，600 多年来一直经受着风雨的洗礼；台伯河的洪水几乎每年都会淹没圆形大厅，在里面像洪流一样涌动；冬天的倾盆大雨穿过穹顶落在凹陷的大理石地板上，被地下水道截住，但都无法撼动这座坚固的建筑。宏伟的前庭有五级台阶通往上方，16 根花岗岩圆柱和白色大理石科林斯式柱头完好无损地矗立在那里。或许，奥古斯都和阿格里帕的雕像还矗立在两个壁龛里，是后者放置在这里的。任何时间的力量都无法破坏由镀金矿石横梁组成的屋顶框架，而覆盖门廊和穹顶的镀金铜瓦也尚未被强盗撕下。我们不知道穹顶的装饰是否还在，对此我们没有任何描述。根据阿格里帕的浴室来看，万神殿最初不可能是一座神庙，但后来阿格里帕在第三任执政官任期内修建的前庭证明，万神殿的用途就是神庙。普林尼已将其命名为 "万神殿"，迪奥-卡西乌斯在其中不仅看到了火星和维纳斯的雕像，还看到了奥古斯都拒绝与之交往的凯撒的雕像。这些雕像揭示了凯撒的目的，即使神庙的总称借用了众神之母赛伯勒（Cybele）的名字，特别称谓借用了朱庇特-奥尔托（Jupiter Ultor）的名字，以纪念奥古斯都在阿克提姆（Actium）的伟大胜利。基督教皇帝的法令下令关闭所有神庙，也许两个世纪以来没有罗马人进入过万神庙内部；不过，西哥特人和汪达尔人肯定曾砸开过覆盖着矿石的大门扇（现在几乎没有了）。但他们并没有在那里找到任何宝藏；金碧辉煌的大理石镶板或装饰着金属玫瑰的拱顶库房都难以诱惑他们的贪欲。在内圈的六个壁龛和壁龛之间的壁龛中，他们发现了被遗弃的神像，他们热衷于从中偷窃有价值的神像，甚至博尼法斯四世可能还在万神殿中发现了一些神像。

这位教皇满怀渴望地凝视着这座如此适合作为教堂的艺术奇迹。教堂的圆形建筑坐落在一个开阔的广场上，与神庙的形状大相径庭，这让他想占有它，而美丽的穹顶是一个升到空中的球体，星光神奇地倾泻而下，在他看来，这是天后玛丽亚的合适居所。末代皇帝在诏书中阐明了这样一个原则，即异教徒的庙宇不应被摧毁，而应献给基督教礼拜；格里高利本人在给梅利图斯主教的诏书中确认了这一原则，至少在不列颠是如此。这一原则已经在古雅典得到了贯彻，在那里，圣母雅典娜的所在地帕台农神庙被改建成了圣母玛利亚教堂。但没有什么比编年史家明确指出博尼法斯向法奥卡斯皇帝请求修建万神殿并将其作为礼物接受更能清楚地证明教皇对罗马古建筑没有所有权了。因此，罗马教会与拜占庭的关系是友好的；罗马人在 608 年才刚刚在广场上为同一位皇帝竖起了荣誉柱。

博尼法斯召集了罗马的神职人员：万神殿带有十字架的大门被推开了，高唱圣歌的基督教神父队伍第一次涌进了高大的圆形大厅，教皇在大理石墙上洒下圣水，墙上的异教痕迹已被清除。恶魔们被穹顶回荡的_Gloria in excelsis_号圣歌吓得魂不附体，现在罗马人可以想象到他们正试图从穹顶的开口处逃走。在博尼法斯时代之前，万神殿一直被视为罗马恶魔的真正所在地。中世纪晚期的人们知道，阿格里帕将万神殿供奉给了西庇阿和所有的神灵，并相信是他在穹顶上方竖立了西庇阿的镀金矿石雕像。十二世纪的说法 12 世纪的说法可能是 600 年前的流行说法，万神殿主要被视为赛比利神庙。我们可以从博尼法斯四世对圆形大厅的称谓中推断出这一点：他将圆形大厅献给了圣母玛利亚和所有殉道者。基督教会喜欢在用于祭祀的神庙中安置圣人，这些圣人在某种程度上与被赶出神庙的神灵相对应。因此，假定的罗穆卢斯和雷穆斯孪生兄弟的神庙被供奉给了科斯马和达米亚努斯孪生兄弟；因此，圣萨宾娜取代了阿汶丁山的戴安娜女神；因此，两位军事护民官塞巴斯蒂安和乔治成为了战神马尔斯的继承者。因此，博尼法斯沿袭了传统：伟大的母亲西庇阿被新的母神取代，"众神 "神庙变成了 "众烈士 "教堂。罗马城市崇拜吸纳了来自所有国家的圣人，在基督教万神殿中找到了合适的表达方式。我们毫不怀疑，博尼法斯抢劫罗马的地下墓穴，就是为了把整车整车的所谓殉教者的骸骨装进新圣殿的名称中。

根据《罗马殉教史》（Martyrologium Romanum）的记载，万神殿于 5 月 13 日落成，但具体年份在 604 年、606 年、609 年和 610 年之间不一。 即使在今天，罗马仍在这一天庆祝他的落成；11 月 1 日是所有圣徒的节日，11 月 2 日是所有受祝福的死者的节日，不管是博尼法斯已经指定了这些日子，还是格里高利四世指定了这些日子。直到九世纪，阿尔卑斯山另一侧的民族也采用了原本罗马人的节日。因此，基督世界的一般哀悼节日从阿格里帕的圆形大厅中诞生；从众神的万神殿中，一种温和的忧郁和神圣的纪念精神涌向了基督教世界，甚至在最近的几个世纪里，这种精神还激励着意大利和德国的音乐天才创作出了一些最美妙的作品。万神殿变成了一座充满崇敬和安魂曲的殿堂，即使在今天，人们也只能怀着虔诚的心情进入它那令人难忘的圆形殿堂。因此，这座古罗马最美丽的建筑之所以能够得救，要归功于教会的衰落，因为教会利用万神殿进行崇拜。如果不是这样，这座奇妙的纪念碑在中世纪就会变成一座贵族城堡，遭受无数战争风暴的摧残，像哈德良陵墓一样只剩下一片废墟。博尼法斯四世的这一幸运行为被正确地认为是伟大的，足以作为不朽的称号铭刻在他的陵墓上。新教堂因其古老、美丽和圣洁，被罗马人视为城市的瑰宝，一直是教皇们精心守护的财产。直到十三世纪，罗马的每一位元老都发誓，除了圣彼得大教堂、圣天使城堡和其他教皇领地外，他还将为教皇保卫和维护圣玛丽亚圆形大教堂。
